app.controller("VoRequestCtrl", [
		'$scope',
		'$state',
		'$uibModal',
		'localStorageService',
		'adapter',
		'validate',
		'adjustHeight',
		'$compile',
		'commonServices',
		function($scope, $state, $uibModal, localStorageService, adapter,
				validate, adjustHeight, $compile,commonServices) {
			adjustHeight.adjustTopBar();
			$scope.ShowContractPage = function(row, event) {
				localStorageService.set('contractRow', row);
		    	if (event.ctrlKey) window.open('#/AdminBoard/ViewContract', '_blank'); // in new tab
				else $state.go('AdminBoard.ViewContract');
			}

			$scope.ShowCustomerPage = function(customerCode, event) {
				localStorageService.set('customerCode', customerCode);
		    	if (event.ctrlKey) window.open('#/AdminBoard/EditCustomer', '_blank'); // in new tab
				else $state.go('AdminBoard.EditCustomer');
			}
			$scope.verifyContract = function(contractCode,row) {
				localStorageService.set('contractCode', contractCode);
				localStorageService.set('requestDetails',row);
				$state.go('VoBoard.VoVerifyContract');
			}
			$scope.PageCntChange = function() {
				$scope.$parent.selectedCnt = $scope.selectedCnt;
				$scope.$parent.PageCntChange();
			}
			$scope.pageChanged = function(event) {
				$scope.$parent.currentPage = $scope.currentPage;
				$scope.$parent.pageChanged(event);
			}

			$scope.exportToCsv=function(e,data){
		    	e.preventDefault();

		    	var filterData=localStorageService.get('filteredContract');

		    	 var selectedColumns = localStorageService.get('selectedColumns');

		    	if(filterData!==null){

		    		var filterOutData=[];

		    		for(var i=0;i<filterData.length;i++){
		    		    angular.forEach(filterData[i], function(value, key) {
		    			  if(selectedColumns.hasOwnProperty(key)){
		    				  //console.log(key);
		    			  }else{
		    				//  filterData.remove(key);
		    				delete filterData[i][key];
		    			  }
		    			});
		    		}

		    		commonServices.exportToCsvCommon(filterData, "Contract Report", true);

		    	}else{
		    		if(data == '')
		                return;
		    		for(var i=0;i<data.length;i++){
		    		    angular.forEach(data[i], function(value, key) {
		    			  if(selectedColumns.hasOwnProperty(key)){
		    				  //console.log(key);
		    			  }else{
		    				//  filterData.remove(key);
		    				delete data[i][key];
		    			  }
		    			});
		    		}

		    		commonServices.exportToCsvCommon(data, "Contract Report", true);

		    		//JSONToCSVConvertor(data, "Contract Report", true);
		    	}




		    }
		} ]);