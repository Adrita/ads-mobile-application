app.controller("CreateEditContractCtrl", ['$scope', '$rootScope', '$state', '$uibModal', 'localStorageService', 'adapter', 'adjustHeight', 'stickyHeader', 'validate', '$location', '$anchorScroll', 'Session', function($scope, $rootScope, $state, $uibModal, localStorageService, adapter, adjustHeight, stickyHeader, validate, $location, $anchorScroll, Session) {
    adjustHeight.adjustTopBar();
    $rootScope.$broadcast('TrackNavBar', false);
    $scope.contractHeader = "Create Contract";
    $scope.loader = false;
    $scope.showCustomer = true;
    $scope.productCurrent = false;
    $scope.pricingCurrent = false;
    $scope.paymentCurrent = false;
    $scope.customerCurrent = false;
    $scope.cheque1Error=true;
    $scope.cheque2Error=true;
    var contractCode, itemPoint,totalItemPoint;
    $scope.activateNav = false;
    var type = "";
    var dontCallService = false;
    $scope.networkError = false;
    $scope.dp1Disabled=false;
    $scope.raceList = ["Chinese", "Indian", "Malay", "Others"];
    $scope.relationList = ["Mother", "Father", "Sister", "Spouse", "Other"];

    var region="";

    var offline = false;
    /*$scope.priceListCodeList=["EA","EB","EE","EH"];
    $scope.termsList=["COD","CAD","6","8","9"];*/
    //$scope.methodOfPaymentList=["CC","COD","Collector"];
    var actualDownPayment1, actualDownPayment2;
    var stateNameForProduct = "";
    $scope.dpLabel = "Down Payment 2";
    $scope.text = {};
    var appInUse = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;
    $scope.text.instalmentAmountLbl = "Monthly Instalment Amount";
    $scope.text.paymentMethodDP2Lbl = "Method of Payment For Instalment";

    $scope.showCreateContract = false;
    $scope.enableRefremove = false;
    $scope.editContractCustomerDetail = false;
    var tempDob;
    $scope.disableDp1 = false;
    $scope.searchCustomerBy = "";

    if (Session.userRoleName.toLowerCase() === "admin") $scope.isAdmin = true;
    else if (Session.userRoleName.toLowerCase().indexOf("sales") > -1) $scope.isSalesRep = true;

    $scope.adminCont = {};
    $scope.adminCont.orderDate = getFormattedDate(new Date());
    $scope.adminCont.deliveryDate = getFormattedDate(new Date());
    $scope.adminCont.commenceMentDate = getFormattedDate(new Date());

    var loggedinUser = Session.userGlobalRole;

    //bypass customer search for sales rep
    if ($scope.isSalesRep) {
        $scope.showCustomer = false;
        $scope.customerCurrent = true;
        $scope.activateNav = true;

        type = "getStateList";
        getServiceCall();

    } else {
        type = "getSalesRepList";
        getServiceCall();
    }

    //$scope.pricing = {};
    $scope.myprod = {};
    $scope.contract = {};

    $scope.contract = {
        productDetails: null
            /*productList1: null,
            priceListCodeList: null,
            installmentList: null,
            downPayment1: null,
            downPayment2: null,
            salesRepContributionStatus: null*/
    };
    $scope.common = {};
    $scope.common = {
        todayDate: null,
        formatedDob: null
    };
    $scope.common.todayDate = getFormattedDate(new Date());

    $scope.bank = {};
    $scope.status_active = null;
    //setting partial view-customer-detail variables
    $scope.currentSate = $state.current.name;
    $scope.remarks = {};
    $scope.remarkCustomer = null;
    $scope.remarkProduct = null;
    $scope.remarkPricing = null;

    /*MODAL IMPLEMENTATION*/
    $scope.animationsEnabled = true;

    $scope.actionButton = "Create";

    $scope.open = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/modals/role/modal.html',
            controller: 'modalCtrl',
            windowClass: 'center-modal',
            backdrop: 'static',
            /* keyboard: false,*/
            size: 'md'
        }).result.then(function(data) {
            if (data === "close") {
                $state.go('AdminBoard.ManageContract')
            }
            if (data === "createAnother") {

                $state.reload();

            }
        });
    };

    var errorName = "Contract";
    var errorCode = "Test1234";
    var contractCode = "";
    var errorMessage = "";

    function modalOpen(modalType) {
        if (errorName !== null && errorName !== "" && errorName !== undefined) {
            if (modalType === "success") {
                var modalObj = {
                    "modalEntity": "Contract ",
                    "body": "(" + errorName + " - " + contractCode + ") ",
                    "backLink": "Back to Manage Contracts",
                    "modalBtnText": "Create Another Contract"
                };
            } else {
                var modalObj = {
                    "modalEntity": "Failed!! ",
                    "body": errorMessage,
                    "backLink": "Back to Manage Contracts",
                    "modalBtnText": "Create Another Contract"
                };
            }

            localStorageService.set("modalObj", modalObj);
            $scope.open();
        }
    }
    /*END*/


    $scope.showOkCustCode = false;
    $scope.showOkCustName = false;
    $scope.showOkIC = false;
    $scope.showOk = function(str) {
        switch (str) {
            case "showOkCustCode":
                {
                    $scope.showOkCustCode = true;
                    break;
                }
            case "showOkCustName":
                {
                    $scope.showOkCustName = true;
                    break;
                }
            case "showOkIC":
                {
                    $scope.showOkIC = true;
                    break;
                }
        }
    }
    $scope.redirect = function(path) {
        $state.go(path);
    }

    $scope.checkCustId = function(typeTpFind) {
        /*if($scope.customerId === "12345"){
        	$scope.showCustomer = false;
        	$scope.customerCurrent = true;
        }*/
        $scope.searchCustomerBy = typeTpFind;

        if (typeTpFind === 'custCode') {
            if ($scope.customerCode !== null && $scope.customerCode !== "") {
                dontCallService = true;
            } else {
                dontCallService = false;
            }
        } else if (typeTpFind === 'custName') {
            if ($scope.customerName !== null && $scope.customerName !== "") {
                dontCallService = true;
            } else {
                dontCallService = false;
            }
        } else if (typeTpFind === 'ic') {
            if ($scope.icNo !== null && $scope.icNo !== "") {
                dontCallService = true;
            } else {
                dontCallService = false;
            }
        }

        if (dontCallService) {
            type = "getCustomer";
            getServiceCall();
            $scope.showCustomer = false;
            $scope.customerCurrent = true;
        } else {
            $scope.showCustomer = false;
            $scope.customerCurrent = true;
            $scope.networkSuccess = false;
            $scope.networkError = true;
            $scope.alertMsg = "New customer will be created.";
            $(".alert-success").fadeTo(2000, 500).slideUp(2000);
            type = "getCustomerState";
            getServiceCall();

        }


    }

    //Get customer list by customer name
    $scope.getCustomerList = function() {
        if (angular.isDefined($scope.customerName) && $scope.customerName !== null && $.trim($scope.customerName) !== "") {
            type = "getCustomerList";
            getServiceCall();
        }
    }

    $scope.customerDetails = {};
    var selectedDOB = "";
    $scope.subContractMenu = function(arg) {


        resetCurrentMenu();
        if ($scope.activateNav) {
            switch (arg) {
                case 'customer':
                    {

                        $scope.showCustomer = false;
                        $scope.customerCurrent = true;
                        if (selectedDOB !== null && selectedDOB !== undefined && $.trim(selectedDOB) !== "") {
                            $scope.customerCurrent = true;
                            $scope.customerDetails.dob = selectedDOB;
                        }
                        selectedDOB = $scope.customerDetails.dob;

                        break;
                    }
                case 'pricing':
                    {
                        //if(validationsProductDetails())
                        $scope.pricingCurrent = true;
                        break;
                    }
                case 'product':{
                        if (validationForCustomerDetails()) {
                        	$("#prodError").css('display', 'none');
                            $scope.productCurrent = true;
                            //console.log($scope.customerDetails);
                            /*var dtArr = $scope.customerDetails.dob.split("-");
                            //var timeZone = "T00:00:00.000Z";
                            var month = ('0' + (parseInt($.inArray( dtArr[1], shortMonthArr )) + 1)).slice(-2);
                            var custDob = dtArr[2] + "-" + month + "-" + dtArr[0];
                            $scope.customerDetails.dob = new Date(custDob);*/

                            $scope.customerDetails.relationships = [];
                            for (var i = 0; i < $scope.referenceModel.length; i++) {
                                if ($scope.referenceModel[i].relation !== "")
                                    $scope.customerDetails.relationships.push($scope.referenceModel[i]);
                                }
                                //console.log(JSON.stringify($scope.referenceModel[i]));
                               // console.log($scope.customerDetails);
                                                            if ($scope.getTotalPointsVal === undefined || $scope.getTotalPointsVal === null || $scope.getTotalPointsVal === 0) {
                                                                type = "getProduct";
                                                                stateNameForProduct = $scope.customerDetails.homeState;
                                                                getServiceCall();
                                                            }
                            }else {
                                 $scope.customerCurrent = true;
                                 $location.hash('top');

                                 // call $anchorScroll()
                                 $anchorScroll();
                             }

                        //console.log(JSON.stringify($scope.customerDetails));
                        break;


                        }




                case 'payment':
                    {
                        if (validationForCustomerDetails()) {
                            if (validationsProductDetails()) {
                                //console.log($scope.productDetails);
                                $scope.paymentCurrent = true;
                                type = "fetchListOfPriceList";
                                getServiceCall();
                            } else {
                                $scope.productCurrent = true;
                            }
                        } else {
                            $scope.customerCurrent = true;
                        }

                        break;
                    }
                case 'reviewContract':
                    {
                        if (validationPricingDetails()) {
                            /*//console.log($scope.pricing);
                            //$scope.showCreateContract = true;
                            console.log('start');
                            console.log($scope.customerDetails);
                            //console.log($scope.productDetails);
                            //console.log($scope.pricing);

                            console.log($scope.remarks.remarkCustomer);
                            console.log($scope.remarks.remarkProduct);
                            console.log($scope.remarks.remarkPricing);
                            console.log('End');*/


                            $scope.contract = $scope.pricing;
                            $scope.contract.productDetails = $scope.productDetails;
                            $scope.remarkProduct = $scope.remarkProduct;

                            $scope.contract.productList1 = $scope.productList1;
                            $scope.contract.priceListCodeList = $scope.priceListCodeList;
                            $scope.contract.installmentList = $scope.installmentList;

                            $scope.contract.downPayment1 = $scope.pricing.downpayment1;
                            $scope.contract.downPayment2 = $scope.pricing.downpayment2;
                            $scope.contract.salesRepContributionStatus = $scope.pricing.salesRepContribution;
                            //$scope.contract.campaignCode = $scope.pricing.salesRepContribution;

                            $scope.contract.campaignCode = $scope.campaignCode;
                            $scope.common.formatedDob = $scope.getCustomDate($scope.customerDetails.dob);

                            $scope.showCreateContract = true;

                            //stickey Header
                            enableStickyHeader($(".sticky-block"));

                        } else
                            $scope.paymentCurrent = true;
                        break;
                    }
            }
        } else {
            $state.reload();
        }
        //$state.reload();

    }

    function resetCurrentMenu() {
        $scope.customerCurrent = false;
        $scope.showCustomer = false;
        $scope.pricingCurrent = false;
        $scope.productCurrent = false;
        $scope.paymentCurrent = false;
        $scope.showCreateContract = false;
    }

    $scope.productModel = [{
        "spinners": 0,
        "products": ''
    }, {
        "spinners": 0,
        "products": ''
    }, {
        "spinners": 0,
        "products": ''
    }];
    $scope.referenceModel = [{
        "relation": "",
        "name": "",
        "phoneNo": "",
        "email": "",
        "addressLine1": "",
        "addressLine2": "",
        "cityName": "",
        "stateName": "",
        "pinCode": "",
        "newlyAdded": true
    }];
    $scope.models = {};
    $scope.incrementer = function(a, i) {

        if ($scope.productDetails[i].productName !== '') {
            $scope.productDetails[i].quantity = parseInt(a) + 1;
            $scope.productDetails[i].productPoints = parseFloat(parseFloat($scope.productDetails[i].productPoint) * $scope.productDetails[i].quantity).toFixed(2);
            $scope.totalPoints = $scope.getTotalPoints(true);
            if ($scope.contract.productDetails !== null) {
                $scope.contract.productDetails[i].quantity = parseInt(a) + 1;
                $scope.contract.productDetails[i].productPoints = parseFloat(parseFloat($scope.contract.productDetails[i].productPoint) * $scope.contract.productDetails[i].quantity).toFixed(2);
            }
            type = "fetchListOfPriceList";
            getServiceCall();
        }




    }


    $scope.fetchPriceList = function() {
        type = "fetchListOfPriceList";
        getServiceCall();
    }

    $scope.decrementer = function(a, d) {
        if ($scope.productDetails[d].productName !== '') {
            if (parseInt(a) > 1) {
                $scope.productDetails[d].quantity = parseInt(a) - 1;
                $scope.productDetails[d].productPoints = parseFloat(parseFloat($scope.productDetails[d].productPoint) * $scope.productDetails[d].quantity).toFixed(2);
                if ($scope.contract.productDetails !== null) {
                    $scope.contract.productDetails[d].quantity = parseInt(a) - 1;
                    $scope.contract.productDetails[d].productPoints = parseFloat(parseFloat($scope.contract.productDetails[d].productPoint) * $scope.contract.productDetails[d].quantity).toFixed(2);
                }
                $scope.totalPoints = $scope.getTotalPoints(true);
            }
            type = "fetchListOfPriceList";
            getServiceCall();
        }


    }



    $scope.changeQuantity = function(a, d) {
        if ($scope.productDetails[d].productName !== '') {
            $scope.productDetails[d].productPoints = parseFloat(parseFloat($scope.productDetails[d].productPoint) * $scope.productDetails[d].quantity).toFixed(2);
            if ($scope.contract.productDetails !== null) {
                $scope.contract.productDetails[d].productPoints = parseFloat(parseFloat($scope.contract.productDetails[d].productPoint) * $scope.contract.productDetails[d].quantity).toFixed(2);
            }
            $scope.totalPoints = $scope.getTotalPoints(true);

            type = "fetchListOfPriceList";
            getServiceCall();
        }

    }
    $scope.showRemoveProdBtn = true;
    $scope.removeProductRow = function(index) {
        /*$scope.productDetails.splice( index, 1);

	//stickey Header
	enableStickyHeader($(".sticky-block"));*/
        if ($scope.productDetails.length > 1) {
            $scope.productDetails.splice(index, 1);
            $scope.myprod.selProductDetails.splice(index, 1);

            type = "fetchListOfPriceList";
            getServiceCall();

            //stickey Header
            enableStickyHeader($(".sticky-block"));
        } else {
            //$scope.showRemoveProdBtn = false;
        }
    };
    $scope.checkContProdAddBtn = function(index) {
        if ($scope.productDetails.length === (index + 1)) return true;
        return false;
    }

    $scope.addProductRow = function() {
        /*$scope.productModel.push({"spinners":0,"products":''});*/

        $scope.productDetails.push({
            productId: "",
            productName: "",
            productPoint: "",
            quantity: 1,
            newlyAdded: true
        });

        //stickey Header
        enableStickyHeader($(".sticky-block"));
    };
    $scope.addReferenceDiv = function() {

        //if(!offline){
        $scope.referenceModel.push({
            "relation": "",
            "name": "",
            "phoneNo": "",
            "email": "",
            "addressLine1": "",
            "addressLine2": "",
            "cityName": "",
            "stateName": "",
            "pinCode": "",
            "newlyAdded": true
        });
        //}


       // console.log(JSON.stringify($scope.referenceModel));
        $scope.enableRefremove = true;
        //stickey Header
        enableStickyHeader($(".sticky-block"));
    }
    $scope.removeReferenceDiv = function(index) {
        if (index > -1) {
            $scope.referenceModel.splice(index, 1);
        }
        if ($scope.referenceModel.length < 2) $scope.enableRefremove = false;
        //stickey Header
        enableStickyHeader($(".sticky-block"));
    }

    /*Sample product data*/
    /* $scope.productList = ["Anlog Communication","Digital Communication","Electro Magnetic and Instrumentation"
     ,"Control System","Operating System&Maitainence"];
    */
    $scope.productList = [];

    function convertSysDate(date) {
        var dtArr = date.split("-");
        //var timeZone = "T00:00:00.000Z";
        var month = ('0' + (parseInt($.inArray(dtArr[1], shortMonthArr)) + 1)).slice(-2);
        var custDob = dtArr[2] + "-" + month + "-" + dtArr[0];
        return new Date(custDob);
    }

    function getServiceCall() {
        adapter.getMappedUrls().then(function(result) {
            var url = adapter.getCurrentUrl(result, type);
            //console.log(url);
            var dataReq;
            if (type === 'getCustomer') {
                $scope.loader = true;

                if (angular.isDefined($scope.hdnSelectedCustomer)) {
                    dataReq = {
                        userId: Session.userId,
                        customerCode: $scope.hdnSelectedCustomer,
                        customerName: $scope.customerName,
                        ic: $scope.icNo
                    }

                } else {
                    dataReq = {
                        userId: Session.userId,
                        customerCode: $scope.customerCode,
                        customerName: $scope.customerName,
                        ic: $scope.icNo
                    }
                }

            } else if (type === 'getSalesRepList') {
                $scope.loader = true;
                dataReq = {};
                url = 'ContractController/salesRepList';

            } else if (type === 'getStateList') {
                $scope.loader = true;
                dataReq = {
                    userId: Session.userId
                };
                url = 'FetchListController/getStateList';
            } else if (type === 'getProduct') {
                $scope.loader = true;
                dataReq = {
                    userId: Session.userId,
                    stateName: stateNameForProduct
                }
            } else if (type === 'getPricingDetails') {
                $scope.loader = true;
                dataReq = {
                    userId: Session.userId,
                    priceListCode: $scope.pricing.priceListCode,
                    points: $scope.pricing.points,
                    stateName: $scope.customerDetails.homeState,
                    itemPoint: itemPoint,
                    terms: $scope.pricing.terms
                }
            } else if (type === 'createContract') {
                $scope.loader = true;

                var paymentDetails = [];

                var chequeDate=new Date($scope.chequeDate).toLocaleDateString("en-GB");

                if ($scope.editContractCustomerDetail) {

                    paymentDetails.push({
                        paymentType: "Deposit1",
                        paymentMethod: $scope.contract.methodOfPaymentDP1,
                        orderDueAmt: $scope.contract.orderDueAmt,
                        paymentAmt: $scope.contract.downPayment1,
                        paymentRefNum: $scope.contract.paymentRefNum,
                        bankId: ($scope.contract.bankDp1===undefined)?null:$scope.contract.bankDp1.bankId,
                        chequeDt:chequeDate

                    });
                    paymentDetails.push({
                        paymentType: "Deposit2",
                        paymentMethod: $scope.contract.methodOfPaymentDP2,
                        orderDueAmt: $scope.contract.orderDueAmt,
                        paymentAmt: $scope.contract.downPayment2,
                        paymentRefNum: $scope.contract.paymentRefNum

                    });
                    if ($scope.contract.salesRepContributionStatus) {
                        var srType = "";
                        if ($scope.contract.commission) {
                            srType = "Commission"
                        } else {
                            srType = "Cash"
                        }
                        paymentDetails.push({
                            paymentType: "SalesRepContribution",
                            paymentMethod: srType,
                            orderDueAmt: $scope.contract.orderDueAmt,
                            paymentAmt: $scope.contract.salesRepContributionAmt,
                            paymentRefNum: $scope.contract.paymentRefNum,
                            bankAccountNo: "5201258633",
                            bankName: "BankAbc"

                        });
                    }


                } else {
                    paymentDetails.push({
                        paymentType: "Deposit1",
                        paymentMethod: $scope.pricing.methodOfPaymentDP1,
                        orderDueAmt: $scope.pricing.orderDueAmt,
                        paymentAmt: $scope.pricing.downpayment1,
                        paymentRefNum: $scope.pricing.paymentRefNum,
                        bankId: ($scope.pricing.bankDp1===undefined)?null:$scope.pricing.bankDp1.bankId,
                        chequeDt:chequeDate

                    });
                    paymentDetails.push({
                        paymentType: "Deposit2",
                        paymentMethod: $scope.pricing.methodOfPaymentDP2,
                        orderDueAmt: $scope.pricing.orderDueAmt,
                        paymentAmt: $scope.pricing.downpayment2,
                        paymentRefNum: $scope.pricing.paymentRefNum

                    });
                    if ($scope.pricing.salesRepContribution) {
                        var srType = "";
                        if ($scope.pricing.payby) {
                            srType = "Commission"
                        } else {
                            srType = "Cash"
                        }
                        paymentDetails.push({
                            paymentType: "SalesRepContribution",
                            paymentMethod: srType,
                            orderDueAmt: $scope.pricing.orderDueAmt,
                            paymentAmt: $scope.pricing.salesRepContributionAmt,
                            paymentRefNum: $scope.pricing.paymentRefNum,
                            bankAccountNo: "5201258633",
                            bankName: "BankAbc"

                        });
                    }
                }
                //console.log(paymentDetails);
                var remarkDetails = [];
                if ($scope.remarks.remarkCustomer !== null && $scope.remarks.remarkCustomer !== undefined && $scope.remarks.remarkCustomer !== "")
                    remarkDetails.push({
                        "remarks": $scope.remarks.remarkCustomer,
                        "remarkType": "Customer_Section"
                    });

                if ($scope.remarks.remarkProduct !== null && $scope.remarks.remarkProduct !== undefined && $scope.remarks.remarkProduct !== "")
                    remarkDetails.push({
                        "remarks": $scope.remarks.remarkProduct,
                        "remarkType": "Product_Section"
                    });

                if ($scope.remarks.remarkPricing !== null && $scope.remarks.remarkPricing !== undefined && $scope.remarks.remarkPricing !== "")
                    remarkDetails.push({
                        "remarks": $scope.remarks.remarkPricing,
                        "remarkType": "Pricing_Section"
                    });

                //if ($scope.customerDetails.dob.search("-")) {
                var dob = null;
                if (angular.isDefined($scope.customerDetails.dob) && $scope.customerDetails.dob !== "" && $scope.customerDetails.dob !== null) {

                    dob = convertSysDate($scope.customerDetails.dob);
                }

                //$scope.customerDetails.dob =

                dataReq = {
                    userId: Session.userId,
                    customerDetails: $scope.customerDetails,
                    priceListCode: $scope.pricing.priceListCode,
                    campaignCode: $scope.campaignCode,
                    points: $scope.pricing.points,
                    terms: $scope.pricing.terms,
                    productDetails: $scope.productDetails,
                    paymentDetails: paymentDetails,
                    remarkDetails: remarkDetails,
                    review_terms: $scope.review_terms
                }
                dataReq.customerDetails.dob = dob;

                if ($scope.isAdmin) {
                    dataReq.salesRepName = $scope.adminCont.salesRepName;

                    if (angular.isDefined($scope.adminCont.orderDate) && $scope.adminCont.orderDate !== "" && $scope.adminCont.orderDate !== null)
                        dataReq.orderDate = convertSysDate($scope.adminCont.orderDate);
                    else dataReq.orderDate = null;

                    if (angular.isDefined($scope.adminCont.deliveryDate) && $scope.adminCont.deliveryDate !== "" && $scope.adminCont.deliveryDate !== null)
                        dataReq.deliveryDate = convertSysDate($scope.adminCont.deliveryDate);
                    else dataReq.deliveryDate = null;

                    if (angular.isDefined($scope.adminCont.commenceMentDate) && $scope.adminCont.commenceMentDate !== "" && $scope.adminCont.commenceMentDate !== null)
                        dataReq.commenceMentDate = convertSysDate($scope.adminCont.commenceMentDate);
                    else dataReq.commenceMentDate = null;

                } else {
                    dataReq.salesRepName = null;
                    dataReq.orderDate = null;
                    dataReq.deliveryDate = null;
                    dataReq.commenceMentDate = null;
                }

                //console.log(JSON.stringify(dataReq));

                $scope.contractData = dataReq;



            } else if (type === 'getCustomerState') {
                dataReq = {
                    userId: Session.userId
                }
            } else if (type === 'getCustomerList') {
                dataReq = {
                    userId: Session.userId,
                    customerName: $scope.customerName
                }
            } else if (type === 'getTerms') {
                $scope.loader = true;
                dataReq = {
                    userId: Session.userId,
                    stateName: $scope.customerDetails.homeState,
                    itemPoint: itemPoint,
                    totalItemPoint:totalItemPoint,
                    customerDetails: {
                        excempted: $scope.customerDetails.excempted,
                        homePinCode: $scope.customerDetails.homePinCode
                    },
                    priceListCode: $scope.pricing.priceListCode
                }

                //console.log("Data while creating"+JSON.stringify(dataReq));
            } else if (type === 'getPointList') {
                $scope.loader = true;
                dataReq = {
                    userId: Session.userId,
                    stateName: $scope.customerDetails.homeState,
                    priceListCode: $scope.pricing.priceListCode,
                    itemPoint: itemPoint,
                    totalItemPoint:totalItemPoint,
                    customerDetails: {
                        excempted: $scope.customerDetails.excempted,
                        homePinCode: $scope.customerDetails.homePinCode
                    },
                    terms: $scope.pricing.terms
                }
            } else if (type === 'fetchListOfPriceList') {
                $scope.loader = true;
                dataReq = {
                    userId: Session.userId,
                    productDetails: angular.isDefined($scope.productDetails) ? $scope.productDetails : null,
                    customerDetails: {
                        excempted: angular.isDefined($scope.customerDetails.excempted) ? $scope.customerDetails.excempted : false,
                        homePinCode: angular.isDefined($scope.customerDetails.homePinCode) ? $scope.customerDetails.homePinCode : null
                    },
                    stateName: (angular.isDefined($scope.customerDetails.homeState) ? $scope.customerDetails.homeState : null)
                }
            }

            adapter.getServiceData(url, dataReq).then(success, error);

            function success(result) {
                $scope.loader = false;

                if (result.statusCode === 200) {

                    if (angular.isDefined($scope.hdnSelectedCustomer)) {
                        custFieldName = "Customer Name";
                        custFieldValue = $scope.customerName;

                    } else if ($scope.searchCustomerBy === "custCode") {
                        custFieldName = "Customer ID";
                        custFieldValue = ($scope.customerCode !== undefined) ? $scope.customerCode : "";

                    } else if ($scope.searchCustomerBy === "custName") {
                        custFieldName = "Customer Name";
                        custFieldValue = ($scope.customerName !== undefined) ? $scope.customerName : "";

                    } else if ($scope.searchCustomerBy === "ic") {
                        custFieldName = "IC No.";
                        custFieldValue = ($scope.icNo !== undefined) ? $scope.icNo : "";

                    } else {
                        custFieldName = "";
                        custFieldValue = "";
                    }

                    if (result.message === "Success" || result.message === "SUCCESS") {
                        if (type === "getCustomer") {
                            //$scope.customerDetails = result.customerDetails;
                            $scope.customerDetails.customerCode = result.customerDetails.customerCode;
                            $scope.customerDetails.name = result.customerDetails.name;
                            $scope.customerDetails.icNo = result.customerDetails.icNo;
                            angular.isDefined($scope.customerDetails) ? $scope.customerDetails.excempted = false : '';
                            $scope.stateList = result.stateList;
                            // $scope.referenceModel = result.customerDetails.relationships;
                            $scope.referenceModel = [];
                            $scope.methodOfPaymentList = result.paymentMethodList;

                            tempDob = $scope.customerDetails.dob;
                            $scope.activateNav = true;
                        } else if (type === "getSalesRepList") {
                            $scope.salesRepList = result.salesRepList;
                        } else if (type === "getStateList") {
                            $scope.stateList = result.stateNameList;
                            angular.isDefined($scope.customerDetails) ? $scope.customerDetails.excempted = false : '';

                            $scope.methodOfPaymentList =["Cash","Credit Card","Cheque"];

                        } else if (type === "getProduct") {
                            $scope.productDetails = [];
                            $scope.productDetails.push({
                                productId: "",
                                productName: "",
                                productPoint: "",
                                quantity: "1",
                                newlyAdded: false
                            });
                            $scope.productList1 = result.productList;

                            $scope.bankList=result.bankList;



                            /*$scope.contract.productList1=result.productList;
                            $scope.contract.priceListCodeList=result.priceListCodeList;
                            $scope.contract.installmentList=result.installmentList;*/

                        } else if (type === 'getTerms') {
                            $scope.installmentList = result.installmentList;
                            $scope.pricing.terms = undefined;
                            $scope.pricing.points = undefined;

                        } else if (type === 'getPointList') {
                            $scope.pointList = result.pointList;
                            $scope.pricing.points = undefined;
                        } else if (type === "getPricingDetails") {
                            //console.log(result);

                            $scope.dp1Error = true;
                            $scope.methodOfDp1Error = true;
                            $scope.refNoError = true;
                            $scope.dp2MethodError = true;
                            $scope.contributionError = true;
                            $scope.priceListCodeError = true;
                            $scope.pricingPointsError = true;
                            $scope.termsError = true;
                            $scope.salesRepError = true;
                            $scope.bankDp1Error=true;
                            $scope.bankDp2Error=true;
                            $scope.cheque1Error=true;
                            $scope.cheque2Error=true;


                            $scope.paymentDetailsShow = true;
                            actualDownPayment1 = result.downPayment1;
                            if(actualDownPayment1===0){
                            	$scope.dp1Disabled=true;
                            }else{
                            	$scope.dp1Disabled=false;
                            }

                            actualDownPayment2 = (result.downPayment2 !== undefined && result.downPayment2 !== null) ? result.downPayment2 : 0;

                            $scope.contract.downPayment2 = $scope.pricing.downpayment2 = result.downPayment2;
                            $scope.contract.downPayment1 = $scope.pricing.downpayment1 = result.downPayment1;
                            $scope.contract.totalAmountOrder = $scope.pricing.totalAmountOrder = result.totalAmountOrder;
                            $scope.contract.totalInstallmentAmount = $scope.pricing.totalInstallmentAmount = result.totalInstallmentAmount;
                            $scope.contract.installmentAmt = $scope.pricing.installmentAmt = result.installmentAmt;
                            $scope.contract.totalOutstandingBalance = $scope.pricing.totalOutstandingBalance = result.totalOutstandingBalance;

                            $scope.contract.bankList = $scope.bankList;
                            $scope.contract.bankDp1 = $scope.pricing.bankDp1;
                            $scope.contract.bankDp2 = $scope.pricing.bankDp2;

                            $scope.contract.salesRepContributionAmt = $scope.pricing.salesRepContributionAmt = undefined;
                            $scope.contract.salesRepContribution = $scope.pricing.salesRepContribution = undefined;
                            $scope.contract.salesRepContribution = $scope.pricing.salesRepContribution = false;
                            $scope.contract.payby = $scope.pricing.payby = false;




                            if (!$scope.editContractCustomerDetail)
                                var data = $scope.pricing.terms;
                            else
                                var data = $scope.contract.terms;

                            if (data === "COD" || data === "CAD")
                                $scope.bank.bankId = data;
                            else
                                $scope.bank.bankId = data + " Months";

                        } else if (type === "createContract") {
                           // console.log(result);
                            contractCode = result.contractCode;

                            localStorageService.set("modalStatus", true);
                            modalOpen("success");

                            //$scope.customerDetails.homeCountry=$scope.customerDetails.country;
                            /*$scope.customerDetails.homeCountry="Malaysia";
		        			type="getProduct";
							getServiceCall();*/
                        } else if (type === "createContractProduct") {
                            //console.log(result);
                        } else if (type === "getCustomerState") {
                           // console.log(result);
                            $scope.stateList = result.stateList;
                            $scope.customerDetails.excempted = false;
                            $scope.methodOfPaymentList = result.paymentMethods;
                            $scope.activateNav = true;
                        }
                        if (type === 'getCustomer') {
                            $scope.networkError = false;
                            $scope.networkSuccess = true;
                            $scope.alertMsg = "Customer detail found for " + custFieldName + " (" + custFieldValue + ").";
                            $(".alert-success").fadeTo(2000, 500).slideUp(2000);
                            //type='getCustomerState';
                            //getServiceCall();
                        } else if (type === 'getCustomerList') {
                            $scope.hdnSelectedCustomer = undefined;
                            $scope.customerList = result.matchedCustomerNameList;

                            if ($scope.customerList.length > 0) angular.element('#prePopulateCustList').css('display', 'block');
                            else angular.element('#prePopulateCustList').css('display', 'none');

                            $scope.selectTPrePopulate = function(index) {
                                $scope.customerName = $scope.customerList[index].customerName;
                                $scope.hdnSelectedCustomer = $scope.customerList[index].custCode;
                                angular.element('#prePopulateCustList').css('display', 'none');
                            }
                        } else if (type === 'fetchListOfPriceList') {
                            console.log(result);
                            if (!angular.isDefined($scope.pricing)) $scope.pricing = {};
                            if (!angular.isDefined($scope.contract)) $scope.contract = {};
                            $scope.pricing.priceListCodeList = $scope.contract.priceListCodeList = $scope.priceListCodeList = result.priceListCodeList;

                            itemPoint = result.itemPoint;
                            totalItemPoint=result.totalItemPoint;

                            $scope.contract.terms = undefined;
                            $scope.contract.points = undefined;
                            $scope.pricing.terms = undefined;
                            $scope.pricing.points = undefined;
                        }

                        if (tempDob !== null && tempDob !== undefined && $.trim(tempDob) !== "")
                            $scope.customerDetails.dob = tempDob;
                        else if ($scope.customerDetails.dob !== null && $scope.customerDetails.dob !== undefined && $.trim($scope.customerDetails.dob) !== "")
                            tempDob = $scope.customerDetails.dob;

                    } else {
                        if (type === 'getCustomer') {
                            $scope.networkSuccess = false;
                            $scope.networkError = true;
                            $scope.alertMsg = "Customer detail not found for " + custFieldName + " (" + custFieldValue + "). New customer will be created.";
                            $(".alert-success").fadeTo(2000, 500).slideUp(2000);

                            $scope.customerDetails.name = $scope.customerName;
                            $scope.customerDetails.icNo = $scope.icNo;

                            type = 'getCustomerState';
                            getServiceCall();
                        } else if (type === 'getPricingDetails') {

                            $scope.paymentDetailsShow = false;
                            actualDownPayment1 = 0;
                            actualDownPayment2 = 0;

                            $scope.contract.downPayment1 = $scope.pricing.downpayment1 = null;
                            $scope.contract.downPayment2 = $scope.pricing.downpayment2 = null;
                            $scope.contract.totalAmountOrder = $scope.pricing.totalAmountOrder = null;
                            $scope.contract.totalInstallmentAmount = $scope.pricing.totalInstallmentAmount = null;
                            $scope.contract.totalOutstandingBalance = $scope.pricing.totalOutstandingBalance = null;

                            $scope.contract.salesRepContributionAmt = $scope.pricing.salesRepContributionAmt = undefined;
                            $scope.contract.salesRepContribution = $scope.pricing.salesRepContribution = undefined;
                            $scope.contract.salesRepContribution = $scope.pricing.salesRepContribution = false;
                            $scope.contract.payby = $scope.pricing.payby = false;
                        }

                    }

                } else {
                    console.log("Service Not Resolved");
                    $scope.loader = false;

                                    if (!appInUse) {

                                        if (tempDob !== null && tempDob !== undefined && $.trim(tempDob) !== "")
                                            $scope.customerDetails.dob = tempDob;
                                        else if ($scope.customerDetails.dob !== null && $scope.customerDetails.dob !== undefined && $.trim($scope.customerDetails.dob) !== "")
                                            tempDob = $scope.customerDetails.dob;
                                    }

                                    console.log("Service not resolved");
                                    if (type !== "get" && !appInUse) {
                                        localStorageService.set("modalStatus", false);
                                        modalOpen("error");
                                    }

                                    if (appInUse) {

                                        if (type === 'getCustomer' || type === 'getStateList') {
                                            var data = {
                                                countryId: loggedinUser.countryId
                                            };

                                           // console.log(loggedinUser.countryId);
                                            app.fetchState(data, callbackFunction);
                                        } else if (type === 'getProduct') {
                                            var data = {
                                                countryId: loggedinUser.countryId
                                            };
                                            app.fetchProduct(data, callbackFunctionProduct);

                                        } else if (type === 'fetchListOfPriceList') {

                                            var data = {
                                                userId: Session.userId,
                                                countryId: loggedinUser.countryId,
                                                productDetails: angular.isDefined($scope.productDetails) ? $scope.productDetails : null,
                                                excempted: angular.isDefined($scope.customerDetails.excempted) ? $scope.customerDetails.excempted : false,
                                                homePinCode: angular.isDefined($scope.customerDetails.homePinCode) ? $scope.customerDetails.homePinCode : null,
                                                stateName: (angular.isDefined($scope.customerDetails.homeState) ? $scope.customerDetails.homeState : null)
                                            }

                                            app.fetchPointsTotal(data,callbackFunctionTotalPoints);

                                        } else if (type === 'getTerms') {
                                            var data = {
                                                userId: Session.userId,
                                                countryId: loggedinUser.countryId,
                                                regionName: region,
                                                itemPoint: itemPoint,
                                                totalItemPoint:totalItemPoint,
                                                excempted: $scope.customerDetails.excempted,
                                                priceListCode: $scope.pricing.priceListCode
                                            };
                                            app.fetchTerms(data, callbackFunctionTerms);
                                        } else if (type === 'getPointList') {
                                            var data = {
                                                userId: Session.userId,
                                                countryId: loggedinUser.countryId,
                                                regionName: region,
                                                itemPoint: itemPoint,
                                                totalItemPoint:totalItemPoint,
                                                excempted: $scope.customerDetails.excempted,
                                                priceListCode: $scope.pricing.priceListCode,
                                                terms: $scope.pricing.terms
                                            };
                                            app.fetchPoints(data, callbackFunctionPoints);
                                        } else if (type === 'getPricingDetails') {
                                            var data = {
                                                userId: Session.userId,
                                                countryId: loggedinUser.countryId,
                                                priceListCode: $scope.pricing.priceListCode,
                                                points: $scope.pricing.points,
                                                stateName: $scope.customerDetails.homeState,
                                                itemPoint: itemPoint,
                                                terms: $scope.pricing.terms
                                            };
                                            app.fetchPricingDetails(data, callbackFunctionPricing);

                                        } else if (type === 'createContract') {

                                            var data = $scope.contractData;
                                           // console.log("Before Relation:  " + data.customerDetails.relationships);
                                            //console.log("Before Relation:  " + JSON.stringify(data.customerDetails.relationships));
                                            //data.customerDetails.relationships=angular.toJson(data.customerDetails.relationships,false);
                                           // console.log("After Relation:  " + JSON.stringify(data.customerDetails.relationships));
                                            data.orderDate = convertSysDate(getFormattedDate(new Date()));
                                            data.isSync = false;
                                     
                                     if($scope.editContractCustomerDetail){
                                     data.totalAmountOrder=$scope.contract.totalAmountOrder;
                                     }else{
                                     data.totalAmountOrder=$scope.pricing.totalAmountOrder;
                                     }
                                            //alert(convertSysDate (getFormattedDate(new Date())));
                                            app.saveContractData(data, callbackFunctionSaveContract);
                                        }
                                    }

                }
            };

            function error() {
                $scope.loader = false;

                if (!appInUse) {

                    if (tempDob !== null && tempDob !== undefined && $.trim(tempDob) !== "")
                        $scope.customerDetails.dob = tempDob;
                    else if ($scope.customerDetails.dob !== null && $scope.customerDetails.dob !== undefined && $.trim($scope.customerDetails.dob) !== "")
                        tempDob = $scope.customerDetails.dob;
                }

                console.log("Service not resolved");
                if (type !== "get" && !appInUse) {
                    localStorageService.set("modalStatus", false);
                    modalOpen("error");
                }

                if (appInUse) {

                    if (type === 'getCustomer' || type === 'getStateList') {
                        var data = {
                            countryId: loggedinUser.countryId
                        };

                       // console.log(loggedinUser.countryId);
                        app.fetchState(data, callbackFunction);
                    } else if (type === 'getProduct') {
                        var data = {
                            countryId: loggedinUser.countryId
                        };
                        app.fetchProduct(data, callbackFunctionProduct);

                    } else if (type === 'fetchListOfPriceList') {

                        var data = {
                            userId: Session.userId,
                            countryId: loggedinUser.countryId,
                            productDetails: angular.isDefined($scope.productDetails) ? $scope.productDetails : null,
                            excempted: angular.isDefined($scope.customerDetails.excempted) ? $scope.customerDetails.excempted : false,
                            homePinCode: angular.isDefined($scope.customerDetails.homePinCode) ? $scope.customerDetails.homePinCode : null,
                            stateName: (angular.isDefined($scope.customerDetails.homeState) ? $scope.customerDetails.homeState : null)
                        }

                        app.fetchPointsTotal(data,callbackFunctionTotalPoints);
                        
                    } else if (type === 'getTerms') {
                        var data = {
                            userId: Session.userId,
                            countryId: loggedinUser.countryId,
                            regionName: region,
                            itemPoint: itemPoint,
                            totalItemPoint:totalItemPoint,
                            excempted: $scope.customerDetails.excempted,
                            priceListCode: $scope.pricing.priceListCode
                        };
                        app.fetchTerms(data, callbackFunctionTerms);
                    } else if (type === 'getPointList') {
                        var data = {
                            userId: Session.userId,
                            countryId: loggedinUser.countryId,
                            regionName: region,
                            itemPoint: itemPoint,
                            totalItemPoint:totalItemPoint,
                            excempted: $scope.customerDetails.excempted,
                            priceListCode: $scope.pricing.priceListCode,
                            terms: $scope.pricing.terms
                        };
                        app.fetchPoints(data, callbackFunctionPoints);
                    } else if (type === 'getPricingDetails') {
                        var data = {
                            userId: Session.userId,
                            countryId: loggedinUser.countryId,
                            priceListCode: $scope.pricing.priceListCode,
                            points: $scope.pricing.points,
                            stateName: $scope.customerDetails.homeState,
                            itemPoint: itemPoint,
                            terms: $scope.pricing.terms
                        };
                        app.fetchPricingDetails(data, callbackFunctionPricing);

                    } else if (type === 'createContract') {

                        var data = $scope.contractData;
                       // console.log("Before Relation:  " + data.customerDetails.relationships);
                        //console.log("Before Relation:  " + JSON.stringify(data.customerDetails.relationships));
                        //data.customerDetails.relationships=angular.toJson(data.customerDetails.relationships,false);
                       // console.log("After Relation:  " + JSON.stringify(data.customerDetails.relationships));
                        data.orderDate = convertSysDate(getFormattedDate(new Date()));
                        data.isSync = false;
                            
                                     if($scope.editContractCustomerDetail){
                                     data.totalAmountOrder=$scope.contract.totalAmountOrder;
                                     }else{
                                     data.totalAmountOrder=$scope.pricing.totalAmountOrder;
                                     }
                        //alert(convertSysDate (getFormattedDate(new Date())));
                        app.saveContractData(data, callbackFunctionSaveContract);
                    }
                }



            }
        });
    }

    function callbackFunction(result) {
    //console.log(result);
        $scope.stateList = result;
        $scope.activateNav = true;
        $scope.customerDetails.excempted = false;
        $scope.referenceModel = [];

        if (type !== 'getStateList') {

            $scope.networkSuccess = false;
            $scope.networkError = true;
            offline = true;


            $scope.alertMsg = "Device is Offline. New customer will be created.";
            $(".alert-success").fadeTo(2000, 500).slideUp(2000);

            $scope.customerDetails.name = $scope.customerName;
            $scope.customerDetails.icNo = $scope.icNo;
        }




        $scope.$apply();

       // console.log($scope.stateList);
    }

    function callbackFunctionProduct(result) {
        $scope.productDetails = [];
        $scope.productDetails.push({
            productId: "",
            productName: "",
            productPoint: "",
            quantity: "1",
            newlyAdded: false
        });
        $scope.productList1 = result;
        $scope.$apply();
    }



    function callbackFunctionTotalPoints(result){
        itemPoint = result.itemPoint;
        totalItemPoint=result.totalItemPoint;

        var data = {
                            homePinCode: angular.isDefined($scope.customerDetails.homePinCode) ? $scope.customerDetails.homePinCode : null
                        }

        app.checkPincode(data,callbackFunctionPincode);

    }

    function callbackFunctionListPriceList(result) {

        if (!angular.isDefined($scope.pricing)) $scope.pricing = {};
        if (!angular.isDefined($scope.contract)) $scope.contract = {};
        $scope.pricing.priceListCodeList = $scope.contract.priceListCodeList = $scope.priceListCodeList = result;

        

        $scope.contract.terms = undefined;
        $scope.contract.points = undefined;
        $scope.pricing.terms = undefined;
        $scope.pricing.points = undefined;
        $scope.$apply();
    }



    function callbackFunctionPincode(result){

        if(!$scope.customerDetails.excempted){
            $scope.customerDetails.excempted=result;
        }

        var data={
            stateName: (angular.isDefined($scope.customerDetails.homeState) ? $scope.customerDetails.homeState : null)
        }

        $scope.$apply();

        app.fetchRegion(data,callbackFunctionRegion);


    }


    function callbackFunctionRegion(result){
        region=result;

        var data = {
                            userId: Session.userId,
                            countryId: loggedinUser.countryId,
                            itemPoint: itemPoint,
                            totalItemPoint:totalItemPoint,
                            excempted: angular.isDefined($scope.customerDetails.excempted) ? $scope.customerDetails.excempted : false,
                            //homePinCode: angular.isDefined($scope.customerDetails.homePinCode) ? $scope.customerDetails.homePinCode : null,
                            regionName: (angular.isDefined($scope.customerDetails.homeState) ? region : null)
                        }
        app.fetchListPriceList(data, callbackFunctionListPriceList);

    }

    function callbackFunctionTerms(result) {
        $scope.installmentList = result;
        $scope.pricing.terms = undefined;
        $scope.pricing.points = undefined;
        $scope.$apply();
    }

    function callbackFunctionPoints(result) {
        $scope.pointList = result;
        $scope.methodOfPaymentList = ["Cash","Credit Card","Cheque","Online Payment"];
        $scope.pricing.points = undefined;
        $scope.$apply();
    }

    function callbackFunctionPricing(result) {
        $scope.paymentDetailsShow = true;
        actualDownPayment1 = result.downPayment1;
        actualDownPayment2 = (result.downPayment2 !== undefined && result.downPayment2 !== null) ? result.downPayment2 : 0;

        $scope.contract.downpayment2 = $scope.pricing.downpayment2 = result.downPayment2;
        $scope.contract.downpayment1 = $scope.pricing.downpayment1 = result.downPayment1;
        $scope.contract.totalAmountOrder = $scope.pricing.totalAmountOrder = result.totalAmountOrder;
        $scope.contract.totalInstallmentAmount = $scope.pricing.totalInstallmentAmount = result.totalInstallmentAmount;
        $scope.contract.installmentAmt = $scope.pricing.installmentAmt = result.installmentAmt;
        $scope.contract.totalOutstandingBalance = $scope.pricing.totalOutstandingBalance = result.totalOutstandingBalance;

        $scope.contract.salesRepContributionAmt = $scope.pricing.salesRepContributionAmt = undefined;
        $scope.contract.salesRepContribution = $scope.pricing.salesRepContribution = undefined;
        $scope.contract.salesRepContribution = $scope.pricing.salesRepContribution = false;
        $scope.contract.payby = $scope.pricing.payby = false;

        // console.log(JSON.stringify(result.bankList));
        


        if (!$scope.editContractCustomerDetail)
            var data = $scope.pricing.terms;
        else
            var data = $scope.contract.terms;

        if (data === "COD" || data === "CAD")
            $scope.bank.bankId = data;
        else
            $scope.bank.bankId = data + " Months";


        var bankData={
            countryId:loggedinUser.countryId
        }

        app.fetchBankList(bankData,callbackFunctionBankList);

        $scope.$apply();

    }


    function callbackFunctionBankList(result){
        $scope.bankList=result;
        $scope.$apply();
    }

    function callbackFunctionSaveContract(result) {
        $scope.saveDraft();
        $scope.$apply();

    }

    $scope.getPricingDetails = function() {
        /*if($(this).val($(this).val().replace(/[^0-9\.]/g,''));
         if ((event.which !== 46 || $(this).val().indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
            return false;
         }*/
        /*if($scope.pricing.priceListCode!=="" && $scope.pricing.points!=="" && $scope.pricing.terms!==""){
        	type="getPricingDetails";
        	getServiceCall();
        }*/
        if ($scope.pricing.points !== "" && $scope.pricing.points !== null) {
            if (validate.validateText($scope.pricing.priceListCode) && validate.validateText($scope.pricing.points) && validate.validateText($scope.pricing.terms) && validate.validateDecimal($scope.pricing.points)) {
                type = "getPricingDetails";
                getServiceCall();
            }
            if (!validate.validateDecimal($scope.pricing.points))
                $scope.pricingPointsError = false;
            else
                $scope.pricingPointsError = true;
        }

    }
    $scope.generateAmount = function(ctype) {
        var dp1;
        if (parseFloat($scope.pricing.downpayment1) === 0) {
            dp1 = 0;
        } else {
            dp1 = parseFloat($scope.pricing.downpayment1) || undefined;
        }

        var dp2 = parseFloat($scope.pricing.downpayment2) || 0;
        var saleRepAmt = parseFloat($scope.pricing.salesRepContributionAmt) || undefined;
        var saleRepRlAmt = parseFloat($scope.pricing.salesRepContributionAmt) || 0;
        var saleRepStatus = $scope.pricing.salesRepContribution;

        var total = dp1 + dp2 + saleRepRlAmt || undefined;
        var actualTotal = parseFloat(actualDownPayment1) + parseFloat(actualDownPayment2);

        if (angular.isDefined(ctype) && ctype === 'dp1') {
            if (dp1 === undefined || dp1 < 0 || dp1 > actualTotal || total > actualTotal) {

                $scope.pricing.downpayment1 = actualDownPayment1;
                $scope.pricing.downpayment2 = actualDownPayment2;
                $scope.pricing.salesRepContributionAmt = undefined;

            } else {
                if (saleRepAmt === undefined || saleRepAmt < 0 || !saleRepStatus) {
                    $scope.pricing.downpayment2 = actualTotal - dp1;
                    $scope.pricing.salesRepContributionAmt = undefined;

                } else {
                    $scope.pricing.downpayment2 = actualTotal - dp1 - saleRepAmt;
                }
            }

        } else {
            var editTotal = dp1 + saleRepAmt || undefined;
            if (saleRepAmt === undefined || saleRepAmt < 0 || saleRepAmt > actualTotal ||
                editTotal === undefined || editTotal > actualTotal || !saleRepStatus) {

                $scope.pricing.downpayment2 = actualTotal - dp1;
                $scope.pricing.salesRepContributionAmt = undefined;

            } else {
                $scope.pricing.downpayment2 = actualTotal - dp1 - saleRepAmt;
            }
        }

        //for contract model
        if ($scope.showCreateContract)
            generateAmountContract(ctype);
    }

    function generateAmountContract(ctype) {
        var dp1;

        if (parseFloat($scope.contract.downPayment1) === 0) {
            dp1 = 0;
        } else {
            dp1 = parseFloat($scope.contract.downPayment1) || undefined;
        }

        var dp2 = parseFloat($scope.contract.downPayment2) || 0;
        var saleRepAmt = parseFloat($scope.contract.salesRepContributionAmt) || undefined;
        var saleRepRlAmt = parseFloat($scope.contract.salesRepContributionAmt) || 0;
        var saleRepStatus = $scope.contract.salesRepContributionStatus;

        var total = dp1 + dp2 + saleRepRlAmt || undefined;
        var actualTotal = parseFloat(actualDownPayment1) + parseFloat(actualDownPayment2);

        if (angular.isDefined(ctype) && ctype === 'dp1') {
            if (dp1 === undefined || dp1 < 0 || dp1 > actualTotal || total > actualTotal) {

                $scope.contract.downPayment1 = actualDownPayment1;
                $scope.contract.downPayment2 = actualDownPayment2;
                $scope.contract.salesRepContributionAmt = undefined;

            } else {
                if (saleRepAmt === undefined || saleRepAmt < 0 || !saleRepStatus) {
                    $scope.contract.downPayment2 = actualTotal - dp1;
                    $scope.contract.salesRepContributionAmt = undefined;

                } else {
                    $scope.contract.downPayment2 = actualTotal - dp1 - saleRepAmt;
                }
            }

        } else {
            var editTotal = dp1 + saleRepAmt || undefined;
            if (saleRepAmt === undefined || saleRepAmt < 0 || saleRepAmt > actualTotal ||
                editTotal === undefined || editTotal > actualTotal || !saleRepStatus) {

                $scope.contract.downPayment2 = actualTotal - dp1;
                $scope.contract.salesRepContributionAmt = undefined;

            } else {
                $scope.contract.downPayment2 = actualTotal - dp1 - saleRepAmt;
            }
        }
    }

    $scope.final = function() {
        if (reviewValidations()) {
            /*console.log($scope.customerDetails);
            console.log($scope.productDetails);*/



            type = "createContract";
            getServiceCall();
        } else {
            $location.hash('bottom');

            // call $anchorScroll()
            $anchorScroll();
        }

    }

    $scope.editPreview = function() {

        $scope.editContractCustomerDetail = !$scope.editContractCustomerDetail;
        var a = $scope.pricing.payby;
        var b = $scope.contract.commission;

        if ($scope.editContractCustomerDetail) {
            $scope.contract.commission = a;
            $scope.totalPoints = $scope.getTotalPoints(true);

        } else {
        	$scope.pricing.payby = b;
        }
        //stickey Header
        enableStickyHeader($(".sticky-block"));
    }

    $scope.myprod.selProductDetails = [];
    $scope.selDetails = [];
    $scope.productDetailErr = [];
    $scope.selectProduct = function(selProduct, parentIndex, index) {
    $("#prodError").css('display', 'none');
    $("#prodEditError").css('display', 'none');
        if (chkIsNewProduct(selProduct, $scope.myprod.selProductDetails, parentIndex)) {
            $scope.selDetails[parentIndex] = "";
            $scope.productDetails[parentIndex].productId = selProduct.productId;
            $scope.productDetails[parentIndex].productName = $scope.myprod.selProductDetails[parentIndex] = selProduct.productName;

            if (selProduct.hasOwnProperty("productPoints"))
                if (selProduct.productPoints !== null && selProduct.productPoints !== undefined && selProduct.productPoints !== "") {
                    $scope.productDetails[parentIndex].productPoints = selProduct.productPoints;
                    $scope.productDetails[parentIndex].productPoint = selProduct.productPoint;
                }


            if (selProduct.quantity !== null && selProduct.quantity !== undefined && $.trim(selProduct.quantity) !== "")
                $scope.productDetails[parentIndex].quantity = selProduct.quantity;

            $scope.totalPoints = $scope.getTotalPoints(false);

            type = "fetchListOfPriceList";
            getServiceCall();

        }
    }

    $scope.selectProductMobile = function(selProduct, parentIndex) {
    $("#prodError").css('display', 'none');
    $("#prodEditError").css('display', 'none');
        if (chkIsNewProduct(selProduct, $scope.myprod.selProductDetails, parentIndex)) {
            $scope.selDetails[parentIndex] = "";
            $scope.productDetails[parentIndex].productId = selProduct.productId;
            $scope.productDetails[parentIndex].productName = selProduct.productName;

            if (selProduct.hasOwnProperty("productPoints"))
                if (selProduct.productPoints !== null && selProduct.productPoints !== undefined && selProduct.productPoints !== "") {
                    $scope.productDetails[parentIndex].productPoints = selProduct.productPoints;
                    $scope.productDetails[parentIndex].productPoint = selProduct.productPoint;
                }


            if (selProduct.quantity !== null && selProduct.quantity !== undefined && $.trim(selProduct.quantity) !== "")
                $scope.productDetails[parentIndex].quantity = selProduct.quantity;

            $scope.totalPoints = $scope.getTotalPoints(false);



        }
    }

    function chkIsNewProduct(selProduct, selProductList, parentIndex) {
        var newProd = true;
        $("#prodError").css('display', 'none');
        $("#prodEditError").css('display', 'none');
                var prevIndex=selProductList.indexOf(selProduct.productName);
                if(prevIndex>=0){
                    if(prevIndex===parentIndex){
                        return newProd;
                    }
                }
        $scope.showProductDetailErr = $scope.productDetailErr[parentIndex] = false;
        angular.forEach(selProductList, function(value, key) {

            if ((typeof value === 'object' && parentIndex !== key && value.productName === selProduct.productName) || (value === selProduct.productName)) {
                newProd = false;
                $scope.productDetailErr[parentIndex] = true;
                return true;
            }
            if (newProd) $scope.hideOption = true;
        });
        angular.forEach($scope.productDetailErr, function(value, key) {
            if (value) {
                $scope.showProductDetailErr = true;
                return true;
            }
        });

        return newProd;
    }


    $scope.changeProduct = function(selectedVal, index) {
        //$scope.hideOption=true;

        $scope.productDetails[index] = selectedVal;
        $scope.totalPoints = $scope.getTotalPoints(false);
    }
    $scope.productSelIndex = [];
    $scope.getTotalPoints = function(chkSelected) {
            var totalPoints = 0;
            $scope.totalQty = 0;
            if (chkSelected) $scope.productSelIndex = [];

            angular.forEach($scope.productDetails, function(item, index) {
                if (item.hasOwnProperty("productPoints"))
                    if (item.productPoints !== null && item.productPoints !== undefined && item.productPoints !== "")
                        totalPoints += parseFloat(item.productPoints);
                if (item.quantity !== null && item.quantity !== undefined && $.trim(item.quantity) !== "")
                    $scope.totalQty += parseInt(item.quantity);

                if (chkSelected) {
                    angular.forEach($scope.productList1, function(value, key) {
                        if (item.productId !== undefined && item.productId === value.productId)
                            $scope.productSelIndex.push(key);
                    });
                }
            });
            $scope.getTotalPointsVal = totalPoints;
            return totalPoints.toFixed(2);
        }
        //todays date in 12-MAR-2016 format
    $scope.getTodayDate = function() {
        return getFormattedDate(new Date());
    };

    $scope.getCustomDate = function(dateObj) {
        if (dateObj !== undefined && dateObj !== null)
        	{
        		var parts=dateObj.split('-');
        		return getFormattedDate(new Date(parts[2],$scope.getMonthNo(parts[1]),parts[0]));
        	}

        else return '';
    };

    $scope.nameError = true;
    $scope.emailError = true;
    $scope.phoneError = true;
    $scope.icError = true;
    $scope.mobileError = true;
    $scope.raceError = true;
    $scope.dobError = true;
    $scope.Homeaddr1Error = true;
    $scope.Homeaddr2Error = true;
    $scope.HomecityError = true;
    $scope.HomeStateError = true;
    $scope.homePinError = true;
    $scope.companyNameError = true;
    $scope.companyDesgError = true;
    $scope.companyAdd1Error = true;
    $scope.companyAddr2Error = true;
    $scope.companyCityError = true;
    $scope.companyStateError = true;
    $scope.companyPincodeError = true;
    $scope.referenceError = true;
    $scope.prodDetailsError = true;
    $scope.dp1Error = true;
    $scope.methodOfDp1Error = true;
    $scope.refNoError = true;
    $scope.dp2MethodError = true;
    $scope.contributionError = true;
    $scope.priceListCodeError = true;
    $scope.pricingPointsError = true;
    $scope.termsError = true;
    $scope.salesRepError = true;
    $scope.bankDp1Error=true;
    $scope.bankDp2Error=true;

    function validationForCustomerDetails() {
        if ($scope.isAdmin &&
            (!angular.isDefined($scope.adminCont.salesRepName) || $scope.adminCont.salesRepName === "-State-" ||
                $scope.adminCont.salesRepName === "" || $scope.adminCont.salesRepName === null))
            $scope.salesRepError = false;
        else $scope.salesRepError = true;

        $scope.nameError = validate.validateText($scope.customerDetails.name);

        $scope.emailError = validate.validateEmail($scope.customerDetails.email);

        $scope.phoneError = validate.phonenumber($scope.customerDetails.phonoNo);

        $scope.icError=validate.validateText($scope.customerDetails.icNo);

        $scope.mobileError = validate.phonenumber($scope.customerDetails.altPhoneNo);

        /*$scope.raceError=validate.validateText($scope.customerDetails.race);
        if($scope.customerDetails.race==="-Select-")
        	$scope.raceError=false;*/

        /*$scope.dobError=validate.validateDate($scope.customerDetails.dob);*/

        $scope.Homeaddr1Error = validate.validateText($scope.customerDetails.homeAddressLine1);

        /*$scope.Homeaddr2Error=validate.validateText($scope.customerDetails.homeAddressLine2);*/

        //$scope.HomecityError=validate.validateText($scope.customerDetails.homeCity);

        $scope.HomeStateError = validate.validateText($scope.customerDetails.homeState);
        if ($scope.customerDetails.homeState === "-State-")
            $scope.HomeStateError = false;

        $scope.homePinError = validate.validateText($scope.customerDetails.homePinCode);

        /*$scope.companyNameError=validate.validateText($scope.customerDetails.company);

        $scope.companyDesgError=validate.validateText($scope.customerDetails.desg);

        $scope.companyAdd1Error=validate.validateText($scope.customerDetails.compAddressLine1);

        $scope.companyAddr2Error=validate.validateText($scope.customerDetails.compAddressLine2);

        $scope.companyCityError=validate.validateText($scope.customerDetails.compCity);

        $scope.companyStateError=validate.validateText($scope.customerDetails.compState);
        if($scope.customerDetails.compState==="-State-")
        	$scope.companyStateError=false;

        $scope.companyPincodeError=validate.validateText($scope.customerDetails.compPinCode);*/
        $scope.referenceError = true;
        /*for(var i=0;i<$scope.referenceModel.length;i++)
        	{
        		var er1=validate.validateText($scope.referenceModel[i].relation);
        		var er2=validate.validateText($scope.referenceModel[i].name);
        		var er3=validate.phonenumber($scope.referenceModel[i].phoneNo);
        		var er4=validate.validateEmail($scope.referenceModel[i].email);
        		var er5=validate.validateText($scope.referenceModel[i].addressLine1);
        		var er6=validate.validateText($scope.referenceModel[i].addressLine2);
        		var er7=validate.validateText($scope.referenceModel[i].cityName);
        		var er8=validate.validateText($scope.referenceModel[i].stateName);
        		var er9=validate.validateText($scope.referenceModel[i].pinCode);
        		var res=er1 && er2 && er3 && er4 && er5 && er6 && er7 && er8 && er9;
        		if(res===false)
        		$scope.referenceError=false;
        		if(res===false)
        			$("#"+i+'refObj').css("display","block");
        		else
        			$("#"+i+'refObj').css("display","none");
        	}*/
        var resultCustomerDetails = $scope.nameError && $scope.emailError && $scope.phoneError &&
            $scope.icError && $scope.mobileError && $scope.raceError && $scope.dobError && $scope.Homeaddr1Error &&
            $scope.Homeaddr2Error && $scope.HomecityError && $scope.HomeStateError && $scope.homePinError &&
            $scope.companyNameError && $scope.companyDesgError && $scope.companyAdd1Error && $scope.companyAddr2Error && $scope.companyCityError &&
            $scope.companyStateError && $scope.companyPincodeError && $scope.referenceError && $scope.salesRepError;
        return resultCustomerDetails;
    }

    function validationsProductDetails() {
        $scope.prodDetailsError = true;
        for (var i = 0; i < $scope.productDetails.length; i++) {
            var er1 = validate.validateText($scope.productDetails[i].productName);
            var er2 = true;
            if ($scope.productDetails[i].quantity < 1)
                er2 = false;
            var res = er1 && er2;
            if (res === false)
                $scope.prodDetailsError = false;
            if (res === false){
            $("#prodError").css('display', 'block');
            	break;
            }else
                $("#prodError").css('display', 'none');


        }
        return $scope.prodDetailsError;
    }

    function validationPricingDetails() {
        if ($scope.pricing === null || $scope.pricing === undefined)
            return false;
        $scope.priceListCodeError = validate.validateText($scope.pricing.priceListCode);
        $scope.pricingPointsError = validate.validateText($scope.pricing.points);
        $scope.termsError = validate.validateText($scope.pricing.terms);
        $scope.dp1Error = validate.validateText($scope.pricing.downpayment1);

        if($scope.pricing.downpayment1>0){
        	/*if($scope.pricing.downpayment1>actualDownPayment1 || ($scope.pricing.terms!=='COD' && $scope.pricing.methodOfPaymentDP1!=='Cheque' && $scope.pricing.downpayment1>=$scope.pricing.totalAmountOrder )){
        		$scope.dp1Error = false;
        	}*/

        	if($scope.pricing.downpayment1>actualDownPayment1){
        		$scope.dp1Error = false;
        	}

        	if($scope.pricing.terms==='COD' && $scope.pricing.methodOfPaymentDP1!=='Cheque' && $scope.pricing.downpayment1>=$scope.pricing.totalAmountOrder){
        		$scope.dp1Error = false;
        	}else if($scope.pricing.terms==='COD' && $scope.pricing.methodOfPaymentDP1==='Cheque' && $scope.pricing.downpayment1===$scope.pricing.totalAmountOrder){
        		$scope.dp1Error = true;
        	}
        }

        if (parseFloat($scope.pricing.downpayment1) != 0) {
            $scope.methodOfDp1Error = validate.validateText($scope.pricing.methodOfPaymentDP1);
        }

        if (parseFloat($scope.pricing.downpayment1) != 0) {
            $scope.refNoError = validate.validateText($scope.pricing.paymentRefNum);
        }

        if ($scope.pricing.installmentAmt != undefined && $scope.pricing.installmentAmt != null && parseFloat($scope.pricing.installmentAmt) != 0)
            $scope.dp2MethodError = validate.validateText($scope.pricing.methodOfPaymentDP2);

        if ($scope.pricing.terms === 'COD')
            $scope.dp2MethodError = true;
        if ($scope.pricing.salesRepContribution === true)
            $scope.contributionError = validate.validateDecimal($scope.pricing.salesRepContributionAmt);
        else
            $scope.contributionError = true;


        if($scope.pricing.methodOfPaymentDP1!==undefined && $scope.pricing.methodOfPaymentDP1!==null && ($scope.pricing.methodOfPaymentDP1.toLowerCase()==='credit card')){
        	$scope.bankDp1Error=validate.validateText($scope.pricing.bankDp1);
        	if($scope.pricing.bankDp1===undefined){
        		$scope.bankDp1Error=false;
        	}
        }else{
        	$scope.bankDp1Error=true;
        }
        if($scope.pricing.methodOfPaymentDP1!==undefined && $scope.pricing.methodOfPaymentDP1!==null && $scope.pricing.methodOfPaymentDP1.toLowerCase()==='cheque'){
        	$scope.cheque1Error=validate.validateText($scope.chequeDate);
        	if($scope.chequeDate!=null && $scope.chequeDate!=undefined){
                 var sixMonthDate=new Date();
                 sixMonthDate.setMilliseconds(0);
                 sixMonthDate.setMinutes(0);
                 sixMonthDate.setHours(0);
                 sixMonthDate.setMonth(sixMonthDate.getMonth()-6);
                 var currentDate=new Date();
                 currentDate.setMilliseconds(0);
                 currentDate.setMinutes(0);
                 currentDate.setHours(0);

                 var parts=$scope.chequeDate.split('-');
                var selectedDate=new Date(parts[2],$scope.getMonthNo(parts[1]),parts[0]);
                var diff=selectedDate-sixMonthDate;
                var futureDiff=currentDate-selectedDate;
                /*if((selectedDate>sixMonthDate) && (currentDate>=selectedDate))*/
                if((selectedDate>sixMonthDate))
                $scope.cheque2Error=true;
                else
                 $scope.cheque2Error=false;
        	}


        }else{
        	$scope.cheque1Error=true;
        	$scope.cheque2Error=true;
        }

        /*if($scope.pricing.methodOfPaymentDP2==='Credit Card'||$scope.pricing.methodOfPaymentDP2==='Online Payment'){
        	$scope.bankDp2Error=validate.validateText($scope.pricing.bankDp2);
        	if($scope.pricing.bankDp2===undefined){
        		$scope.bankDp2Error=false;
        	}
        }else{
        	$scope.bankDp2Error=true;
        }*/


        var result = $scope.priceListCodeError && $scope.pricingPointsError && $scope.termsError && $scope.dp1Error && $scope.methodOfDp1Error && $scope.refNoError && $scope.dp2MethodError && $scope.contributionError && $scope.bankDp1Error  && $scope.cheque1Error && $scope.cheque2Error;
        return result;
    }

    //Stickey Header
    function enableStickyHeader(obj) {
        var didScroll = true;
        $(window).scroll(function(event) {
            if (didScroll) {
                didScroll = false;
                stickyHeader.enableStickyHeader(obj);
            }
        });
    }
    $scope.dp1EditError = true;
    $scope.methodOfDp1EditError = true;
    $scope.dp2EditMethodError = true;
    $scope.refError = true;
    $scope.prodReviewDetailsError = true;
    $scope.priceListCodeEditError = true;
    $scope.termsEditError = true;
    $scope.pricingPointsEditError = true;
    $scope.review_termsError = true;
    $scope.contributionEditError = true;
    $scope.bankDp1EditError=true;
    $scope.bankDp2EditError=true;

    function reviewValidations() {
        $scope.prodReviewDetailsError = true;
        $scope.dp1EditError = validate.validateText($scope.contract.downPayment1);

        if($scope.contract.downPayment1>0){
        	/*if($scope.contract.downPayment1>actualDownPayment1 || $scope.contract.downPayment1>=$scope.contract.totalAmountOrder ){
        		$scope.dp1EditError = false;
        	}else{
        		$scope.dp1EditError = true;
        	}*/



        	/*if($scope.pricing.downpayment1>actualDownPayment1 || ($scope.pricing.terms!=='COD' && $scope.pricing.methodOfPaymentDP1!=='Cheque' && $scope.pricing.downpayment1>=$scope.pricing.totalAmountOrder )){
        		$scope.dp1Error = false;
        	}*/

        	if($scope.contract.downpayment1>actualDownPayment1){
        		$scope.dp1EditError = false;
        	}

        	if($scope.contract.terms==='COD' && $scope.contract.methodOfPaymentDP1!=='Cheque' && $scope.contract.downpayment1>=$scope.contract.totalAmountOrder){
        		$scope.dp1EditError = false;
        	}else if($scope.contract.terms==='COD' && $scope.contract.methodOfPaymentDP1==='Cheque' && $scope.contract.downpayment1===$scope.contract.totalAmountOrder){
        		$scope.dp1EditError = true;
        	}

        }

        if (parseFloat($scope.contract.downPayment1) != 0)
            $scope.methodOfDp1EditError = validate.validateText($scope.contract.methodOfPaymentDP1);

        if ($scope.contract.installmentAmt != undefined && $scope.contract.installmentAmt != null && parseFloat($scope.contract.installmentAmt) != 0)
            $scope.dp2EditMethodError = validate.validateText($scope.contract.methodOfPaymentDP2);

        if ($scope.contract.terms === 'COD')
            $scope.dp2EditMethodError = true;

        //$scope.dp2EditMethodError=validate.validateText($scope.contract.methodOfPaymentDP2);
        if (parseFloat($scope.contract.downPayment1) != 0)
            $scope.refError = validate.validateText($scope.contract.paymentRefNum);
        $scope.priceListCodeEditError = validate.validateText($scope.contract.priceListCode);
        $scope.termsEditError = validate.validateText($scope.contract.terms);
        $scope.pricingPointsEditError = validate.validateText($scope.contract.points);
        for (var i = 0; i < $scope.contract.productDetails.length; i++) {
            var er1 = validate.validateText($scope.contract.productDetails[i].productName);
            var er2 = true;
            if ($scope.productDetails[i].quantity < 1)
                er2 = false;
            var res = er1 && er2;
            if (res === false)
                $scope.prodReviewDetailsError = false;
            if (res === false){
            	$("#prodEditError").css('display', 'block');
            	break;
            }

            else
                $("#prodEditError").css('display', 'none');


        }
        if ($scope.contract.salesRepContributionStatus === true)
            $scope.contributionEditError = validate.validateDecimal($scope.contract.salesRepContributionAmt);
        else
            $scope.contributionEditError = true;
        var customerDetailsError = validationForCustomerDetails();
        $scope.review_termsError = ($scope.review_terms === true) ? true : false;

        if($scope.contract.methodOfPaymentDP1!==undefined && $scope.contract.methodOfPaymentDP1!==null && ($scope.contract.methodOfPaymentDP1==='Credit Card')){
        	$scope.bankDp1EditError=validate.validateText($scope.contract.bankDp1);
        	if($scope.contract.bankDp1===undefined){
        		$scope.bankDp1EditError=false;
        	}
        }else{
        	$scope.bankDp1EditError=true;
        }

        if($scope.contract.methodOfPaymentDP1!==undefined && $scope.contract.methodOfPaymentDP1!==null && $scope.contract.methodOfPaymentDP1.toLowerCase()==='cheque'){
        	$scope.cheque1Error=validate.validateText($scope.chequeDate);
        	if($scope.chequeDate!=null && $scope.chequeDate!=undefined){
                var sixMonthDate=new Date();
                sixMonthDate.setMilliseconds(0);
                sixMonthDate.setMinutes(0);
                sixMonthDate.setHours(0);
                sixMonthDate.setMonth(sixMonthDate.getMonth()-6);
                var currentDate=new Date();
                currentDate.setMilliseconds(0);
                currentDate.setMinutes(0);
                currentDate.setHours(0);

                var parts=$scope.chequeDate.split('-');
               var selectedDate=new Date(parts[2],$scope.getMonthNo(parts[1]),parts[0]);
               var diff=selectedDate-sixMonthDate;
               var futureDiff=currentDate-selectedDate;
               /*if((selectedDate>sixMonthDate) && (currentDate>=selectedDate))*/
               if((selectedDate>sixMonthDate))
               $scope.cheque2Error=true;
               else
                $scope.cheque2Error=false;
       	}


        }else{
        	$scope.cheque1Error=true;
        	$scope.cheque2Error=true;
        }

      /*  if($scope.contract.methodOfPaymentDP2==='Credit Card'||$scope.contract.methodOfPaymentDP2==='Online Payment'){
        	$scope.bankDp2EditError=validate.validateText($scope.contract.bankDp2);
        	if($scope.contract.bankDp2===undefined){
        		$scope.bankDp2EditError=false;
        	}
        }else{
        	$scope.bankDp2EditError=true;
        }*/

        var result = $scope.dp1EditError && $scope.methodOfDp1EditError && $scope.dp2EditMethodError && $scope.refError && $scope.prodReviewDetailsError && customerDetailsError && $scope.review_termsError && $scope.contributionEditError && $scope.bankDp1EditError && $scope.bankDp2EditError && $scope.cheque1Error && $scope.cheque2Error;
        return result;
    }

    $(".allownumericwithdecimal").on("keypress keyup blur", function(event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');

    });
    $scope.getTerms = function() {
        type = "getTerms";
        getServiceCall();
        $scope.paymentDetailsShow = false;
        actualDownPayment1 = 0;
        actualDownPayment2 = 0;

        $scope.contract.downPayment1 = $scope.pricing.downpayment1 = null;
        $scope.contract.downPayment2 = $scope.pricing.downpayment2 = null;
        $scope.contract.totalAmountOrder = $scope.pricing.totalAmountOrder = null;
        $scope.contract.totalInstallmentAmount = $scope.pricing.totalInstallmentAmount = null;
        $scope.contract.totalOutstandingBalance = $scope.pricing.totalOutstandingBalance = null;
        $scope.dp1Error = true;
        $scope.methodOfDp1Error = true;
        $scope.refNoError = true;
        $scope.dp2MethodError = true;
        $scope.contributionError = true;
        $scope.priceListCodeError = true;
        $scope.pricingPointsError = true;
        $scope.termsError = true;
        $scope.salesRepError = true;
        $scope.bankDp1Error=true;
        $scope.bankDp2Error=true;
        $scope.cheque1Error=true;
        $scope.cheque2Error=true;
    }

    $scope.getPointList = function() {
            if ($scope.pricing.terms === "CAD" || $scope.contract.terms === "CAD") {
                $scope.text.instalmentAmountLbl = "Order Value";
                $scope.text.paymentMethodDP2Lbl = "Method of Payment For CAD";
            }else if($scope.pricing.terms === "COD" || $scope.contract.terms === "COD"){
            	$scope.text.instalmentAmountLbl = "Order Value";
				$scope.text.paymentMethodDP2Lbl = "Method of Payment For COD";
            } else {
                $scope.text.instalmentAmountLbl = "Monthly Instalment Amount";
                $scope.text.paymentMethodDP2Lbl = "Method of Payment For Instalment";
            }
            type = "getPointList";
            getServiceCall();
            $scope.paymentDetailsShow = false;
            actualDownPayment1 = 0;
            actualDownPayment2 = 0;

            $scope.contract.downPayment1 = $scope.pricing.downpayment1 = null;
            $scope.contract.downpayment2 = $scope.pricing.downpayment2 = null;
            $scope.contract.totalAmountOrder = $scope.pricing.totalAmountOrder = null;
            $scope.contract.totalInstallmentAmount = $scope.pricing.totalInstallmentAmount = null;
            $scope.contract.totalOutstandingBalance = $scope.pricing.totalOutstandingBalance = null;
            $scope.dp1Error = true;
            $scope.methodOfDp1Error = true;
            $scope.refNoError = true;
            $scope.dp2MethodError = true;
            $scope.contributionError = true;
            $scope.priceListCodeError = true;
            $scope.pricingPointsError = true;
            $scope.termsError = true;
            $scope.salesRepError = true;
            $scope.bankDp1Error=true;
            $scope.bankDp2Error=true;
            $scope.cheque1Error=true;
            $scope.cheque2Error=true;
        }
        /*$scope.getProductList=function(stateName){
        	stateNameForProduct=stateName;
        	type="getProduct";
        	getServiceCall();
        }*/

    $scope.toggleSalesRep = function() {
        if (!$scope.contributionError) {
            $scope.contributionError = true;
        }
        if (!$scope.contributionEditError) {
            $scope.contributionEditError = true;
        }

        //Sales rep contribution calculation on off
        $scope.pricing.salesRepContributionAmt = undefined;
        $scope.generateAmount();
    }
    $scope.reloadPage = function() {
        $state.reload();
    }
    $scope.reset = function(type) {
        if (type === 'email')
            $scope.emailError = true;
        else if (type === 'phone')
            $scope.phoneError = true;
        else if (type === 'mobile')
            $scope.mobileError = true;
        else if (type === 'addressLine1')
            $scope.Homeaddr1Error = true;
        else if (type === 'pin') {
            $scope.homePinError = true;
            $scope.fetchPriceList();
        } else if (type === 'state'){
        	$scope.HomeStateError = true;
        }else if (type === 'custName'){
			$scope.nameError=true;
		}else if(type==='icNo'){
			$scope.icError=true;
		}else if(type==='salesRep'){
			$scope.salesRepError=true;
		}

    }


    $scope.saveDraft = function() {
            modalEntity = "Contract has been saved successfully as draft.";
            modalBody = "";
            modalOpenDraft("success");


        }
        /* MODAL IMPLEMENTATION */
    $scope.animationsEnabled = true;

    $scope.openDraft = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/modals/draft-modal/draft-mobile.html',
            controller: 'modalDraftCtrl',
            windowClass: 'center-modal',
            backdrop: 'static',
            keyboard: false,
            size: 'md'
        }).result.then(function(data) {
            if (data === "close") {
                $state.go('AdminBoard.ManageContract')
            }
            if (data === "createAnother") {
                $state.reload();

            }

        });
    };


    function modalOpenDraft(modalType) {
        if (errorName !== null && errorName !== "") {
            if (modalType === "success") {
                var modalObj = {
                    "modalEntity": modalEntity,
                    "body": modalBody,
                    "modalBtnText": "Back to Manage Contract",
                    "backLink": "OK",
                    "singleBtn": true
                };
            } else {
                var modalObj = {
                    "modalEntity": "Failed!! ",
                    "body": errorMessage,
                    "modalBtnText": "",
                    "backLink": "OK",
                    "singleBtn": true
                };
            }

            localStorageService.set("modalObj",
                modalObj);
            $scope.openDraft();
        }
    }

    $scope.getBankName=function(bankId){
    	var bankName="";

    	for(var i=0;i<=$scope.bankList.length;i++){
    		if($scope.bankList[i].bankId===bankId){
    			bankName=$scope.bankList[i].accountName;
    		}
    	}
    	return bankName;
    }
    $scope.updatechequeDate=function(partial){
    	$scope.chequeDate=partial.chequeDate;
    }
    $scope.updatechequeDateparial=function(){
    	$scope.partial.chequeDate=$scope.chequeDate;
    }
    $scope.getMonthNo=function(monthName){
    	var month;
	    	switch(monthName.toLowerCase()){
	    	case 'jan':
	        	month=0;
	        	break;
	    	case 'feb':
	        	month=1;
	        	break;
	    	case 'mar':
	        	month=2;
	        	break;
	    	case 'apr':
	        	month=3;
	        	break;
	    	case 'may':
	    		month=4;
	    		break;
	    	case 'jun':
	    		month=5;
	    		break;
	    	case 'jul':
	    		month=6;
	    		break;
	    	case 'aug':
	    		month=7;
	    		break;
	    	case 'sep':
	    		month=8;
	    		break;
	    	case 'oct':
	    		month=9;
	    		break;
	    	case 'nov':
	    		month=10;
	    		break;
	    	case 'dec':
	    		month=11;
	    		break;
	    }
	    	return month;
    }

}]);
