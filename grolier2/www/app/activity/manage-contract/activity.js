app.controller("ManageContractCtrl", ['$scope', '$state', '$uibModal', 'Session', 'localStorageService', 'adapter', 'validate', 'adjustHeight', '$compile','commonServices', function($scope, $state, $uibModal, Session, localStorageService, adapter, validate, adjustHeight, $compile ,commonServices) {
    $scope.topTotalPoints = 0;
    $scope.topTotalOrderAmount = 0;
    $scope.topOutstandingAmount = 0;
    var callType = "";
    $scope.isEmpty = false;
    adjustHeight.adjustTopBar();
    $scope.concatanateString = function(str) {
        return str.replace(/\s+/g, '-').toLowerCase();
    }
    $scope.ShowContractPage = function(row, event) {
    	if($state.current.name==='SalesRep.ManageContractSales'){
    		localStorageService.set('contractRow', row);
    		$state.go('SalesRep.ViewContract');
    	}
    	else{
    		if (event.ctrlKey) {

                localStorageService.set('contractRow', row);
                /*if ($state.current.name === 'SalesRep.ManageContractSales') {
                    window.open('#/SalesRep/ViewContract', '_blank'); // in new tab
                } else {*/
                window.open('#/AdminBoard/ViewContract', '_blank'); // in new tab
                //}
            } else {
                localStorageService.set('contractRow', row);
                /*if ($state.current.name === 'SalesRep.ManageContractSales') {
                    $state.go('SalesRep.ViewContract');
                } else {*/
                $state.go('AdminBoard.ViewContract');
                //}
            }
    	}
    }

    $scope.ShowCustomerPage = function(row, event) {

        if (event.ctrlKey) {

            localStorageService.set('customerCode', row.customerCode);
            window.open('#/AdminBoard/EditCustomer', '_blank'); // in new tab

        } else {
            localStorageService.set('customerCode', row.customerCode);
            $state.go('AdminBoard.EditCustomer');
        }
    }

    $('#Unassigned').addClass('tablink_active');
    $('#Assigned').removeClass('tablink_active');
    $scope.openGrid = function(type1) {
        if (type1 === 'unassigned') {
            $scope.unassigned = true;
            $scope.assigned = false;
            $('#Unassigned').addClass('tablink_active');
            $('#Assigned').removeClass('tablink_active');
            type = "manageUnassigned";
            getServiceCall();
            $scope.loading = true;
        } else {
            $scope.unassigned = false;
            $scope.assigned = true;
            $scope.loading = true;
            $('#Assigned').addClass('tablink_active');
            $('#Unassigned').removeClass('tablink_active');
            type = "manageAssigned";
            getServiceCall();
            $scope.loading = true;
        }
    }

    $scope.getCurrentTotalPoints = function(data) {
        /*var topTotalPoints = 0,
            topTotalOrderAmount = 0,
            topOutstandingAmount = 0;*/

        var totalPoints = 0;
        $scope.totalOrderAmount = 0;
        $scope.outstandingAmount = 0;

        angular.forEach(data, function(item, index) {
            totalPoints += (item.points === 0 || item.points === "" || item.points === null) ? 0 : item.points;
            $scope.totalOrderAmount += (item.totalAmountOrder === 0 || item.totalAmountOrder === "" || item.totalAmountOrder === null) ? 0 : item.totalAmountOrder;
            $scope.outstandingAmount += (item.totalOutstandingBalance === 0 || item.totalOutstandingBalance === "" || item.totalOutstandingBalance === null) ? 0 : item.totalOutstandingBalance;
        });

        //Total
        /*if (callType !== 'rejectedOrderSalesDM') {
            angular.forEach(localStorageService.get('manageData'), function(item, index) {
                //console.log(item);
                topTotalPoints += (item.points === 0 || item.points === "" || item.points === null || item.status.toLowerCase() === "cancelled" || item.status.toLowerCase() === "cancellation initiated" || item.status.toLowerCase() === "dm rejected" || item.status.toLowerCase() === "rejected") ? 0 : item.points;
                topTotalOrderAmount += (item.totalAmountOrder === 0 || item.totalAmountOrder === "" || item.totalAmountOrder === null || item.status.toLowerCase() === "cancelled" || item.status.toLowerCase() === "cancellation initiated" || item.status.toLowerCase() === "dm rejected" || item.status.toLowerCase() === "rejected") ? 0 : item.totalAmountOrder;
                topOutstandingAmount += (item.totalOutstandingBalance === 0 || item.totalOutstandingBalance === "" || item.totalOutstandingBalance === null || item.status.toLowerCase() === "cancelled" || item.status.toLowerCase() === "cancellation initiated" || item.status.toLowerCase() === "dm rejected" || item.status.toLowerCase() === "rejected") ? 0 : item.totalOutstandingBalance;
            });
            $scope.topTotalPoints = topTotalPoints;
            $scope.topTotalOrderAmount = topTotalOrderAmount;
            $scope.topOutstandingAmount = topOutstandingAmount;
            localStorageService.get('manageData', '');
        }*/


        return totalPoints.toFixed(2);
    }

    function getServiceCall() {
        var data = null;
        if (type === 'manageAssigned') {
            data = {}
            url = "ContractController/manageAssignedContract";
        } else if (type === 'manageUnassigned') {
            data = {}
            url = "ContractController/manageUnassignedContract";
        } else if (type === 'rejectedOrderSalesDM') {
            data = {
                userId: Session.userId
            };
            url = "ContractController/manageRejectedContract";

        }
        adapter.getServiceData(url, data).then(success, error);

        function success(result) {
            if (result.statusCode === 200) {

                if (type === "manageAssigned" || type === "manageUnassigned") {
                    $scope.$broadcast('gridData', result.manageContractList);
                    $scope.loading = false;
                } else if (type === 'rejectedOrderSalesDM') {
                    $scope.$parent.loading = false;
                    $scope.$parent.colInfo = $scope.headings;
                    $scope.$parent.gridOptions.data = result.manageContractList;
                    $scope.$parent.colData = result.manageContractList;
                    $scope.$parent.serviceData = result.manageContractList;
                    $scope.$parent.coList = result.manageContractList;
                    $scope.$parent.configGrid();
                    $scope.getCurrentTotalPoints(result.manageContractList);
                    for (var i = 0; i < result.manageContractList.length; i++) {
                        if (result.manageContractList[i].points !== null && result.manageContractList[i].points !== undefined)
                            $scope.topTotalPoints += result.manageContractList[i].points;
                        if (result.manageContractList[i].totalAmountOrder !== null && result.manageContractList[i].totalAmountOrder !== undefined)
                            $scope.topTotalOrderAmount += result.manageContractList[i].totalAmountOrder;
                        if (result.manageContractList[i].totalOutstandingBalance !== null && result.manageContractList[i].totalOutstandingBalance !== undefined)
                            $scope.topOutstandingAmount += result.manageContractList[i].totalOutstandingBalance;
                    }

                }
            }
        }

        function error() {
            console.log("Service not resolved");



        }

    }
    $scope.headings = [{
        title: 'Contract Code',
        width: '9%'
    }, {
        title: 'Order Date',
        width: '9%'
    }, {
        title: 'Customer Name',
        width: '9%'
    }, {
        title: 'Sales Rep',
        width: '9%'
    }, {
        title: 'VR officer',
        width: '9%'
    }, {
        title: 'Collection Officer',
        width: '9%'
    }, {
        title: 'Payment Method',
        width: '9%'
    }, {
        title: 'Points',
        width: '9%'
    }, {
        title: 'Total Amount',
        width: '9%'
    }, {
        title: 'Outstanding Amount',
        width: '9%'
    }, {
        title: 'Status',
        width: '9%'
    }]
    if ($state.current.name === 'SalesRep.ManageContractSales') {
        $scope.$parent.gridOptions.columnDefs = $scope.headings;
        callType = "rejectedOrderSalesDM";
        type = "rejectedOrderSalesDM";
        getServiceCall();
    }

    $scope.exportToCsv=function(e,data){
        	e.preventDefault();

        	var filterData=localStorageService.get('filteredContract');

        	 var selectedColumns = localStorageService.get('selectedColumns');

        	if(filterData!==null){

        		var filterOutData=[];

        		for(var i=0;i<filterData.length;i++){
        		    angular.forEach(filterData[i], function(value, key) {
        			  if(selectedColumns.hasOwnProperty(key)){
        				//  console.log(key);
        			  }else{
        				//  filterData.remove(key);
        				delete filterData[i][key];
        			  }
        			});
        		}

        		commonServices.exportToCsvCommon(filterData, "Contract Report", true);

        	}else{
        		if(data == '')
                    return;
        		for(var i=0;i<data.length;i++){
        		    angular.forEach(data[i], function(value, key) {
        			  if(selectedColumns.hasOwnProperty(key)){
        				  //console.log(key);
        			  }else{
        				//  filterData.remove(key);
        				delete data[i][key];
        			  }
        			});
        		}

        		commonServices.exportToCsvCommon(data, "Contract Report", true);

        		//JSONToCSVConvertor(data, "Contract Report", true);
        	}




        }


}]);