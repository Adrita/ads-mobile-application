var Login = angular.module('Login', []);
Login.controller( "LoginCtrl", ['$scope','$rootScope','$state','authentication','localStorageService','$cookies','$cookieStore','$window','validate','userRole','$base64','Session','adapter','commonServices','$uibModal', function($scope, $rootScope, $state,authentication,localStorageService,$cookies,$cookieStore,$window,validate,userRole,$base64,Session,adapter,commonServices,$uibModal)
{
	$scope.userNameError=true;
	$scope.passwordError=true;
	var padTop=$('header').outerHeight() + $('nav').outerHeight()+"px";
	var userRoleRouteMap = Session.userGlobalRole;
	$('.mainContent').css('padding-top',padTop);
	var appInUse = true;
	if (window.innerWidth < 960)
	 appInUse = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;

	$scope.showLoginLink=!appInUse;
	$scope.unAuth=true;
	if($cookieStore!=null){
		$scope.UserName=$cookieStore.get('loggedUser');
		$scope.Password=$cookieStore.get('password');
		$scope.rememberMe=true;
		$scope.login="Login";
		$scope.loginName=true;
	}
	$scope.myFunc=function()
	{
		if(checkValidations())
		login();
	}
    function userLogout(redirect) {
            var loggedinUser = Session.userGlobalRole;
            if (angular.isDefined(loggedinUser)) {
                authentication.logout().then(function(result) {
                    if (result.errorCode === "200") {
                        Session.destroy();
                        if (redirect) $state.go('Login');
                    }
                });
            }
        }

	
	//Error msg for unauthorised access START
	$rootScope.$on('unauthorisedAccess', function (event, data) {
		if (data) {
			errorMsg("Unauthorised Access");
			$rootScope.$broadcast('unauthorisedAccess', false);
		}
	});
	var unAuthAccess = localStorageService.get("unauthorisedAccess");
	if (angular.isDefined(unAuthAccess) && unAuthAccess === "true") {
		errorMsg("Unauthorised Access");
		localStorageService.set('unauthorisedAccess', false);
		
		//Logout once unauthorised access
		userLogout(true);
	}
	
	function errorMsg(msg) {
		$scope.loginErMsg = msg;
    	$scope.loading = false;
    	$scope.loader = false;
    	$scope.loginName = true;
    	$scope.unAuth = false;
	}
	//Error msg for unauthorised access END	
	
    $scope.LoginAuthentication = function(){login();}
    $scope.loginName=true;
    function login(){
    	$scope.loading=false;
    	$scope.loginName=true;
    	$scope.loader = false;
		$scope.unAuth=true;

		if(Session!==null){

		Session.destroy();
		}

    	if(checkValidations()){

			$scope.loading=true;
			$scope.loginName=false;
    		
			//Addition for Authentication Change
			$scope.EncryptAuthInfo=$base64.encode($scope.UserName+":"+$scope.Password);				
    		config = {		
    			headers: {		
    				'Authorization': $scope.EncryptAuthInfo		
    			}		
            };
    		
    		
    		
    		var req = {
    			    userName : $scope.UserName,
    			    password: $scope.Password
    			}
    		//Remove config if not needed




    			authentication.loginService(req,config).then(success,error);
    			 function success(result){
    				if (result.statusCode === "200") {
    					if($scope.rememberMe){
    						$cookies.loggedUser = $scope.UserName;
    						$cookies.password = $scope.Password;
    						$cookieStore.put('loggedUser',$cookies.loggedUser);
    						$cookieStore.put('password',$cookies.password);
    						localStorageService.set('loggedUser',$scope.UserName);
    						localStorageService.set('password',$scope.Password);
    						
    					} else if ($scope.UserName === $cookieStore.get('loggedUser')) {
    						$cookies.remove('loggedUser');
    						$cookies.remove('password');
    						$cookieStore.remove('loggedUser');
    						$cookieStore.remove('password');
    					}
    					/*if(checkValidations()==true){
    					}*/

//    					 app.sync($scope.UserName,$scope.Password);


    					if(Session.userGlobalRole.loginCount === 1) {
                        						$scope.loader = true;
                        						$scope.loading = false;
                        						$scope.login="Login";
                        						$scope.loginName = true;

                        						$scope.openPasswordModal();
                        					}
                        					else
                        						$state.go(userRole.defaultLangingPage());




    					//$state.go(userRole.defaultLangingPage());

    	 	        } else {



                           errorMsg("Invalid Username or Password");
                           console.log('unauthenticated user');


    	 	        }
    	   	    };
    	   	    function error(){


                                           if(window.innerWidth <960 && appInUse){
                                           //alert("Cordova app");

                                           var result=null;
                                           app.readFromFile($scope.UserName, $scope.Password, callBackFunction,callBackFunctionError);
                                           //result=readFromFile($scope.UserName,$scope.Password);

                                           }
    	   	    }
    	}
    	
	}
	var userSpecifiedRole = {
    			stateName: "",
    			summaryAccess: "N",
    			viewDetailsAccess: "N",
    			editDetailsAccess: "N",
    			draftAccess: "N"
    		};

	function getUserScreenRoleAccess (curState) {

    		userSpecifiedRole.stateName = curState;
    		var isFound = false,
    		userRole = userSpecifiedRole;

    		angular.forEach(userRoleRouteMap.screenAccess, function(value, key) {
    			if (value.stateName === curState) {
    				userRole = value;
    				isFound = true;
    				return true;
    			}
    		});
    		//if (!isFound) {}
    		return userRole;
    	}


    	function callBackFunctionError(message){
    	errorMsg(message);
        console.log('unauthenticated user');

        $scope.$apply();
    	}

	function callBackFunction(resultJson){

	console.log("In Success");
	console.log(JSON.stringify(resultJson));

		//alert("Alert From Callback: "+JSON.stringify(resultJson));
		var result=resultJson;

		var sessionId = result.principal+result.userDetails.userId;

		console.log(sessionId);
		console.log(result.userDetails.userId);

		localStorageService.set('countryId',result.userDetails.countryId);

        Session.create(sessionId, result.userDetails.userId, {});

		//userRoleRouteMap = getUserScreenAllRoleAccess ();
		userRoleRouteMap = result.userDetails;
		Session.createGlobalRoles(userRoleRouteMap);

		//Store user configuration into local storage
		var newConfig=null;
		Session.storeAuthData('configData', newConfig);
		Session.storeAuthData('globalUserRoles', { userInfo: result });

		//app.storeUserRole(result);

		//localStorageService.set('configData', newConfig);
		//localStorageService.set('authorizationData', { userInfo: result });

		//contract view and user view access
		Session.createContractViewAccess(getUserScreenRoleAccess ("AdminBoard.ViewContract"));
		Session.createUserViewAccess(getUserScreenRoleAccess ("AdminBoard.EditCustomer"));

		console.log(userRole.defaultLangingPage());

		$state.go(userRole.defaultLangingPage());

		//$state.go('AdminBoard.ManageContract');
	}
    function checkValidations()
    {
    	$scope.userNameError=validate.validateText($scope.UserName);
    	$scope.passwordError=validate.validateText($scope.Password);
    	/*$scope.loginName=true;
    	$scope.loading=false;*/
    	var result=$scope.userNameError && $scope.passwordError;
    	return  result;
    	
    	/*if(result.checkValidations==true){
        	$scope.loading=true;
         	$scope.loginName=false;
        }*/
    }

      $scope.downloadApp = function (e) {
        	e.preventDefault();
        	adapter.getApkFile('ApkDownloadController/apkDownload');
        }
        $scope.load = function () {
        	commonServices.loginHeight();
        }
        $scope.openPasswordModal = function () {
    	   	  modalInstance = $uibModal.open({
    	      animation: true,
    	      templateUrl: 'app/modals/changePassword/view.html',
    	      controller: 'passwordChangeCtrl',
    	      windowClass: 'password-modal',
    	      size: 'md',
    	      backdrop:'static',
    	      keyboard: false
    	    }).result.then(function(data){
    	    	console.log(data);
    	    });

    }

    
    
}]);