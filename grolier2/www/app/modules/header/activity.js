var Header = angular.module('Header', []);

Header.controller( "HeaderCtrl", ['$scope','$http','$state','localize','adjustHeight','$rootScope','$cookies','$cookieStore','Session', 'authentication', 'userRole' ,function($scope, $http,$state,localize,adjustHeight,$rootScope,$cookies,$cookieStore,Session,authentication,userRole){
	
	$scope.locale = localize.getLocalizedResources();
  
  $scope.mobileNavMenu = false;
  $scope.submenu = false;
  $scope.inActiveSearch = true;
  $scope.HideNavBar = function(subStateType){
    $rootScope.$broadcast('SubStateType', subStateType);    
  }

  $scope.resetMainNav = function(){
	  $rootScope.$broadcast('TrackNavBar', false);
  }

 $scope.$on('HideMobileNav', function (event, arg) { 
    if(arg === true){   $scope.mobileNavMenu = false;}
});
 
$scope.logout = function() {
	authentication.logout().then(function(result) {
		if (result.errorCode === "200") {
			Session.destroy();
			$state.go('Login');			
        }
	});
}

$scope.headerItems = {};
$scope.headerItems.hamburgerMenu = true;
$scope.headerItems.logo = true;
$scope.headerItems.region = true;
$scope.headerItems.notification = true;
$scope.headerItems.otherLinks = false;
$scope.headerItems.accountLinks = true;

var loggedinUser = Session.userGlobalRole;
$scope.headerItems.regionName = "Asia";

if (angular.isDefined(loggedinUser)) {
	
	var country = loggedinUser.countryList;
	var conLen = angular.isDefined(country)?country.length:0;
	if (conLen === 1) $scope.headerItems.regionName = country[0];
	
	$scope.loggedinUserName = loggedinUser.userName;
	if (loggedinUser.roleName === "Admin" || loggedinUser.roleName === "Super Admin")
		$scope.headerItems.otherLinks = true;
	
} else {
	$scope.loggedinUserName = "";
}
$rootScope.regionName = $scope.headerItems.regionName;

//$scope.loggedinUserName = (angular.isDefined(loggedinUser))?loggedinUser.userName:"";
//if (angular.isDefined(loggedinUser) && (loggedinUser.roleName == "Admin" || loggedinUser.roleName == "Super Admin")) $scope.headerItems.otherLinks = true;

 $scope.redirectHome = function() {
	 //$state.go('AdminBoard.ManageUser');
	 $state.go(userRole.getUserHomePage());
 }

$scope.showSubmenu = function(){
     $scope.submenu = true;
}
$scope.hideSubmenu = function(){
     $scope.submenu = false;
}
/*Get data from server*/
/*$http.get("http://localhost:8080/SGIntranetService/api/notifications/")
     .success(function (data) {
         $scope.notifications = data;
});*/
/*$scope.showActiveSearch = function(){
  $scope.inActiveSearch = false;
}
$scope.showInActiveSearch = function(){
  $scope.inActiveSearch = true;
}*/

/*sample notification json*/
var serviceData =  [
            {
                "RequestUser": "Koushik",
                "ContractNo": 885578,
                "RequestType": "Registration",
                "Duration": "1 min ago",
                "url": "distribution/images/Registration.JPG"
            },
            {
                "RequestUser": "Adrita",
                "ContractNo": 808085,
                "RequestType": "Cancellation",
                "Duration": "02 hours ago",
                "url": "distribution/images/Cancellation.JPG"
            },
            {
                "RequestUser": "Sourav",
                "ContractNo": 834859,
                "RequestType": "Return",
                "Duration": "12 hours ago",
                "url": "distribution/images/Return.JPG"
            },
            {
                "RequestUser": "Reya",
                "ContractNo": 869827,
                "RequestType": "Return",
                "Duration": "22 hours ago",
                "url": "distribution/images/Return.JPG"
            },
            {
                "RequestUser": "Debendu",
                "ContractNo": 445523,
                "RequestType": "Registration",
                "Duration": "2 days ago",
                "url": "distribution/images/Registration.JPG"
            },
            {
                "RequestUser": "Debashrita",
                "ContractNo": 885578,
                "RequestType": "Cancellation",
                "Duration": "1 month ago",
                "url": "distribution/images/Cancellation.JPG"
            }
 ];
 $scope.notiLength = serviceData.length;
 $scope.notifications = [serviceData[0],serviceData[1],serviceData[2],serviceData[3]];


$scope.showOtherLink = false;
$scope.showNotiyLink = false;
$scope.showAcntLink = false;

 $scope.toggleOtherlink = function() {
  $scope.showNotiyLink = false;
  $scope.showAcntLink = false;
  $scope.showOtherLink = !$scope.showOtherLink;
 }
  $scope.toggleAcntlink = function() {
    $scope.showOtherLink = false;
    $scope.showNotiyLink = false;
    $scope.showAcntLink = !$scope.showAcntLink;
 }

 $scope.toggleNotiylink = function() {
  $scope.showOtherLink = false;
  $scope.showAcntLink = false;
  $scope.showNotiyLink = !$scope.showNotiyLink;
 }
 $scope.closeDropdown1=function()
 {
	 $("#OtherLinks").dropdown('toggle');
	 $("#dropArrowOtherLinks").attr('src','distribution/images/topnav_arrow.png');
	 
 };
 $scope.closeDropdown2=function()
 {
	 $("#AccountSettings").dropdown('toggle');
	 $("#dropArrowMyProfile").attr('src','distribution/images/topnav_arrow.png');
	 
 };
 
 $('#accountSettings').data('open',false);
 $('#otherLinks').data('open',false);
 $('#accountSettings').click(function(){
	 $('#dropArrowOtherLinks').attr('src','distribution/images/topnav_arrow.png');
	 if( $('#accountSettings').data('open'))
	 {
		 $('#accountSettings').data('open',false);
		 $('#dropArrowMyProfile').attr('src','distribution/images/topnav_arrow.png');
	 }
	 else
	{
		 $('#accountSettings').data('open',true);
		 $('#dropArrowMyProfile').attr('src','distribution/images/top_downArrow.png');
	}
 });
 $('#OtherLinks').click(function(){
	 $('#accountSettings').attr('src','distribution/images/topnav_arrow.png');
	 if( $('#otherLinks').data('open'))
	 {
		 $('#otherLinks').data('open',false);
		 $('#dropArrowOtherLinks').attr('src','distribution/images/topnav_arrow.png');
	 }
	 else
		 {
		 	$('#otherLinks').data('open',true);
		 	$('#dropArrowOtherLinks').attr('src','distribution/images/top_downArrow.png');
		 }
 });
 $(document).click(function(){
	
	 if($('#accountSettings').data('open'))
		 {
		 	$('#accountSettings').data('open',false);
		 	$('#dropArrowMyProfile').attr('src','distribution/images/topnav_arrow.png');
		 }
	 if( $('#otherLinks').data('open'))
	 {
		 $('#otherLinks').data('open',false);
		 $('#dropArrowOtherLinks').attr('src','distribution/images/topnav_arrow.png');
	 }
 });
/* $('#hamburger').data('open',false);
 */
 
/* $('#hamburger').click(function(){ 
	 if($(".navbar-collapse").hasClass("in")){
     $(".mainContent").css("padding-top", "44px");
 }
 else {
     $(".mainContent").css("padding-top", "114px");
 }
 });*/
 
 
 
 
}]);