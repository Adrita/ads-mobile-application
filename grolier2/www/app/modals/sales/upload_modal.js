app.controller( "FileUploadCtrl", ['$scope','$uibModalInstance','$state','localStorageService','$rootScope','adapter',function($scope,$uibModalInstance,$state,localStorageService,$rootScope,adapter)
{
    $scope.picDemo=false;
    var oldVal=null;
    var newVal=null;
    /*var dropbox = document.getElementById("dropbox")
    $scope.dropText = 'Drop files here...'*/
    $('#picDemo').css('display','block');
    $scope.fileChooser=function()
    {
    	$scope.fileName=null;
    	$('#picDemo').css('display','none');
    };
    /*$scope.file_changed = function(element) {
        	alert("Hola");
        	$('#picDemo').css('display','block');
   };*/
   $scope.dropText = 'Drop files here...';
   window.onload = function() {
  var dropbox = document.getElementById("dropbox");
  
   function dragEnterLeave(evt) {
       evt.stopPropagation()
       evt.preventDefault()
       $scope.$apply(function(){
           $scope.dropText = 'Drop files here...'
           $scope.dropClass = ''
       })
   }
   
   dropbox.addEventListener("dragenter", dragEnterLeave, false);
   dropbox.addEventListener("dragleave", dragEnterLeave, false)
   dropbox.addEventListener("dragover", function(evt) {
       evt.stopPropagation()
       evt.preventDefault()
       var clazz = 'not-available'
       var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
       $scope.$apply(function(){
           $scope.dropText = ok ? 'Drop files here...' : 'Only files are allowed!'
           $scope.dropClass = ok ? 'over' : 'not-available'
       })
   }, false);
   dropbox.addEventListener("drop", function(evt) {
       console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
       evt.stopPropagation()
       evt.preventDefault()
       $scope.$apply(function(){
           $scope.dropText = 'Drop files here...'
           $scope.dropClass = ''
       })
       var files = evt.dataTransfer.files
       if (files.length > 0) {
           $scope.$apply(function(){
               $scope.files = []
               for (var i = 0; i < files.length; i++) {
                   $scope.files.push(files[i])
               }
           })
       }
   }, false);
   $scope.setFiles = function(element) {
	   $scope.$apply(function(scope) {
	     console.log('files:', element.files);
	     // Turn the FileList object into an Array
	       $scope.files = []
	       for (var i = 0; i < element.files.length; i++) {
	         $scope.files.push(element.files[i])
	       }
	     $scope.progressVisible = false
	     });
	   };
	   $scope.uploadFile = function() {
	       var fd = new FormData();
	       for (var i in $scope.files) {
	           fd.append("uploadedFile", $scope.files[i])
	       }
	       var xhr = new XMLHttpRequest()
	       xhr.upload.addEventListener("progress", uploadProgress, false)
	       xhr.addEventListener("load", uploadComplete, false)
	       xhr.addEventListener("error", uploadFailed, false)
	       xhr.addEventListener("abort", uploadCanceled, false)
	       xhr.open("POST", "/fileupload")
	       $scope.progressVisible = true
	       xhr.send(fd)
	   }
	   
   
   }
   
   $scope.cancel=function(){
	   $uibModalInstance.dismiss();
	}
   
   $scope.serviceHit=function(){
	   $uibModalInstance.dismiss();
	   $state.go('AdminBoard.NewSalesRep');
   }

   // init event handlers
/*   function dragEnterLeave(evt) {
       evt.stopPropagation()
       evt.preventDefault()
       $scope.$apply(function(){
           $scope.dropText = 'Drop files here...'
           $scope.dropClass = ''
       })
   }*/
  // dropbox.addEventListener("dragenter", dragEnterLeave, false)
   /*dropbox.addEventListener("dragleave", dragEnterLeave, false)
   dropbox.addEventListener("dragover", function(evt) {
       evt.stopPropagation()
       evt.preventDefault()
       var clazz = 'not-available'
       var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
       $scope.$apply(function(){
           $scope.dropText = ok ? 'Drop files here...' : 'Only files are allowed!'
           $scope.dropClass = ok ? 'over' : 'not-available'
       })
   }, false)*/
   /*dropbox.addEventListener("drop", function(evt) {
       console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
       evt.stopPropagation()
       evt.preventDefault()
       $scope.$apply(function(){
           $scope.dropText = 'Drop files here...'
           $scope.dropClass = ''
       })
       var files = evt.dataTransfer.files
       if (files.length > 0) {
           $scope.$apply(function(){
               $scope.files = []
               for (var i = 0; i < files.length; i++) {
                   $scope.files.push(files[i])
               }
           })
       }
   }, false)*/
   //============== DRAG & DROP =============

/*   $scope.setFiles = function(element) {
   $scope.$apply(function(scope) {
     console.log('files:', element.files);
     // Turn the FileList object into an Array
       $scope.files = []
       for (var i = 0; i < element.files.length; i++) {
         $scope.files.push(element.files[i])
       }
     $scope.progressVisible = false
     });
   };*/

  /* $scope.uploadFile = function() {
       var fd = new FormData()
       for (var i in $scope.files) {
           fd.append("uploadedFile", $scope.files[i])
       }
       var xhr = new XMLHttpRequest()
       xhr.upload.addEventListener("progress", uploadProgress, false)
       xhr.addEventListener("load", uploadComplete, false)
       xhr.addEventListener("error", uploadFailed, false)
       xhr.addEventListener("abort", uploadCanceled, false)
       xhr.open("POST", "/fileupload")
       $scope.progressVisible = true
       xhr.send(fd)
   }*/

  /* function uploadProgress(evt) {
       $scope.$apply(function(){
           if (evt.lengthComputable) {
               $scope.progress = Math.round(evt.loaded * 100 / evt.total)
           } else {
               $scope.progress = 'unable to compute'
           }
       })
   }
*/
  /* function uploadComplete(evt) {
        This event is raised when the server send back a response 
       alert(evt.target.responseText)
   }

   function uploadFailed(evt) {
       alert("There was an error attempting to upload the file.")
   }

   function uploadCanceled(evt) {
       $scope.$apply(function(){
           $scope.progressVisible = false
       })
       alert("The upload has been canceled by the user or the browser dropped the connection.")
   }*/
   
   
   
   $scope.uploadFile = function (input) {
	   
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	 
	            //Sets the Old Image to new New Image
	            $('#photo-id').attr('src', e.target.result);
	 
	            //Create a canvas and draw image on Client Side to get the byte[] equivalent
	            var canvas = document.createElement("canvas");
	            var imageElement = document.createElement("img");
	 
	            imageElement.setAttribute('src', e.target.result);
	            canvas.width = imageElement.width;
	            canvas.height = imageElement.height;
	            var context = canvas.getContext("2d");
	            context.drawImage(imageElement, 0, 0);
	            var base64Image = canvas.toDataURL("image/jpeg");
	            console.log(base64Image);
	 
	            //Removes the Data Type Prefix 
	            //And set the view model to the new value
	            $scope.data.UserPhoto = base64Image.replace(/data:image\/jpeg;base64,/g, '');
	        }
	                
	        //Renders Image on Page
	        reader.readAsDataURL(input.files[0]);
	    }
	    
        	/*alert("Hola");*/
        	$('#picDemo').css('display','block');
	    
	};
   
   
}]);
   
 
   
   
