app.controller('deliveryClaimFormCtrl',['$scope','$uibModalInstance','adapter',function ($scope,$uibModalInstance,adapter) {
  
	$scope.locationList=[];
	$scope.suggestionList=[];
	$scope.locationSuggestion=false;
	$scope.voList=[];
	$scope.locationName="";
	$scope.voId="";
	$scope.voSuggestionList=[];
	adapter.getServiceData("ReportController/getLocationDetails",{}).then(success,error);
	adapter.getServiceData("ReportController/getVODetails",{}).then(success,error);
	function success(result){
		if(result.locationLst!==null && result.locationLst!==undefined)
		$scope.locationList=result.locationLst;
		if(result.voMAP!==null && result.voMAP!==undefined)
			{
				$.each(result.voMAP, function(i, val) {
					var obj={
							voId:i,
							voName:val
					}
					$scope.voList.push(obj)
				});
			}
			
			
	}
	function error()
	{
		$uibModalInstance.dismiss();
	}
	$scope.weekList=[];
	for(var i=1;i<=52;i++)
		$scope.weekList.push(i);
  $scope.submitPressed=false;
  $scope.deliveryClaimForm={};
  $scope.close= function () {
	  $scope.submitPressed=true;
	if($scope.deliveryClaimForm.$valid)
		{
			var reqURL="ReportController/getWeeklyDeliverRequest?voName="+$scope.voNAme+"&toWeek="+$scope.endWeek+
			"&fromWeek="+$scope.startWeek+"&locationId="+$scope.locationId+"&year="+$scope.year+"&locationName="+$scope.locationName+"&voCode="+$scope.voId;
			adapter.getReport(reqURL);
			$uibModalInstance.close($scope.claimFormObject);
		}
    
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };
  $scope.getSelectedLocation=function(location)
  {
	  $scope.locationId=location.locationId;
	  $scope.locationName=location.locationName;
	  $scope.location=location.locationName;
	  $scope.locationSuggestion=false;
  }
  $scope.getSuggestion=function()
  {
	  $scope.suggestionList=[];
	  if($scope.location!==null && $scope.location!==undefined && $scope.location!=="" )
		  {
		  		for(var i=0;i<$scope.locationList.length;i++)
		  			{
		  				if($scope.locationList[i].locationName.toLowerCase().indexOf($scope.location.toLowerCase())!==-1)
		  					{
		  						$scope.suggestionList.push($scope.locationList[i]);
		  					}
		  			}
		  		if($scope.suggestionList.length>0)
		  			$scope.locationSuggestion=true;
		  }
  }
  $scope.getVOSuggestionList=function()
  {
	  $scope.voSuggestionList=[];
	  $scope.showVOList=false;
	  if($scope.voNAme!==null && $scope.voNAme!==undefined && $scope.voNAme!=="")
		  {
		  		for(var i=0;i<$scope.voList.length;i++)
		  			{
		  				if($scope.voList[i].voName.toLowerCase().indexOf($scope.voNAme.toLowerCase())!==-1)
		  					$scope.voSuggestionList.push($scope.voList[i]);
		  			}
		  		if($scope.voSuggestionList.length>0)
		  			 $scope.showVOList=true;
		  }
  }
  $scope.getSelectedVO=function(vo)
  {
	  $scope.voId=vo.voId;
	  $scope.voNAme=vo.voName;
	  $scope.showVOList=false;
  }
  
}]);