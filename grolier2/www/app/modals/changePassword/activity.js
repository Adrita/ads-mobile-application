app.controller('passwordChangeCtrl',['$scope','$state','$uibModalInstance','userRole','Session','adapter','localStorageService','$cookies','$cookieStore',function ($scope,$state,$uibModalInstance,userRole,Session,adapter,localStorageService,$cookies,$cookieStore) {
  
	$scope.passWordRules=false;
	$scope.passwordForm={};
	$scope.submitPressed=false;
	$scope.disableSubmit=false;
	//var loginDetails=localStorageService.get('globalUserRoles');
	$scope.changePassword=function(){
		$scope.submitPressed=true;
		if($scope.passwordForm.$valid && $scope.passWordRules)
			{
			$scope.disableSubmit=true;
					var data = {
						userId : Session.userId,
						currentPswd : $scope.currentPswd,
						newPswd : $scope.newPassword,
						repeatPswd : $scope.confirmPassword,
						loginCount : Session.userGlobalRole.loginCount
					};

					adapter.getServiceData(
							"UserRegisterController/changePassword", data)
							.then(success, error);
					function success(result) {
						$uibModalInstance.close();

						localStorageService.set('password',$scope.newPassword);
						$cookies.password = $scope.newPassword;
						$cookieStore.put('password',$cookies.newPassword);

						app.sync(localStorageService.get('loggedUser'),localStorageService.get('password'),successLogin,errorLogin);



					}
					function error() {

					}
			}
		
	}

	function successLogin(response){
		$state.go(userRole.defaultLangingPage());
	}

	function errorLogin(){

	}
	
	$scope.checkPassword = function() {
		$scope.passWordRules=true;
		 if ($scope.newPassword !== "" && $scope.newPassword !==undefined && $scope.newPassword !==null) {
			 if ($scope.newPassword.length >= 6) {
				 $('.caution:eq(0)').css("color",
				 "#85CD00");
				 console
				 .log("Password length color green");
			 } else {
				 $('.caution:eq(0)').css("color", "red");
				 console
				 .log("Password length color red");
				 $scope.passWordRules=false;
			 }

			 if (/\d/.test($scope.newPassword)) {
				 $('.caution:eq(3)').css("color",
				 "#85CD00");
				 console.log("Password has one number");
			 } else {
				 $('.caution:eq(3)').css("color", "red");
				 console
				 .log("Password dont have any number");
				 $scope.passWordRules=false;
			 }

			 if (/[a-z]/g.test($scope.newPassword)) {
				 $('.caution:eq(1)').css("color",
				 "#85CD00");
				 console
				 .log("Password has one lower case");
			 } else {
				 $('.caution:eq(1)').css("color", "red");
				 console
				 .log("Password dont have lower case");
				 $scope.passWordRules=false;
			 }

			 if (/[A-Z]/g.test($scope.newPassword)) {
				 $('.caution:eq(2)').css("color",
				 "#85CD00");
				 console
				 .log("Password has one upper case");
			 } else {
				 $('.caution:eq(2)').css("color", "red");
				 console
				 .log("Password dont have upper case");
				 $scope.passWordRules=false;
			 }

			 if (/(?=.*[!@#\$%\^&\*])/g
					 .test($scope.newPassword)) {
				 $('.caution:eq(4)').css("color",
				 "#85CD00");
				 console
				 .log("Password has one special character");
			 } else {
				 $('.caution:eq(4)').css("color", "red");
				 console
				 .log("Password dont have special character");
				 $scope.passWordRules=false;
			 }

		 } else {
			 $('.caution').css("color", "red");
			 $scope.passWordRules=false;
		 }
	 }
  
}]);