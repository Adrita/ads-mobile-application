app.controller( "modalDraftCtrl", ['$scope','$uibModalInstance','$state','localStorageService',function($scope,$uibModalInstance,$state,localStorageService)
{
	$scope.hidegreenTick=false;
	$scope.hideRedCross=true;
    modalObj = localStorageService.get('modalObj');
    if(modalObj !== null){
    	$scope.modalEntity=modalObj.modalEntity;
    	$scope.modalBtn = modalObj.modalBtnText;
        $scope.modalBody = modalObj.body;
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
      };
      
      $scope.ok = function (str) {
          localStorageService.set('modalObj',null);
          $uibModalInstance.close(str);
        };
    
}]);