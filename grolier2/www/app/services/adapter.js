app.factory('adapter', ['$http', '$q', '$state', 'localize', '$location', 'localStorageService','Session',
	function($http, $q, $state, localize, $location, localStorageService,Session) {
		var currentURL = {};
		var params = "adsstage.sintl.org:8080";
		var baseURL = "http://" + params + "/rest/";
		
		function getMappedUrls() {
			var def = $q.defer();
			var req = {
				method : 'GET',
				url : "distribution/data/service-url-map.json"
			};
			$http(req).success(function(data) {
				def.resolve(data);
			}).error(function() {
				def.reject("Failed to get Localize String");
			});
			return def.promise;
		};
		
		function getServiceData(url, data) {
			var def = $q.defer();
			var reqURL = baseURL + url;
			//var req = { method:'POST', url:reqURL, data:data };

			// Changes for Authentication
			var req = {
				method : 'POST',
				url : reqURL,
				data : data
			};
			var configuration = localStorageService.get('configData');
			if (angular.isDefined(configuration) && configuration !== null
					&& configuration !== "")
				req.headers = configuration.headers;

			$http(req).success(function(data) {
				if (data.errorCode === "402") {
					localStorageService.set('unauthorisedAccess', true);

					var loggedinUser = Session.userGlobalRole;
					if (angular.isDefined(loggedinUser)) Session.destroy();
					$state.go('Login');
				}
				def.resolve(data);
			}).error(function() {
				def.reject("Failed to get Localize String");
			});
			return def.promise;
		};
		
		function getReport(url) {
			var def = $q.defer();
			var reqURL = baseURL + url;

			// Changes for Authentication
			var req = {
				method : 'GET',
				url : reqURL,
				responseType : 'arraybuffer'

			};
			var configuration = localStorageService.get('configData');
			if (angular.isDefined(configuration) && configuration !== null
					&& configuration !== "")
				req.headers = {
					'AccessToken' : configuration.headers.AccessToken,
					'Content-type' : 'application/pdf'
				};

			$http(req).success(function(data) {
				if (data.errorCode === "402") {
					localStorageService.set('unauthorisedAccess', true);

					var loggedinUser = Session.userGlobalRole;
					if (angular.isDefined(loggedinUser)) Session.destroy();
					$state.go('Login');
				} else {
					var file = new Blob([ data ], {
						type : 'application/pdf'
					});
					var fileURL = URL.createObjectURL(file);
					window.open(fileURL);
					saveAs(file, 'filename.pdf');

				}
				def.resolve(data);
			}).error(function() {
			});
			return def.promise;

		};
		
		function getApkFile(url) {
			var def = $q.defer();
			var reqURL = baseURL + url;

			// Changes for Authentication
			var req = {
				method : 'GET',
				url : reqURL,
				responseType : 'arraybuffer'

			};
			var configuration = localStorageService.get('configData');
			if (angular.isDefined(configuration) && configuration !== null
					&& configuration !== "")
				req.headers = {
					'AccessToken' : configuration.headers.AccessToken,
					//'Content-type' : 'application/pdf'
					
					/*'Content-Type: application/vnd.android.package-archive';
						'Content-Disposition: attachment; filename="Grolier.apk"';*/
				};

			$http(req).success(function(data) {
				if (data.errorCode === "402") {
					localStorageService.set('unauthorisedAccess', true);

					var loggedinUser = Session.userGlobalRole;
					if (angular.isDefined(loggedinUser)) Session.destroy();
					$state.go('Login');
				} else {
					var file = new Blob([ data ], {
						//type : 'application/pdf'
					});
					var fileURL = URL.createObjectURL(file);
					saveAs(file, 'Grolier.apk');

				}
				def.resolve(data);
			}).error(function() {
			});
			return def.promise;

		};
		
		function getCurrentUrl(urls, type) {
			if (urls !== undefined) {
				$.each(urls, function(key, val) {
					if (key === $state.current.url) {
						currentURL = val;
						return false;
					}
				});
				if (type !== undefined) {
					$.each(currentURL, function(key, val) {
						if (key === type) {
							currentURL = val;
							return false;
						}
					});
				}
			}
			return currentURL;
		};
		
		function getSideFilterService(url, data) {
			var def = $q.defer();
			var reqURL = baseURL + url;
			/*
			 * var req = { method:'POST', url:reqURL, data:data };
			 */

			// Changes for Authentication
			var req = {
				method : 'POST',
				url : reqURL,
				data : data
			};
			var configuration = localStorageService.get('configData');
			if (angular.isDefined(configuration) && configuration !== null
					&& configuration !== "")
				req.headers = configuration.headers;

			$http(req).success(function(data) {
				def.resolve(data);
			}).error(function() {
				def.reject("Failed to get Localize String");
			});
			return def.promise;
		};
		
		function openPDF(resData, fileName) {
			var ieEDGE = navigator.userAgent.match(/Edge/g);
			var ie = navigator.userAgent.match(/.NET/g); // IE 11+
			var oldIE = navigator.userAgent.match(/MSIE/g);

			var blob = new window.Blob([ resData ], {
				type : 'application/pdf'
			});

			if (ie || oldIE || ieEDGE) {
				window.navigator.msSaveBlob(blob, fileName);
			} else {
				var reader = new window.FileReader();
				reader.onloadend = function() {
					window.location.href = reader.result;
				};
				reader.readAsDataURL(blob);
			}
		};

		return {
			getMappedUrls : getMappedUrls,
			getCurrentUrl : getCurrentUrl,
			getServiceData : getServiceData,
			getSideFilterService : getSideFilterService,
			getReport : getReport,
			getApkFile : getApkFile
		};
	}
]);
