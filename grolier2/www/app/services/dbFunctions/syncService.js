app.factory('syncService', ['$http', '$q','insertData','$timeout','$state', function ($http, $q,insertData,$timeout,$state) {

var sERVICE_ENDPOINT = "http://adsstage.sintl.org:8080/rest/",
	countryUrl = sERVICE_ENDPOINT + "CountryController/manageCountry",
    bankUrl = sERVICE_ENDPOINT + "BankController/manageBank",
    stateUrl = sERVICE_ENDPOINT + "CountryController/getStatesForMobile",
    locationUrl = sERVICE_ENDPOINT + "LocationController/manageLocationForMobile",
    dutyFreePincodeUrl = sERVICE_ENDPOINT + "DutyFreePincodeController/getDutyFreePincodeListForMobile",
    itemDetailsUrl = sERVICE_ENDPOINT + "ItemController/manageItemForMobile",
    priceListUrl = sERVICE_ENDPOINT + "PriceListController/managePriceList",
    lastUpdatedTimeUrl = sERVICE_ENDPOINT + "CheckLastUpdatedTimeController/checkLastUpdatedTimeForMobile",
    verificationRequestSyncUrl = sERVICE_ENDPOINT + "VerificationController/manageAssignedVerificationRequestForMobile",

    createContractUrl = sERVICE_ENDPOINT + "ContractController/createContractForMobile",
    verifyContractUrl = sERVICE_ENDPOINT + "VerificationController/verifyContract",
    verifyPaymentUrl = sERVICE_ENDPOINT + "VerificationController/collectPaymentSubmitForMobile",
    collecPaymentUrl = sERVICE_ENDPOINT + "CollectionController/collectPaymentSubmit",
    saveNoteUrl = sERVICE_ENDPOINT+ "CollectionController/saveNote";

    var anyTableExists=false;

    var tableUrlMap={};

    tableUrlMap[constants.country_table]=countryUrl;
    tableUrlMap[constants.state_table]=stateUrl;
    tableUrlMap[constants.location_table]=locationUrl;
    tableUrlMap[constants.duty_free_zone_table]=dutyFreePincodeUrl;
    tableUrlMap[constants.item_master_table]=itemDetailsUrl;
    tableUrlMap[constants.price_list_master_table]=priceListUrl;
    tableUrlMap[constants.last_updated_time_table]=lastUpdatedTimeUrl;
    tableUrlMap[constants.bank_master_table]=bankUrl;

    var deleteDataOfTables=["last_updated_time_tb"];




var service_url_list=[countryUrl,bankUrl,stateUrl,locationUrl,dutyFreePincodeUrl,itemDetailsUrl,priceListUrl,lastUpdatedTimeUrl];

var syncService={};

var deleteVerificationList=[];

var collectionContractList=[];

var noteDeletionList=[];

var options = { dimBackground: true };

var db_name_list=["ads_country_master_tb","ads_state_master_tb","ads_location_master_tb","ads_duty_free_pincode_tb","ads_item_master_tb","ads_price_list_master_tb","last_updated_time_tb","ads_bank_master_tb"];

var lastUpdatedData={};



syncService.syncAllData=function(data){

for(var i=0;i<service_url_list.length;i++){

	SpinnerPlugin.activityStart("Sync in Progress....", options);

    switch(service_url_list[i]){

        case countryUrl:
                syncService.syncCountryData(data);
                break;

        case locationUrl:
                syncService.syncLocationData(data);
                break;

        case stateUrl:
                syncService.syncStateData(data);
                break;

        case bankUrl:
                syncService.syncBankData(data);
                break;

        case dutyFreePincodeUrl:
                syncService.syncDutyFreePinCode(data);
                break;

        case itemDetailsUrl:
                syncService.syncItemDetails(data);
                break;

        case priceListUrl:
                syncService.syncPriceListData(data);
                break;

        case lastUpdatedTimeUrl:
                syncService.syncLastUpdatedTimeData(data,"type1");
                break;

    }



}


}


syncService.checkLastUpdateTime=function(data){


	var db = window.sqlitePlugin.openDatabase({name: "Grolier.db"});
    		db.transaction(function(tx) {
                        tx.executeSql("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+"last_updated_time_tb"+"'", [], function(tx, rs) {
                          ////console.log('Record length: ' + rs.rows.length);
                          if(rs.rows.length>0){
                          	anyTableExists=true;

                          	syncService.syncLastUpdatedTimeData(data,"type2");

                          }else{
								fileSync.createTable(successDb,errorDb);

                          }
                        }, function(tx, error) {
                          console.log('SELECT error: ' + error.message);

                        });
                      });
}

function successDb(result){

	var reqData={
		userId:1
	}
	syncService.syncAllData(reqData);
}

function errorDb(result){

}





syncService.syncCountryData=function(data){
	var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: countryUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...Country Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

            	if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
            		//alert(JSON.stringify(result));

                    //console.log("countries:::::" + result.countries.length);
                    insertData.insertCountryData(result.countries);


            	}

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncBankData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: bankUrl,
            data: data
        };
        //SpinnerPlugin.activityStart("Sync in Progress...Bank Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));

                  //  console.log("bank:::::" + result.bankList.length);
                    insertData.insertBankData(result.bankList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncStateData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: stateUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...State Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));

                    //console.log("states:::::" + result.stateList.length);
                    insertData.insertStateData(result.stateList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncDutyFreePinCode=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: dutyFreePincodeUrl,
            data: data
        };
        //SpinnerPlugin.activityStart("Sync in Progress...Pincode Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));

                  //  console.log("pincode:::::" + result.dutyFreePincodes.length);
                    insertData.insertPincodeData(result.dutyFreePincodes);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncPriceListData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: priceListUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...Pricelist Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));

                    //console.log("price list:::::" + result.priceList.length);
                    insertData.insertPriceListData(result.priceList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncItemDetails=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: itemDetailsUrl,
            data: data
        };
//SpinnerPlugin.activityStart("Sync in Progress...Item Details", options);

        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode && result.statusCode === 200)) {

                    insertData.insertItemData(result.itemList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncLastUpdatedTimeData=function(data,type){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: lastUpdatedTimeUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {


					if(type==="type1"){
						insertData.insertLastUpdatedRow(result);
						SpinnerPlugin.activityStop();
					}else{
						lastUpdatedData=result;
						fileSync.fetchLastUpdatedRow(successCallLastUpdatedRow,errorCallLastUpdatedRow);
					}




                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

function successCallLastUpdatedRow(result){
	//alert(JSON.stringify(result));

	if(result.length!==0){
		syncService.compareOldToNew(result,lastUpdatedData);
	}else{
		var reqData={
        		userId:1
        	}
		syncService.syncAllData(reqData);
	}


}

function errorCallLastUpdatedRow(result){

}


syncService.compareOldToNew=function(oldData,newData){

for(var i=0;i<oldData.length;i++){

	switch(oldData[i].tableName){

            case constants.country_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastCountryMasterUpdateTime);
                    break;

            case constants.state_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastStateMasterUpdateTime);
                    break;

            case constants.location_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastLocationMasterUpdateTime);
                    break;

            case constants.duty_free_zone_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastDutyFreePincodeUpdateTime);
                    break;

            case constants.item_master_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastItemMasterUpdateTime);
                    break;

            case constants.price_list_master_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastPriceListMasterUpdateTime);
                    syncService.syncAgain();
                    break;

//            case constants.last_updated_time_table:
//                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastCountryMasterUpdateTime);
//                    break;
//
            case constants.bank_master_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastBankMasterUpdateTime);
                    break;

        }


}


}

syncService.syncAgain=function(){
//	if(deleteDataOfTables.length>0){
//		fileSync.deleteTables(deleteDataOfTables,successDeletion,errorDeletion);
//	}

for(var i=0;i<deleteDataOfTables.length;i++){
		fileSync.deleteTables(deleteDataOfTables[i],successDeletion,errorDeletion);
}

}

function errorDeletion(error){
	console.log(error);
}

function successDeletion(result){

deleteDataOfTables=["last_updated_time_tb"];

var data={
	userId:1
};

//for (var i in tableUrlMap){
//		if(tableUrlMap[i]!==null){
			switch(tableUrlMap[result]){

                    case countryUrl:
                            syncService.syncCountryData(data);
                            break;

                    case locationUrl:
                            syncService.syncLocationData(data);
                            break;

                    case stateUrl:
                            syncService.syncStateData(data);
                            break;

                    case bankUrl:
                            syncService.syncBankData(data);
                            break;

                    case dutyFreePincodeUrl:
                            syncService.syncDutyFreePinCode(data);
                            break;

                    case itemDetailsUrl:
                            syncService.syncItemDetails(data);
                            break;

                    case priceListUrl:
                            syncService.syncPriceListData(data);
                            break;

                    case lastUpdatedTimeUrl:
                            syncService.syncLastUpdatedTimeData(data,"type1");
                            break;

                }
		//}
//}


}

syncService.updateTableUrlMap=function(tableName,oldUpdateTime,lastUpdateTime){
	if (oldUpdateTime == "null" && lastUpdateTime == null) {
                // no need to update
                if (tableName in tableUrlMap) {
                    //tableUrlMap.put(tableName, null);
                    tableUrlMap[tableName]=null;
                }
            } else if (oldUpdateTime == "null" && lastUpdateTime != null) {
                // need to update
                deleteTableData(tableName);
            } else if (oldUpdateTime != null && oldUpdateTime != "null" && lastUpdateTime == null) {
                // need to update
                deleteTableData(tableName);
            } else if (oldUpdateTime==lastUpdateTime) {
                // no need to update
                if (tableName in tableUrlMap) {
                    tableUrlMap[tableName]=null;
                }
                console.log("Table = " + tableName + " no update require");
            } else {
                // need to update
                deleteTableData(tableName);
                console.log(tableName + " update require");
            }


}

function deleteTableData(tableName){
	console.log("Delete Table "+tableName);

	deleteDataOfTables.push(tableName);
}

syncService.syncLocationData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            //*url:"http://"+params[2]+"/rest/LoginController/login",*//*
            url: locationUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                  //  console.log("location:::::" + result.locationList.length);
                    insertData.insertLocationData(result.locationList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}


syncService.createContract=function(mobileRowId,data){

    console.log("Create Contract::::"+ JSON.stringify(data));

    SpinnerPlugin.activityStart("Sync in progress.... for draft Contract", options);

var def = $q.defer();

    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: createContractUrl,
            data: data
        };





        $http(req)
            .success(function(result, status, headers, config) {

                var date=new Date();

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {

                    var responseCodeList=result.mobileRowIdResponseCodeList;

                    var mobileRowIdList=[];

                    for(var i=0;i<responseCodeList.length;i++){

                        if(responseCodeList[i].responseCode===200){

                            mobileRowIdList.push(responseCodeList[i].mobileRowId);

                        }
                    }


                    app.deleteContractText(mobileRowIdList);
                    SpinnerPlugin.activityStop();


                    if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                        $state.reload();
                    }


                }else{
                	SpinnerPlugin.activityStop();
                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);
                SpinnerPlugin.activityStop();

            });
        // },60000);
    // }

    


return def.promise;

}


syncService.verifyContract=function(data,index,resultLength){

  //  console.log("Verify contract vo:::::"+JSON.stringify(data));

  SpinnerPlugin.activityStart("Sync in progress....", options);

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: verifyContractUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

               // console.log(result);

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {

                   // app.deleteFromVerifyFile(data.contractId);

                    deleteVerificationList.push(data.contractId);



                    if(index===resultLength-1){

                    SpinnerPlugin.activityStop();
                        app.collectPaymentVo(deleteVerificationList,successCallbackPayment,errorCallbackPayment);

                        deleteVerificationList=[];



                    }

                    if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                                            $state.reload();
                                        }
                   
                   


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                SpinnerPlugin.activityStop();
                def.reject(error);

            });


}


function successCallbackPayment(result){

                        $state.reload();


                        var data={
                            userId:1,
                            verifyContractList:result
                        };

                    
                        syncService.syncPaymentVO(data);
                    
                    

                   }

                   function errorCallbackPayment(message){

                    console.log("Error");
                   }

syncService.syncPaymentVO=function(data){

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

    SpinnerPlugin.activityStart("Sync in progress....", options);

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: verifyPaymentUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

             //   console.log(result);

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                   
                   var responseCodeList=result.contractIdResponseCodeList;
                   var contractIdList=[];

                   for(var i=0;i<responseCodeList.length;i++){

                    if(responseCodeList[i].responseCode===200){
                        contractIdList.push(responseCodeList[i].contractId);
                    }

                   }

                   SpinnerPlugin.activityStop();

                   app.deleteFromVerificationFile(contractIdList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);
                SpinnerPlugin.activityStop();

            });


}


syncService.fetchVerificationData=function(data){

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: verificationRequestSyncUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

              //  console.log(result);

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                   
                   app.saveVerificationRequestData(result);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


}


syncService.collectionContract= function(data,index,arrayLength){

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

    SpinnerPlugin.activityStart("Sync in progress....", options);


        var req = {
            method: 'POST',
            url: collecPaymentUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {

                   // app.deleteCollectionRecord(data.contractId);



                   collectionContractList.push(data.contractId);

                   if(index===arrayLength-1){
                        app.deleteCollectionRecord(collectionContractList,successDeletionCollectionRecord);

                        SpinnerPlugin.activityStop();
                        collectionContractList=[];
                        if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                                                                    $state.reload();
                                                                }

                   }

                   SpinnerPlugin.activityStop();


                }else{
                    app.deleteCollectionRecord(collectionContractList,successDeletionCollectionRecord);
                    collectionContractList=[];
                    SpinnerPlugin.activityStop();
                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);

                app.deleteCollectionRecord(collectionContractList,successDeletionCollectionRecord);

                SpinnerPlugin.activityStop();

                collectionContractList=[];

                def.reject(error);

            });

}


function successDeletionCollectionRecord(result){
    app.syncNoteList(successNote,errorNote);
}

function successNote(result){

    for(var i=0;i<result.length;i++){
        syncService.noteSync(result[i],i,result.length);
    }

        
    }

    function errorNote(result){
        console.log("Error");
    }

syncService.noteSync=function(data,index,arrayLength){

var def = $q.defer();
SpinnerPlugin.activityStart("Sync in progress....", options);
    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            url: saveNoteUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                   
                   //console.log(result);
                   noteDeletionList.push(data.contractId);

                   if(index===arrayLength-1){

                     app.deleteNoteRecord(noteDeletionList);

                     SpinnerPlugin.activityStop();

                     noteDeletionList=[];

                    if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                        $state.reload();
                    }

                   }


                }else{
                    app.deleteNoteRecord(noteDeletionList);
					SpinnerPlugin.activityStop();

                     noteDeletionList=[];
                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);

                app.deleteNoteRecord(noteDeletionList);

                SpinnerPlugin.activityStop();

                noteDeletionList=[];

                console.log(error);
                def.reject(error);

            });


}



syncService.checkState=function(){

    //alert($state.current.name);

}

return syncService;



}])