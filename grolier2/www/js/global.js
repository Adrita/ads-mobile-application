
var ApplicationUserType = ['Sales Team','Collection Officer','Verification Officer','VO Manager','CO Manager','Admin'];
var SalesTeamDesignation = ['DM','DDM','FM','SM','SR'];
var CountryName = ['Singapore','Malaysia','Thailand','Vietnam','China','India','Indonesia','Philipines','Burma','Japan','Srilanka','Brunei'];
var Status = ['Active','Inactive'];
var DivisionCode = ['DIV01','DIV02','DIV03','DIV04','DIV05'];
var RecordsPerPage = [10,20,30];
var PageNo = [1,2,3,4,5];
var x;
var fileterDateRanageArray = ['This Week', 'Last Week', 'This Month', 'Last Month', 'This Quarter', 'Last Quarter', 'This Year', 'Last Year'];
var shortMonthArr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var getFormattedDate = function (dateObj) {
	var date = ("0" + dateObj.getDate()).slice(-2);
	var month = shortMonthArr[dateObj.getMonth()];
	var year = dateObj.getFullYear();
	return date + "-" + month + "-" + year;
}
var currency = "SGD";
Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}