var constants={
	
dbName:"Grolier.db",

dbVersion:"1.0",

cREATE_COUNTRY_MASTER_TB : "CREATE TABLE IF NOT EXISTS  ads_country_master_tb ( country_id INTEGER NOT NULL, country_code TEXT NOT NULL, country_name  TEXT NOT NULL, max_cashup_discount INTEGER, PRIMARY KEY ( country_id))",
cREATE_STATE_MASTER_TB : "CREATE TABLE IF NOT EXISTS ads_state_master_tb ( state_id INTEGER NOT NULL, state_code TEXT NOT NULL,state_name TEXT NOT NULL, pricing_area TEXT, country_id INTEGER NOT NULL, point_conv_rate DOUBLE, price_list_price DOUBLE,PRIMARY KEY ( state_id))",
cREATE_LOCATION_MASTER_TB : "CREATE TABLE IF NOT EXISTS  ads_location_master_tb  (location_id  INTEGER NOT NULL,location_name TEXT NOT NULL, default_location TEXT(1) NOT NULL, PRIMARY KEY (location_id ))",
cREATE_DUTY_FREE_PINCODE_TB : "CREATE TABLE IF NOT EXISTS ads_duty_free_pincode_tb ( id INTEGER NOT NULL, pincode TEXT, duty_free_zone TEXT(1), PRIMARY KEY ( id))",
cREATE_ITEM_MASTER_TB : "CREATE TABLE IF NOT EXISTS ads_item_master_tb ( item_id INTEGER NOT NULL, product_code TEXT, item_desc TEXT,country_id INTEGER, tax_percentg DOUBLE, points DOUBLE, PRIMARY KEY ( item_id ))",
//cREATE_ITEM_DETAILS_MASTER_TB : "CREATE TABLE IF NOT EXISTS ads_item_details_master_tb ( item_id INTEGER NOT NULL, product_code TEXT, country_id INTEGER, tax_percentg DOUBLE, points DOUBLE, PRIMARY KEY (item_id , country_id ))",
cREATE_PRICE_LIST_MASTER_TB : "CREATE TABLE IF NOT EXISTS ads_price_list_master_tb  ( id INTEGER NOT NULL, price_list_code TEXT,country_id INTEGER, points DOUBLE, installment_no TEXT, first_down_pymnt DOUBLE, sec_down_paymnt DOUBLE, installment_amt DOUBLE, total_sale_amt DOUBLE, gst_points_from DOUBLE, gst_points_to DOUBLE, region_name TEXT,PRIMARY KEY ( id))",
cREATE_LAST_UPDATED_TIME_TB : "CREATE TABLE IF NOT EXISTS last_updated_time_tb (rec_id INTEGER PRIMARY KEY AUTOINCREMENT, table_name TEXT, last_update_time TEXT )",
cREATE_BANK_MASTER : "CREATE TABLE IF NOT EXISTS ads_bank_master_tb ( bank_id INTEGER NOT NULL, country_id INTEGER, bank_name TEXT, account_name  TEXT,account_number  TEXT, PRIMARY KEY (bank_id ))",

sERVICE_ENDPOINT : "http://adsstage.sintl.org:8080/rest/",
countryUrl : "http://adsstage.sintl.org:8080/rest/CountryController/manageCountry",

createContractUrl:"http://adsstage.sintl.org:8080/rest/ContractController/createContract",

country_table:"ads_country_master_tb",
state_table:"ads_state_master_tb",
location_table:"ads_location_master_tb",
duty_free_zone_table:"ads_duty_free_pincode_tb",
item_master_table:"ads_item_master_tb",
price_list_master_table:"ads_price_list_master_tb",
last_updated_time_table:"last_updated_time_tb",
bank_master_table:"ads_bank_master_tb"

}