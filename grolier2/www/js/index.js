/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {

    	// document.addEventListener('online', this.onOnline, false);
        // document.addEventListener('offline', this.onOffline, false);
         document.addEventListener('deviceready', this.onDeviceReady, false);


    },

    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {

        //alert("PhoneGap is working in index.js onDeviceReady");

        app.receivedEvent('deviceready');

        

    },

    

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
        app.createDataBase();


    },

    createDataBase: function() {
    //alert("In create database");
        var success = function(message) {
		//alert("Successfully created database");
        }
        var error = function(message) {

        //alert("Error in creating database");

        }
        fileSync.createDataBase(success, error);

    },

    sync: function(userName, password,successCallback,errorCallback) {
        var success = function(response) {
        	successCallback(response);
        };
        var error = function(message) {
        	errorCallback(message);
        };
        fileSync.syncDevice(userName, password, success, error);
    },

    syncLocalData:function(data,success,error){
        var success=function(){};
        var error=function(message){};
        fileSync.syncLocalData(data,success,error);

    },


    readFromFile: function(userName, password, callback, callbackError) {
        var userData = null;
        var success = function(message) {

            userData = message;
            // alert("In Success UserData is >>>"+JSON.stringify(userData));
            callback(userData);
        };
        var error = function(message) {
            callbackError(message)
        };

        fileSync.readFromFile(success, error, userName, password);

    },
    storeUserRole: function(data,successCallback,errorCallback) {
        var success = function(message) {
            successCallback(message);
        };
        var error = function(message) {};
        fileSync.storeUserRole(success, error, data);

    },
    fetchState: function(data, callback) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {

        }
        fileSync.fetchState(success, error, data);
    },

    fetchCountryList: function(data,callback){
        var success=function(message){
        callback(message);
        }

        var error=function(message){

        }

        fileSync.fetchCountryList(success,error,data);

    },

    fetchProduct: function(data, callback) {

        var success = function(message) {
            callback(message);
        }

        var error = function(message) {

        }

        fileSync.fetchProduct(success, error, data);
    },

    fetchPointsTotal:function(data,callback){
        var success= function(message){
            callback(message);
        }
        var error = function(message) {

        }

        fileSync.fetchPointsTotal(success,error,data);
    },

    checkPincode:function(data,callback){
        var success= function(message){
            callback(message);
        }
        var error = function(message) {

        }

        fileSync.checkPincode(success,error,data);
    },

    fetchRegion: function(data,callback){
            var success= function(message){
            callback(message);
        }
        var error = function(message) {

        }

        fileSync.fetchRegion(success,error,data);
    },

    fetchListPriceList: function(data, callback) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {

        }
        fileSync.fetchListPriceList(success, error, data);

    },

    fetchTerms: function(data, callback) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {

        }
        fileSync.fetchTerms(success, error, data);
    },
    fetchPoints: function(data, callback) {
        var success = function(message) {
            callback(message);
        }

        var error = function(message) {

        }

        fileSync.fetchPoints(success, error, data);
    },

    fetchPricingDetails: function(data, callback) {
        var success = function(message) {
            callback(message);
        }

        var error = function(message) {

        }
        fileSync.fetchPricingDetails(success, error, data);

    },

    saveContractData: function(data, callback) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {

        }

        fileSync.saveContractData(success, error, data);

    },
    fetchDraftContract: function(data, callback,callbackError) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {
			callbackError(message);
        }
        fileSync.fetchDraftContract(success, error, data);
    },

    viewContract: function(data, callback) {

        var success = function(message) {
            callback(message);
        }
        var error = function(message) {

        }

        fileSync.viewContract(success, error, data);
    },

    saveVerificationRequestData: function(data) {
        var success = function(message) {}
        var error = function(message) {
            //alert(message);
        }

        fileSync.saveVerificationRequestData(success, error, data);


    },

    fetchVerificationRequest: function(data, callback) {

        var success = function(message) {
            callback(message);
        }

        var error = function(message) {
            alert(message);
        }

        fileSync.fetchVerificationRequest(success, error, data);
    },

    verifyContract: function(data, callback) {

        var success = function(message) {
            callback(message);
        }

        var error = function(message) {
            alert(message);
        }
        fileSync.verifyContract(success, error, data);

    },

    fetchLocation: function(data, callback) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {
            alert(message);
        }
        fileSync.fetchLocation(success, error, data);

    },
    submitPayment: function(data, callback) {
        var success = function(message) {
            callback(message);
        }

        var error = function(message) {
            alert(message);
        }
        fileSync.submitPayment(success, error, data);

    },

    saveCollectionRequest: function(data) {
        var success = function(message) {

        }
        var error = function(message) {
            alert(message);
        }
        fileSync.saveCollectionRequest(success, error, data);

    },

    fetchCollectionRequest: function(data, callback) {
        var success = function(message) {
            callback(message);
        }
        var error = function(message) {
            alert(message);
        }

        fileSync.fetchCollectionRequest(success, error, data);

    },

    fetchBankList:function(data,callback){
        var success=function(message){
            callback(message);
        }
        var error=function(message){
            alert(message);
        }
        fileSync.fetchBankList(success,error,data);
    },

    savePaymentCo: function(data, callback) {
        var success = function(message) {
            callback(message);
        }

        var error = function(message) {
            alert(message);
        }

        fileSync.savePaymentCo(success, error, data);
    },

    saveNote:function(data,callback){

    var success=function(message){
    callback(message);
    }

    var error=function(message){
    alert(message);
    }

    fileSync.saveNote(success,error,data);
    },

    insertCountryRow: function(data){

        

   //    console.log("inserting country");

        fileSync.insertCountryRow(data);

    },
    insertBankRow: function(data){

      //  console.log("inserting bank");

        
        fileSync.insertBankRow(data);

    },
    insertStateRow: function(data){
   // console.log("inserting state");
       
        fileSync.insertStateRow(data);

    },
    insertPincodeRow: function(data){

        
      // console.log("inserting pincode");
        fileSync.insertPincodeRow(data);

    },
    insertPriceListRow:function(data){

       
       // console.log(data.priceCode);

      // console.log("inserting price list");

        fileSync.insertPriceListRow(data);
    },
    insertItemListRow:function(data){

       
       // console.log("inserting item");
        fileSync.insertItemListRow(data);
    },
    insertItemDetailsRow:function(data){

        
        fileSync.insertItemDetailsRow(data);
    },
    insertLastUpdatedRow:function(data){

        
        fileSync.insertLastUpdatedRow(data);
        
    },
    insertLocationListRow:function(data){

        
        fileSync.insertLocationListRow(data);
        
    },
    syncServiceData:function(success,error){

        var successCallback=function(message){
    success(message);
    }

    var errorCallback=function(message){
    error(message);
    }
        fileSync.syncServiceData(successCallback,errorCallback);
    },
    syncVerificationList:function(success,error){
        var successCallback=function(message){
    success(message);
    }

    var errorCallback=function(message){
    error(message);
    }
        fileSync.syncVerificationList(successCallback,errorCallback);

    },

    syncCollectionList:function(success,error){
        var successCallback=function(message){
    success(message);
    }

    var errorCallback=function(message){
    error(message);
    }
        fileSync.syncCollectionList(successCallback,errorCallback);
    },

    syncNoteList:function(success,error){
        var successCallback=function(message){
    success(message);
    }

    var errorCallback=function(message){
    error(message);
    }
        fileSync.syncNoteList(successCallback,errorCallback);

    },

    deleteContractText:function(data){

        fileSync.deleteContractText(data);

    },

    deleteFromVerificationFile:function(data){

        fileSync.deleteFromVerificationFile(data);

    },
    deleteFromVerifyFile:function(data){

        fileSync.deleteFromVerifyFile(data);

    },

    deleteCollectionRecord:function(data,success){

        var successCallback=function(message){
            success(message);
        }

        fileSync.deleteCollectionRecord(data,successCallback);

    },

    collectPaymentVo:function(data,success,error){
        var successCallback=function(message){
    success(message);
    }

    var errorCallback=function(message){
    error(message);
    }
        fileSync.collectPaymentVo(data,successCallback,errorCallback);
    },
    deleteNoteRecord:function(data){
        fileSync.deleteNoteRecord(data);
    },
    readLogFile:function(success,error){
        var successCallback=function(message){
            success(message);
        }

        var errorCallback=function(message){
            error(message);
        }

        fileSync.readLogFile(successCallback,errorCallback);
    }

};