app.controller( "modalCtrl", ['$scope','$uibModalInstance','$state','localStorageService',function($scope,$uibModalInstance,$state,localStorageService)
{
	$scope.hidegreenTick=false;
	$scope.hideRedCross=true;
    modalObj = localStorageService.get('modalObj');
    if(modalObj !== null){
    	$scope.modalEntity=modalObj.modalEntity;
        $scope.modalBtn = modalObj.modalBtnText;
        $scope.modalBody = modalObj.body;
        $scope.modalLink = modalObj.backLink;
    }

    $scope.showModalBtn = false;
    //$scope.btnAlignCenter = "marginLink1";
    if(localStorageService.get('currentRecord')!==null){
        $scope.modalMessge= "Edited Successfully";
        if($state.current.name==="AdminBoard.CreateEditSequenceGenerator" || ($state.current.name==="AdminBoard.ViewContract" && modalObj.modalMessge!==undefined && modalObj.modalMessge!==null)||$state.current.name==="AdminBoard.CreateHouseSales")
        	$scope.modalMessge = modalObj.modalMessge;
       
    }
    else{
    	
        $scope.showModalBtn = true;
        //$scope.btnAlignCenter = "marginLink0";
        $scope.modalMessge= "Created Successfully";
        if($state.current.name==="AdminBoard.CreateEditSequenceGenerator" || ($state.current.name==="AdminBoard.ViewContract" && modalObj.modalMessge!==undefined && modalObj.modalMessge!==null) ||$state.current.name==="AdminBoard.CreateHouseSales")
        	$scope.modalMessge = modalObj.modalMessge;
        	
    }

    if (modalObj.singleBtn!==undefined && modalObj.singleBtn!==null && modalObj.singleBtn) {
    	$scope.showModalBtn = false;
    	$scope.modalMessge = modalObj.modalMessge;    	
    }
    
    if (modalObj.confirmModal!==undefined && modalObj.confirmModal!==null && modalObj.confirmModal) {
    	$scope.showModalBtn = true;
    	$scope.confirmModal = true;
    	$scope.grayBtnText = modalObj.grayBtnText; 
    	$scope.modalMessge = modalObj.modalMessge; 
    }
    
    if (modalObj.modalMessge!==undefined && modalObj.modalMessge!==null && modalObj.modalMessge!=="" && modalObj.modalMessge) {
    	$scope.modalMessge = modalObj.modalMessge;    	
    }
    
    if(localStorageService.get("modalStatus")==="false")
        {
    	$('.modal-content').css('height','196px');
       /* $scope.modalMessge= "Failed";*/
    	/*$scope.modalEntity="Failed";*/
    	$scope.hideRedCross=false;
    	 $scope.modalMessge= "";
        $scope.hidegreenTick=true;
        }
     $scope.ok = function (str) {
       localStorageService.set('modalObj',null);
       $uibModalInstance.close(str);
     };

     $scope.cancel = function () {
       $uibModalInstance.dismiss();
     };
    if($state.current.name==="AdminBoard.CreateEditSequenceGenerator" || $state.current.name==="AdminBoard.CreateEditCommissionRate"){
    	$('.modal-content').css('width','650px');
		$('.modal-content').css('margin-left','-57px');
		$('.modalBtnWidth').removeProp('width');
    	
    };
    if($state.current.name==="AdminBoard.CreateEditCustomer")
    	{
    		$('.modal-content').css('width','550px');
    		$('.modal-content').css('margin-left','27%');
    	}
    
   /* $scope.modalBtnPosition = function (showModalBtn) {
    	if (showModalBtn !== undefined)
    		return (showModalBtn) ? 'marginLink0' : 'marginLink1';
    }; */
    if($state.current.name==="VoBoard.VoReceive" || $state.current.name==="VoBoard.VoBankIn" || $state.current.name==="AdminBoard.AdminReceiveBankIn" || $state.current.name==="AdminBoard.AdminClearBankIn" ||$state.current.name==="CoBoard.CollectorBankIn" || $state.current.name==="VoBoard.ViewVerificationReport")
	   	$scope.suggestionText='';
    else
    	$scope.suggestionText='What do you want to do next?';

}]);