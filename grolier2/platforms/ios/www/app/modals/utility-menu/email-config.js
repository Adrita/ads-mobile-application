(function () {
	'use strict';
	
	app.controller ("emailConfigCtrl", emailConfig);
	emailConfig.$inject = ['$scope', '$uibModalInstance', '$state', 'localStorageService', 'adapter'];
	
	function emailConfig ($scope, $uibModalInstance, $state, localStorageService, adapter) {

		$scope.sendEmail = sendEmail;
		$scope.cancel = dismissModal;
		//$scope.email={};
		$scope.submitPressed=false;
		$scope.customerStatement={};
		
		/////////////
		
		function sendEmail () {
			var data = {
				recipients : $scope.recipient,
				//subject : $scope.subject,
				msg : $scope.message
			},
			url = "SendEmailController/sendEmails";
			adapter.getServiceData(url, data).then(success, error);
			
			function success(result) {
				 if (result.statusCode === 200) {
					 console.log("Sent");
	
				 } else {
					 console.log("Service Not Resolved");
				 }
				 $scope.loading = false;
			 };
			 function error(result) {
				 $scope.loading = false;
				 errorMessage = result;
			 }
			
			$uibModalInstance.close("sendEmail");
		}
		function dismissModal () {
			$uibModalInstance.dismiss();
		}
		
		$scope.close= function () {
			  $scope.submitPressed=true;
			if($scope.emailStatment.$valid)
		    $uibModalInstance.close($scope.recipient);
		};
		
		/*function validateEmails(emails)
		{
		    var res = emails.split(",");
		    for(i = 0; i < res.length; i++)
		        if(!validateEmail(res[i])) return false;
		    return true;
		}*/
		
		//$scope.submitPressed=true;
	}
	
})();