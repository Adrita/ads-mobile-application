var Navigator = angular.module('Navigator', []);
Navigator.controller("NavMenuCtrl", [
		'$rootScope',
		'$scope',
		'$state',
		'localize',
		'adjustHeight',
		'Session',
		'userRole',
		'authentication',
		'$timeout',
		function($rootScope, $scope, $state, localize, adjustHeight, Session, userRole, authentication, $timeout) {
			
			$scope.locale = localize.getLocalizedResources();
			var didScroll;
			var lastScrollTop = 0;
			var delta = 5;

			$scope.ShowNavBar = false;
			$scope.$on('TrackNavBar', function(event, arg) {
				$scope.userRoleSubNav = false;
				$scope.contractSubNav = false;
				$scope.contractVoSubNav = false;
				$scope.ShowCustomersMenu = false;

				angular.forEach($scope.menuIndex, function(value, index) {
					$scope.menuIndex[index] = false;
				});
			});

			$scope.$on('showCollapseMenu', function(event, arg) {
				if (arg === false) {
					$("#main-nav").collapse('hide');
					$timeout(function() {
						$("#hamburger").addClass("collapsed");
					}, 0);
					
				} else {
					$("#main-nav").collapse('show');
					$timeout(function() {
						$("#hamburger").removeClass("collapsed");
					}, 0);
				}
			});

			$scope.userRoleSubNav = false;
			$scope.contractSubNav = false;
			$scope.contractVoSubNav = false;
			$scope.ShowCustomersMenu = false;
			$scope.currentState = $state.current.name;

			$scope.$on('AdminStateTrack', function(event, arg) {
				$scope.currentState = arg;

				// hide button, link, and icons as per defined user role.

			});

			$scope.logout = function() {
				authentication.logout().then(function(result) {
					if (result.errorCode === "200") {
						Session.destroy();
						$state.go('Login');
					}
				});
			}

			$scope.ShowUserRolesMenu = true;
			$scope.userRoleMenu = true;

			$scope.menuIndex = [];
			$scope.ShowSecondNav = function(menuTab, key) {

				// var subNavHeight = 0;
				angular.forEach($scope.menuIndex, function(value, index) {
					if (key !== index) {
						$scope.menuIndex[index] = false;
					} else {
						$scope.menuIndex[index] = !$scope.menuIndex[index];
						// if ($scope.menuIndex[index] && (key === 1 || key ===
						// 2)) subNavHeight = 66;
						// else subNavHeight = 0;
					}
				});
				// if (stateName !== null) $state.go(stateName);
				if (menuTab === 'dashboard') {
					$state.go("SalesRep.Dashboard");
				} else if (menuTab === 'customer') {
					$state.go("AdminBoard.ManageCustomer");
				}

				adjustHeight.adjustTopBar();
			};

			$('.navbar-toggle').on('hidden.bs.collapse', function() {
				// console.log('navbar closed');
				adjustHeight.adjustTopBar();
			});
			$('.navbar-toggle').on('shown.bs.collapse', function() {
				// console.log('navbar opened');
				adjustHeight.adjustTopBar();
			});
			$('.collapse').on('hidden.bs.collapse', function() {
				// console.log('navbar closed');
				$("#hamburger").addClass("collapsed");
				adjustHeight.adjustTopBar();
			});
			$('.collapse').on('shown.bs.collapse', function() {
				// console.log('navbar opened');
				$("#hamburger").removeClass("collapsed");
				adjustHeight.adjustTopBar();
			});

			/** *********Role based access START********** */
			var navDetails = userRole.getNavigation();

			$scope.mainNav = navDetails.mainMenu;
			$scope.menuIndex = navDetails.menuIndex;
			$scope.gridClass = navDetails.gridClass;
			$scope.removeUserLink = navDetails.removeUserLink;

			/** *********Role based access END********** */
		} ]);