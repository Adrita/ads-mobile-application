var SideFilterBar = angular.module('SideFilterBar', []);

SideFilterBar.controller( "SideFilterMenuCtrl", ['$scope','$rootScope','$state','localStorageService','adapter','Session','$timeout',function($scope,$rootScope,$state,localStorageService,adapter,Session,$timeout)
{
	$scope.campaignCodeSuggestionList=false;
	$scope.userTypeIcon=false;
	$scope.salesTeamTypeIcon=true;
	$scope.divisionIcon=true;
	$scope.countryIcon=true;
	$scope.statusIcon=true;
	$scope.coIcon=true;
	$scope.voIcon=true;
	$scope.stateIcon=true;
	$scope.turnInStatusIcon=true;
	$scope.paymentTypeIcon=true;
    $scope.Usertypes = ApplicationUserType;
    $scope.StatusType = Status;
    $scope.StatusTypeTurn = Status;
    $scope.showCampaignCodeFilter=true;
    $scope.showPaymentMethodFilter=false;  
    $scope.ShowSidebarFilter = true;
    $scope.RemoveFilterBar = false;
    $scope.currentPage = $state.current.name;
    $scope.ToggleUserType=true;
    $scope.ShowCountryFilter = false;
    $scope.ShowUserFilter=false;
    $scope.ShowDivisionFilter = false;
    $scope.ShowDivisionList = false;
    $scope.ShowDesignationFilter=false;
    $scope.ToggSalesDesig=true;
    $scope.showDateRanageFilter=false;
    $scope.ToggleCountryName=true;
    $scope.ToggDivision=true;
    $scope.ToggleState=true;
    $scope.showStateFilter=false;
    $scope.ShowStatusFilter=true;
    $scope.ToggleCo=true;
    $scope.showCoFilter=false;
    $scope.ToggleVo=true;
    $scope.showVoFilter=false;
    $scope.ToggleStatus=true;
    $scope.TogglesalesRep=true;
    $scope.showSalesRepFilter = true;
    $scope.showDelinqPeriodFilter=false;
    $scope.ToggleDelinquentPeriod=true;
    $scope.ToggleTurnInStatus=true;
    $scope.ShowTurnInStatusFilter=false;
    $scope.TogglePaymentType=true;
    $scope.showPaymentTypeFilter=false;
    $scope.showShipmentStatusFilter=false;
    $scope.ToggleShipmentStatusFilter=true;
    $scope.showFiscalWeekFilter=false;
    $scope.ToggleFiscalWeekFilter=true;
    $scope.ShowTransIdFilter = false;
    $scope.ToggleTransIdFilter = true;
    $scope.requestTypeFilter=false;
    $scope.toggleRequestType=false;
    $scope.deductionModeFilter=false;
    $scope.deductionModeIcon=false;
    $scope.turnInStatusIcon=true;
    $scope.toggleDeductionMode=false;
    $scope.togglevmCompletedFilter=true;
    $scope.DeductionModeList=['Single','Week','Month'];
    $scope.requestType=['3rd Party Payment','Expense Recovery','Adjustment','Downline Incentive','Expense Transfer'];
    $scope.methodOfPayment=["Cash","Cheque","Credit Card"];
    $scope.ShipmentStatusList=["Delivered","Partially Delivered","Pending Shipment"];
    $scope.vmFilters=["Completed","Verified","Assigned"];
    $scope.cmFilters=["Completed","Collected","Uncollected","Partially_Collected","Reassigned"];
    $scope.cmCompletedFilter=false;
    $scope.vmCompletedFilter=false;
    $scope.cmCompletedFilterIcon=true;
    $scope.togglecmCompletedFilter=true;
    var filterObj=[];
    localStorageService.set('filteredContract',null);

    (function showFilterType(){
      clearAllFilters();
        switch($state.current.name){
         /* case 'AdminBoard.ManageBank':{
            $scope.ShowUserFilter = false;
            $scope.ShowDivisionFilter = false;
            $scope.searchBoxField = "Bank";
            $scope.boxPlaceHolder = "Enter Bank Code,Name";
            break;
          }*/
          case 'AdminBoard.ManageRole':{
            $scope.searchBoxField = "Role";
            $scope.boxPlaceHolder = "Enter Role";
            $scope.ShowCountryFilter = false;
            $scope.ShowUserFilter=true;
            $scope.ShowDivisionFilter = false;
            $scope.ShowDivisionList = false;
            $scope.showCampaignCodeFilter=false;
            $scope.ShowDesignationFilter=true;
            $scope.showSalesRepFilter=false;
       	 	$scope.TogglesalesRep = false;
       	 	$scope.showPaymentMethodFilter=false;
            break;
          }
          case 'AdminBoard.ManageUser':{
        	  $scope.ShowCountryFilter = true;
        	  $scope.ShowUserFilter=true;
        	  $scope.ShowDivisionFilter = true;
        	  $scope.ShowDivisionList = true;
        	  $scope.ShowDesignationFilter=true;
        	  $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
         	  $scope.searchBoxField = "User";
         	  $scope.boxPlaceHolder = "Enter Name, IC";
            break;
          }
          case 'AdminBoard.ManageCountry':{
              $scope.searchBoxField = "Country";
              $scope.boxPlaceHolder = "Enter Country Code,State";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=false;
              $scope.ShowDivisionFilter = false;
              $scope.ShowDivisionList = false;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
          case 'AdminBoard.ManageLocation':{
              $scope.searchBoxField = "Location";
              $scope.boxPlaceHolder = "Enter Location Ext. Id,State Code,State";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=false;
              $scope.ShowDivisionFilter =false;
              $scope.ShowDivisionList = false;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
          case 'AdminBoard.ManageSequenceGenerator':{
              $scope.searchBoxField = "Sequence";
              $scope.boxPlaceHolder = "Enter Data Element,Code";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=false;
              $scope.ShowDivisionFilter =false;
              $scope.ShowDivisionList = false;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
          case 'AdminBoard.SRRegRequest':{
              $scope.RemoveFilterBar = true;
              break;
            }
          case 'AdminBoard.ManagePriceList':{
              $scope.searchBoxField = "Price list";
              $scope.boxPlaceHolder = "Enter Price list Code";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=false;
              $scope.ShowDivisionFilter = false;
              $scope.ShowDivisionList = false;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
          case 'AdminBoard.ManageItem':{
              $scope.searchBoxField = "Product";
              $scope.boxPlaceHolder = "Enter Product Name,Product Code";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=false;
              $scope.ShowDivisionFilter = false;
              $scope.ShowItemFilter = false;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
            /*case 'AdminBoard.ManageRegion':{
              $scope.searchBoxField = "Region";
              $scope.boxPlaceHolder = "Enter Region Name,Region Code";
              break;
            }*/
            case 'AdminBoard.ManageDivision':{
              $scope.searchBoxField = "Division";
              $scope.boxPlaceHolder = "Enter Division Name,Division Code";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=false;
              $scope.ShowDivisionFilter = true;
              $scope.ShowDivisionList = true;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
             case 'AdminBoard.ManageCommissionRate':{
            	 $scope.ShowCountryFilter = true;
            	 $scope.ShowUserFilter=true;
            	 $scope.ShowDivisionFilter = false;
            	 $scope.ShowDesignationFilter=false;
            	 $scope.showSalesRepFilter=false;
            	 $scope.TogglesalesRep = false;
            	 $scope.showCampaignCodeFilter=false;
            	 $scope.showPaymentMethodFilter=false;
                 $scope.searchBoxField = "Commission Rate";
                 $scope.boxPlaceHolder = "Enter Commission Rate Name,Code";
              break;
            }
             case 'AdminBoard.ManageScreenAccess':{
            	 $scope.ShowCountryFilter = false;
            	 $scope.ShowUserFilter=false;
            	 $scope.ShowDivisionFilter = false;
            	 $scope.ShowDesignationFilter=false;
            	 $scope.showSalesRepFilter=false;
            	 $scope.TogglesalesRep = false;
            	 $scope.showCampaignCodeFilter=false;
            	 $scope.showPaymentMethodFilter=false;
            	 $scope.searchBoxField = "Screen";
            	 $scope.boxPlaceHolder = "Enter Screen Name,Screen Code";
              break;
            }
            case 'AdminBoard.ManageUserType':{
              $scope.searchBoxField = "User Type";
              $scope.boxPlaceHolder = "Enter User Type Name,Code";
              $scope.ShowCountryFilter = true;
              $scope.ShowUserFilter=true;
              $scope.ShowDivisionFilter = false;
              $scope.ShowDesignationFilter=false;
              $scope.showSalesRepFilter=false;
         	  $scope.TogglesalesRep = false;
         	  $scope.showCampaignCodeFilter=false;
         	  $scope.showPaymentMethodFilter=false;
              break;
            }
            case 'AdminBoard.ManageContract':{
            	$scope.ShowDivisionFilter = false;
            	$scope.ShowDesignationFilter=false;
            	$scope.showCampaignCodeFilter=true;
            	$scope.showPaymentMethodFilter=true;
            	$scope.showShipmentStatusFilter=true;
                $scope.searchBoxField = "Search";
                $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
                break;
            }
            case 'SalesRep.ManageContractSales':{
            	$scope.ShowDivisionFilter = false;
            	$scope.ShowDesignationFilter=false;
            	$scope.showCampaignCodeFilter=true;
            	$scope.showPaymentMethodFilter=true;
            	//$scope.TogglePaymentMethod=true;
            	$scope.showPaymentMethodFilter=true;
                $scope.searchBoxField = "Search";
                $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
                break;
            }
            case 'VoBoard.ManageVoManagerVerification':{
                $scope.searchBoxField = "Verification Request";
                $scope.boxPlaceHolder = "Enter Contract code,Cust. Name";
                $scope.showStateFilter=true;
                $scope.ShowStatusFilter=false;
                $scope.ToggleState=true;
                $scope.ToggleVo=true;
           	 	$scope.showVoFilter=true;
           	 	$scope.showSalesRepFilter=false;
           	 	$scope.TogglesalesRep = false;
           	 	$scope.showCampaignCodeFilter=false;
           	 	$scope.showShipmentStatusFilter=true;
           	 	$scope.showPaymentMethodFilter=false;
           	 	$scope.ShowStatusFilter=true;
           	 	$scope.showDateRanageFilter = true;
           	 	$scope.showShipmentStatusFilter=true;
           	 	$scope.vmCompletedFilter=true;
                break;
              }
            case 'VoBoard.ManageVoCompleteRequest':{
                $scope.searchBoxField = "Verification Request";
                $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
                $scope.showStateFilter=false;
                $scope.ShowStatusFilter=true;
                $scope.ToggleState=false;
                $scope.ToggleVo=true;
           	 	$scope.showVoFilter=true;
           	 	$scope.showSalesRepFilter=false;
           	 	$scope.TogglesalesRep = false;
           	 	$scope.showCampaignCodeFilter=false;
           	 	$scope.showShipmentStatusFilter=true;
           	 	$scope.showPaymentMethodFilter=false;
           	 	$scope.ShowStatusFilter=true;
        	 	$scope.showDateRanageFilter = true;
        	 	$scope.showShipmentStatusFilter=true;
        	 	$scope.vmCompletedFilter=true;
                break;
              }
            case 'VoBoard.ManageVoPriceDifference':{
                $scope.searchBoxField = "Search";
                $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
                $scope.showStateFilter=true;
                $scope.ShowStatusFilter=false;
                $scope.ToggleState=true;
                $scope.showSalesRepFilter=true;
                $scope.TogglesalesRep = true;
                $scope.showCampaignCodeFilter=false;
                $scope.showPaymentMethodFilter=false;

                break;
              }
            case 'VoBoard.VoVerificationRequest':{
                $scope.searchBoxField = "Verification Request";
                $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
                $scope.showStateFilter=true;
                $scope.ShowStatusFilter=false;
                $scope.ToggleState=true;
                $scope.showSalesRepFilter=false;
                $scope.TogglesalesRep = false;
                $scope.showCampaignCodeFilter=false;
                $scope.showPaymentMethodFilter=false;

                break;
              }
            case 'VoBoard.VoCompleteRequest':{
                $scope.searchBoxField = "Verification Request";
                $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
                $scope.showStateFilter=true;
                $scope.ShowStatusFilter=true;
                $scope.ToggleState=true;
                $scope.showSalesRepFilter=false;
                $scope.TogglesalesRep = false;
                $scope.showCampaignCodeFilter=false;
                $scope.showShipmentStatusFilter=true;
                $scope.showPaymentMethodFilter=false;
                $scope.vmCompletedFilter=true;

                break;
              }
            case 'CoBoard.CollectionContracts':{
            	 $scope.searchBoxField = "Collection Request";
                 $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
            	 $scope.showStateFilter=true;
            	 $scope.ToggleState=true;
            	 $scope.ShowStatusFilter=false;
            	 $scope.ToggleCo=true;
            	 $scope.showCoFilter=true;
            	 $scope.showSalesRepFilter=false;
               	 $scope.TogglesalesRep = false;
               	 $scope.showCampaignCodeFilter=false;
               	 $scope.showPaymentMethodFilter=false;
               	 $scope.ShowStatusFilter=true;
          	 	 $scope.showDateRanageFilter = true;
          	     $scope.showShipmentStatusFilter=true;
          	     $scope.cmCompletedFilter=true;
            	 break;

            }
            case 'CoBoard.ManageCollectorCollectionRequest':{
           	 $scope.searchBoxField = "Collection Request";
             $scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
           	 $scope.showStateFilter=true;
           	 $scope.ToggleState=true;
           	 $scope.ShowStatusFilter=false;
           	 $scope.showSalesRepFilter=false;
           	 $scope.TogglesalesRep = false;
           	 $scope.showDelinqPeriodFilter=true;
             $scope.ToggleDelinquentPeriod=true;
             $scope.showCampaignCodeFilter=false;
             $scope.showPaymentMethodFilter=false;
           	 break;

           }
            case 'CoBoard.CompletedCollectionContracts':{
            		$scope.searchBoxField = "Collection Request";
            		$scope.boxPlaceHolder = "Enter Contract Code,Cust. Name";
            		$scope.showStateFilter=true;
            		$scope.ToggleState=true;
            		$scope.ShowStatusFilter=false;
            		$scope.ToggleCo=true;
               	 	$scope.showCoFilter=true;
               	    $scope.showSalesRepFilter=false;
               	    $scope.TogglesalesRep = false;
               	    $scope.showCampaignCodeFilter=false;
               	    $scope.showPaymentMethodFilter=false;
               	    $scope.ShowStatusFilter=true;
          	 	 	$scope.showDateRanageFilter = true;
          	     	$scope.showShipmentStatusFilter=true;
          	     	$scope.cmCompletedFilter=true;
            		break;
            }
            case 'AdminBoard.ManageCustomer':{
            	$scope.searchBoxField = "Customer";
        		$scope.boxPlaceHolder = "Enter Customer ID,Cust. Name";
        		$scope.ShowCountryFilter = true;
        		$scope.showStateFilter = true;
        		$scope.ShowStatusFilter=false;
        		$scope.showCampaignCodeFilter=false;
        		$scope.showPaymentMethodFilter=false;
            	break;
            }
            case 'VoBoard.VoTurnIn':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter Customer ID,Cust. Name";
        		$scope.StatusTypeTurn=["Pending-For-BankIn","Banked-In By VO","Cleared By CO"];
        		$scope.ShowStatusFilter=false;
        		$scope.ShowTurnInStatusFilter=true;
        		$scope.showPaymentTypeFilter=true;
        		$scope.showCampaignCodeFilter=false;
        		$scope.showPaymentMethodFilter=false;
        		break;
            }
            case 'CoBoard.CollectionTurnIn':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter Customer Code,Cust. Name";
        		$scope.ShowStatusFilter=false;
        		$scope.ShowTurnInStatusFilter=true;
        		$scope.showPaymentTypeFilter=true;
        		$scope.showCampaignCodeFilter=false;
        		$scope.showPaymentMethodFilter=false;
        		break;
            }
            case 'AdminBoard.AdminTurnIn':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter Customer Code,Cust. Name";
        		$scope.ShowStatusFilter=false;
        		$scope.ShowTurnInStatusFilter=true;
        		$scope.showPaymentTypeFilter=true;
        		$scope.showCampaignCodeFilter=false;
        		$scope.showPaymentMethodFilter=false;
        		break;
            }
            case 'SalesRep.ManageTurnIn':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter Customer Code,Cust. Name";
        		$scope.ShowStatusFilter=false;
        		$scope.ShowTurnInStatusFilter=false;
        		$scope.showPaymentTypeFilter=true;
        		$scope.showCampaignCodeFilter=false;
        		$scope.showPaymentMethodFilter=false;
        		break;
            }
            case 'AdminBoard.HouseSales':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Search";
        		$scope.ShowStatusFilter = false;
        		$scope.ShowCountryFilter = true;
        		$scope.showFiscalWeekFilter = true;
        		break;
            }
            case 'AdminBoard.ManageCommissionDetails':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter sales rep name, code, desc...";
        		$scope.ShowStatusFilter = true;
        		$scope.ToggleStatus = true;
        		$scope.ShowCountryFilter = false;
        		$scope.showFiscalWeekFilter = true;
        		$scope.showSalesRepFilter=true;
                $scope.TogglesalesRep = true;
                $scope.ShowTransIdFilter = false;
        		break;
            }
            case 'AdminBoard.ManageAdvanceRequests':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter sales rep name, code,desc...";
        		$scope.ShowStatusFilter = true;
        		$scope.ShowCountryFilter = false;
        		$scope.showFiscalWeekFilter = true;
        		$scope.requestTypeFilter=true;
        		$scope.toggleRequestType=true;
        		$scope.StatusType=['Pending Approval','Approved','Rejected'];
        		$scope.deductionModeFilter=true;
        		$scope.toggleDeductionMode=true;
        		break;
            }
            case 'AdminBoard.ManageAdvanceRequestsAdmin':{
            	$scope.searchBoxField = "Search";
        		$scope.boxPlaceHolder = "Enter sales rep name, code,desc...";
        		$scope.ShowStatusFilter = true;
        		$scope.ShowCountryFilter = false;
        		$scope.showFiscalWeekFilter = true;
        		$scope.requestTypeFilter=true;
        		$scope.toggleRequestType=true;
        		$scope.StatusType=['Pending Approval','Approved','Rejected'];
        		$scope.deductionModeFilter=true;
        		$scope.toggleDeductionMode=true;
        		break;
            }

            default:
            	{
            		$scope.ShowCountryFilter = false;
            		$scope.ShowUserFilter=false;
            		$scope.ShowDivisionFilter = false;
            		$scope.showCampaignCodeFilter=false;
            		$scope.showPaymentMethodFilter=false;
            		$scope.showFiscalWeekFilter = false;
            		break;
            	}
        }
    }());

    function clearAllFilters(){

      $scope.ShowBankFilter = false;
      $scope.ShowRoleFilter = false;
      $scope.ShowUserFilter = false;
      $scope.ShowDivisionFilter = false;
      $scope.ShowCountryFilter = false;
      $scope.ShowLocationFilter = false;
      $scope.ShowSequenceFilter = false;
      $scope.ShowItemFilter = false;
      $scope.ShowDivisionList = false;
      $scope.showSalesRepFilter = false;
      $scope.showStateFilter = false;
      $scope.showCoFilter = false;
      $scope.showVoFilter = false;
      $scope.showCampaignCodeFilter=false;
      $scope.showPaymentMethodFilter=false;
      $scope.showShipmentStatusFilter=false;
      $scope.showFiscalWeekFilter = false;
      $scope.fiscalDate=null;
    }


    if(window.innerWidth < 960){
    	$scope.ShowSidebarFilter = false;
    }

     $scope.HideFilterBar = function(){
      if(window.innerWidth < 960) {
    	  $(".sub-container").css({"min-height": "750px"});
    	  $(".container-xs").css({"min-height": "400px"});
      }

      $scope.ShowSidebarFilter = false;
      $rootScope.$broadcast('ModifiedGridWidth', "col-xs-10 grid_custom_width desktop hidden-xs filter-off");
      $rootScope.$broadcast('ModifiedMobileWidth', "col-xs-12 visible-xs container-xs manage-xs filter-off");

      $rootScope.$broadcast('SidebarWidth', "filter-off");
    };


    $scope.ShowFilterBar = function(){
      if (window.innerWidth < 960) {
    	  setTimeout(function() {
        	  var sidebarHeight = $(".side_bar").outerHeight(true);
        	  $(".sub-container, .container-xs").css({"min-height": sidebarHeight});
      	  }, 5);
      }

      $scope.ShowSidebarFilter = true;
      $rootScope.$broadcast('ModifiedGridWidth', "col-xs-10 desktop hidden-xs filter-on");

      $rootScope.$broadcast('SidebarWidth', "filter-on");

      if(window.innerWidth < 960){
        $rootScope.$broadcast('ModifiedGridWidth',"col-xs-12 desktop hidden-xs filter-on");
      }
      if(window.innerWidth < 768){
        $rootScope.$broadcast('ModifiedMobileWidth',"col-xs-12 visible-xs container-xs manage-xs filter-on");
      }

      //$state.reload();
    };


    filterService();
       /*service call*/
    function filterService() {
      var data = {userId:Session.userId}
      /*adapter.getSideFilterService('FetchListController/getRoleList',data).then(function(result) {
        $scope.SalesTeamDesignations = result.roleNameList;
        localStorageService.set("roleListSideBar",$scope.SalesTeamDesignations);
       });*/
      if($scope.ShowCountryFilter)
       adapter.getSideFilterService('FetchListController/getCountryList',data).then(function(result) {
        $scope.CountryNames = result.countryNameList;
        localStorageService.set("countryListSideBar",$scope.CountryNames);


       });
      if($scope.ShowDivisionFilter)
        adapter.getSideFilterService('FetchListController/getDivisionList',data).then(function(result) {
         $scope.DivisionCodes = result.divisionNameList;
         localStorageService.set("divisionListSideBar",$scope.DivisionCodes);


       });
      	if($scope.ShowUserFilter)
        adapter.getSideFilterService('FetchListController/getUserTypeList',data).then(function(result) {
            $scope.UserTypeList = result.userTypeList;
            var desg=[];
            //console.log($scope.UserTypeList);
            var userTypeNameList=[];
            if ($scope.UserTypeList !== null/* || $scope.UserTypeList !== "" || $scope.UserTypeList !== undefined*/) {
	            for(var i=0;i<$scope.UserTypeList.length;i++){
	            	if(userTypeNameList.indexOf($scope.UserTypeList[i].userType)===-1)
	            	userTypeNameList.push($scope.UserTypeList[i].userType);

	            	if($scope.UserTypeList[i].desgList!==null && $scope.UserTypeList[i].desgList.length!==0)
        			{
        				for(var j=0;j<$scope.UserTypeList[i].desgList.length;j++)
        					if(desg.indexOf($scope.UserTypeList[i].desgList[j].desgName)===-1)
        					desg.push($scope.UserTypeList[i].desgList[j].desgName);
        			}
	            }
            }

            $scope.SalesTeamDesignations =desg;
            $scope.userTypeLists=userTypeNameList;
            localStorageService.set("userTypeListSideBar",$scope.UserTypeList);


          });
        if($state.current.name === 'VoBoard.VoTurnIn' || $state.current.name === 'CoBoard.CollectionTurnIn' || $state.current.name === 'AdminBoard.AdminTurnIn'){
        	generateTurnInStatus();
        }
        if($state.current.name === 'AdminBoard.ManageContract'||$state.current.name === 'SalesRep.ManageContractSales' || $state.current.name ==='VoBoard.VoCompleteRequest' || $state.current.name ==='VoBoard.VoVerificationRequest' || $state.current.name ==='VoBoard.ManageVoManagerVerification' || $state.current.name==='CoBoard.CollectionContracts' || $state.current.name==='VoBoard.ManageVoCompleteRequest' || $state.current.name==='CoBoard.CompletedCollectionContracts'){
        	generateDateRanage();
        	generateContractStatus();
        }
        if($scope.showStateFilter)
        	{
        	adapter.getSideFilterService('FetchListController/getStateList',data).then(function(result) {
        		$scope.stateList = result.stateNameList;



              });
        	}
        if($scope.showCoFilter)
        	{
        		adapter.getSideFilterService('FetchListController/getCoList',data).then(function(result) {
        			$scope.collectionOfficerList = result.coNameList;


        		});

        	}
        if($scope.showVoFilter)
    	{
    		adapter.getSideFilterService('FetchListController/getVoList',data).then(function(result) {
    			$scope.voList = result.voNameList;

    		});
    	}
        if($state.current.name === 'VoBoard.ManageVoCompleteRequest')
        	{

        			generateContractStatus();

        	}
        if($state.current.name === 'VoBoard.VoCompleteRequest')
    	{

    			generateContractStatus();

    	}
        if($state.current.name === 'CoBoard.CollectionContracts')
    	{

    			generateContractStatus();

    	}
        if($state.current.name === 'CoBoard.ManageCollectorCollectionRequest')
    	{

        	generateCollectionPeriod();

    	}
        if($state.current.name === 'AdminBoard.ManageContract' || $state.current.name ==='SalesRep.ManageContractSales')
        {
        	adapter.getSideFilterService('FetchListController/getCampaignCodeList',data).then(function(result) {
    			$scope.campaignCodeList = result.campaignCodeList;

    		});

        }

        if($state.current.name === 'AdminBoard.ManageContract' || $state.current.name ==='SalesRep.ManageContractSales')
        {
        	adapter.getSideFilterService('FetchListController/getPaymentList',data).then(function(result) {
    			$scope.paymentList = result.paymentList;

    		});

        }
        if($state.current.name === 'VoBoard.ManageVoPriceDifference' || $state.current.name === 'AdminBoard.ManageCommissionDetails')
    	{

        	adapter.getSideFilterService('FetchListController/getSalesRepList',data).then(function(result) {
    			$scope.salesList = result.salesNameList;
    		});
    	}

        if($state.current.name === 'AdminBoard.ManageCommissionDetails') {
        	$scope.StatusType = ['Payment Pending', 'Payment Released'];
    	}

    };




    function generateContractStatus() {
								    	$scope.StatusType = [
										'Pending VO Allocation',
										'Pending Verification',
										'Cancellation Initiated',
										'Cancelled',
										'Pending Collection',
										'Pending CO Allocation',
										'Closed',
										'Confirmed',
										'Rejected',
										'DM Rejected',
										'Cancellation Approved for Product Reprocessed',
										'Product Reprocessed' ];
    }

    function generateTurnInStatus() {
    	$scope.StatusTypeTurn = ['Banked In By VO', 'Banked In By CO', 'Banked In By SR','Banked In By Admin','With CO','With VO','With Admin'];
    }
    function generateDateRanage() {
    	$scope.showDateRanageFilter = true;
    	$scope.dateRanageArray = fileterDateRanageArray; //global js
    }

    function generateCollectionPeriod() {
    	$scope.DelinquentPeriodType = ['9 Months', '8 Months', '7 Months', '6 Months', '5 Months', '4 Months', '3 Months', '2 Months','1 Months','Non Delinquent'];
    }

    //Reset side bar
	$scope.resetSidebarFilter = function () {
		$rootScope.$broadcast('filterObj', null);

		$scope.searchedText = '';
		$scope.searchedCountry = '';
		$scope.searchedCampaignCode = '';
		$scope.transId = '';
		$scope.fiscalDate=undefined;
		$("input[name='usertype']").attr('checked', false);
		$("input[name='salesrepdesig']").attr('checked', false);
		$("input[name='divisioncode']").attr('checked', false);
		$("input[name='country']").attr('checked', false);
		$("input[name='status']").attr('checked', false);
		$("input[name='paymentType']").attr('checked', false);
		$("input[name='cashCheck']").attr('checked', false);
		$("input[name='creditCheck']").attr('checked', false);
		$("input[name='chequeCheck']").attr('checked', false);
		$("input[name='campaignCode']").attr('checked', false);
		$("input[name='dateRanage']").attr('checked', false);
		$("input[name='turnInSttausFilter']").attr('checked', false);
		$("input[name='vmFilter']").attr('checked', false);
		$("input[name='cmFilter']").attr('checked', false);
		$("input[name='salesRep']").attr('checked', false);
		$("input[name='state']").attr('checked', false);
        $("input[name='DelinquentPeriod']").attr('checked',false);
		//$('#dropdownCampaign').css('display','none');
		$scope.campaignCodeSuggestionList=false;
	}

    $scope.filterGrid = function(type) {
      if(type === 'reset') {
    	localStorageService.set('filterObj',null);
        $rootScope.$broadcast('filterObj', null);

      }
      else {
	      var selecteduser = [];
	      var selectedsalesrep = [];
	      var selecteddivision = [];
	      var selectedcountry = [];
	      var selectedstatus = [];
	      var selectedpaymentType = [];
	      var selectedcampaignCode = [];
	      var selecteddateRanage = [];
	      var selectedState=[];
	      var selectedCO=[];
	      var statusListCo=[];
	      var voList=[];
	      var campaignCodeList=[];
	      var selectedSalesRep=[];
	      var turnInStatus=[];
	      var shipmentStatus=[];
	      var requestType=[];
	      var deductionTypeList=[];
	      var delinquentMonths=[];
	      var vmFilter=[];
	      var cmFilter=[];
	      var weekNr;
	      var year;


	      $.each($("input[name='usertype']:checked"), function(){
	          selecteduser.push($(this).val());
	      });
	      $.each($("input[name='requestType']:checked"), function(){
	    	  requestType.push($(this).val());
	      });
	      $.each($("input[name='deductionType']:checked"), function(){
	    	  deductionTypeList.push($(this).val());
	      });
	      $.each($("input[name='salesrepdesig']:checked"), function(){
	          selectedsalesrep.push($(this).val());
	      });
	      $.each($("input[name='divisioncode']:checked"), function(){
	          selecteddivision.push($(this).val());
	      });
	      $.each($("input[name='country']:checked"), function(){
	          selectedcountry.push($(this).val());
	      });
	      $.each($("input[name='status']:checked"), function(){
	          selectedstatus.push($(this).val());
	      });
	      $.each($("input[name='paymentType']:checked"), function(){
	    	  selectedpaymentType.push($(this).val());
	      });
	      $.each($("input[name='campaignCode']:checked"), function(){
	    	  selectedcampaignCode.push($(this).val());
	      });
	      $.each($("input[name='dateRanage']:checked"), function(){
	    	  selecteddateRanage.push($(this).val());
	      });
	      $.each($("input[name='state']:checked"), function(){
	    	  selectedState.push($(this).val());
	      });
	      $.each($("input[name='co']:checked"), function(){
	    	  selectedCO.push($(this).val());
	      });
	      $.each($("input[name='statuslisCO']:checked"), function(){
	    	  statusListCo.push($(this).val());
	      });
	      $.each($("input[name='vo']:checked"), function(){
	    	  voList.push($(this).val());
	      });
	      $.each($("input[name='salesRep']:checked"), function(){
	    	  selectedSalesRep.push($(this).val());
	      });
	      $.each($("input[name='turnInSttausFilter']:checked"), function(){
	    	  turnInStatus.push($(this).val());
	      });
	      $.each($("input[name='shipmentstatusfilter']:checked"), function(){
	    	  shipmentStatus.push($(this).val());
	      });
	      $.each($("input[name='DelinquentPeriod']:checked"), function(){
	    	  delinquentMonths.push($(this).val());
	      });

	      $.each($("input[name='vmFilter']:checked"), function(){
	    	  vmFilter.push($(this).val());
	      });
	      $.each($("input[name='cmFilter']:checked"), function(){
	    	  cmFilter.push($(this).val());
	      });

	     if($scope.fiscalDate!==null && $scope.fiscalDate!==undefined){
	    	 year=$scope.year;
	    	 weekNr=$scope.weekNr;
	     }



										      filterObj = {
										'name' : $scope.searchedText,
										'usertype' : selecteduser,
										'salesdesig' : selectedsalesrep,
										'divcode' : selecteddivision,
										'country' : selectedcountry,
										'status' : selectedstatus,
										'currentState' : $state.current.name,
										'paymentType' : selectedpaymentType,
										'campaignCode' : selectedcampaignCode,
										'dateRanage' : selecteddateRanage,
										'state' : selectedState,
										'co' : selectedCO,
										'statusListCO' : statusListCo,
										'vo' : voList,
										'salesRep' : selectedSalesRep,
										'shipmentStatus' : shipmentStatus,
										'fiscalWeek' : weekNr,
										'fiscalYear' : year,
										'turnInStatus' : turnInStatus,
										'requestType' : requestType,
										'deductionTypeList' : deductionTypeList,
										'delinquentMonthList' : delinquentMonths,
										'vmFilter' : vmFilter,
										'cmFilter' : cmFilter
									};


	      localStorageService.set('filterObj',filterObj);



										      if ($scope.searchedText === ""
											&& selecteduser.length === 0
											&& selectedsalesrep.length === 0
											&& selecteddivision.length === 0
											&& selectedcountry.length === 0
											&& selectedstatus.length === ''
											&& selectedpaymentType.length === 0
											&& selectedcampaignCode.length === 0
											&& selecteddateRanage.length === ''
											&& shipmentStatus.length === 0
											&& turnInStatus.length === 0
											&& requestType.length === 0
											&& deductionTypeList.length === 0
											&& vmFilter.length === 0
											&& cmFilter.length===0) {
										$rootScope
												.$broadcast('filterObj', null);
									}
	      else {
	          $rootScope.$broadcast('filterObj', filterObj);
	      }
	      if($state.current.name==='SalesRep.ManageTurnIn'|| $state.current.name==='CoBoard.CollectionTurnIn' || $state.current.name==='AdminBoard.AdminTurnIn' || $state.current.name==='VoBoard.VoTurnIn')
    	  {
    	  	$rootScope.$broadcast('eventName', { paymentOptions: selectedpaymentType, turnInStatus : turnInStatus });
    	  }
      }
  }
    function getRegionNames(arr)
    {
    	var list=[];
    	for(var i=0;i<arr.length;i++)
    		{
    			if(list.indexOf(arr[i].regionName)===-1)
    			list.push(arr[i].regionName);
    		}
    	return list;

    }

    $scope.toggleSidebar = function (block) {

    	switch(block) {
	    	case "search": {
	    		$scope.ToggleSearchPanel = !$scope.ToggleSearchPanel;
	    		$scope.searchTypeIcon = !$scope.searchTypeIcon;
	    		break;
	    	}
	    	case "userType": {
	    		$scope.ToggleUserType = !$scope.ToggleUserType;
	    		$scope.userTypeIcon = !$scope.userTypeIcon;
	    		break;
	    	}
	    	case "designation": {
	    		$scope.ToggSalesDesig = !$scope.ToggSalesDesig;
	    		$scope.salesTeamTypeIcon = !$scope.salesTeamTypeIcon;
	    		break;
	    	}
	    	case "division": {
	    		$scope.ToggDivision = !$scope.ToggDivision;
	    		$scope.divisionIcon = !$scope.divisionIcon;
	    		break;
	    	}
	    	case "country": {
	    		$scope.ToggleCountryName = !$scope.ToggleCountryName;
	    		$scope.countryIcon = !$scope.countryIcon;
	    		break;
	    	}
	    	case "status": {
	    		$scope.ToggleStatus = !$scope.ToggleStatus;
	    		$scope.statusIcon = !$scope.statusIcon;
	    		break;
	    	}
	    	case "paymentMethod": {
	    		$scope.TogglePaymentMethod = !$scope.TogglePaymentMethod;
	    		$scope.paymentMethodIcon = !$scope.paymentMethodIcon;
	    		break;
	    	}
	    	case "campaignCode": {
	    		$scope.ToggleCampaignCode = !$scope.ToggleCampaignCode;
	    		$scope.campaignCodeIcon = !$scope.campaignCodeIcon;

	    		if($scope.ToggleCampaignCode){
	    			 //$('#dropdownCampaign').css('display','none');
	    			 $scope.campaignCodeSuggestionList=false;
	    			 $scope.searchedCampaignCode="";
	    		}
	    		else
	    			$scope.campaignCodeSuggestionList=true;

	    		break;
	    	}
	    	case "dateRanage": {
	    		$scope.ToggleDateRanage = !$scope.ToggleDateRanage;
	    		$scope.dateRanageIcon = !$scope.dateRanageIcon;
	    		break;
	    	}
	    	case "state": {
	    		$scope.ToggleState = !$scope.ToggleState;
	    		$scope.stateIcon = !$scope.stateIcon;
	    		break;
	    	}
	    	case "city": {
	    		$scope.ToggleCity = !$scope.ToggleCity;
	    		$scope.cityIcon = !$scope.cityIcon;
	    		break;
	    	}
	    	case "vo": {
	    		$scope.ToggleVo = !$scope.ToggleVo;
	    		$scope.voIcon = !$scope.voIcon;
	    		break;
	    	}
	    	case "co":{
	    		$scope.ToggleCo = !$scope.ToggleCo;
	    		$scope.coIcon = !$scope.coIcon;
	    		break;
	    	}
	    	case "salesRep":{
	    		$scope.TogglesalesRep = !$scope.TogglesalesRep;
	    		$scope.salesRepIcon = !$scope.salesRepIcon;
	    		break;
	    	}
	    	case "DelinquentPeriod":{
	    		$scope.ToggleDelinquentPeriod = !$scope.ToggleDelinquentPeriod;
	    		$scope.DelinquentPeriodIcon = !$scope.DelinquentPeriodIcon;
	    		break;
	    	}
	    	case "paymentType":{
	    		$scope.TogglePaymentType=!$scope.TogglePaymentType;
	    		$scope.paymentTypeIcon=!$scope.paymentTypeIcon;
	    		break;
	    	}
	    	case "toggleTurnInStatus":{
	    		$scope.ToggleTurnInStatus=!$scope.ToggleTurnInStatus;
	    		$scope.turnInStatusIcon=!$scope.turnInStatusIcon;
	    		break;
	    	}
	    	case "toggleShipmentStatusFilter":{
	    		$scope.ToggleShipmentStatusFilter=!$scope.ToggleShipmentStatusFilter;
	    		$scope.shipmentStatusIcon=!$scope.shipmentStatusIcon;
	    		break;
	    	}
	    	case "toggleFiscalWeekFilter":{
	    		$scope.toggleFiscalWeekFilter=!$scope.toggleFiscalWeekFilter;
	    		$scope.fiscalWeekIcon=!$scope.fiscalWeekIcon;
	    		break;
	    	}
	    	case "toggleTransIdFilter":{
	    		$scope.ToggleTransIdFilter=!$scope.ToggleTransIdFilter;
	    		$scope.transIdIcon=!$scope.transIdIcon;
	    		break;
	    	}
	    	case "requestType":{
	    		$scope.toggleRequestType=!$scope.toggleRequestType;
	    		$scope.requestIcon=!$scope.requestIcon;
	    		break;
	    	}
	    	case 'deductionMode':{
	    		$scope.deductionModeIcon=!$scope.deductionModeIcon;
	    		$scope.toggleDeductionMode=!$scope.toggleDeductionMode;
	    		break;
	    	}
	    	case 'toggleFiscalWeekFilter':{
	    		$scope.fiscalWeekIcon=!$scope.fiscalWeekIcon;
	    		$scope.ToggleFiscalWeekFilter=!$scope.ToggleFiscalWeekFilter;
	    		break;
	    	}
	    	case 'vmCompletedFilter':{
	    		$scope.vmCompletedFilterIcon=!$scope.vmCompletedFilterIcon;
	    		$scope.togglevmCompletedFilter=!$scope.togglevmCompletedFilter;
	    		break;
	    	}
	    	case 'cmCompletedFilter':
	    		$scope.cmCompletedFilterIcon=!$scope.cmCompletedFilterIcon;
	    		$scope.togglecmCompletedFilter=!$scope.togglecmCompletedFilter;
	    		break;
    	}
    }

    $scope.showHideDropdown=function(){
    	if($scope.searchedCampaignCode!==""){
    		$scope.campaignCodeSuggestionList=true;
    	}else{
    		$scope.campaignCodeSuggestionList=false;
    	}
    }
    $scope.$on('getSelectedContracts', function (event, args) {
    	 $scope.contractStatus = args.type;
    		 $('#thismonth').prop('checked', true);
    		 if($scope.contractStatus.toLowerCase()==='customercancelled')
    		 {
    		 	$('#cancelled').prop('checked', true);
    		 	$scope.filterGrid();
    		 }
    		 else if($scope.contractStatus.toLowerCase()==='confirmed')
    			 {
    			 	$('#pendingcoallocation').prop('checked',true);
    			 	$scope.filterGrid();
    			 }
    		 else if($scope.contractStatus.toLowerCase()==='rejected')
    			 {
    			 	$('#rejected').prop('checked',true);
    			 	$scope.filterGrid();
    			 }
    		 else if($scope.contractStatus.toLowerCase()==='delivered')
    			 {
    			 	$('#shipment0').prop('checked',true);
    			 	$scope.filterGrid();
    			 }
    		 else if($scope.contractStatus.toLowerCase()==='partiallydelivered')
    			 {
    			 	$('#shipment1').prop('checked',true);
    			 	$scope.filterGrid();
    			 }
    		 else if($scope.contractStatus.toLowerCase()==='ordered')
    		 	{
    			 	console.log("Ordered");
    			 	$scope.filterGrid();
    		 	}
    		 else if($scope.contractStatus.toLowerCase()==='verified'){
    			 $('#confirmed').prop('checked',true);
    			 $('#rejected').prop('checked',true);
    			 $scope.filterGrid();
    		 }
    		  else if($scope.contractStatus.toLowerCase()==='unverified'){
    			 $scope.filterGrid();
    		 }
    		  else if($scope.contractStatus.toLowerCase()==='voverified'){
    			  $('#thismonth').prop('checked', false);
    			  $('#Verifiedvmfilter').prop('checked',true);
    			  $scope.filterGrid();
    		  }
    	 });
    $scope.$on('getVMSelectedContracts', function (event, args) {
   	 $scope.contractStatus = args.type;
   		 $('#thismonth').prop('checked', true);
   		 if($scope.contractStatus.toLowerCase()==='accepted')
   		 {
   		 	$('#pendingcoallocation').prop('checked', true);
   		 	$scope.filterGrid();
   		 }
   		 else if($scope.contractStatus.toLowerCase()==='rejected'){
   			$('#rejected').prop('checked', true);
   		 	$scope.filterGrid();
   		 }
   		 else if($scope.contractStatus.toLowerCase()==='fully-delivered'){
   			$('#shipment0').prop('checked', true);
   		 	$scope.filterGrid();
   		 }
   		 else if($scope.contractStatus.toLowerCase()==='partially-deliverd'){
   			$('#shipment1').prop('checked', true);
   		 	$scope.filterGrid();
   		 }
   		else if($scope.contractStatus.toLowerCase()==='verified'){
   			$('#thismonth').prop('checked', false);
   			$('#Verifiedvmfilter').prop('checked', true);
   		 	$scope.filterGrid();
   		 }
   		else if($scope.contractStatus.toLowerCase()==='completed'){
   			$('#thismonth').prop('checked', false);
   			$('#Completedvmfilter').prop('checked', true);
   		 	$scope.filterGrid();
   		 }


   	 });
    $scope.$on('vmContracts', function (event, args) {
   	 $scope.contractStatus = args.type;
   		 $('#thismonth').prop('checked', true);
   		 if($scope.contractStatus.toLowerCase()==='assigned'){
   			 $('#thismonth').prop('checked', false);
   			 $('#Assignedvmfilter').prop('checked',true);
   			 $scope.filterGrid();
   		 }
   		 else if($scope.contractStatus.toLowerCase()==='unassigned'){
  			 $('#pendingvoallocation').prop('checked',true);
  			 $scope.filterGrid();
  		 }
   		 else if($scope.contractStatus.toLowerCase()==='unverfiedvmcount'){
   		 $('#pendingverification').prop('checked',true);
 			 $scope.filterGrid();
   		 }

   	 });
    $scope.$on('cmContractType', function (event, args) {
      	 $scope.contractStatus = args.type;
      		 $('#thismonth').prop('checked', true);
      		 if($scope.contractStatus.toLowerCase()==='cmunassigned'){
      			 $('#pendingcoallocation').prop('checked',true);
      			 $scope.filterGrid();
      		 }
      		 else if($scope.contractStatus.toLowerCase()==='cmassigned'){
     			 $('#pendingcollection').prop('checked',true);
     			 $scope.filterGrid();
     		 }
      		else if($scope.contractStatus.toLowerCase()==='cancelinitiated'){
    			 $('#cancellationinitiated').prop('checked',true);
    			 $scope.filterGrid();
    		 }
     		 else if($scope.contractStatus.toLowerCase()==='cancelapproved'){
     			 $('#cancelled').prop('checked',true);
    			 $scope.filterGrid();
     		 }
     		 else if($scope.contractStatus.toLowerCase()==='reassigned'){
     			 $('#Reassignedcmfilter').prop('checked', true);
     			 $('#thismonth').prop('checked', false);
     			 $scope.filterGrid();

     		 }
     		 else if($scope.contractStatus.toLowerCase()==='completed'){
     			 $('#Completedcmfilter').prop('checked', true);
     			 $('#thismonth').prop('checked', false);
     			 $scope.filterGrid();
     		 }
     		 else if($scope.contractStatus.toLowerCase()==='collected'){
     			 $('#Collectedcmfilter').prop('checked', true);
     			 $('#thismonth').prop('checked', false);
     			 $scope.filterGrid();
     		 }
     		 else if($scope.contractStatus.toLowerCase()==='partiallycollected'){
     			 $('#Partially_Collectedcmfilter').prop('checked', true);
     			 $('#thismonth').prop('checked', false);
     			 $scope.filterGrid();
     		 }
     		else if($scope.contractStatus.toLowerCase()==='uncollected'){
    			 $('#Uncollectedcmfilter').prop('checked', true);
    			 $('#thismonth').prop('checked', false);
    			 $scope.filterGrid();
    		 }

      	 });
    $scope.getId=function(value)
    {
    	var id=' ';
    	for(var i=0;i<value.length;i++)
    		{
    			var x=value.charAt(i);
    			if(x!==' ')
    				id+=x;
    		}
    	return id.trim().toLowerCase();
    }
    $scope.$on('receiveSRName', function (event, arg) {
    	 $scope.searchedText=arg;
    	 $scope.filterGrid();

    });

    $scope.$on('receiveTurnName', function (event, arg) {
   	 $scope.searchedText=arg;
   	 $scope.filterGrid();
   });
    
    $scope.filterDate=function(){
    	
    	if(angular.isDefined($scope.fiscalDate)){
    		var target  = new Date($scope.fiscalDate.valueOf());  
      	  
      	  // ISO week date weeks start on monday  
      	  // so correct the day number  
      	  var dayNr   = (target.getDay() + 6) % 7;  

      	  // Set the target to the thursday of this week so the  
      	  // target date is in the right year  
      	  target.setDate(target.getDate() - dayNr + 3);  

      	  // ISO 8601 states that week 1 is the week  
      	  // with january 4th in it  
      	  var jan4    = new Date(target.getFullYear(), 0, 4);  

      	  // Number of days between target date and january 4th  
      	  var dayDiff = (target - jan4) / 86400000;    

      	  // Calculate week number: Week 1 (january 4th) plus the    
      	  // number of weeks between target date and january 4th    
      	  $scope.weekNr = 1 + Math.ceil(dayDiff / 7);
      	  $scope.year=target.getFullYear();
      	  $scope.fiscalDate=$scope.weekNr+"/"+$scope.year;
      	  $scope.filterGrid();
    		
    	}
    	
    	    	
    }
  

}]);
