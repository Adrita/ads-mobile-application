app.controller( "VoReceiveCtrl", ['$scope','$state','$uibModal','localStorageService','adapter','validate','adjustHeight','$compile','ModalService','Session',function($scope,$state,$uibModal,localStorageService,adapter,validate,adjustHeight,$compile,ModalService,Session)
{
	  adjustHeight.adjustTopBar();
	  $scope.pageActionHeader="Receive Collections";
	  $scope.CashBankIn=[];
	  $scope.ChequeBankIn=[];
	  $scope.CreditCardBankIn=[];
	  $scope.localData=localStorageService.get('voTurnInData');
	  $scope.cashTotal=0;
	  $scope.chequeTotal=0;
	  $scope.creditCardTotal=0;
	  $scope.submitData=[];
	  if($scope.localData!==null && $scope.localData!==undefined && $scope.localData.length>0)
	  var srName=$scope.localData[0].srName;
	  var obj={};
	  $scope.loader=false;
	  $scope.submitPressed=false;
	  var type='submit';
	  if($scope.localData==null)
		  $state.go('VoBoard.VoTurnIn');
	  else
		  {
		  	for(var i=0;i<$scope.localData.length;i++)
		  		{
		  			if($scope.localData[i].paymentMethod.toLowerCase()==='cash')
		  				{
		  				 $scope.CashBankIn.push($scope.localData[i]);
		  				 $scope.cashTotal+=$scope.localData[i].amount;
		  				}
		  			else if($scope.localData[i].paymentMethod.toLowerCase()==='cheque')
		  				{
		  					$scope.ChequeBankIn.push($scope.localData[i]);
		  					$scope.chequeTotal+=$scope.localData[i].amount;
		  				}
		  				 
		  			else if($scope.localData[i].paymentMethod.toLowerCase()==='credit card')
		  				{
		  				 	$scope.CreditCardBankIn.push($scope.localData[i]);
		  				 	$scope.creditCardTotal+=$scope.localData[i].amount;
		  				}
		  			$scope.cashAmount=$scope.cashTotal;
		  			
		  				
		  		}
		  	for(var i=0;i<$scope.localData.length;i++)
		  		{
		  			obj={paymentMethod:$scope.localData[i].paymentMethod,amount:$scope.localData[i].amount,paymentIdList:$scope.localData[i].paymentIdList};
		  			$scope.submitData.push(obj);
		  		}
		  		if($scope.localData.length>=1)
					{
						var firstItem=$scope.localData[0].srName;
						$scope.firstItem=$scope.localData[0].srName;
						var count=0;
						for ( var i = 0; i < $scope.localData.length; i++) {
							if($scope.localData[i].srName===firstItem)
								count++;
						}
						if(count===$scope.localData.length)
							$scope.showReceivingFromMultipleBlock=false;
						else
							$scope.showReceivingFromMultipleBlock=true;
						
					}

		  }
	  function getServiceCall() {

			var data = {};
			var url = "";
			if (type === 'submit') {
				$scope.loader=true;
				url = "VoBankInTurnInController/voBankInTurnInReceiveInRequest";
				data = {
					userId : Session.userId,
					remarks:$scope.remarks,
					cashAmount:null,
					receiveInBankInDetails:$scope.submitData
				};
				$scope.loader = true;
			} 
			adapter.getServiceData(url, data).then(success,
					error);
			function success(result) {
				$scope.loader=false;
				$scope.loader = false;
				if (result.statusCode === 200) {
					if (type === 'submit') {
						var total=0;
						if($scope.cashTotal>0)
						total+=parseFloat($scope.cashTotal);
						if($scope.chequeTotal>0)
						total+=parseFloat($scope.chequeTotal);
						if($scope.creditCardTotal>0)
						total+=parseFloat($scope.creditCardTotal);
						 modalEntity = total
						 + " "
						 + " has been received successfully from "+ srName;
						 modalBody = "";
						 localStorageService.set(
								 "modalStatus", true);
						 modalOpen("success");
						$scope.loader = false;
					}  
				} else if (result.statusCode === 401) {
					$scope.loader = false;
					 //modalOpen("error");
					 //data.srName=srName;

                     app.saveVOReceiveData(data,successCallback,errorCallback);

				}
			}
			function error() {
				$scope.loader = false;
				/*console.log("Service not resolved");
				modalOpen("error");*/

				//data.srName=srName;

				app.saveVOReceiveData(data,successCallback,errorCallback);

			}

		}
	  /* MODAL IMPLEMENTATION */
		 $scope.animationsEnabled = true;

		 $scope.open = function() {
			 var modalInstance = $uibModal.open({
				 animation : $scope.animationsEnabled,
				 templateUrl : 'app/modals/role/modal.html',
				 controller : 'modalCtrl',
				 windowClass : 'center-modal',
				 backdrop : 'static',
				 keyboard : false,
				 size : 'md'
			 }).result.then(function(data) {
				 if (data === "close") {
					 $state.go('VoBoard.VoTurnIn');
				 }
				 if (data === "createAnother") {
					 $state.reload();
				 }
			 });
		 };

		 var errorName = "Turn In";
		 var errorMessage="Failed to receive";

		 function modalOpen(modalType) {
			 if (errorName !== null && errorName !== "") {
				 if (modalType === "success") {
					 var modalObj = {
							 "modalEntity" : modalEntity,
							 "body" : modalBody,
							 "modalBtnText" : "OK",
							 "backLink" : "OK",
							 "singleBtn" : true
					 };
				 } else {
					 var modalObj = {
							 "modalEntity" : "Failed!! ",
							 "body" : errorMessage,
							 "modalBtnText" : "",
							 "backLink" : "Ok",
							 "singleBtn" : true
					 };
				 }

				 localStorageService.set("modalObj",
						 modalObj);
				 $scope.open();
			 }
		 }
		 $scope.voReceive={};
		 $scope.serviceHit=function()
		 {
			 $scope.submitPressed=true;
			 if($scope.voReceive.$valid)
				 {
				 	type='submit';
				 	getServiceCall();
				 }
			 
		 }
		 $scope.cancel=function()
		 {
			 $state.go('VoBoard.VoTurnIn');
		 }

		 function successCallback(result){
			var total=0;
			if($scope.cashTotal>0)
			total+=parseFloat($scope.cashTotal);
			if($scope.chequeTotal>0)
			total+=parseFloat($scope.chequeTotal);
			if($scope.creditCardTotal>0)
			total+=parseFloat($scope.creditCardTotal);
			 modalEntity = total
			 + " "
			 + " has been received successfully from "+ srName;
			 modalBody = "";
			 localStorageService.set(
					 "modalStatus", true);
			 modalOpen("success");
		 }

		 function errorCallback(result){
		 	alert(result);
		 	modalOpen("error");
		 }

}]);
