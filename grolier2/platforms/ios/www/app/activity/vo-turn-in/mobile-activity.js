app
    .controller(
        "VoTurnInCtrlMobile", [
            '$scope',
            '$state',
            '$rootScope',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'ModalService',
            'Session',
            '$filter',
            '$cordovaNetwork',
            'syncService',
            function($scope, $state, $rootScope, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, ModalService, Session,$filter,$cordovaNetwork,syncService) {



				$scope.detailsView=false;

                $scope.openGrid = function(type) {
                    if (type === 'receive') {
                        $scope.tab1 = true;
                        $scope.tab2 = false;
                        $scope.tab0 = false;


                        $scope.cashDetailsList=[];
                        $scope.chequeDetailsList=[];
                        $scope.creditDetailsList=[];

                        getServiceCall(type);
                    }else if(type ==='historyTab'){
						 $scope.tab0 = true;
						 $scope.tab1 = false;
						 $scope.tab2 = false;

						 $scope.cashDetailsList=[];
						 $scope.chequeDetailsList=[];
						 $scope.creditDetailsList=[];

						 getServiceCall(type);
					 } else {
                        $scope.tab1 = false;
                        $scope.tab2 = true;
                        $scope.tab0 = false;
                        $scope.cashDetailsList=[];
						$scope.chequeDetailsList=[];
						$scope.creditDetailsList=[];
                        $scope.paymentTypeList=["Cash","Cheque","Credit Card"];
                        $scope.paymentType=$scope.paymentTypeList[0];
                        getServiceCall(type);
                    }
                };

                function getServiceCall(type) {
                    var url = "";
                    var data = {};

                    $scope.loader = true;

                    if (type === 'receive') {

                        url = "VoBankInTurnInController/voBankInTurnInReceiveRequest";
                        data = {
                            userId: Session.userId
                        };

                    } else if (type === 'Summary') {
                        url = "VoBankInTurnInController/voBankInTurnInSummaryRequest";
                        data = {
                            userId: Session.userId
                        }

                    }else if(type==='pending'){
                        url="VoBankInTurnInController/voBankInTurnInPendingForClearanceRequest";
                        data={
                            userId:Session.userId
                        }
                    }else if(type==='history'){
						 url="VoBankInTurnInController/voBankInTurnInHistoryRequest";
						 data={
							 userId:Session.userId
						 }
					 }

                    adapter.getServiceData(url, data).then(success, error);

                    function success(result) {

                        $scope.loader = false;

                        if (type === 'receive' && result.statusCode === 200) {
                            for (var i = 0; i < result.voBankInTurnInReceiveRows.length; i++)
                                result.voBankInTurnInReceiveRows[i].id = i;

                            $scope.totalData=result.voBankInTurnInReceiveRows;
                            formatGridData(result.voBankInTurnInReceiveRows);
                            //app.saveVerificationTurnInReceive(result);
                            app.saveDataIntoFile('saveVerificationTurnInReceive',result);

                            type = 'Summary';
                            getServiceCall(type);
                        } else if (type === 'Summary' && result.statusCode === 200) {
                            $scope.cashSummary = result.cashSummary;
                            $scope.chequeSummary = result.chequeSummary;
                            $scope.creditCardSummary = result.creditCardSummary;

                            app.saveDataIntoFile('saveVOSummary',result);
                            checkForLocation();

                        } else if(type === 'pending' && result.statusCode === 200) {
                            for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++)
                                    result.voBankInTurnInPendingClearanceRows[i].id = i;
                            formatGridData(result.voBankInTurnInPendingClearanceRows);

                            app.saveDataIntoFile('saveVerificationTurnInPending',result);

                            type = 'Summary';
                            getServiceCall(type);
                        }else if(type=== 'history' && result.statusCode === 200){
							 $scope.turnInHistory = result.voBankInTurnInHistoryRowList;

							 localStorageService.set( 'voTurnInHistory', result.voBankInTurnInHistoryRowList);

							 type = "Summary";
							 getServiceCall(type);
						 }

                    }

                    function error() {
                        $scope.loader = false;

                        if(type==='receive'){
                        	//app.fetchVOTurnInReceive(successFetchTurnInReceive,errorCallback);
                        	app.fetchDataFromFile('saveVerificationTurnInReceive',successFetchTurnInReceive,errorCallback);

                        }else if(type==='Summary'){
                        	//app.fetchSummaryVO(successFetchSummary,errorCallback);
                        	app.fetchDataFromFile('saveVOSummary',successFetchSummary,errorCallback);
                        }else if(type==='pending'){
                        	app.fetchDataFromFile('saveVerificationTurnInPending',successFetchPending,errorCallback);
                        }
                    }
                }



                function checkForLocation(){
                                	if (navigator.geolocation) {
                						navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, {
                							timeout: 30000
                						});
                					} else {
                						alert("Geolocation is not supported");
                					}

                					function geolocationSuccess(position) {
                                    //				alert('Latitude: ' + position.coords.latitude + '\n' +
                                    //					'Longitude: ' + position.coords.longitude + '\n' +
                                    //					'Altitude: ' + position.coords.altitude + '\n' +
                                    //					'Accuracy: ' + position.coords.accuracy + '\n' +
                                    //					'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                                    //					'Heading: ' + position.coords.heading + '\n' +
                                    //					'Speed: ' + position.coords.speed + '\n' +
                                    //					'Timestamp: ' + position.timestamp + '\n');


                						localStorage.setItem('lat',position.coords.latitude);
                						localStorage.setItem('lon',position.coords.longitude);


                							app.syncVerificationReceiveTurnIn(successVerificationReceiveTurnIn, errorVerificationReceiveTurnIn);


                					}

                					function geolocationError(error) {
                                    				switch (error.code) {
                                    					case error.PERMISSION_DENIED:
                                    						alert("User denied the request for Geolocation.");
                                    						break;
                                    					case error.POSITION_UNAVAILABLE:
                                    						alert("Location information is unavailable.");
                                    						break;
                                    					case error.TIMEOUT:
                                    						alert("The request to get user location timed out.");
                                    						break;
                                    					case error.UNKNOWN_ERROR:
                                    						alert("An unknown error occurred.");
                                    						break;
                                    				}
                					}

                                }

                        function successVerificationReceiveTurnIn(result){
                        	if(result.length>0){
                        		var data = {
									deviceId: localStorage.getItem('deviceId'),
									lat: "" + localStorage.getItem('lat'),
									lon: "" + localStorage.getItem('lon'),
									offlineReceiveInBankInRecords: result
								}
								syncService.syncVerificationReceive(data);
                        	}else{
                        		app.syncVerificationBankInTurnIn(successVerificationBankInCallback,errorVerificationBankInCallback);
                        	}

                        }

                        function errorVerificationReceiveTurnIn(result){

                        	app.syncVerificationBankInTurnIn(successVerificationBankInCallback,errorVerificationBankInCallback);

                        }

                        function successVerificationBankInCallback(result){

                        	if(result.length>0){
                        		var data = {
									deviceId: localStorage.getItem('deviceId'),
									lat: "" + localStorage.getItem('lat'),
									lon: "" + localStorage.getItem('lon'),
									offlineBankInRecords: result
								}
								syncService.syncVerificationBankIn(data);
                        	}

                        }

                        function errorVerificationBankInCallback(result){

                        }


				var type="";

				$scope.isOnline=$cordovaNetwork.isOnline();

				if($scope.isOnline){
					type="history";
					$scope.tab0 =true;
					 $scope.tab1 = false;
					 $scope.tab2 = false;
				}else{

					$scope.tab0=false;
					$scope.tab1=true;
					$scope.tab2=false;
					type="receive";
				}


				$scope.$on('$cordovaNetwork:online', function(event, networkState){

					if(!$scope.isOnline){
						$state.reload();
						$scope.isOnline=true;
					}
				});

				$scope.$on('$cordovaNetwork:offline', function(event, networkState){
					$scope.isOnline=false;
					$state.reload();
				});

                getServiceCall(type);

                function formatGridData(listArr) {
                    $scope.cashDetailsList = [];
                    $scope.chequeDetailsList = [];
                    $scope.creditDetailsList = [];
                    $scope.salesRepNames = [];
                    angular.forEach(listArr, function(value, key) {

                        if($scope.tab1){
                            $scope.salesRepNames.push(value.srName);
                        }
                        
                        if (value.paymentMethod === 'Cash') {
                            $scope.cashDetailsList.push(value);
                        } else if (value.paymentMethod === 'Cheque') {
                            $scope.chequeDetailsList.push(value);
                        } else {
                            $scope.creditDetailsList.push(value);
                        }
                        
                    });
                    if($scope.tab1){
                        //$scope.salesRepNames = _.uniq($scope.salesRepNames);
                        $scope.salesRepNames = uniqueSalesRep($scope.salesRepNames);
                        $scope.salesRepName = $scope.salesRepNames[0];
                     }
                }

                function uniqueSalesRep(salesRepNames){
                	var newSalesRepArr=[];

                	angular.forEach(salesRepNames,function(value,key){
                		if(newSalesRepArr.indexOf(value)===-1){
                			newSalesRepArr.push(value);
                		}

                	});

                	return newSalesRepArr;

                }

                $scope.concatanateString = function(str) {
                    if(str){
                        return str.replace(/\s+/g, '-').toLowerCase();
                    }
                 
                }

                $scope.receiveFrom=function(salesRepName){
                     var receiveList=$filter('filterBySR')($scope.totalData,salesRepName);
                     console.log(receiveList);
                     var tempArr=[];
                     var diff=0;

                     if(receiveList.length>0){

                        angular.forEach(receiveList,function(value,key){
                            if(value.paymentMethod==='Cheque' && value.chequeDate){
                                var currentDate = new Date();
                                currentDate.setMilliseconds(0);
                                currentDate.setMinutes(0);
                                currentDate.setHours(0);
                                diff = currentDate - new Date(value.chequeDate);

                                if(diff>=0){
                                    tempArr.push(value);
                                }
                            }else{
                                tempArr.push(value);
                            }

                        });
                        localStorageService.set('voTurnInData', tempArr);
                        $state.go('VoBoard.VoReceive');
                     }
                }

                $scope.bankInVO=function(){
                    if($scope.paymentType==='Cash' && $scope.cashDetailsList){

                        var tempArr=[];
                        angular.forEach($scope.cashDetailsList,function(value,key){
                            if(value.bankedInBy==='With VO'){
                                tempArr.push(value);
                            }
                        });

                        localStorageService.set('voTurnInData', tempArr);
                        $state.go('VoBoard.VoBankIn');
                    }else if($scope.paymentType==='Cheque' && $scope.chequeDetailsList){
                        var tempArr=[];
                        var diff=0;

                        angular.forEach($scope.chequeDetailsList,function(value,key){
                            if(value.chequeDate){
                                var currentDate = new Date();
                                currentDate.setMilliseconds(0);
                                currentDate.setMinutes(0);
                                currentDate.setHours(0);
                                diff = currentDate - new Date(value.chequeDate);

                                if(diff>=0 && value.bankedInBy==='With VO'){
                                    tempArr.push(value);
                                }
                            }

                        });

                        if(tempArr && tempArr.length>0){
                            localStorageService.set('voTurnInData', tempArr);
                            $state.go('VoBoard.VoBankIn');
                        }
                        
                    }
                }

                $scope.singleSubmit=function(row){
                    if(row.paymentMethod==='Cheque' && row.chequeDate){
                        var tempArr=[];
                        var diff=0;
                        var currentDate = new Date();
                        currentDate.setMilliseconds(0);
                        currentDate.setMinutes(0);
                        currentDate.setHours(0);
                        diff = currentDate - new Date(row.chequeDate);

                        if(diff>=0 && row.bankedInBy==='With VO'){
                            tempArr.push(row);
                        }

                        if(tempArr && tempArr.length>0){
                            localStorageService.set('voTurnInData', tempArr);
                            $state.go('VoBoard.VoBankIn');
                        }
                    }else if(row.paymentMethod==='Cash'){
                        var tempArr=[];
                        if(row.bankedInBy==='With VO'){
                            tempArr.push(row);
                            localStorageService.set('voTurnInData',tempArr);
                            $state.go('VoBoard.VoBankIn');
                        }
                        
                    }
                }

                function successFetchTurnInReceive(result){
                	for (var i = 0; i < result.voBankInTurnInReceiveRows.length; i++)
                                                    result.voBankInTurnInReceiveRows[i].id = i;

					$scope.totalData=result.voBankInTurnInReceiveRows;
					formatGridData(result.voBankInTurnInReceiveRows);

					$scope.$apply();

					type = 'Summary';
					getServiceCall(type);
                }

                function errorCallback(message){
                	alert(message);
                }

                function successFetchSummary(result){

                $scope.cashSummary = result.cashSummary;
				$scope.chequeSummary = result.chequeSummary;
				$scope.creditCardSummary = result.creditCardSummary;

				$scope.$apply();

                }

                function successFetchPending(result){
                	for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++)
							result.voBankInTurnInPendingClearanceRows[i].id = i;
					formatGridData(result.voBankInTurnInPendingClearanceRows);

					$scope.$apply();

					type = 'Summary';
					getServiceCall(type);
                }

                $scope.viewDetails=function(row){
					$scope.detailsView=true;
					$scope.detailList=row;
				}
				$scope.backToTurnIn=function(){
					$scope.detailsView=false;
				}

            }
        ]);