app
    .controller(
        "ManageCollectorCollectionRequestCtrl", [
            '$scope',
            '$state',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'Session',
            'commonServices',
            function($scope, $state, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, Session, commonServices) {
				$scope.showSelected=[];
				$scope.loading = true ;
				var offline=false;
                var type = "getList";
                getServiceCall();
                $scope.isEmpty=false;

                function getServiceCall() {
                    var data = {};
                    $scope.loading = true ;
                    var url = "";
                    if (type === 'getList') {
                        url = "CollectionController/manageAssignedCollection";
                        data = {
                            userId: Session.userId,
                            pageSize:null,
                         currentPage:null,
                         filtered:localStorageService.get('isFiltered'),
                         filterObj:localStorageService.get('filterObj')
                        };
                    } else if (type === "assigned") {
                        url = "CollectionController/manageAssignedCollection";
                        data = {
                            userId: Session.userId,
                            pageSize:$scope.$parent.selectedCnt,
                            currentPage:$scope.$parent.currentPage,
                            filtered:localStorageService.get('isFiltered'),
                            filterObj:localStorageService.get('filterObj')
                        };
                    }else if( type==="saveNote"){
                    	url="CollectionController/saveNote";
                    	data={
                    		contractId:$scope.contractId,
                    		collectorNote:$scope.collectorNote
                    	};
                    }

                    adapter.getServiceData(url, data).then(success,
                        error);

                    function success(result) {

                   // console.log(JSON.stringify(result));
                        if (result.statusCode === 200) {
                        offline=false;
                            $scope.loading = false;
                            if (type === "getList") {
                                $scope.$parent.colInfo = {
                                    "contractCode": {
                                        "title": "Contract Code",
                                        "width": "10%"
                                    },
                                    "custName": {
                                        "title": "Customer Name",
                                        "width": "12%"
                                    },
                                    "custHomeAddr": {
                                        "title": "Address",
                                        "width": "15%"
                                    },
                                    "phoneNumber": {
                                        "title": "Phone Number",
                                        "width": "10%"
                                    },
                                    "fiscalYear": {
                                        "title": "Fiscal Week/Year",
                                        "width": "15%"
                                    },
                                    "lastPaymentDt": {
                                        "title": "Last Payment Date",
                                        "width": "10%"
                                    },
                                    "nextPaymentDueDt": {
                                        "title": "Next Due Date",
                                        "width": "10%"
                                    },
                                    "orderStatus": {
                                        "title": "Status",
                                        "width": "10%"
                                    },
                                    "collectorNote": {
                                        "title": "Notes",
                                        "width": "8%"
                                    },
                                    "collectPayment": {
                                        "title": "Collect Payment",
                                        "width": "10%"
                                    }
                                };

                                if(result.collectionList==null || result.collectionList ==undefined){
                                	$scope.isEmpty=true;
                                }else{
                                	$scope.isEmpty=false;
                                }
                                $scope.$parent.gridOptions.data = result.collectionList;
                                $scope.$parent.colData = result.collectionList;
                                $scope.$parent.serviceData = result.collectionList;
                                $scope.$parent.totalItemCount=result.totalNumberOfRecords;
                                localStorageService.set('isFiltered',result.filtered);
                                localStorageService.set('internetConnected',true);
                                $scope.$parent.configGrid();
                                app.saveCollectionRequest(result);
                            }else if(type==="saveNote"){

                           // console.log(JSON.stringify($scope.showSelected));
                            	angular.forEach($scope.showSelected,function(vel,key){
                            		//console.log("Key>>"+key+" : Val"+vel);
                            		$scope.showSelected[key]=false;

                            	});

                            }

                        }
                    }

                    function error() {
                    localStorageService.set('internetConnected',false);
                        console.log("Service not resolved");
                        if (type === 'getList') {
                        offline=true;
                            var data = "";
                            app.fetchCollectionRequest(data, callbackCollection);

                        }else if(type==='saveNote'){
                        	var data={
                                       contractId:$scope.contractId,
                                       collectorNote:$scope.collectorNote,
                                       isSync:false
                                     };
                            app.saveNote(data,callbackNote);
                        }
                    }

                }


                function callbackNote(result){
               // console.log(JSON.stringify($scope.showSelected));
				angular.forEach($scope.showSelected,function(vel,key){
				//	console.log("Key>>"+key+" : Val"+vel);
					$scope.showSelected[key]=false;

				});

				$scope.$apply();

                }

                function callbackCollection(result) {
                $scope.loading = false;
                    $scope.$parent.colInfo = {
                        "contractCode": {
                            "title": "Contract Code",
                            "width": "10%"
                        },
                        "custName": {
                            "title": "Customer Name",
                            "width": "12%"
                        },
                        "custHomeAddr": {
                            "title": "Address",
                            "width": "15%"
                        },
                        "phoneNumber": {
                            "title": "Phone Number",
                            "width": "10%"
                        },
                        "fiscalYear": {
                            "title": "Fiscal Week/Year",
                            "width": "15%"
                        },
                        "lastPaymentDt": {
                            "title": "Last Payment Date",
                            "width": "10%"
                        },
                        "nextPaymentDueDt": {
                            "title": "Next Due Date",
                            "width": "10%"
                        },
                        "orderStatus": {
                            "title": "Status",
                            "width": "10%"
                        },
                        "collectorNote": {
                            "title": "Notes",
                            "width": "8%"
                        },
                        "collectPayment": {
                            "title": "Collect Payment",
                            "width": "10%"
                        }
                    };
                    $scope.$parent.gridOptions.data = result.collectionList;
                    $scope.$parent.colData = result.collectionList;
                    $scope.$parent.serviceData = result.collectionList;
                    $scope.$parent.totalItemCount=result.collectionList.length;

                    $scope.$parent.configGrid();

                    $scope.$apply();

                }
                $scope.payment = function(row) {
                    if (row.outstandingInstallments !== null && row.outstandingInstallments !== 0) {
                        localStorageService.set('paymentDetails', row);
                        $state.go('CoBoard.CoCollectPayment');
                    }
                }


                $scope.saveNote=function(row){
                $scope.contractId=row.contractId;
                $scope.collectorNote=row.collectorNote;
                type="saveNote";
                getServiceCall();
                }



                $scope.showSelectedRow=function(indx){
                	angular.forEach($scope.showSelected,function(val,key){
                	$scope.showSelected[key]=false;
                	});
                	$scope.showSelected[indx]=true;
                }

                $scope.viewContract=function(row,event){
                if(!offline){
                if(event.ctrlKey){
                                localStorageService.set('contractRow',row);

                					if($state.current.name==='SalesRep.ManageContractSales'){
                					window.open('#/SalesRep/ViewContract','_blank');
                					}else{
                					window.open('#/AdminBoard/ViewContract','_blank');
                					}

                                }else{
                                		localStorageService.set('contractRow',row);
                                		if($state.current.name==='SalesRep.ManageContractSales'){
                                			$state.go('SalesRep.ViewContract');
                                		}else{
                                			$state.go('AdminBoard.ViewContract');
                                		}

                                }

                }


                }

                $scope.viewCustomer=function(row){
                if(!offline){
                localStorageService.set('customerCode', row.custCode);
                $state.go('AdminBoard.EditCustomer');
                }


                }

                $scope.$on('ManageCollectorCollectionRequest', function (event, args) {
                
                     type = "getList";
                
                 getServiceCall();
                 });

                $scope.$on('scrollCollectionRequest', function (event, args) {
                 
                 if($(window).scrollTop() + $(window).height() == $(document).height()) {
                       var limit = 5;
                        $scope.$parent.selectedCnt = $scope.$parent.selectedCnt + limit;
                        if($scope.$parent.gridOptions.data.length!==null && $scope.$parent.gridOptions.data.length!==undefined && $scope.$parent.gridOptions.data.length<$scope.$parent.totalItemCount){
                            type = "getList";
                             getServiceCall();
                 }
                   }
                    
                 
             });




                            }







        ]);