app
.controller(
		"CreateEditCountryCtrl",
		[
		 '$scope',
		 '$state',
		 'localStorageService',
		 'adapter',
		 'adjustHeight',
		 'validate',
		 '$uibModal',
		 'Session',
		 function($scope, $state, localStorageService, adapter,
				 adjustHeight, validate, $uibModal, Session) {
			 adjustHeight.adjustTopBar();
			 $('[data-toggle="popover"]').popover();
			 $scope.newStateError = true;
			 $scope.countrySelect = true;
			 $scope.cashUpError = true;
			 $scope.pageActionHeader = "Create Country";
			 $scope.actionButton = "Create";
			 $scope.opType = "create";
			 $scope.addState = false;
			 $scope.loader = false;
			 var type = "get";
			 $scope.country = {};
			 $scope.backToLink = "AdminBoard.ManageCountry";
			 $scope.backLinkText = "Back to Manage Country";
			 var countryName = "";
			 /* MODAL IMPLEMENTATION */
			 $scope.animationsEnabled = true;

			 $scope.open = function() {
				 var modalInstance = $uibModal.open({
					 animation : $scope.animationsEnabled,
					 templateUrl : 'app/modals/role/modal.html',
					 controller : 'modalCtrl',
					 windowClass : 'center-modal',
					 backdrop : 'static',
					 keyboard : false,
					 size : 'md'
				 }).result.then(function(data) {
					 if (data === "close") {

						 $state.go($scope.backToLink);
					 }
					 if (data === "createAnother") {
						 $state.reload();
					 }
				 });
			 };

			 function modalOpen(modalType) {
				 if (countryName !== null && countryName !== "") {
					 if (modalType === "success") {
						 var modalObj = {
								 "modalEntity" : "Country ",
								 "body" : "(" + countryName + " - "
								 + countryCode + ") ",
								 "backLink" : $scope.backLinkText,
								 "modalBtnText" : "Create Another Country"
						 };
					 } else {
						 var modalObj = {
								 "modalEntity" : "Failed!! ",
								 "body" : errorMessage,
								 "backLink" : $scope.backLinkText,
								 "modalBtnText" : "Create Another Country"
						 };
					 }

					 localStorageService.set("modalObj",
							 modalObj);
					 $scope.open();
				 }
			 }


			 if (localStorageService.get('lastPage') !== null) {
				 $scope.backToLink = localStorageService
				 .get('lastPage');
				 console.log("Back me to"
						 + localStorageService.get('lastPage'));
				 $scope.backLinkText = localStorageService
				 .get('backLinkText');
			 }

			 var countryRow = localStorageService
			 .get('currentRecord');
			 if (countryRow !== null) {
				 $scope.pageActionHeader = "Edit Country";
				 $scope.actionButton = "Submit";
				 $scope.opType = "edit";
				 $scope.country.countryName = countryRow.countryName;
				 $scope.countryName = countryRow.countryName;
				 $scope.country.countryCode = countryRow.countryCode;
				 if (countryRow.active === "Active")
					 $scope.status_active = true;
				 else
					 $scope.status_active = false;
				 type = "edit";
				 getServiceCall();
			 } else {
				 $scope.country.countryName = '';
				 $scope.status_active = true;
				 type = "get";
				 getServiceCall();
			 }
			 $scope.setCountry = function(countryName) {
				 $scope.country.countryName = countryName;
				 $scope.country.countryCode = getCountryCode(countryName);
			 }

			 $scope.newlyAddedRegion = false;
			 $scope.regionChange = function(regionName, pos,
					 newRegName) {
				 if (regionName === "Add New Region") {

					 $scope.addReg(pos);
				 } else {
					 for ( var i = 0; i < $scope.country.stateList.length; i++) {
						 if (i === pos) {
							 var changedRegion = {
									 regionId : "",
									 regionCode : "",
									 regionName : regionName
							 };
							 $scope.country.stateList[i].region = changedRegion;
							 break;
						 }
					 }
					 var id = '#' + pos + "regDropdown";
					 $(id).css('display', 'none');
				 }
			 }

			 $scope.addReg = function(pos) {

				 var text = $('#TempId' + pos + 'regionName')
				 .val();
				 var newRegion = {
						 regionId : "",
						 regionName : text,
						 regionCode : ""
				 };
				 if (ifRegionExists(text) === false) {
					 $scope.country.regionList.push(newRegion);
					 $scope.country.stateList[pos].region = newRegion;
					 $('#' + pos + 'regAdd').css('display',
					 'none');
				 } else
					 alert("Region Already Exists");
			 }
			 $scope.newregionChange = function(regionName) {
				 if (regionName === "Add New Region") {

					 $scope.addnewRegion();

				 } else {
					 $scope.newRegion = regionName;
				 }
				 $('#newRegDropDown').css('display', 'none');
			 }
			 $scope.addnewRegion = function() {

				 var text = $('#regionSelect').val();
				 if (text !== undefined && text !== null
						 && text.trim() !== "")
					 if (ifRegionExists(text) === false) {
						 var newRegion = {
								 regionId : "",
								 regionCode : "",
								 regionName : text
						 };
						 $scope.country.regionList
						 .push(newRegion);
						 $scope.newRegion = text;

					 } else {
						 alert("Region Already Exists");
					 }

			 }

			 $scope.addnewState = function() {
				 if ($scope.newStateName === null
						 || $scope.newStateName === undefined
						 || $scope.newStateName.trim() === "") {

					 $scope.newStateError = false;

				 } else {
					 $scope.newStateError = true;

					 var newReg = {
							 regionId : "",
							 regionCode : "",
							 regionName : $scope.newRegion
					 };
					 var state = {
							 id : "",
							 checked : true,
							 stateId : "",
							 stateCode : "",
							 stateName : $scope.newStateName,
							 isAdded : false,
							 isNewlyAdded : true,
							 region : newReg
					 };
					 if (ifStateExists($scope.newStateName) === false) {
						 $scope.country.stateList.push(state);
						 if ($scope.country.stateList.length < 5) {
							 console.log($scope.country.stateList.length);
						 }

					 } else {
						 alert("State Already Exists");
					 }

					 // Display hide the add State div
					 $scope.addState = false;
				 }
				 $scope.newStateName = "";
				 $scope.newRegion = "";
				 $scope.newCityAdd = "";
			 }

			 $scope.showAddState = function() {
				 $scope.addState = !$scope.addState;
				 $scope.newStateName = "";
				 $scope.newRegion = "";
				 $scope.newCityAdd = "";
			 }

			 $scope.cancelState = function() {
				 $scope.addState = false;
				 $scope.newStateName = "";
				 $scope.newRegion = "";
				 $scope.newCityAdd = "";
			 }

			 function dataQuery() {

				 if (type === "get") {
					 $scope.loader = true;
					 return {
						 userId : Session.userId
					 };
				 }
				 if (type === "create") {
					 var country = {
							 countryName : $scope.country.countryName,
							 states : $scope.country.stateList,
							 active : ($scope.status_active === true) ? 'Active'
									 : 'Inactive',
									 maxCashUpDiscount : parseInt($scope.cashUpDiscount)
					 };
					 var data = {
							 userId : Session.userId,
							 country : country
					 };
					 return data;
				 }
				 if (type === "edit") {
					 var data = {
							 "userId" : Session.userId,
							 "country" : {
								 "countryId" : parseInt(countryRow.countryId)
							 }

					 }
					 return data;
				 }
				 if (type === "update") {
					 $scope.loader = true;
					 var data = {
							 "userId" : Session.userId,
							 "country" : {
								 "countryId" : parseInt(countryRow.countryId),
								 "countryCode" : countryRow.countryCode,
								 "countryName" : countryRow.countryName,
								 "active" : ($scope.status_active === true) ? 'Active'
										 : 'Inactive',
										 "states" : $scope.country.stateList,
										 "maxCashUpDiscount" : parseInt($scope.cashUpDiscount)
							 }

					 }
					 return data;
				 }
			 }

			 function getServiceCall() {
				 adapter
				 .getMappedUrls()
				 .then(
						 function(result) {
							 var url = adapter
							 .getCurrentUrl(
									 result,
									 type);
							 var dataReq = dataQuery();
							 if (type === "get") {
								 $scope.loader = true;
								 adapter.getServiceData(
										 url, dataReq)
										 .then(success,
												 error);
							 } else if (type === "edit")
								 adapter.getServiceData(
										 url, dataReq)
										 .then(success,
												 error);
							 else if (type === "update"
								 && checkValidations() === true) {
								 $scope.loader = true;
								 adapter.getServiceData(
										 url, dataReq)
										 .then(success,
												 error);
							 } else if (type === "create"
								 && createCountryValidations()
								 && checkValidations() === true) {
								 $scope.loader = true;
								 adapter.getServiceData(
										 url, dataReq)
										 .then(success,
												 error);
							 }
							 function success(result) {
								 if (result.statusCode === 200) {

									 if (type === "get") {
										 $scope.loader = true;

										 $scope.country.countryNameList = result.countries;
									 }
									 if (type === "create") {
										 $scope.loader = true;
										 localStorageService
										 .set(
												 "modalStatus",
												 true);
										 countryCode = result.countryCode;
										 modalOpen("success");
									 }
									 if (type === "edit") {
										 $scope.country.stateList = result.country.states;
										 $scope.country.regionList = result.country.regions;
										 if (result.country.states.length === 0)
											 $scope.country.stateList = [];
										 if (result.country.regions === null
												 || result.country.regions === undefined
												 || result.country.regions.length === 0)
											 $scope.country.regionList = [];

										 $scope.cashUpDiscount = result.country.maxCashUpDiscount;
									 }
									 if (type === "update") {
										 $scope.loader = true;
										 localStorageService
										 .set(
												 "modalStatus",
												 true);
										 countryCode = result.countryCode;
										 modalOpen("success");
									 }

								 } else {
									 console
									 .log("Service Not Resolved");
									 if (type !== "get") {
										 localStorageService
										 .set(
												 "modalStatus",
												 false);
										 errorMessage = result.message;
										 modalOpen("error");
									 }
								 }
								 $scope.loader = false;
							 }
							 function error(result) {
								 console
								 .log(type
										 + " Inside Error Block");
								 if (type !== "get") {
									 localStorageService
									 .set(
											 "modalStatus",
											 false);
									 errorMessage = result;
									 modalOpen("error");
								 }
							 }

						 })
			 }
			 ;

			 function ifRegionExists(regionName) {
				 if ($scope.country.regionList !== null && $scope.country.regionList!==undefined)
					 for ( var i = 0; i < $scope.country.regionList.length; i++) {
						 if ($scope.country.regionList[i].regionName === regionName)
							 return true;
					 }
				 else {
					 // during create
					 $scope.country.regionList = [];
				 }
				 return false;
			 }
			 function ifStateExists(stateName) {
				 if ($scope.country.stateList !== null && $scope.country.stateList !== undefined)
					 for ( var i = 0; i < $scope.country.stateList.length; i++) {
						 if ($scope.country.stateList[i].stateName
								 .toUpperCase() === stateName
								 .toUpperCase())
							 return true;
					 }
				 else {
					 $scope.country.stateList = [];
				 }
				 return false;
			 }
			 function checkValidations() {
				 var flag = 0;
				 if ($scope.country !== null
						 && $scope.country.stateList !== null && $scope.country.stateList !== undefined) {
					 for ( var i = 0; i < $scope.country.stateList.length; i++) {
						 if ($scope.country.stateList[i].checked === true) {

							 if ($('#TempId' + i + 'regionName')
									 .val() === null
									 || $(
											 '#TempId'
											 + i
											 + 'regionName')
											 .val() === undefined
											 || $(
													 '#TempId'
													 + i
													 + 'regionName')
													 .val().trim() === "") {
								 $('#TempId' + i + 'regionName')
								 .css('border',
								 '1px solid red');
								 flag++;
							 } else
								 $('#' + i + 'regionSelect')
								 .css('border',
								 '1px solid #ccc');
						 }
					 }
				 }

				 $scope.cashUpError = validate
				 .validateInteger($scope.cashUpDiscount);
				 if ($scope.cashUpError === true)
					 if (parseInt($scope.cashUpDiscount) > 10
							 && parseInt($scope.cashUpDiscount) > 0)
						 $scope.cashUpError = false;

				 if (flag === 0)
					 return true && $scope.cashUpError;
				 else {
					 alert("Please Select Region of selected states");
					 return false && $scope.cashUpError;
				 }
			 }
			 function createCountryValidations() {
				 if ($scope.country === null
						 || $scope.country.countryName.trim() === ""
							 || $scope.country.countryName === undefined)
					 $scope.countrySelect = false;
				 else
					 $scope.countrySelect = true;
				 if ($scope.countrySelect === false)
					 $('#countrySelectButton').css('border',
					 '1px solid red');
				 else
					 $('#countrySelectButton').css('border',
					 '1px solid #ccc');
				 return $scope.countrySelect;
			 }

			 $scope.serviceHit = function() {

				 type = countryRow !== null ? "update" : "create";
				 countryName = $scope.country.countryName;
				 if (type === "update")
					 countryName = $scope.countryName;
				 getServiceCall();
			 };
			 $scope.cancel = function() {
				 $state.go('AdminBoard.ManageCountry');
			 }

			 $scope.showDropdown = function(pos, regName) {
				 var id = '#' + pos + "regDropdown";
				 if (regName !== null && regName !== undefined
						 && regName.trim() !== "")
					 $(id).css('display', 'block');
				 else
					 $(id).css('display', 'none');

			 }
			 $scope.showNewRegDropdown = function() {
				 var text = $('#regionSelect').val();
				 if (text !== null && text !== undefined
						 && text.trim() !== "")
					 $('#newRegDropDown')
					 .css('display', 'block');
				 else
					 $('#newRegDropDown').css('display', 'none');
			 };
			 $(document).click(function() {
				 $('.regSearch').css("display", "none");
				 $('#newRegDropDown').css("display", "none");
			 });
			 $(".dropdown-menu").on('show.bs.dropdown',
					 function() {
				 $("#searchCountry").focus();
			 });
			 $("#countrySelectButton").click(function() {
				 setTimeout(function() {
					 $("#searchCountry").focus();
				 }, 100);
			 });
			 function getCountryObj(countryName) {
				 for ( var i = 0; i < $scope.country.countryNameList.length; i++) {
					 if ($scope.country.countryNameList[i].countryName === countryName) {
						 return $scope.country.countryNameList[i];
					 }
				 }
			 }
			 function getCountryCode(countryName) {
				 for ( var i = 0; i < $scope.country.countryNameList.length; i++) {
					 if ($scope.country.countryNameList[i].countryName === countryName) {
						 return $scope.country.countryNameList[i].countryCode;
					 }
				 }
			 }

		 } ]);
