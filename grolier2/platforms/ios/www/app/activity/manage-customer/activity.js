app.controller("ManageCustomerCtrl", [
		'$scope',
		'$state',
		'$uibModal',
		'localStorageService',
		'adapter',
		'validate',
		'adjustHeight',
		'$compile',
		function($scope, $state, $uibModal, localStorageService, adapter,
				validate, adjustHeight, $compile) {

			$scope.ShowCustomerPage = function(row) {
				localStorageService.set('customerCode', row.customerCode);
				$state.go('AdminBoard.EditCustomer');
			}
		} ]);