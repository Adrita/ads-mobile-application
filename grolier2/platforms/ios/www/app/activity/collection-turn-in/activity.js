app
.controller(
		"CoTurnInCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'adapter',
		 'validate',
		 'adjustHeight',
		 '$compile',
		 'ModalService',
		 'Session',
		 function($scope, $state, $uibModal,
				 localStorageService, adapter, validate,
				 adjustHeight, $compile, ModalService, Session) {
			 adjustHeight.adjustTopBar();
			 $scope.historyTab = true;
			 $scope.Clearance = false;
			 $scope.bankIn = false;
			 $scope.clearanceReset = false;
			 $scope.clearanceSelect = false;
			 $scope.threeDotted = true;
			 $scope.perPage = true;
			 $scope.image = true;
			 var type = "";
			 $scope.totalOutStandingCreditCard = 0;
			 $scope.totalOutStandingCash = 0;
			 $scope.totalOutStandingCheque = 0;
			 $scope.$parent.gridOptions.data = [];
			 $scope.common = {};
			 $scope.common.currency = currency; // from global
			 // js
			 $scope.typeCash = [];
			 $scope.typeCredit = [];
			 $scope.typeCheque = [];
			 $scope.rowWidth = [];
			 $scope.cashSelect = false;
			 $scope.chequeSelect = false;
			 $scope.cardSelect = false;
			 $scope.tempArr = [];
			 $scope.bankInCash=0;
			 $scope.cashInHand=0;
			 $scope.chequeInHand=0;
			 $scope.bankInCheque=0;
			 var paymentIdListCalling=[];
			 $scope.count=0;

			 $scope.$on('ngRepeatFinished', function(
					 ngRepeatFinishedEvent) {
				 $('[data-toggle="popover"]').popover();

			 });

			 $scope.loader = false;
			 $scope.recordType = "Unassigned";
			 $('#History').addClass('tablink_active');
			 $('#PendingClearance')
			 .removeClass('tablink_active');
			 $scope.subTableHeadings = {
					 "contractCode" : {
						 "title" : "Contract Code",
						 "width" : "10%"
					 },
					 "typePayment" : {
						 "title" : "Payment Type",
						 "width" : "10%"
					 },
					 "sumOfPaymentAmt" : {
						 "title" : "Amount",
						 "width" : "10%"
					 },
					 "chqDate" : {
						 "title" : "Cheque Date",
						 "width" : "10%"
					 },
					 "chqNumber" : {
						 "title" : "Cheque Number",
						 "width" : "10%"
					 },
					 "TransactionNumber" : {
						 "title" : "Bank-in slip/Transaction No.",
						 "width" : "15%"
					 },
					 "turnedInBy" : {
						 "title" : "Cleared by",
						 "width" : "15%"
					 },
					 "message" : {
						 "title" : "Remarks",
						 "width" : "13%"
					 },
					 "statusPayment" : {
						 "title" : "Status",
						 "width" : "12%"
					 }

			 };

			 $scope.openGrid = function(openTab) {
				 if (openTab === 'history') {
					 $scope.historyTab = true;
					 $scope.Clearance = false;
					 $scope.threeDotted = true;
					 $scope.perPage = true;
					 $scope.$parent.colInfo = {
							 "datePayment" : {
								 "title" : "Date",
								 "width" : "10%"
							 },
							 "count" : {
								 "title" : "Number Of Transactions",
								 "width" : "20%"
							 },
							 "processedBy" : {
								 "title" : "Processed By",
								 "width" : "20%"
							 },
							 "sumOfPaymentAmt" : {
								 "title" : "Total Amount",
								 "width" : "20%"
							 },
							 "batchNumber" : {
								 "title" : "Batch Number",
								 "width" : "20%"
							 }
					 };
					 $('#History').addClass('tablink_active');
					 $('#PendingClearance').removeClass(
					 'tablink_active');
					 type = "manageHistory";
					 $scope.loader = true;
					 getServiceCall();
				 }
				 if (openTab === "pending") {
					 $scope.bankIn = false;
					 $scope.historyTab = false;
					 $scope.Clearance = true;
					 $scope.threeDotted = true;
					 $scope.perPage = true;
					 $scope.image = true;
					 $scope.$parent.colInfo = {
							 "datePayment" : {
								 "title" : "Date",
								 "width" : "15%"
							 },
							 "sumOfPaymentAmt" : {
								 "title" : "Amount",
								 "width" : "15%"
							 },
							 "ChequeDate" : {
								 "title" : "Cheque Date",
								 "width" : "10%"
							 },
							 "ChequeNumber" : {
								 "title" : "Cheque Number",
								 "width" : "15%"
							 },
							 "TransactionNumber" : {
								 "title" : "Bank-in slip/Transaction No.",
								 "width" : "15%"
							 },
							 "message" : {
								 "title" : "Remarks",
								 "width" : "15%"
							 },
							 "statusPayment" : {
								 "title" : "Bank-in",
								 "width" : "10%"
							 }
					 };
					 $scope.rowWidth = [ "5%", "15%", "15%",
					                     "10%", "15%", "15%", "15%", "10%" ];
					 type = "getPendingClearance";
					 $scope.loader = true;
					 getServiceCall();

				 }
			 }

			 $scope.openGrid('history');

			 function getServiceCall() {

				 var data = {};
				 var url = "";
				 if (type === 'getPendingClearance') {
					 url = "CollectorManageTurnInController/pendingClearance";
					 data = {
							 userId : Session.userId
					 };
				 } else if (type === 'manageHistory') {
					 url = "CollectorManageTurnInController/manageHistory";
					 data = {
							 userId : Session.userId
					 };
				 }

				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {
					 if (result.statusCode === 200)
					 {

						 if (type === 'manageHistory') {
							 $scope.$parent.gridOptions.data = result.listTurnIn;
							 $scope.$parent.colData = result.listTurnIn;
							 $scope.$parent.serviceData = result.listTurnIn;
							 $scope.$parent.configGrid();
							 $scope.totalOutstandingCash = result.totalOutstandingCash;
							 $scope.totalOutstandingCheque = result.totalOutstandingCheque;
							 $scope.totalOutstandingCreditCard = result.totalOutstandingCreditCard;
							 $scope.totalBankedInCash = result.totalBankedInCash;
							 $scope.totalBankedInCheque = result.totalBankedInCheque;
							 $scope.totalBankedInCreditCard = result.totalBankedInCreditCard;
							 $scope.totalSubmittedCash=result.totalSubmittedCash;
							 $scope.totalSubmittedCheque=result.totalSubmittedCheque;
							 $scope.totalSubmittedCreditCard=result.totalSubmittedCreditCard;
							 $scope.totalReceiveCashSummary=result.totalReceiveCashSummary;
							 $scope.totalReceiveChequeSummary=result.totalReceiveChequeSummary;
							 $scope.totalReceiveCreditCardSummary=result.totalReceiveCreditCardSummary;
							 
							 
							 
							 
							 $scope.turnInHistoryData = result.listTurnIn;
							 localStorageService .set("manageHistory",result.listTurnIn);
							 $scope.loader = false;
						 }
						 if (type === 'getPendingClearance') {
							 for(var i=0;i<result.listTurnIn.length;i++)
									result.listTurnIn[i].rowId=i;
							 
							 $scope.$parent.gridOptions.data = result.listTurnIn;
							 $scope.$parent.colData = result.listTurnIn;
							 $scope.$parent.serviceData = result.listTurnIn;
							 $scope.totalOutstandingCash = result.totalOutstandingCash;
							 $scope.totalOutstandingCheque = result.totalOutstandingCheque;
							 $scope.totalOutstandingCreditCard = result.totalOutstandingCreditCard;
							 $scope.totalBankedInCash = result.totalBankedInCash;
							 $scope.totalBankedInCheque = result.totalBankedInCheque;
							 $scope.totalBankedInCreditCard = result.totalBankedInCreditCard;
							 $scope.totalSubmittedCash=result.totalSubmittedCash;
							 $scope.totalSubmittedCheque=result.totalSubmittedCheque;
							 $scope.totalSubmittedCreditCard=result.totalSubmittedCreditCard;
							 $scope.totalReceiveCashSummary=result.totalReceiveCashSummary;
							 $scope.totalReceiveChequeSummary=result.totalReceiveChequeSummary;
							 $scope.totalReceiveCreditCardSummary=result.totalReceiveCreditCardSummary;
							 $scope.$parent.configGrid();
							 checkTotal(result.listTurnIn);
							 $scope.loader = false;
						 }
					 }else{
						 $scope.loader = false;
					 }
				 }
				 function error() {
					 console.log("Service not resolved");
					 $scope.loader = false;
				 }

			 }

			 function updateTotal(gridData) {
				 $scope.totalOutStandingCreditCard = 0;
				 $scope.totalOutStandingCash = 0;
				 $scope.totalOutStandingCheque = 0;
				 angular
				 .forEach(
						 gridData,
						 function(value, key) {
							 if (value.typePayment === 'Credit Card') {
								 $scope.totalOutStandingCreditCard += parseFloat(value.sumOfPaymentAmt);
							 } else if (value.typePayment === 'Cash') {
								 $scope.totalOutStandingCash += parseFloat(value.sumOfPaymentAmt);
							 } else {
								 $scope.totalOutStandingCheque += parseFloat(value.sumOfPaymentAmt);
							 }
						 });
			 }

			 function updateHistoryTotal(gridData) {
				 $scope.totalOutStandingCreditCard = 0;
				 $scope.totalOutStandingCash = 0;
				 $scope.totalOutStandingCheque = 0;
				 angular
				 .forEach(
						 gridData,
						 function(value, key) {
							 angular
							 .forEach(
									 gridData[key].listTurnIn,
									 function(
											 value,
											 key) {
										 if (value.typePayment === 'Credit Card') {
											 $scope.totalOutStandingCreditCard += parseFloat(value.sumOfPaymentAmt);
										 } else if (value.typePayment === 'Cash') {
											 $scope.totalOutStandingCash += parseFloat(value.sumOfPaymentAmt);
										 } else {
											 $scope.totalOutStandingCheque += parseFloat(value.sumOfPaymentAmt);
										 }
									 });

						 });
			 }

			 $scope.concatanateString = function(str) {
				 return str.replace(/\s+/g, '-').toLowerCase();
			 }

			 /*$scope.collapseExpand = function(pos) {
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
					 if ($scope.$parent.gridOptions.data[i].id === parseInt(pos))
						 $scope.$parent.gridOptions.data[i].showSubTable = !$scope.$parent.gridOptions.data[i].showSubTable;
				 }

			 }*/
			 
			 
			 
			 $scope.collapseExpand = function(pos) {
				 for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
					 if (i === parseInt(pos)) {
						 if ($scope.turnInHistoryData[i].showSubTable === undefined
								 || $scope.turnInHistoryData[i].showSubTable === null)
							 $scope.turnInHistoryData[i].showSubTable = false;
						 $scope.turnInHistoryData[i].showSubTable = !$scope.turnInHistoryData[i].showSubTable;
					 }

				 }

			 }

			 $scope.selectAll = function() {
				 if ($scope.Clearance) {
					 $scope.image = false;
					 $scope.threeDotted = false;
					 $scope.perPage = false;
					 $scope.clearanceReset = true;
					 $scope.clearanceSelect = true;
					 $scope.bankIn = true;
					 $scope.bulkSelect = true;
					 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
						 $scope.$parent.gridOptions.data[i].Selected = true;
				 }
			 }

			 $scope.reset = function() {
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
					 $scope.$parent.gridOptions.data[i].Selected = false;
				 $scope.bulkSelect = false;
				 $scope.bankIn = false;
				 $scope.clearanceReset = false;
				 $scope.clearanceSelect = false;
				 $scope.threeDotted = true;
				 $scope.perPage = true;
				 $scope.image = true;
				 $scope.cashSelect = false;
				 $scope.chequeSelect = false;
				 $scope.cardSelect = false;
			 }

			 $scope.checkUncheck = function() {
				 tempArr = [];
				 var obj = {};
				 if ($scope.bulkSelect) {
					 $scope.cashSelect = true;
					 $scope.chequeSelect = true;
					 $scope.cardSelect = true;
					 $scope.cashSelectAll();
					 $scope.chequeSelectAll();
					 $scope.creditCardSelectAll();
				 } else {
					 $scope.cashSelect = false;
					 $scope.chequeSelect = false;
					 $scope.cardSelect = false;
					 $scope.bankIn = false;
					 $scope.clearanceReset = false;
					 $scope.clearanceSelect = false;
					 $scope.threeDotted = true;
					 $scope.perPage = true;
					 $scope.image = true;
					 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
						 $scope.$parent.gridOptions.data[i].Selected = false;
				 }

				
				
				
				

			 }
			 $scope.cashSelectAll=function()
			 {
				 var tempArr=[];
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
					 if ($scope.$parent.gridOptions.data[i].typePayment
							 .toLowerCase() === 'cash') {
						 if ($scope.cashSelect && $scope.$parent.gridOptions.data[i].statusPayment.toLowerCase()==='with co' )
							 $scope.$parent.gridOptions.data[i].Selected = true;
						 else
							 $scope.$parent.gridOptions.data[i].Selected = false;
					 }

				 }
				
			 }
			 $scope.chequeSelectAll=function()
			 {
				 var tempArr=[];
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
					 if ($scope.$parent.gridOptions.data[i].typePayment
							 .toLowerCase() === 'cheque') {
						 if ($scope.chequeSelect && $scope.$parent.gridOptions.data[i].statusPayment.toLowerCase()==='with co')
							 $scope.$parent.gridOptions.data[i].Selected = true;
						 else
							 $scope.$parent.gridOptions.data[i].Selected = false;
					 }

				 }
				 
			 }
			 $scope.creditCardSelectAll=function()
			 {
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
					 if ($scope.$parent.gridOptions.data[i].typePayment
							 .toLowerCase() === 'credit card') {
						 if ($scope.cardSelect)
							 $scope.$parent.gridOptions.data[i].Selected = true;
						 else
							 $scope.$parent.gridOptions.data[i].Selected = false;
					 }

				 }
			 }
			

			 $scope.bankInPage = function(datePayment) {
				 localStorageService.set('paymentIdListCalling',
						 paymentIdListCalling);
				 $state.go('CoBoard.CollectorBankIn');
			 }
			 
			 $scope.singleSubmit=function(row)
             {
				 for(var i = 0; i < $scope.$parent.gridOptions.data.length; i++){
					 if(row.id===$scope.$parent.gridOptions.data[i].id){
						 $scope.$parent.gridOptions.data[i].Selected=true;
						 break;
					 }
						 
				 }
                 
                 var tempArr=[];
                  $scope.count=0;
                  var cashArr=[];
                  for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                       if(row.id===$scope.$parent.gridOptions.data[i].id){
                       		if ($scope.$parent.gridOptions.data[i].typePayment.toLowerCase()!=='cash') {
                          obj = {
                                  'paymentId' : $scope.$parent.gridOptions.data[i].id,
                                  'datePayment' : $scope.$parent.gridOptions.data[i].datePayment,
                                  'typePayment':$scope.$parent.gridOptions.data[i].typePayment,
                                  'paymentIdList':$scope.$parent.gridOptions.data[i].paymentIdList
                          };
                          
                          tempArr.push(obj);
                          $scope.count++;
                      }
					   if ($scope.$parent.gridOptions.data[i].typePayment.toLowerCase() === 'cash') {
						   if ($scope.$parent.gridOptions.data[i].paymentIdList != null) {
							   for (var j = 0; j < $scope.$parent.gridOptions.data[i].paymentIdList.length; j++) {
								obj = {
									'paymentId' : $scope.$parent.gridOptions.data[i].id,
									'datePayment' : null,
									'typePayment' : $scope.$parent.gridOptions.data[i].typePayment,
									'paymentIdList':$scope.$parent.gridOptions.data[i].paymentIdList
								};
		
								tempArr.push(obj);
								$scope.count++;
							}
						   } 
						   else {
							obj = {
								'paymentId' : 0,
								'datePayment' : null,
								'typePayment' : $scope.$parent.gridOptions.data[i].typePayment,
								'paymentIdList':$scope.$parent.gridOptions.data[i].paymentIdList
							};
							tempArr.push(obj);
							$scope.count++;
						}
		
					}
                      


                  }
                  localStorageService.set('cashRecordsCoTurnIn',cashArr);
                  paymentIdListCalling=tempArr;
                  localStorageService.set('paymentIdListCalling',paymentIdListCalling);
                 $scope.submitPayment();
             }
             }

			 
			 $scope.submitPayment=function()
				{
					
							$state.go('CoBoard.CollectorBankIn');
						
				}
				
			 
			 $scope.filterPaymentType = function(type) {
				 var filterData = [];
				 if (type === 'cash') {
					 for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
						 if ($scope.$parent.gridOptions.data[i].typePayment
								 .toLowerCase() === 'cash')
							 filterData
							 .push($scope.$parent.gridOptions.data[i]);
					 }
				 } else if (type === 'credit card') {
					 for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
						 if ($scope.$parent.gridOptions.data[i].typePayment
								 .toLowerCase() === 'credit card')
							 filterData
							 .push($scope.$parent.gridOptions.data[i]);
					 }
				 } else if (type === 'cheque') {
					 for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
						 if ($scope.$parent.gridOptions.data[i].typePayment
								 .toLowerCase() === 'cheque')
							 filterData
							 .push($scope.$parent.gridOptions.data[i]);
					 }
				 }
				 return filterData;
			 }
			 $scope
			 .$watch(
					 '$parent.gridOptions.data',
					 function() {
						 console
						 .log("Change in GridOptions");
						 if (type === "getPendingClearance") {
							 $scope.typeCash = $scope
							 .filterPaymentType('cash');
							 $scope.typeCredit = $scope
							 .filterPaymentType('credit card');
							 $scope.typeCheque = $scope
							 .filterPaymentType('cheque');
							 var tempArr=[];
							 var cashArr=[];
							 $scope.count=0;
							 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
								 if ($scope.$parent.gridOptions.data[i].Selected && $scope.$parent.gridOptions.data[i].typePayment.toLowerCase()!=='cash') {
									 obj = {
											 'paymentId' : $scope.$parent.gridOptions.data[i].id,
											 'datePayment' : $scope.$parent.gridOptions.data[i].datePayment,
											 'typePayment':$scope.$parent.gridOptions.data[i].typePayment,
											 'paymentIdList':$scope.$parent.gridOptions.data[i].paymentIdList
									 };
									 tempArr.push(obj);
									 $scope.count++;
								 }
								 if($scope.$parent.gridOptions.data[i].typePayment.toLowerCase()==='cash' && $scope.$parent.gridOptions.data[i].Selected){
									 if($scope.$parent.gridOptions.data[i].paymentIdList!=null){
										 for(var j=0;j<$scope.$parent.gridOptions.data[i].paymentIdList.length;j++){
								 			 obj = {
											  'paymentId' : $scope.$parent.gridOptions.data[i].id,
											  'datePayment' : null,
											  'typePayment':$scope.$parent.gridOptions.data[i].typePayment,
											  'paymentIdList':$scope.$parent.gridOptions.data[i].paymentIdList
										};
                          				tempArr.push(obj);
                          				$scope.count++;
								 		}
									 }
									 else{
										 obj = {
												  'paymentId' : 0,
												  'datePayment' : null,
												  'typePayment':$scope.$parent.gridOptions.data[i].typePayment,
												  'paymentIdList':$scope.$parent.gridOptions.data[i].paymentIdList
											};
										 tempArr.push(obj);
	                          			  $scope.count++;
									 }
								 		
												
								 }

							 }
							 localStorageService.set('cashRecordsCoTurnIn',cashArr);
							 paymentIdListCalling=tempArr;
							 localStorageService.set('paymentIdListCalling',paymentIdListCalling);
						 }

					 }, true);

			 $scope.status = function(status) {
				 if (status.toUpperCase() === 'WITH CO') {
					 return true;
				 } else
					 return false;
			 }
			 
			 
			 
			 
			 $scope.contains = function(paymentType) {
					for ( var i = 0; i < $scope.paymentList.length; i++) {
						if (paymentType.toUpperCase() === $scope.paymentList[i]
								.toUpperCase()) {
							return true;
						}
					}
				}
			 
			 
			 
			 $scope.getOpenTabs = function() {
					var opentabs = [];
					for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
						opentabs
								.push($scope.turnInHistoryData[i].showSubTable);
					}
					return opentabs;
				}
			 
			 
			 
			 $scope
			 .$on(
					 'eventName',
					 function(event, args) {
						 if (type === "manageHistory") {
							 $scope.paymentList = args.paymentOptions;
							 $scope.turnInStatus=args.turnInStatus;
							 var opentabs = $scope
								.getOpenTabs();
							 var tempArr = [];
							 $scope.turnInHistoryData = localStorageService
							 .get("manageHistory");
							 if ($scope.paymentList.length > 0) {
								 for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
									 tempArr = [];
									 $scope.turnInHistoryData[i].showSubTable = opentabs[i];
									 for ( var j = 0; j < $scope.turnInHistoryData[i].listTurnIn.length; j++) {
									 		var typeOfPayment=$scope.turnInHistoryData[i].listTurnIn[j].typePayment;
										 if ($scope.contains(typeOfPayment)) {
											 tempArr.push($scope.turnInHistoryData[i].listTurnIn[j]);
										 }
									 }
									 $scope.turnInHistoryData[i].listTurnIn = tempArr;
								 }

							 }else if($scope.turnInStatus.length>0)
								{
									for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
										tempArr = [];
										$scope.turnInHistoryData[i].showSubTable = opentabs[i];
										for ( var j = 0; j < $scope.turnInHistoryData[i].listTurnIn.length; j++) {
											if ($scope
													.containsStatus($scope.turnInHistoryData[i].listTurnIn[j].statusPayment)) {
												tempArr
														.push($scope.turnInHistoryData[i].listTurnIn[j]);
											}
										}
										
										if ($scope
												.containsStatus($scope.turnInHistoryData[i].statusPayment)) {
											tempArr
													.push($scope.turnInHistoryData[i]);
										}
										$scope.turnInHistoryData[i].listTurnIn = tempArr;
									
									}
								} else
								 {
								 
								 var opentabs = $scope
									.getOpenTabs();
								 $scope.turnInHistoryData = localStorageService.get("manageHistory");
								 for ( var i = 0; i < $scope.turnInHistoryData.length; i++)
								 $scope.turnInHistoryData[i].showSubTable = opentabs[i];
								
						 }
						 }
					 });
			 
			 $scope.containsStatus=function(turnInStatus)
				{
					for ( var i = 0; i < $scope.turnInStatus.length; i++) {
						if (turnInStatus.toUpperCase() === $scope.turnInStatus[i]
								.toUpperCase()) {
							return true;
						}
					}
				}
			 
			 
			 function checkTotal(data){
				 $scope.bankInCash=0;
				 $scope.cashInHand=0;
				 $scope.bankInCheque=0;
				 $scope.chequeInHand=0;
				 for(var i=0;i<data.length;i++){
					 if(data[i].typePayment==="Cash"){
						 if(data[i].statusPayment.indexOf("Banked")>=0){
							 $scope.bankInCash=parseFloat($scope.bankInCash)+parseFloat(data[i].sumOfPaymentAmt);
						 }else if(data[i].statusPayment.indexOf("With")>=0){
							 $scope.cashInHand=parseFloat($scope.cashInHand)+parseFloat(data[i].sumOfPaymentAmt);
						 }
					 }else if(data[i].typePayment==="Cheque"){
						 if(data[i].statusPayment.indexOf("Banked")>=0){
							 $scope.bankInCheque=parseFloat($scope.bankInCheque)+parseFloat(data[i].sumOfPaymentAmt);
						 }else if(data[i].statusPayment.indexOf("With")>=0){
							 $scope.chequeInHand=parseFloat($scope.chequeInHand)+parseFloat(data[i].sumOfPaymentAmt);
						 }
					 }
				 }
			 }
			 $scope.selectCheckBox=function(row,pos,type){
				 if(row.statusPayment.toLowerCase==='with co' && type==='cash')
					 $scope.typeCash[pos].Selected=false;
				 else if(row.statusPayment.toLowerCase==='with co' && type==='cheque')
					 $scope.typeCheque[pos].Selected=false;
			 }
		 } ]);
