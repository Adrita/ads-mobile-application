app
    .controller(
        "CoTurnInCtrlMobile", [
        	'$rootScope',
            '$scope',
            '$state',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'ModalService',
            'Session',
            '$cordovaNetwork',
            'syncService',
            function($rootScope,$scope, $state, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, ModalService, Session, $cordovaNetwork, syncService) {

                $scope.paymentTypeList = ["Cash", "Cheque", "Credit Card"];
                $scope.paymentType = $scope.paymentTypeList[0];

				$scope.historyTab=true;
				$scope.Clearance=false;
				$scope.detailsView=false;
				var type="";

				$scope.isOnline=$cordovaNetwork.isOnline();

				if($scope.isOnline){
					type="historyTab";
				}else{

					$scope.historyTab=false;
					$scope.Clearance=true;
					type="pendingClearance";
				}


				$scope.$on('$cordovaNetwork:online', function(event, networkState){

					if(!$scope.isOnline){
						//
						$scope.isOnline=true;

						$rootScope.$broadcast('connectionEstablished',true);
						$state.reload();

					}
				});

				$scope.$on('$cordovaNetwork:offline', function(event, networkState){
					$scope.isOnline=false;
					$state.reload();
				});


				getServiceCall(type);


                function getServiceCall(type) {
                    $scope.loader = true;
                    var url = "";
                    var data = {};

                    if (type === 'pendingClearance') {
                        url = "CollectorManageTurnInController/pendingClearance";
                        data = {
                            userId: Session.userId
                        };
                    }else if(type==='historyTab'){
						url = "CollectorManageTurnInController/manageHistory";
						 data = {
								 userId : Session.userId
						 };
                     }else if(type==='Summary'){
						url="CollectorManageTurnInController/getSummaryValue";
						data={
								userId:Session.userId
							}
					}

                    adapter.getServiceData(url, data).then(success, error);

                    function success(result) {
                        $scope.loader = false;
                        //$scope.formatSummarySection(result);


                        if(type==='pendingClearance'){
                        	for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++)
									result.voBankInTurnInPendingClearanceRows[i].id = i;
							$scope.formatGridData(result.voBankInTurnInPendingClearanceRows);

							//app.saveDataIntoFile('saveVerificationTurnInPending',result);
							localStorageService.set('internetConnected',true);

							type = 'Summary';
							getServiceCall(type);
							//$scope.formatGridData(result.listTurnIn);
							//app.saveCollectorTurnIn(result);
							app.saveDataIntoFile('saveCollectorTurnIn',result);
                        }else if(type=== 'historyTab' && result.statusCode === 200){
							 $scope.turnInHistory = result.listTurnIn;

							 localStorageService.set( 'voTurnInHistory', result.voBankInTurnInHistoryRowList);
							 localStorageService.set('internetConnected',true);

							 type = "Summary";
							 getServiceCall(type);
						 }else if (type === 'Summary' && result.statusCode === 200) {
							  $scope.cashSummary = result.cashSummary;
							  $scope.chequeSummary = result.chequeSummary;
							  $scope.creditCardSummary = result.creditCardSummary;
							  localStorageService.set('internetConnected',true);

							  app.saveDataIntoFile('saveCOSummary',result);


							  checkForLocation();
							  //app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);

						  }


                    }

                    function error() {
                        $scope.loader = false;
                        //app.fetchCollectorTurnIn(successCallback, errorCallback);

                        localStorageService.set('internetConnected',false);

                        if(type==='pendingClearance'){
                        	app.fetchDataFromFile('saveCollectorTurnIn',successCallback,errorCallback);
                        }else if(type==='Summary'){
							//app.fetchSummaryVO(successFetchSummary,errorCallback);
							app.fetchDataFromFile('saveCOSummary',successFetchSummary,errorCallback);
						 }

                    }
                }

                function checkForLocation(){
                	if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, {
							timeout: 30000
						});
					} else {
						alert("Geolocation is not supported");
					}

					function geolocationSuccess(position) {
                    //				alert('Latitude: ' + position.coords.latitude + '\n' +
                    //					'Longitude: ' + position.coords.longitude + '\n' +
                    //					'Altitude: ' + position.coords.altitude + '\n' +
                    //					'Accuracy: ' + position.coords.accuracy + '\n' +
                    //					'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                    //					'Heading: ' + position.coords.heading + '\n' +
                    //					'Speed: ' + position.coords.speed + '\n' +
                    //					'Timestamp: ' + position.timestamp + '\n');


						localStorage.setItem('lat',position.coords.latitude);
						localStorage.setItem('lon',position.coords.longitude);


							app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);


					}

					function geolocationError(error) {
                    				switch (error.code) {
                    					case error.PERMISSION_DENIED:
                    						alert("User denied the request for Geolocation.");
                    						break;
                    					case error.POSITION_UNAVAILABLE:
                    						alert("Location information is unavailable.");
                    						break;
                    					case error.TIMEOUT:
                    						alert("The request to get user location timed out.");
                    						break;
                    					case error.UNKNOWN_ERROR:
                    						alert("An unknown error occurred.");
                    						break;
                    				}
					}

                }

                function successCollectorTurnIn(result) {

					if (result.length > 0) {
						//    		navigator.geolocation.getCurrentPosition(geolocationSuccess,geolocationError,[geolocationOptions]);
//						$state.reload();
						var data = {
							deviceId: localStorage.getItem('deviceId'),
							lat: "" + localStorage.getItem('lat'),
							lon: "" + localStorage.getItem('lon'),
							offlineBankInRecords: result
						}
						syncService.syncCollectorBankIn(data);
					}

				}

				function errorCollectorTurnIn(result) {
				}


                $scope.formatGridData=function(listTurnIn){

                		 		$scope.cashDetailsList=[];
                		 		$scope.chequeDetailsList=[];

                		 		_.forEach(listTurnIn,function(value,key){
                		 			if(value.paymentMethod==="Cash"){
                		 				$scope.cashDetailsList.push(value);
                		 			}else if(value.paymentMethod==="Cheque"){
                		 				$scope.chequeDetailsList.push(value);
                		 			}
                		 		})
                		 	}

                $scope.concatanateString = function(str) {
                    return str.replace(/\s+/g, '-').toLowerCase();
                }


                $scope.singleSubmit=function(row){
                				 if(row.paymentMethod==='Cheque' && row.chequeDate){
                                     var tempArr=[];
                                     var diff=0;
                                     var currentDate = new Date();
                                     currentDate.setMilliseconds(0);
                                     currentDate.setMinutes(0);
                                     currentDate.setHours(0);
                                     diff = currentDate - new Date(row.chequeDate);

                                     if(diff>=0 && row.bankedInBy==='With CO'){
                                         tempArr.push(row);
                                     }

                                     if(tempArr && tempArr.length>0){
                                         localStorageService.set('voTurnInData', tempArr);
                                         $state.go('CoBoard.CollectorBankIn');
                                     }
                                 }else if(row.paymentMethod==='Cash'){
                                     var tempArr=[];
                                     if(row.bankedInBy==='With CO'){
                                         tempArr.push(row);
                                         localStorageService.set('voTurnInData',tempArr);
                                         $state.go('CoBoard.CollectorBankIn');
                                     }

                                 }
                			 	//$state.go('CoBoard.CollectorBankIn');
                }

                $scope.bankIn=function(){
                				 if($scope.paymentType==='Cash' && $scope.cashDetailsList){

                                     var tempArr=[];
                                     angular.forEach($scope.cashDetailsList,function(value,key){
                                         if(value.bankedInBy==='With CO'){
                                             tempArr.push(value);
                                         }
                                     });

                                     localStorageService.set('voTurnInData', tempArr);
                                     $state.go('CoBoard.CollectorBankIn');
                                 }else if($scope.paymentMethod==='Cheque' && $scope.chequeDetailsList){
                                     var tempArr=[];
                                     var diff=0;

                                     angular.forEach($scope.chequeDetailsList,function(value,key){
                                         if(value.chequeDate){
                                             var currentDate = new Date();
                                             currentDate.setMilliseconds(0);
                                             currentDate.setMinutes(0);
                                             currentDate.setHours(0);
                                             diff = currentDate - new Date(value.chequeDate);

                                             if(diff>=0 && value.bankedInBy==='With CO'){
                                                 tempArr.push(value);
                                             }
                                         }

                                     });

                                     if(tempArr && tempArr.length>0){
                                         localStorageService.set('voTurnInData', tempArr);
                                         $state.go('CoBoard.CollectorBankIn');
                                     }

                                 }
                			 }

//                $scope.formatBankInRequest = function(requestList) {
//                    var obj = {};
//                    var paymentIdListCalling = [];
//                    _.forEach(requestList, function(value, key) {
//                        if (value.statusPayment !== 'Banked In By CO') {
//                            if (value.typePayment.toLowerCase() !== 'cash') {
//                                obj = {
//                                    'paymentId': value.id,
//                                    'datePayment': value.datePayment,
//                                    'typePayment': value.typePayment,
//                                    'paymentIdList': value.paymentIdList
//                                };
//                                paymentIdListCalling.push(obj);
//                            } else if (value.typePayment.toLowerCase() === 'cash') {
//
//                                if (value.paymentIdList != null) {
//                                    obj = {
//                                        'paymentId': value.id,
//                                        'datePayment': null,
//                                        'typePayment': value.typePayment,
//                                        'paymentIdList': value.paymentIdList
//                                    }
//                                } else {
//                                    obj = {
//                                        'paymentId': 0,
//                                        'datePayment': null,
//                                        'typePayment': value.typePayment,
//                                        'paymentIdList': value.paymentIdList
//                                    }
//                                }
//
//                                paymentIdListCalling.push(obj);
//
//                            }
//
//                        }
//                    });
//                    localStorageService.set('paymentIdListCalling', paymentIdListCalling);
//                    $state.go('CoBoard.CollectorBankIn');
//
//
//                }


                function successCallback(result) {

                    //$scope.formatSummarySection(result);
//                    $scope.formatGridData(result.listTurnIn);
//                    $scope.$apply();
					for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++)
							result.voBankInTurnInPendingClearanceRows[i].id = i;
					$scope.formatGridData(result.voBankInTurnInPendingClearanceRows);

					$scope.$apply();

					type = 'Summary';
					getServiceCall(type);

                }

                function successFetchSummary(result){
                	$scope.cashSummary = result.cashSummary;
					$scope.chequeSummary = result.chequeSummary;
					$scope.creditCardSummary = result.creditCardSummary;

					$scope.$apply();
                }

                function errorCallback(message) {
                    alert(message);
                }


                $scope.viewDetails=function(row){
					$scope.detailsView=true;
					$scope.detailList=row;
				}
				$scope.backToTurnIn=function(){
					$scope.detailsView=false;
				}

				$scope.openGrid=function(tabName){
                		 		if(tabName==='history'){
                		 			$scope.historyTab=true;
                		 			$scope.Clearance=false;
                		 			type="historyTab";
                		 			getServiceCall(type);
                		 		}else{
                		 			$scope.historyTab=false;
                		 			$scope.Clearance=true;
                		 			type="pendingClearance";
                		 			getServiceCall(type);
                		 		}
                		 	}




            }
        ]);