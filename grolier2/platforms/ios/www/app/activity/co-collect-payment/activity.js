app
    .controller(
        "CoCollectPaymentCtrl", [
            '$scope',
            '$state',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'Session',
            function($scope, $state, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, Session) {
                adjustHeight.adjustTopBar();
                $scope.collectPaymentForm = {};
                $scope.status_collect = true;
                var data;
                var url;
                $scope.status_collect = true;
                $scope.submitPressed = false;
                $scope.past6monthError=true;
                $scope.common = {};
                $scope.common.currency = currency; // from global
                // js
                $scope.paymentDisable = false;
                var collectType;
                var appInUse = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;
                var loggedinUser = Session.userGlobalRole;
                /* MODAL IMPLEMENTATION */
                $scope.animationsEnabled = true;

                $scope.open = function() {
                    var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'app/modals/role/modal.html',
                            controller: 'modalCtrl',
                            windowClass: 'center-modal fixPosition-xs',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'md'
                        }).result
                        .then(function(data) {
                            if (data === "close") {
                                $state
                                    .go('CoBoard.ManageCollectorCollectionRequest');
                            }
                            if (data === "createAnother") {
                                $state.reload();
                            }
                        });
                };

                var errorName = "Contract";

                function modalOpen(modalType) {
                    if (errorName !== null && errorName !== "") {
                        if (modalType === "success") {
                            var modalObj = {
                                "modalEntity": modalEntity,
                                "modalMessge": modalMessge,
                                "body": modalBody,
                                "backLink": "Back to Manage Collector Collection Request",
                                "modalBtnText": "",
                                "singleBtn": true
                            };
                        } else {
                            var modalObj = {
                                "modalEntity": "Failed!! ",
                                "body": errorMessage,
                                "backLink": "Back to Manage Collector Collection Request",
                                "modalBtnText": "",
                                "singleBtn": true
                            };
                        }

                        localStorageService.set("modalObj",
                            modalObj);
                        $scope.open();
                    }
                }

                $scope.contractDetails = {};
                var type = "contractDetails";
                var contractDt = localStorageService
                    .get('paymentDetails');
                getServiceCall();

                function getServiceCall() {
                    $scope.loading = true;
                    if (type === 'contractDetails') {
                        data = {
                            userId: Session.userId,
                            contractId: contractDt.contractId
                        };
                        url = "CollectionController/collectPayment";
                    }
                    if (type === 'submitPayment') {
                        collectType = $scope.status_collect ? 'normal' :
                            'cashup';
                        var maxCashupDiscountPercent = $scope.status_collect ? '0' :
                            $scope.cashUpPercentage;

                            var chequeDate=new Date($scope.chequeDate).toLocaleDateString("en-GB");
                        data = {
                            userId: Session.userId,
                            contractId: contractDt.contractId,
                            paymentTypeId: $scope.paymentMethod,
                            amtToBeCollected: $scope.amount,
                            remarks: $scope.remark,
                            paymentRef: $scope.referenceNo,
                            collectType: collectType,
                            maxCashupDiscountPercent: maxCashupDiscountPercent,
                            bankId:$scope.selectedBank!=null?$scope.selectedBank.bankId:'',
                            // chequeDt:new Date($scope.chequeDate).toLocaleDateString("en-GB")
                            chequeDt:chequeDate
                        };

                        $scope.paymentToSave = data;
                        url = "CollectionController/collectPaymentSubmit";
                    }
                    adapter.getServiceData(url, data).then(success,
                        error);

                    function success(result) {
                        if (result.statusCode === 200) {
                            if (type === "contractDetails") {
                                $scope.contractDetails = result;
                                if($scope.contractDetails.monthlyInstallmentAmt <= $scope.contractDetails.currentOutStandingAmt)
                                 $scope.amount = $scope.contractDetails.monthlyInstallmentAmt;
                                else
                                 $scope.amount = $scope.contractDetails.currentOutStandingAmt;
                                $scope.cashUpDiscount = result.maxCashupDiscount;
                                $scope.maxCashUpDiscount = result.maxCashupDiscount;
                                $scope.bankList=result.bankList;
                            } else if (type === "submitPayment") {
                                if(result.nextPaymentDueDt!==null && result.nextPaymentDueDt!==""){
                                								 modalEntity = $scope.amount
                                								 + " "
                                								 + " was collected successfully";
                                								 modalBody = "";
                                								 modalMessge = " from "
                                									 + $scope.contractDetails.custName
                                									 + ". The next payment due date is "
                                									 + result.nextPaymentDueDt;
                                								 if (parseFloat($scope.amount) === parseFloat($scope.contractDetails.currentOutStandingAmt)
                                										 || collectType === 'cashup')
                                									 modalMessge = " from "
                                										 + $scope.contractDetails.custName
                                										 + ".";
                                								 localStorageService.set(
                                										 "modalStatus", true);
                                								 modalOpen("success");

                                							 }else{
                                								 modalEntity = $scope.amount
                                								 + " "
                                								 + " was collected successfully";
                                								 modalBody = "";
                                								 modalMessge = " from "
                                									 + $scope.contractDetails.custName
                                									 + ".";
                                								 if (parseFloat($scope.amount) === parseFloat($scope.contractDetails.currentOutStandingAmt)
                                										 || collectType === 'cashup')
                                									 modalMessge = " from "
                                										 + $scope.contractDetails.custName
                                										 + ".";
                                								 localStorageService.set(
                                										 "modalStatus", true);
                                								 modalOpen("success");

                                							 }

                            }

                        } else {
                            console.log("Service Not Resolved");
                            $scope.loading = false;

                                                    if (!appInUse) {
                                                        localStorageService.set("modalStatus",
                                                            false);
                                                        errorMessage = result;
                                                        modalOpen("error");
                                                    } else {

                                                        if (type === 'contractDetails') {
                                                            $scope.contractDetails = localStorageService.get('paymentDetails');
                                                            $scope.amount = $scope.contractDetails.monthlyInstallmentAmt;
                                                            $scope.cashUpDiscount = $scope.contractDetails.maxCashupDiscount;
                                                            $scope.maxCashUpDiscount = $scope.contractDetails.maxCashupDiscount;
                                                            $scope.contractDetails.currentOutStandingAmt=$scope.contractDetails.outstandingBal;

                                                            $scope.contractDetails.paymentMethodList = [];

                                                            $scope.contractDetails.paymentMethodList.push({
                                                                paymentMethodId: 2,
                                                                paymentMethodName: "Cash"
                                                            });
                                                            $scope.contractDetails.paymentMethodList.push({
                                                                paymentMethodId: 3,
                                                                paymentMethodName: "Cheque"
                                                            });
                                                            $scope.contractDetails.paymentMethodList.push({
                                                                paymentMethodId: 1,
                                                                paymentMethodName: "Credit Card"
                                                            });

                                                            var data = {countryId: loggedinUser.countryId};
                                                            app.fetchBankList(data, callbackBankList);


                                                        } else if (type === 'submitPayment') {

                                                            var data = $scope.paymentToSave;
                                                            data.isSync = false;

                                                            app.savePaymentCo(data, callbackSubmitPayment);

                                                        }

                                                    }
                        }
                        $scope.loading = false;
                    };

                    function error(result) {
                        $scope.loading = false;

                        if (!appInUse) {
                            localStorageService.set("modalStatus",
                                false);
                            errorMessage = result;
                            modalOpen("error");
                        } else {

                            if (type === 'contractDetails') {
                                $scope.contractDetails = localStorageService.get('paymentDetails');
                                $scope.amount = $scope.contractDetails.monthlyInstallmentAmt;
                                $scope.cashUpDiscount = $scope.contractDetails.maxCashupDiscount;
                                $scope.maxCashUpDiscount = $scope.contractDetails.maxCashupDiscount;
                                $scope.contractDetails.currentOutStandingAmt=$scope.contractDetails.outstandingBal;

//                                $scope.contractDetails.paymentMethodList = [];
//
//                                $scope.contractDetails.paymentMethodList.push({
//                                    paymentMethodId: 2,
//                                    paymentMethodName: "Cash"
//                                });
//                                $scope.contractDetails.paymentMethodList.push({
//                                    paymentMethodId: 3,
//                                    paymentMethodName: "Cheque"
//                                });
//                                $scope.contractDetails.paymentMethodList.push({
//                                    paymentMethodId: 1,
//                                    paymentMethodName: "Credit Card"
//                                });

                                var data = {countryId: loggedinUser.countryId};
                                app.fetchBankList(data, callbackBankList);


                            } else if (type === 'submitPayment') {

                                var data = $scope.paymentToSave;
                                data.isSync = false;

                                app.savePaymentCo(data, callbackSubmitPayment);

                            }

                        }

                    }
                }

                $scope.cancel = function() {
                    $state
                        .go('CoBoard.ManageCollectorCollectionRequest');
                }

                $scope.submit = function() {
                				 $scope.submitPressed = true;
                				 if ($scope.collectPaymentForm.$valid) {
                					 if($scope.getPayemtMethod($scope.paymentMethod).toLowerCase()=='cheque'){
                						 	var sixMonthDate=new Date();
                						 	sixMonthDate.setHours(0);
                						 	sixMonthDate.setMinutes(0);
                						 	sixMonthDate.setSeconds(0);
                							sixMonthDate.setMilliseconds(0);
                							sixMonthDate.setMonth(sixMonthDate.getMonth()-6);
                							var currentDate=new Date();
                							currentDate.setHours(0);
                							currentDate.setMinutes(0);
                							currentDate.setSeconds(0);
                							currentDate.setMilliseconds(0);
                							var selectedDate=new Date($scope.chequeDate.replace('-',' '));
                							var diff=selectedDate-sixMonthDate;
                							var futureDiff=currentDate-selectedDate;
                							/*if(selectedDate>sixMonthDate && currentDate>=selectedDate)*/
                							if(selectedDate>sixMonthDate)
                							$scope.past6monthError=true;
                							else
                							$scope.past6monthError=false;
                							//console.log($scope.past6monthError);
                					 }
                					 if($scope.getPayemtMethod($scope.paymentMethod).toLowerCase()==='cheque' && $scope.past6monthError){
                						 type = 'submitPayment';
                						 getServiceCall();
                					 }
                					 else if($scope.getPayemtMethod($scope.paymentMethod).toLowerCase()!=='cheque'){
                						 type = 'submitPayment';
                						 getServiceCall();
                					 }

                				 }

                			 }








                $scope.setCashUpDiscount = function() {
                    if ($scope.status_collect === false) {
                         var outStandingAmount = $scope.contractDetails.currentOutStandingAmt
                        					 - ($scope.contractDetails.currentOutStandingAmt * $scope.cashUpPercentage)
                        					 / 100;
                        					 $scope.amount = parseFloat(outStandingAmount.toFixed(2));
                        					 $scope.paymentDisable = true;
                        				 } else
                        					 {
                        					 	$scope.amount = parseFloat(($scope.contractDetails.monthlyInstallmentAmt).toFixed(2));
                        					 	$scope.paymentDisable = false;
                        					 }


                }

                $scope.getPayemtMethod=function(paymentId){
                			 	if($scope.contractDetails.paymentMethodList!==null && $scope.contractDetails.paymentMethodList!==undefined)
                				 for(var i=0;i<$scope.contractDetails.paymentMethodList.length;i++){
                					 if(parseInt(paymentId)===$scope.contractDetails.paymentMethodList[i].paymentMethodId)
                						 return $scope.contractDetails.paymentMethodList[i].paymentMethodName;
                				 }
                			 }

                function callbackSubmitPayment(result) {

                    modalEntity = $scope.amount +
                        " " +
                        " was collected successfully";
                    modalBody = "";
                    modalMessge = " from " +
                        $scope.contractDetails.custName +
                        ".";
                    if (parseFloat($scope.amount) === parseFloat($scope.contractDetails.currentOutStandingAmt) ||
                        collectType === 'cashup')
                        modalMessge = " from " +
                        $scope.contractDetails.custName +
                        ".";
                    localStorageService.set(
                        "modalStatus", true);
                    modalOpen("success");

                    $scope.$apply();
                }

                function callbackBankList(result){
                    $scope.bankList=result;
                    //console.log(JSON.stringify($scope.bankList));
                    app.fetchPaymentMethod(callbackPaymentMethod);
                     $scope.$apply();
                }

                function callbackPaymentMethod(result){
					$scope.contractDetails.paymentMethodList=result;
					$scope.$apply();
				}


            }


        ]);