app.controller("ViewContractCtrl", ['$scope', '$state', '$uibModal', '$rootScope', 'localStorageService', 'adapter', 'stickyHeader', 'validate', 'adjustHeight', 'Session', function($scope, $state, $uibModal, $rootScope, localStorageService, adapter, stickyHeader, validate, adjustHeight, Session) {
    $rootScope.$broadcast('TrackNavBar', false);
    adjustHeight.adjustTopBar();
    $scope.contractDetails = true;
    $scope.paymentDetails1 = false;
    $scope.remarksDetails = false;
    $scope.reportDetails = false;
    $scope.showRemoveProdBtn = true;
    $scope.loading = false;
    var stateNameForProduct = "";
    var type, itemPoint, totalItemPoint;
    var statusEditMode = ["Pending Confirmation", "Pending VO Allocation", "Pending Verification"];
    var prevDp1 = 0,
        salesContr = 0,
        adjustMent = 0;
    var adjustStatus = false;
    $scope.dpLabel = "Down Payment 2";
    $scope.text = {};
    $scope.text.instalmentAmountLbl = "Monthly Instalment Amount";
    $scope.text.paymentMethodDP2Lbl = "Method of Payment For Instalment";
    var removedProductId=[];

    $scope.disableDp1 = true;
    $scope.disableInput = false;
    $scope.rejection = false;
    $scope.adminCont = {};
    $scope.myprod = {};
    var salesRepCode;
    $scope.within30Km=false;
    var paymentId;
    $scope.dp1Disabled=true;
    $scope.upgradeDowngradeC=false;
    $scope.currency=localStorageService.get('currency');
    var generatedPdf = '';

    $scope.cheque1Error=true;
    $scope.cheque2Error=true;


    //Back to previous page
    if ($rootScope.previousState !== "AdminBoard.CancelContract" &&
            $rootScope.previousState !== "AdminBoard.ViewContract" &&
                $rootScope.previousState !== "VoBoard.ViewVerificationReport") {

        localStorageService.set("previousStateTitle", $rootScope.previousStateTitle);
        localStorageService.set("previousState", $rootScope.previousState);
    }
    $scope.previousStateTitle = $rootScope.previousStateTitle;
    $scope.previousState = $rootScope.previousState;
    if ($rootScope.previousState === "") {
        $scope.previousStateTitle = "Manage Contract";
        $scope.previousState = "AdminBoard.ManageContract";

    } else if ($rootScope.previousState === "AdminBoard.CancelContract" ||
                $rootScope.previousState === "AdminBoard.ViewContract" ||
                    $rootScope.previousState === "VoBoard.ViewVerificationReport") {

        $scope.previousStateTitle = "Manage Contract";
        $scope.previousState = "AdminBoard.ManageContract";

        if (angular.isDefined(localStorageService.get("previousStateTitle")) &&
            localStorageService.get("previousStateTitle") !== null &&
            angular.isDefined(localStorageService.get("previousState")) &&
            localStorageService.get("previousState") !== null) {

            $scope.previousStateTitle = localStorageService.get("previousStateTitle");
            $scope.previousState = localStorageService.get("previousState");
        }
    }

    var managePageLink, modalEntity, backText, createText, editText, confirmModal = false,
        grayBtnText = "",
        cancelReason;
    $scope.vo = {};
    //setting view Verification Report tab data
    setViewVerificationReportTab();

    //setting cancellation Report tab data
    setCancellationReportTab();

    if (Session.userRoleName.toLowerCase() === "admin") $scope.isAdmin = true;
    else if (Session.userRoleName.toLowerCase() === "collection officer") $scope.isCO = true;
    //else if (Session.userRoleName === "Sales Rep") $scope.isSalesRep = true;
    else if (Session.userRoleName.toLowerCase().indexOf("sales") > -1) $scope.isSalesRep = true;
    else if (Session.userRoleName.toLowerCase() === "verification officer") $scope.isVO = true;
    else if (Session.userRoleName.toLowerCase() === "collection manager") $scope.isCM = true;

    if (Session.userType.toLowerCase().indexOf("sales") > -1) $scope.removeEditBtn = true;

    if ($state.current.name === 'VoBoard.ViewVerificationReport') {

        $scope.contractDetails = false;
        $scope.reportDetails = true;
        $scope.contract = {};
        $scope.contract = {
            contractCode: null,
            customerCode: null,
            customerDetails: {
                name: null
            }
        };
        $scope.removeEditBtn = true;

        $scope.contract = localStorageService.get('contractRow');

    } else if ($state.current.name === 'VoBoard.UpdateVoPriceDifference') {

        $scope.contract = {};
        $scope.contract = {
            contractCode: null,
            customerCode: null,
            customerDetails: {
                name: null
            }
        };
        //$scope.vo = {};
        //$scope.priceDifferenceRow = localStorageService.get('priceDifferenceRow');
        //$scope.contract.contractCode = $scope.priceDifferenceRow.contractNo;

        $scope.contract = localStorageService.get('contractRow');
        $scope.vo = {
            customer: {
                priceDiff: $scope.contract.priceDiffAmt,
                updatedPriceDiff: $scope.contract.updPriceDiffAmt === null ? '-' : $scope.contract.updPriceDiffAmt
            }
        };

        if ($scope.contract.updPriceDiffAmt !== null) {

            $scope.disableInput = true;
        }

        $scope.updatePriceDiff = {};
        $scope.submitPriceDiffPressed = false;
        $scope.vo.savePriceDiff = function() {
            $scope.submitPriceDiffPressed = true;
            if ($scope.updatePriceDiff.$valid) {
                getServiceCall("updatePriceDiff");
               // console.log($scope.vo.customer.updatedPriceDiff);
            }

        }

    } else {
        $scope.contract = localStorageService.get('contractRow');
        //$scope.contract = {};
        //$scope.contract.contractCode = "160418mm";
    }


    //stickey Header
    enableStickyHeader($(".sticky-block-view"));

    function decryptLangValuePair(data) {
        if (data !== undefined && data !== null && data !== "") {
            var langArr = data.split(",");
            var finalLangArr = [];
            angular.forEach(langArr, function(value, key) {
                var temp = value.split("|");
                if (temp[1] === "Y" || (temp[1] === "Others" && temp[0] !== "NilLang"))
                    finalLangArr.push(temp[0]);
            });
            return finalLangArr.join(", ");
        }
        return "NA";
    }
    $scope.remarkList = [];
    $scope.viewRemark = '';

    $scope.raceList = ["Chinese", "Indian", "Malay", "Others"];
    $scope.relationList = ["Mother", "Father", "Sister", "Spouse", "Other"];
    $scope.priceListCodeList = ["EA", "EB", "EE", "EH"];
    $scope.termsList = ["COD", "CAD", "6", "8", "9"];
    //$scope.methodOfPaymentList=["CC","COD","Collector"];
    var stateCode;
    var actualDownPayment1, actualDownPayment2;

    $scope.getCustomDate = function(dateObj) {
        if (dateObj !== null)
        {
            var parts=dateObj.split('-');
            return getFormattedDate(new Date(parts[2],$scope.getMonthNo(parts[1]),parts[0]));
        }
        else
            return "";
    };
    $scope.common = {};
    $scope.common = {
        todayDate: null,
        formatedDob: null
    };
    $scope.common.currency = currency; //from global js
    $scope.common.todayDate = getFormattedDate(new Date());

    $scope.salesRepContributionStatus = false;

    $scope.remarks = {};
    $scope.remarkCustomer = null;
    $scope.remarkProduct = null;
    $scope.remarkPricing = null;
    var tempDob;
    $scope.bank = {};

    /*MODAL IMPLEMENTATION*/
    $scope.animationsEnabled = true;

    $scope.actionButton = "Create";

    $scope.open = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/modals/role/modal.html',
            controller: 'modalCtrl',
            windowClass: 'center-modal',
            backdrop: 'static',
            keyboard: false,
            size: 'md'
        }).result.then(function(data) {
            if (data === "close") {
                if (confirmModal) { //confirm modal NO

                    confirmModal = false;
                } else {
                    var d = managePageLink;
                    var e = $state.current.name;
                    if (d !== e) $state.go(managePageLink);
                    else $state.reload();
                }

            } else if (data === "createAnother") {
                if (confirmModal) { //confirm modal YES

                    confirmModal = false;
                    if ($scope.rejection) {
                        $scope.rejection = false;
                        rejectCancellationRequest();
                    } else
                        initiateCancellationRequest();
                } else {
                    $state.reload();
                }
                //$state.go('AdminBoard.CreateEditContract');
            }
        });
    };

    var errorName = "Contract";
    var errorCode = "Test1234";
    var contractCode = ($scope.contract !== undefined && $scope.contract.contractCode !== undefined) ? $scope.contract.contractCode : "";
    var errorMessage = "";

    function modalOpen(modalType) {
        if (errorName !== null && errorName !== "" && errorName !== undefined) {
            if (modalType === "success") {
                var modalObj = {
                    "modalEntity": modalEntity,
                    "body": "(" + errorName + " - " + contractCode + ") ",
                    "backLink": backText,
                    "modalBtnText": createText,
                    "modalMessge": editText,
                    "singleBtn": true,
                    "grayBtnText": grayBtnText,
                    "confirmModal": confirmModal
                };
            } else {
                var modalObj = {
                    "modalEntity": "Failed!! ",
                    "body": errorMessage,
                    "backLink": backText,
                    "modalBtnText": createText,
                    "singleBtn": true,
                    "grayBtnText": grayBtnText,
                    "confirmModal": confirmModal
                };
            }

            localStorageService.set("modalObj", modalObj);
            $scope.open();
        }
    }
    /*END*/

    /*editContract menu initialization*/

    $scope.editProduct = false;
    $scope.editCustomer = false;
    $scope.editPayment = false;
    $scope.removeEditBtn = false;


    $scope.stickyMenu = function(type) {
        resetStickymenu();
        switch (type) {
            case 'contract':
                {
                    $scope.contractDetails = true;
                    break;
                }
            case 'payment':
                {
                    $scope.paymentDetails1 = true;
                    break;
                }
            case 'remarks':
                {
                    $scope.remarksDetails = true;
                    break;
                }
            case 'report':
                {
                    $scope.reportDetails = true;
                    break;
                }
            case 'cancellation':
                {
                    $scope.cancellationDetails = true;
                    break;
                }
        }
    }

    function resetStickymenu() {
        $scope.contractDetails = false;
        $scope.paymentDetails1 = false;
        $scope.remarksDetails = false;
        $scope.reportDetails = false;
        $scope.cancellationDetails = false;

        //stickey Header
        enableStickyHeader($(".sticky-block-view"));
    }
    $scope.currentSate = $state.current.name;

    $scope.ShowCustomerPage = function(customerCode, event) {
        localStorageService.set('customerCode', customerCode);
        if (event.ctrlKey) window.open('#/AdminBoard/EditCustomer', '_blank'); // in new tab
        else $state.go('AdminBoard.EditCustomer');
    }
    $scope.ShowSalesRep = function(srCode, event) {
        //localStorageService.set('customerCode', srCode);
        salesRepCode = srCode;
        getServiceCall("getSalesRepDetails");
    }

    getServiceCall("viewContract");

    $scope.sendForVerification = function() {
        getServiceCall("updateContractStatus");
    }

    function getServiceCall(type) {
        if (type === 'viewContract') {
            $scope.loading = true;
            var data = {
                userId: Session.userId,
                contractCode: $scope.contract.contractCode
            }
            url = "ContractController/viewContract";
        }

        if (type === 'cancelContract') {
            $scope.loading = true;

            $scope.vo.cancelData.userId = Session.userId;
            $scope.vo.cancelData.contractId = $scope.contract.contractId;
            var data = $scope.vo.cancelData,
                //url="ContractCancellationController/initiateCancellation";
                url = "ContractCancellationController/updateCancellation";
        }
        if (type === 'approveCancelContract') {
            $scope.loading = true;
            /*var data={
                userId:Session.userId,
                contractId:$scope.contract.contractId
            },*/
            $scope.vo.cancelData.userId = Session.userId;
            $scope.vo.cancelData.contractId = $scope.contract.contractId;
            $scope.vo.cancelData.remarks=$scope.cancelApproveRejectRemarks;
            $scope.vo.cancelData.locationId=$scope.cancellationLocation;
            var data = $scope.vo.cancelData,
                url = "ContractCancellationController/approveCancellation";
        }
        if (type === 'rejectCancelContract') {
            $scope.loading = true;
            var data = {
                    userId: Session.userId,
                    contractId: $scope.contract.contractId,
                    remarks:$scope.cancelApproveRejectRemarks,
                    locationId:$scope.cancellationLocation
                },
                /*$scope.vo.cancelData.userId = Session.userId;
                $scope.vo.cancelData.contractId = $scope.contract.contractId;
                var data = $scope.vo.cancelData,*/
                url = "ContractCancellationController/rejectCancellation";
        }

        if (type === 'updateContractStatus') {
            $scope.loading = true;
            var data = {
                userId: Session.userId,
                contractId: $scope.contract.contractId
            }
            url = "ContractController/updateContractStatus";
        }

        if (type === "addRemark") {
            $scope.loading = true;
            data = {
                userId: Session.userId,
                contractCode: $scope.contract.contractCode,
                remarks: $scope.viewRemark
            }
            url = "ContractController/addRemark";
        }
        if (type === "editContract") {
            $scope.loading = true;
            $scope.contract.userId = Session.userId;
            var remarkDetails = [];
            remarkDetails.push({
                "remarks": $scope.remarks.remarkCustomer,
                "remarkType": "contract_customer"
            });
            remarkDetails.push({
                "remarks": $scope.remarks.remarkProduct,
                "remarkType": "product"
            });
            remarkDetails.push({
                "remarks": $scope.remarks.remarkPricing,
                "remarkType": "pricing_payment"
            });
            $scope.contract.remarkDetails = remarkDetails;
            data = $scope.contract;
            var dob = null;
            if ($scope.contract.customerDetails.dob !== null && $scope.contract.customerDetails.dob !== undefined && $scope.contract.customerDetails.dob !== "") {
                var dtArr = $scope.contract.customerDetails.dob.split("-");
                //var timeZone = "T00:00:00.000Z";
                var month = ('0' + (parseInt($.inArray(dtArr[1], shortMonthArr)) + 1)).slice(-2);
                var custDob = dtArr[2] + "-" + month + "-" + dtArr[0];
                dob = new Date(custDob);
            }

            data.customerDetails.dob = dob;

            data.orderDate = $scope.contract.orderDate;

            if (angular.isDefined($scope.contract.deliveryDate) && $scope.contract.deliveryDate !== "" && $scope.contract.deliveryDate !== null)
                data.deliveryDate = $scope.contract.deliveryDate;
            else data.deliveryDate = null;

            if (angular.isDefined($scope.contract.commenceMentDate) && $scope.contract.commenceMentDate !== "" && $scope.contract.commenceMentDate !== null)
                data.commenceMentDate = $scope.contract.commenceMentDate;
            else data.commenceMentDate = null;

            if ($scope.isAdmin) {
                data.salesRepName = $scope.adminCont.salesRepNameEdit;
            } else {
                data.salesRepName = null;
                //data.salesRepName = $scope.contract.salesRepName;
            }

            if($scope.upgradeDowngradeC){
                data.returnedProductId=returnedProductId;
            }


            url = "ContractController/editContract";
        }
        if(type==='createNewContract'){
            $scope.loading = true;
            $scope.contract.userId = Session.userId;
            var remarkDetails = [];
            remarkDetails.push({
                "remarks": $scope.remarks.remarkCustomer,
                "remarkType": "contract_customer"
            });
            remarkDetails.push({
                "remarks": $scope.remarks.remarkProduct,
                "remarkType": "product"
            });
            remarkDetails.push({
                "remarks": $scope.remarks.remarkPricing,
                "remarkType": "pricing_payment"
            });
            $scope.contract.remarkDetails = remarkDetails;
            data = $scope.contract;
            var dob = null;
            if ($scope.contract.customerDetails.dob !== null && $scope.contract.customerDetails.dob !== undefined && $scope.contract.customerDetails.dob !== "") {
                var dtArr = $scope.contract.customerDetails.dob.split("-");
                //var timeZone = "T00:00:00.000Z";
                var month = ('0' + (parseInt($.inArray(dtArr[1], shortMonthArr)) + 1)).slice(-2);
                var custDob = dtArr[2] + "-" + month + "-" + dtArr[0];
                dob = new Date(custDob);
            }

            data.customerDetails.dob = dob;

            data.orderDate = $scope.contract.orderDate;

            if (angular.isDefined($scope.contract.deliveryDate) && $scope.contract.deliveryDate !== "" && $scope.contract.deliveryDate !== null)
                data.deliveryDate = $scope.contract.deliveryDate;
            else data.deliveryDate = null;

            if (angular.isDefined($scope.contract.commenceMentDate) && $scope.contract.commenceMentDate !== "" && $scope.contract.commenceMentDate !== null)
                data.commenceMentDate = $scope.contract.commenceMentDate;
            else data.commenceMentDate = null;

            if ($scope.isAdmin) {
                data.salesRepName = $scope.adminCont.salesRepNameEdit;
            } else {
                data.salesRepName = null;
                //data.salesRepName = $scope.contract.salesRepName;
            }

            if($scope.upgradeDowngradeC){
                data.removedProductId=removedProductId;
            }

            url = "ContractController/upgradeDowngradeContract";
        }
        if (type === 'getPricingDetails') {
            $scope.loading = true;
            data = {
                userId: Session.userId,
                priceListCode: $scope.contract.priceListCode,
                points: $scope.contract.points,
                stateName: $scope.contract.customerDetails.homeState,
                itemPoint: $scope.contract.itemPoint,
                terms: $scope.contract.terms
            }
            url = "ContractController/getPriceListDetails";
        }
        if (type === 'updatePriceDiff') {
            $scope.loading = true;
            data = {
                contractId: $scope.contract.contractId,
                finalDiffAmt: parseFloat($scope.vo.customer.updatedPriceDiff)
            }
            url = "VerificationController/updatePriceDiff";
        }
        if (type === 'viewVerifiedContract') {
            $scope.loading = true;
            data = {
                contractId: $scope.contract.contractId,
                userId:Session.userId
            }
            url = "VerificationController/verificationReport";
        }
        if (type === 'submitVerificationReport') {
            $scope.loading = true;
            $scope.vo.data.userId=Session.userId;
            $scope.vo.data.locationId = $scope.vo.selectedLocation.locationId;
            $scope.vo.data.distance=$scope.within30Km;

            var chequeDate=new Date($scope.vo.chequeDate).toLocaleDateString("en-GB");

            $scope.vo.data.chequeDt=chequeDate;
            data = $scope.vo.data;

            if($scope.vo.data.locationId!==undefined){
                $scope.binError=false;
                $scope.loading=true;
                url = "VerificationController/verificationReportSubmit";
            }else{
                $scope.binError=true;
            }



        }
        if (type === 'getPointList') {
            $scope.loading = true;
            data = {
                userId: Session.userId,
                priceListCode: $scope.contract.priceListCode,
                stateName: $scope.contract.customerDetails.homeState,
                itemPoint: itemPoint,
                totalItemPoint:totalItemPoint,
                customerDetails: {
                    excempted: $scope.contract.customerDetails.excempted,
                    homePinCode: $scope.contract.customerDetails.homePinCode
                },
                terms: $scope.contract.terms
            }
            url = "ContractController/getPointList";
        }
        if (type === 'getTerms') {
            $scope.loading = true;
            data = {
                userId: Session.userId,
                priceListCode: $scope.contract.priceListCode,
                itemPoint: itemPoint,
                totalItemPoint:totalItemPoint,
                customerDetails: {
                    excempted: $scope.contract.customerDetails.excempted,
                    homePinCode: $scope.contract.customerDetails.homePinCode
                },
                stateName: $scope.contract.customerDetails.homeState
            }
            url = "ContractController/getTerms";
        }
        if (type === 'getProduct') {
            $scope.loading = true;
            data = {
                userId: Session.userId,
                stateName: stateNameForProduct
            }
            url = "ContractController/getContract";

        } else if (type === 'fetchListOfPriceList') {
            $scope.loader = true;
            data = {
                userId: Session.userId,
                productDetails: angular.isDefined($scope.contract.productDetails) ? $scope.contract.productDetails : null,
                customerDetails: {
                    excempted: angular.isDefined($scope.contract.customerDetails.excempted) ? $scope.contract.customerDetails.excempted : false,
                    homePinCode: angular.isDefined($scope.contract.customerDetails.homePinCode) ? $scope.contract.customerDetails.homePinCode : null
                },
                stateName: angular.isDefined($scope.contract.customerDetails.homeState) ? $scope.contract.customerDetails.homeState : null,
            }
            url = "ContractController/getPriceList";
        } else if (type === 'getSalesRepList') {
            $scope.loader = true;
            data = {};
            url = 'ContractController/salesRepList';

        } else if (type === 'getSalesRepDetails') {
            $scope.loader = true;
            data = {
                userId: null,
                userCode: salesRepCode
            };
            url = 'UserRegisterController/editUser';

        }
        if (type === 'acceptedByDM') {
            $scope.loading = true;
            url = 'ContractController/updateRejectedContract';
            data = {
                userId: Session.userId,
                contractId: $scope.contract.contractId,
                contractRejected: 'N'
            }
        } else if (type === 'rejecetdByDM') {
            $scope.loading = true;
            url = 'ContractController/updateRejectedContract';
            data = {
                userId: Session.userId,
                contractId: $scope.contract.contractId,
                contractRejected: 'Y'
            }
        }else if(type==='sendEmail'){
            $scope.loading = true;


            url = 'ContractEmailController/sendEmail';
            data = {
                    to:$scope.contract.customerDetails.email,
                    contractCode:$scope.contract.contractCode,
                    pdfRequest:generatedPdf.replace("data:application/pdf;base64,", "")
            }
        }
        adapter.getServiceData(url, data).then(success, error);

        function success(result) {
            if (result.statusCode === 200 || result.status === 200) {
                if (type === 'acceptedByDM') {
                    $scope.loading = false;
                    $scope.dmName = $scope.contract.salesRepName;
                    modalEntity = "You accepted the order(" + $scope.contract.contractCode + ").";
                    modalBody = "";
                    localStorageService.set("modalStatus", true);
                    modalOpenDM('success');
                } else if (type === 'rejecetdByDM') {
                    $scope.loading = false;
                    $scope.dmName = $scope.contract.salesRepName;
                    modalEntity = "You rejected the order(" + $scope.contract.contractCode + ").";
                    modalBody = "";
                    localStorageService.set("modalStatus", true);
                    modalOpenDM('success');
                }
                if (result.message === "Success" || result.message === "SUCCESS") {

                    if (type === "viewContract") {


                        $scope.contract = result;
                        itemPoint = result.itemPoint;
                        totalItemPoint=result.totalItemPoint;


                        $scope.installmentList = result.installmentList;
                        $scope.pointList = result.pointList;
                        $scope.referenceModel1 = result.customerDetails.relationships;
                        $scope.productList1 = result.productList;
                        $scope.bankList=result.bankList;
                        if(result.bankDp1!=null && $scope.bankList!=null)
                        for(var i=0;i<$scope.bankList.length;i++){
                            if($scope.bankList[i].bankId==result.bankDp1.bankId)
                            {
                                $scope.contract.bankDp1=$scope.bankList[i];
                                break;
                            }
                        }



                        if(result.bankDp2!=null && $scope.bankList!=null)
                            for(var i=0;i<$scope.bankList.length;i++){
                                if($scope.bankList[i].bankId==result.bankDp2.bankId)
                                {
                                    $scope.contract.bankDp2=$scope.bankList[i];
                                    break;
                                }
                            }
                        //$scope.contract.bankDp1=result.bankDp1;
                        $scope.payment = result.paymentDetails;
                        //For mobile screen listing
                        $scope.listing = {};
                        $scope.listing.sortedByList = [{
                            value: "paymentAmt",
                            text: "Amount"
                        }, {
                            value: "lastPaymentDt",
                            text: "Status Date"
                        }];

                        //Mobile product selection
                        angular.forEach(result.productDetails, function(value, key) {
                            $scope.myprod.selProductDetails[key] = value;
                        });

                        var limit = 10;
                        var len = $scope.payment.length;
                        $scope.listing.limit = len > limit ? limit : len;

                        $scope.listing.showLoader = len > $scope.listing.limit ? true : false;
                        $scope.listing.loadMore = function() {
                            var increamented = $scope.listing.limit + limit;
                            $scope.listing.limit = increamented > len ? len : increamented;
                            $scope.listing.showLoader = len > $scope.listing.limit ? true : false;
                        }

                        $scope.remarkList = result.remarkDetails;
                        for (var i = 0; i < $scope.remarkList.length; i++)
                            $scope.remarkList[i].showShortRemark = true;
                        stateCode = result.stateCode;
                        $scope.paymentDetailsShow = true;
                        if (result.salesRepContributionAmt !== null) {
                            salesContr = result.salesRepContributionAmt;
                        }

                        if (result.paymentDetails !== null && result.paymentDetails !== undefined && result.paymentDetails.length > 0) {
                            for(var i=0;i<result.paymentDetails.length;i++){
                                if(result.paymentDetails[i].paymentType==='Deposit1'){
                                    prevDp1 = result.paymentDetails[i].paymentAmt;
                                    paymentId = result.paymentDetails[i].paymentId;
                                    $scope.contract.paymentRefNum = result.paymentDetails[i].paymentRefNum;
                                    break;
                                }

                            }

                        }

                        $scope.methodOfPaymentList = result.paymentMethodList;

                        //$scope.contract.customerDetails.icNumber = $scope.contract.customerDetails.icNo;
                        $scope.review_terms = result.review_terms;

                        if ($scope.remarkList[0] !== undefined)
                            $scope.remarks.remarkCustomer = $scope.remarkList[0].remarks;
                        if ($scope.remarkList[1] !== undefined)
                            $scope.remarks.remarkProduct = $scope.remarkList[1].remarks;
                        if ($scope.remarkList[2] !== undefined)
                            $scope.remarks.remarkPricing = $scope.remarkList[2].remarks;

                        $scope.salesRepContributionStatus = result.salesRepContribution;

                        $scope.common.formatedDob = $scope.getCustomDate($scope.contract.customerDetails.dob);

                        //get total points
                        $scope.totalPoints = $scope.getTotalPoints(true);


                        actualDownPayment1 = result.downPayment1;
                        actualDownPayment2 = (result.downPayment2 !== undefined && result.downPayment2 !== null) ? result.downPayment2 : 0;

                        $scope.contract.salesRepContributionAmt = result.salesRepContribution;

                        var data = $scope.contract.terms;
                        if (data === "COD" || data === "CAD")
                            $scope.bank.bankId = data;
                        else
                            $scope.bank.bankId = data + " Months";

                        if ($scope.contract.terms === "CAD") {
                            $scope.text.instalmentAmountLbl = "Order Value";
                            $scope.text.paymentMethodDP2Lbl = "Method of Payment For CAD";
                        }

                        if ($scope.contract.adjustment) $scope.dpLabel = "Adjustment";

                        /*if ($scope.contract.status !== "Pending Vo Allocation" && $scope.contract.status !== "Pending VO Allocation") $scope.removeEditBtn = true;*/

                        if (statusEditMode.indexOf($scope.contract.status) === -1) {
                            $scope.removeEditBtn = true;
                        }

                        if (Session.userType.toLowerCase().indexOf("sales") > -1) $scope.removeEditBtn = true;

                        /**********Cancel Contract Start************/
                        $scope.showCancelTab = ($scope.contract.status === "Cancellation Initiated"
                            || $scope.contract.status === "Cancelled"
                                || $scope.contract.status === "Cancel Request Rejected"
                                    || $scope.contract.status.toLowerCase()==="cancellation approved for product reprocessed"
                                        || $scope.contract.status.toLowerCase()==="product reprocessed") ? true : false;
                        //$scope.showCancelTab = true;

                        if (!$scope.isSalesRep) { //Not for Sales Rep

                            $scope.vo.cancelQtyError = [];
                            $scope.vo.cancelData = {};
                            $scope.vo.cancelData.productDetails = [];
                            angular.forEach($scope.contract.productDetails, function(value, key) {

                                $scope.contract.productDetails[key].remainingProdQty = $scope.contract.productDetails[key].deliveredQty - $scope.contract.productDetails[key].returnedQty;
                                //$scope.contract.productDetails[key].remarks = "";

                                var obj = {
                                    itemId: value.itemId,
                                    quantity: $scope.contract.productDetails[key].quantity,
                                    deliveredQty: $scope.contract.productDetails[key].deliveredQty,
                                    returnedQty: $scope.contract.productDetails[key].returnedQty,
                                    remainingProdQty: $scope.contract.productDetails[key].remainingProdQty,
                                    verifiedProdQty: $scope.contract.productDetails[key].verifiedProdQty,
                                    remarks: $scope.contract.productDetails[key].remarks
                                };

                                $scope.vo.cancelData.productDetails.push(obj);
                                $scope.vo.cancelQtyError.push(false);
                            });
                        }
                        $scope.vo.cancelData.cancelRequestDate = $scope.contract.cancelDate;
                        cancelReason = $scope.vo.cancelData.cancelReason = $scope.contract.cancelReason;
                        /**********Cancel Contract End************/

                        //Get Sales Rep List
                        getServiceCall("getSalesRepList");

                        if ($state.current.name === 'VoBoard.ViewVerificationReport' || $scope.contract.status === "Confirmed" || $scope.contract.shipmentStatus === "Partially Delivered" || $scope.contract.shipmentStatus === "Delivered") {

                            $scope.showReportTab = true;
                            getServiceCall("viewVerifiedContract");

                        } else {
                            $scope.showReportTab = false;
                        }
                        if ($scope.isSalesRep) $scope.showReportTab = false;

                        $scope.loading = false;

                        //stickey Header
                        enableStickyHeader($(".sticky-block-view"));
                        for(var i=0;i<result.remarkDetails.length;i++){
                            if(result.remarkDetails[i].remarkType.toLowerCase()==='cancellation approved' && ($scope.contract.status.toLowerCase()==='cancelled'|| $scope.contract.status.toLowerCase()==='product reprocessed'||$scope.contract.status.toLowerCase()==='cancellation approved for product reprocessed'))
                                $scope.cancelApproveRejectRemarks=result.remarkDetails[i].remarks;
                            else if(result.remarkDetails[i].remarkType.toLowerCase()==='cancellation rejected' && $scope.contract.status.toLowerCase()!=='cancelled')
                                $scope.cancelApproveRejectRemarks=result.remarkDetails[i].remarks;
                        }
                        $scope.locationList=result.locationList;
                        for(var i=0;i<$scope.locationList.length;i++){
                            if($scope.locationList[i].locationId===result.defaultLocation.locationId && $scope.contract.status.toLowerCase()!=='cancelled'){
                                $scope.cancellationLocation=$scope.locationList[i];
                                break;
                            }
                            else if(result.selectedLocation!==null && $scope.locationList[i].locationId===result.selectedLocation.locationId && $scope.contract.status.toLowerCase()==='cancelled'){
                                $scope.cancellationLocation=$scope.locationList[i];
                                break;
                            }
                        }

                    } else if (type === 'cancelContract') {

                        cancelContractModelSetting();
                        $scope.loading = false;
                        localStorageService.set("modalStatus", true);
                        modalOpen("success");

                    } else if (type === 'approveCancelContract') {

                        cancelContractModelSetting();
                        $scope.loading = false;
                        localStorageService.set("modalStatus", true);
                        modalOpen("success");

                    } else if (type === 'rejectCancelContract') {
                        rejectContractModelSetting();
                        $scope.loading = false;
                        localStorageService.set("modalStatus", true);
                        modalOpen("success");

                    } else if (type === "addRemark") {
                        getServiceCall("viewContract");
                        $scope.loading = false;
                        $scope.viewRemark = "";
                    } else if (type === "editContract" || type === "createNewContract") {

                        contractCode = $scope.contract.contractCode;
                        $scope.loading = false;

                        editContractModelSetting();
                        if(type === "editContract"){
                            localStorageService.set("modalStatus", true);
                            modalOpen("success");
                        }
                        else if(type === "createNewContract"){
                            modalEntity="Old contract ("+$scope.contract.contractCode +") has been cancelled and a new Contact ("+result.contractCode+") has been created.";
                            localStorageService.set("modalStatus", true);
                            modalOpen("createNew");
                        }


                    } else if (type === "getPricingDetails") {

                        $scope.paymentDetailsShow = true;
                        actualDownPayment1 = result.downPayment1 - prevDp1 - salesContr;
                        actualDownPayment2 = result.downPayment2;
                        adjustMent = (result.downPayment1 - prevDp1 - salesContr);

                        if (adjustMent >= 0) {
                            $scope.dpLabel = "Down Payment 2";
                            $scope.contract.downPayment2 = result.downPayment2 + adjustMent;
                            $scope.contract.totalAmountOrder = result.totalAmountOrder;
                            $scope.contract.totalInstallmentAmount = result.totalInstallmentAmount;
                            $scope.contract.installmentAmt = result.installmentAmt;
                            $scope.contract.totalOutstandingBalance = result.totalOutstandingBalance - prevDp1- salesContr;


                        } else {
                            if (result.downPayment2 < Math.abs(adjustMent)) {
                                $scope.dpLabel = "Adjustment";
                                adjustStatus = true;
                            } else {
                                $scope.dpLabel = "Down Payment 2";
                            }
                            $scope.contract.downPayment2 = result.downPayment2 + adjustMent;
                            $scope.contract.downPayment2 = Math.abs($scope.contract.downPayment2);
                            $scope.contract.totalAmountOrder = result.totalAmountOrder;
                            $scope.contract.totalInstallmentAmount = result.totalInstallmentAmount;
                            $scope.contract.installmentAmt = result.installmentAmt;
                            $scope.contract.totalOutstandingBalance = result.totalOutstandingBalance+adjustMent;
                        }

                        $scope.contract.downPayment1 = prevDp1;

                      //  $scope.contract.totalOutstandingBalance = result.totalOutstandingBalance;

                        var data = $scope.contract.terms;
                        if (data === "COD" || data === "CAD")
                            $scope.bank.bankId = data;
                        else
                            $scope.bank.bankId = data + " Months";

                        $scope.loading = false;

                    } else if (type === "updatePriceDiff") {
                        $state.go('VoBoard.ManageVoPriceDifference');

                    } else if (type === "updateContractStatus") {
                        $state.go('SalesRep.ManageContractSales');
                    } else if (type === 'viewVerifiedContract') {

                        $scope.vo.response = result;

                        $scope.vo.reportStatus = ($scope.vo.response.recommendContract === "Accept_Contract") ? "Accepted" : (($scope.vo.response.recommendContract === "Reconsider_Contract") ? "Reconsidered" : (($scope.vo.response.recommendContract === "Reject_Contract") ? "Rejected" : ""));
                        $scope.vo.recommendTo = ($scope.vo.response.recommendContract === "Accept_Contract") ? "Accept" : (($scope.vo.response.recommendContract === "Reconsider_Contract") ? "Reconsider" : (($scope.vo.response.recommendContract === "Reject_Contract") ? "Reject" : ""));
                        $scope.vo.languageSpeak = decryptLangValuePair($scope.vo.response.languageSpeak);
                        $scope.vo.compOperating = decryptLangValuePair($scope.vo.response.compOperating);


                        $scope.vo.selectedLocation = {
                          //  locationId: $scope.vo.response.locationId
                        };

                        $scope.vo.data = {
                            contractVerificationId: $scope.vo.response.contractVerificationId,
                            remarks: $scope.vo.response.remarks
                        };

                        $scope.vo.showPartialFields = false;
                        $scope.vo.partialProduct = [];
                        if (($scope.vo.response.orderStatus === "Confirmed" || $scope.vo.response.shipmentStatus === "Partially Delivered" || $scope.vo.response.shipmentStatus === "Delivered") && ($scope.vo.response.recommendContract !== "Reject_Contract")) {

                          //  $scope.vo.data.locationId = $scope.vo.selectedLocation.locationId;
                            $scope.vo.data.paymentMethod = $scope.vo.response.itemDeliverPaymentInfo.paymentMethod;
                            $scope.vo.data.paymentRef = $scope.vo.response.itemDeliverPaymentInfo.paymentRefNum;
                            $scope.vo.data.amtToBeCollected = $scope.vo.response.itemDeliverPaymentInfo.amtTobeCollDwPay2;

                            $scope.vo.data.itemList = [];
                            $scope.vo.qtyError = [];
                            angular.forEach($scope.vo.response.itemDeliverPaymentInfo.itemList, function(value, key) {

                                $scope.vo.qtyError.push(false);
                                if (value.orderedProdQty > value.deliveredProdQty) {
                                    var remainingProdQtyVal = value.orderedProdQty - value.deliveredProdQty;
                                    var items = {
                                        itemId: value.itemId,
                                        orderedProdQty: value.orderedProdQty,
                                        remainingProdQty: remainingProdQtyVal,
                                        deliveredProdQty: value.orderedProdQty - value.deliveredProdQty
                                    };
                                    $scope.vo.data.itemList.push(items);

                                    $scope.vo.partialProduct.push($scope.vo.response.itemDeliverPaymentInfo.itemList[key]);
                                    $scope.vo.showPartialFields = true;
                                }

                            });
                        }
                        $scope.loading = false;

                    } else if (type === 'submitVerificationReport') {

                        $scope.loading = false;

                        veriReportModelSetting();
                        localStorageService.set("modalStatus", true);
                        modalOpen("success");

                    } else if (type === 'getTerms') {
                        $scope.loading = false;
                        $scope.paymentDetailsShow = false;
                        actualDownPayment1 = 0;
                        actualDownPayment2 = 0;

                        $scope.contract.downPayment1 = null;
                        $scope.contract.downPayment2 = null;
                        $scope.contract.totalAmountOrder = null;
                        $scope.contract.totalInstallmentAmount = null;
                        $scope.contract.totalOutstandingBalance = null;
                        $scope.installmentList = result.installmentList;


                    } else if (type === 'getPointList') {
                        $scope.loading = false;
                        $scope.paymentDetailsShow = false;
                        actualDownPayment1 = 0;
                        actualDownPayment2 = 0;

                        $scope.contract.downPayment1 = null;
                        $scope.contract.downPayment2 = null;
                        $scope.contract.totalAmountOrder = null;
                        $scope.contract.totalInstallmentAmount = null;
                        $scope.contract.totalOutstandingBalance = null;
                        $scope.pointList = result.pointList;
                    } else if (type === 'getProduct') {
                        $scope.productList1 = result.productList;
                    } else if (type === 'fetchListOfPriceList') {
                       // console.log(result);
                        $scope.contract.priceListCodeList = result.priceListCodeList;
                        itemPoint = result.itemPoint;
                        totalItemPoint=result.totalItemPoint;

                    } else if (type === "getSalesRepList") {
                        var a = null;
                        var salesRepList = result.salesRepList;
                        $scope.salesRepList = salesRepList;

                        angular.forEach(salesRepList, function(value, key) {
                            if (value === $scope.contract.salesRepName) {
                                a = key;
                                return true;
                            }
                        });

                        if (a !== null) $scope.adminCont.salesRepNameEdit = a;

                    }else if(type==="sendEmail"){
                        $scope.loading=false;
                    }
                    else if (type === "getSalesRepDetails") {
                        var userData = result;

                        localStorageService.set('currentRecord', userData);
                        if (event.ctrlKey) window.open('#/AdminBoard/CreateEditUser', '_blank'); // in new tab
                        else $state.go('AdminBoard.CreateEditUser');
                    }

                    //stickey Header
                    enableStickyHeader($(".sticky-block-view"));

                } else {
                    if (type === 'getPricingDetails') {
                        $scope.paymentDetailsShow = false;
                        actualDownPayment1 = 0;
                        actualDownPayment2 = 0;

                        $scope.contract.downPayment1 = null;
                        $scope.contract.downPayment2 = null;
                        $scope.contract.totalAmountOrder = null;
                        $scope.contract.totalInstallmentAmount = null;
                        $scope.contract.totalOutstandingBalance = null;
                    }
                    $scope.loading = false;
                }
            } else {

            if(type!=='getSalesRepList'){
            	console.log("Service Not Resolved");
                                $scope.loading = false;

                                errorModelSetting();
                                localStorageService.set("modalStatus", false);
                                modalOpen("error");
            }

            }

           // console.log(type);
           // console.log(result);
        };

        function error() {
            $scope.loading = false;
            console.log("Service not resolved");

            var appInUse = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

            if (!appInUse) {
                errorModelSetting();
                localStorageService.set("modalStatus", false);
                modalOpen("error");
            } else {
                if (type === 'viewContract') {

                    //alert(JSON.stringify($scope.contract));
                    var data = $scope.contract;

                    data.countryId=localStorageService.get('countryId');

                    app.viewContract(data, callbackViewContract);

                }

            }

        }
    }


    function callbackViewContract(result) {

        //alert(JSON.stringify(result));

        $scope.contract = result;
        $scope.referenceModel1 = result.customerDetails.relationships;

        $scope.payment = result.paymentDetails;
        //For mobile screen listing
        $scope.listing = {};
        $scope.listing.sortedByList = [{
            value: "paymentAmt",
            text: "Amount"
        }, {
            value: "lastPaymentDt",
            text: "Status Date"
        }];

        var limit = 10;
        var len = $scope.payment.length;
        $scope.listing.limit = len > limit ? limit : len;

        $scope.listing.showLoader = len > $scope.listing.limit ? true : false;
        $scope.listing.loadMore = function() {
            var increamented = $scope.listing.limit + limit;
            $scope.listing.limit = increamented > len ? len : increamented;
            $scope.listing.showLoader = len > $scope.listing.limit ? true : false;
        }

        $scope.remarkList = result.remarkDetails;
        for (var i = 0; i < $scope.remarkList.length; i++)
            $scope.remarkList[i].showShortRemark = true;
        stateCode = result.stateCode;
        $scope.paymentDetailsShow = true;
        if (result.salesRepContributionAmt !== null) {
            salesContr = result.salesRepContributionAmt;
        }

        if (result.paymentDetails !== null && result.paymentDetails !== undefined && result.paymentDetails.length > 0) {
            prevDp1 = result.paymentDetails[0].paymentAmt;
            paymentId = result.paymentDetails[0].paymentId;
            $scope.contract.paymentRefNum = result.paymentDetails[0].paymentRefNum;
        }

        $scope.methodOfPaymentList = result.paymentMethodList;

        //$scope.contract.customerDetails.icNumber = $scope.contract.customerDetails.icNo;
        $scope.review_terms = result.review_terms;

        if ($scope.remarkList[0] !== undefined)
            $scope.remarks.remarkCustomer = $scope.remarkList[0].remarks;
        if ($scope.remarkList[1] !== undefined)
            $scope.remarks.remarkProduct = $scope.remarkList[1].remarks;
        if ($scope.remarkList[2] !== undefined)
            $scope.remarks.remarkPricing = $scope.remarkList[2].remarks;

        $scope.salesRepContributionStatus = result.salesRepContribution;

        $scope.common.formatedDob = $scope.getCustomDate($scope.contract.customerDetails.dob);

        //get total points
        $scope.totalPoints = $scope.getTotalPoints(true);

        /*var prevBalance = 0;
                        var newPayment = {};
                        angular.forEach($scope.payment, function(value, key) {
                            $scope.payment[key].prevBalance = prevBalance;
                            prevBalance = value.orderDueAmt - value.paymentAmt;
                        });*/

        actualDownPayment1 = result.downPayment1;
        actualDownPayment2 = (result.downPayment2 !== undefined && result.downPayment2 !== null) ? result.downPayment2 : 0;

        $scope.contract.salesRepContributionAmt = result.salesRepContribution;

        var data = $scope.contract.terms;
        if (data === "COD" || data === "CAD")
            $scope.bank.bankId = data;
        else
            $scope.bank.bankId = data + " Months";

        if ($scope.contract.terms === "CAD") {
            $scope.text.instalmentAmountLbl = "Order Value";
            $scope.text.paymentMethodDP2Lbl = "Method of Payment For CAD";
        }

        if ($scope.contract.adjustment) $scope.dpLabel = "Adjustment";

        /*if ($scope.contract.status !== "Pending Vo Allocation" && $scope.contract.status !== "Pending VO Allocation") $scope.removeEditBtn = true;*/

        if (statusEditMode.indexOf($scope.contract.status) === -1) {
            $scope.removeEditBtn = true;
        }

        if (Session.userType.toLowerCase().indexOf("sales") > -1) $scope.removeEditBtn = true;

        /**********Cancel Contract Start************/
        $scope.showCancelTab = ($scope.contract.status === "Cancellation Initiated" || $scope.contract.status === "Cancelled") ? true : false;
        //$scope.showCancelTab = true;

        if (!$scope.isSalesRep) { //Not for Sales Rep

            $scope.vo.cancelQtyError = [];
            $scope.vo.cancelData = {};
            $scope.vo.cancelData.productDetails = [];
            angular.forEach($scope.contract.productDetails, function(value, key) {

                $scope.contract.productDetails[key].remainingProdQty = $scope.contract.productDetails[key].deliveredQty - $scope.contract.productDetails[key].returnedQty;
                //$scope.contract.productDetails[key].remarks = "";

                var obj = {
                    itemId: value.itemId,
                    quantity: $scope.contract.productDetails[key].quantity,
                    deliveredQty: $scope.contract.productDetails[key].deliveredQty,
                    returnedQty: $scope.contract.productDetails[key].returnedQty,
                    remainingProdQty: $scope.contract.productDetails[key].remainingProdQty,
                    verifiedProdQty: $scope.contract.productDetails[key].verifiedProdQty,
                    remarks: $scope.contract.productDetails[key].remarks
                };

                $scope.vo.cancelData.productDetails.push(obj);
                $scope.vo.cancelQtyError.push(false);
            });
        }
        $scope.vo.cancelData.cancelRequestDate = $scope.contract.cancelDate;
        cancelReason = $scope.vo.cancelData.cancelReason = $scope.contract.cancelReason;
        /**********Cancel Contract End************/

        //Get Sales Rep List
        getServiceCall("getSalesRepList");

        if ($state.current.name === 'VoBoard.ViewVerificationReport' || $scope.contract.status === "Confirmed" || $scope.contract.status === "Partially Delivered" || $scope.contract.status === "Delivered") {

            $scope.showReportTab = true;
            getServiceCall("viewVerifiedContract");

        } else {
            $scope.showReportTab = false;
        }
        if ($scope.isSalesRep) $scope.showReportTab = false;

        $scope.loading = false;

        //stickey Header
        enableStickyHeader($(".sticky-block-view"));

        $scope.$apply();
    }

    $scope.changeProduct = function(selectedVal, index) {

        $scope.productList1[productSelIndex[index]] = selectedVal;
        $scope.totalPoints = $scope.getTotalPoints(false);
    }
    $scope.productSelIndex = [];
    $scope.getTotalPoints = function(chkSelected) {
        var totalPoints = 0;
        $scope.totalQty = 0;
        if (chkSelected) $scope.productSelIndex = [];

        angular.forEach($scope.contract.productDetails, function(item, index) {
            if (item.hasOwnProperty("productPoints"))
                if (item.productPoints !== null && item.productPoints !== undefined && item.productPoints !== "")
                    totalPoints += parseFloat(item.productPoints);

            if (item.quantity !== null && item.quantity !== undefined && $.trim(item.quantity) !== "")
                $scope.totalQty += parseInt(item.quantity);

            if (chkSelected) {
                angular.forEach($scope.productList1, function(value, key) {
                    if (item.productId !== undefined && item.productId === value.productId)
                        $scope.productSelIndex.push(key);
                });
            }
        });
        $scope.getTotalPointsVal = totalPoints;
       // return totalPoints.toFixed(2);

        return totalPoints;
    }

    $scope.concatanateString = function(str) {
        if (str !== undefined && str !== null && str !== "")
            return str.replace(/\s+/g, '-').toLowerCase();
        return str;
    }
    $scope.cancellEditCustomer = function() {
        /* $scope.editCustSummary = false;
         $scope.editCustomer = false;
         $scope.editProduct = false;
         $scope.editPayment = false;
         //stickey Header
         enableStickyHeader($(".sticky-block-view"));*/
        $state.reload();
    }
    $scope.editContract = function(field) {
        switch (field) {
            case 'summary':
            case 'customer':
            case 'product':
            case 'payment':
                {
                    $scope.editCustSummary = true;
                    $scope.editCustomer = true;
                    $scope.editProduct = true;
                    $scope.editPayment = true;

                    //stickey Header
                    enableStickyHeader($(".sticky-block-view"));
                    break;
                }
        }
    }
    $scope.upgradeContract = function(type) {


        $scope.upgradeDowngrade=true;
        $scope.editCustSummary = false;
        $scope.editCustomer = false;
        $scope.editCustSummaryPen = true;
        $scope.editCustomerPen = true;

        $scope.editProduct = true;
        $scope.editPayment = true;
        $scope.upgradeDowngradeC=true;
        $scope.upgradeDowngradeContract=true;
        enableStickyHeader($(".sticky-block-view"));
        $scope.showNewContractSaveButton=true;
    }



    $scope.paymentList = {
        "lastPaymentDt": "Posting Details",
        //"paymentId": "OR No",
        //"orderDate": "OR Date",
        "paymentMethod": "Type",
        "paymentType":"Method",
        //"bankTagNo": "Bank",
        "bankAccountNo": "Account/Cheque No",
        "cheqDate": "Cheque Date/ Payment Date",
        "collectedBy":"Collected By",
        /*"preBalance": "Prev.Balance",*/
        "paymentAmt": "Amount Paid/Penality",
        "orderDueAmt": "Outstanding Balance",
        "statusDate": "Status Date",
        "bankInStatus":"Bank In Status"
    };

    $scope.addRemark = function() {
        getServiceCall("addRemark");
    }

    $scope.saveContract = function() {
        if (validationForCustomerDetailsEdit() && validationPricingDetailsEdit() && validationsProductDetailsEdit()) {
            var paymentDetails = [];
            paymentDetails.push({
                paymentId: paymentId,
                paymentType: "Deposit1",
                paymentMethod: $scope.contract.methodOfPaymentDP1,
                orderDueAmt: $scope.contract.orderDueAmt,
                paymentAmt: prevDp1,
                paymentRefNum: $scope.contract.paymentRefNum,
                bankId:($scope.contract.bankDp1===undefined||$scope.contract.bankDp1===null)?null:$scope.contract.bankDp1.bankId

            });
            paymentDetails.push({
                paymentType: "Deposit2",
                paymentMethod: $scope.contract.methodOfPaymentDP2,
                orderDueAmt: $scope.contract.orderDueAmt,
                paymentAmt: $scope.contract.downPayment2,
                paymentRefNum: $scope.contract.paymentRefNum,
                bankId:($scope.contract.bankDp2===undefined||$scope.contract.bankDp2===null)?null:$scope.contract.bankDp2.bankId

            });
            if ($scope.contract.salesRepContributionStatus) {
                var srType = "";
                if ($scope.contract.commission) {
                    srType = "Commission"
                } else {
                    srType = "Cash"
                }
                paymentDetails.push({
                    paymentType: "SalesRepContribution",
                    paymentMethod: srType,
                    orderDueAmt: $scope.contract.orderDueAmt,
                    paymentAmt: $scope.contract.salesRepContributionAmt,
                    paymentRefNum: $scope.contract.paymentRefNum,
                    bankAccountNo: "5201258633",
                    bankName: "BankAbc"

                });
            }
            $scope.contract.paymentDetails = paymentDetails;
            if (adjustStatus) {
                $scope.contract.adjustment = true;
            } else {
                $scope.contract.adjustment = false;
            }
            //console.log($scope.contract);
            getServiceCall("editContract");
        }

    }

    $scope.fetchPriceList = function() {
        getServiceCall("fetchListOfPriceList");
    }

    //enableStickyHeader ($(".sticky_images_tab"));
    enableStickyHeader($(".sticky-block-view"));
    //Stickey Header
    function enableStickyHeader(obj) {
        var didScroll = true;
        $(window).scroll(function(event) {
            if (didScroll) {
                didScroll = false;
                stickyHeader.enableStickyHeader(obj);
            }
        });
    }

    //setting view Verification Report tab data
    function setViewVerificationReportTab() {

        //$scope.vo = {};
        $scope.vo.submitVerificationReport = function() {

            if(chequeDateValidation()){
                getServiceCall("submitVerificationReport");
            }

            //console.log($scope.vo.data);
        }

        $scope.vo.voVerificationForm = {};
        $scope.vo.validateInput = function(index, orderedProdQty) {

            $scope.vo.voVerificationForm.disableSubmit = true;
            $scope.vo.qtyError[index] = true;
            $scope.showDeliveryError = false;

            var deliveredProdQty = $.trim($scope.vo.data.itemList[index].deliveredProdQty);
            var pattern = /^(\d)+$/;
            if (pattern.test(deliveredProdQty) && (orderedProdQty - deliveredProdQty >= 0)) {

                $scope.vo.qtyError[index] = false;
                var submitDisable = false;
                var count = 0;
                var len = $scope.vo.data.itemList.length;
                angular.forEach($scope.vo.qtyError, function(value, key) {
                    if (len > key && $scope.vo.data.itemList[key].deliveredProdQty > 0) { count++; }
                    if (value) {
                        submitDisable = true;
                        return true;
                    }
                });
                if (count === 0) {$scope.showDeliveryError = true; submitDisable = true;}
                $scope.vo.voVerificationForm.disableSubmit = submitDisable;
                //$scope.deliveryQuantityCheck();
            }
        }
        $scope.deliveryQuantityCheck = function() {
            $scope.vo.voVerificationForm.disableSubmit = true;
            $scope.showDeliveryError = false;
            var count = 0;
            for (var i = 0; i < $scope.vo.data.itemList.length; i++) {
                if ($scope.vo.data.itemList[i].deliveredProdQty > 0) {
                    $scope.vo.voVerificationForm.disableSubmit = false;
                    count++;
                    break;
                }
            }
            if (count === 0)
                $scope.showDeliveryError = true;
        }

        $scope.vo.reportProductColList = [{
            col: "S.No",
            width: "10%"
        }, {
            col: "Products",
            width: "50%"
        }, {
            col: "Ordered Qty",
            width: "12%"
        }, {
            col: "Delivered Qty",
            width: "12%"
        }];

        $scope.vo.remainingProductColList = [{
            col: "S.No",
            width: "10%"
        }, {
            col: "Products",
            width: "50%"
        }, {
            col: "Remaining Qty",
            width: "12%"
        }, {
            col: "Delivered Qty",
            width: "12%"
        }];

        managePageLink = 'VoBoard.VoCompleteRequest';
        modalEntity = "Delivery details of ";
        backText = "Back to Manage Verification";
        createText = "View Verification Report";
        editText = " has been updated successfully";
    }

    //setting cancellation Report tab data
    function setCancellationReportTab() {

        //$scope.vo = {};

        $scope.vo.submitCancellationRequest = function() {

            confirmModelSetting();
            localStorageService.set("modalStatus", true);
            modalOpen("success");
        }

        $scope.vo.approveCancellation = function() {

            confirmModelSetting();
            localStorageService.set("modalStatus", true);
            modalOpen("success");
        }
        $scope.vo.rejectCancellation = function() {

            $scope.rejection = true;
            confirmModelSettingReject();
            localStorageService.set("modalStatus", true);
            modalOpen("success");
        }

        $scope.vo.cancelData = {};

        ///Reset Start
        if (!$scope.isSalesRep) { //Not for Sales Rep
            //cancelReason = $scope.vo.cancelData.cancelReason = "Customer doesn't have enough money to pay for all the products.";

            $scope.vo.resetCancellationRequest = function() {

                $scope.vo.cancelData.cancelReason = cancelReason;
                angular.forEach($scope.contract.productDetails, function(value, key) {
                    $scope.vo.cancelData.productDetails[key].returnedQty = value.returnedQty;
                    $scope.vo.cancelData.productDetails[key].verifiedProdQty = value.verifiedProdQty;
                    $scope.vo.cancelData.productDetails[key].remarks = value.remarks;

                    $scope.vo.cancelQtyError[key] = false;
                    $scope.vo.voCancellationForm.disableSubmit = false;
                });
            }
        }
        ///Reset End

        $scope.vo.validateCancelQtyInput = function(index, deliveredQty, returnedQty) {

            $scope.vo.voCancellationForm.disableSubmit = true;
            $scope.vo.cancelQtyError[index] = true;

            var pattern = /^(\d)+$/;
            if (pattern.test(returnedQty)) {

                $scope.vo.cancelData.productDetails[index].remainingProdQty = deliveredQty - returnedQty;
                if (deliveredQty - returnedQty >= 0) {

                    $scope.vo.cancelQtyError[index] = false;
                    var submitDisable = false;
                    angular.forEach($scope.vo.cancelQtyError, function(value, key) {
                        if (value) {
                            submitDisable = true;
                            return true;
                        }
                    });
                    $scope.vo.voCancellationForm.disableSubmit = submitDisable;
                }
            }
        }

        $scope.vo.voCancellationForm = {};

        $scope.vo.cancelProductColList = [{
            col: "S.No",
            width: "8%"
        }, {
            col: "Product",
            width: "36%"
        }, {
            col: "Ordered Qty",
            width: "8%"
        }, {
            col: "Delivered Qty",
            width: "8%"
        }, {
            col: "Returned Qty",
            width: "8%"
        }, {
            col: "Remaining Qty",
            width: "9%"
        }, {
            col: "Remarks (Optional)",
            width: "23%"
        }];
        $scope.vo.cancelProductSalesCol = [{
            col: "S.No",
            width: "10%"
        }, {
            col: "Product",
            width: "78%"
        }, {
            col: "Ordered Qty",
            width: "12%"
        }];
        $scope.vo.adminCancelProductColList = [{
            col: "S.No",
            width: "8%"
        }, {
            col: "Product",
            width: "36%"
        }, {
            col: "Ordered Qty",
            width: "8%"
        }, {
            col: "Delivered Qty",
            width: "8%"
        }, {
            col: "Returned Qty",
            width: "8%"
        }, {
            col: "Verified Qty",
            width: "9%"
        }, {
            col: "Remarks (Optional)",
            width: "23%"
        }];

        $scope.showCancelInitiateBtn = function() {
            var show = false;
            /*if (($scope.contract.status !== 'Cancellation Initiated' && $scope.contract.status !== 'Cancelled' && $scope.contract.status !== 'Closed') &&
                ($scope.isCO || ($scope.isSalesRep && $scope.contract.status === 'Pending VO Allocation'))) {
                show = true;
            }*/

            if (($scope.contract.status !== 'Cancellation Initiated'
                && $scope.contract.status !== 'Cancelled'
                    && $scope.contract.status.toLowerCase() !== 'cancellation approved for product reprocessed'
                        && $scope.contract.status.toLowerCase() !== 'product reprocessed'  && $scope.contract.status !== 'Closed') && ($scope.isCO || $scope.isAdmin)) {
                show = true;
            }
            return show;
        }
    }

    function editContractModelSetting() {
        managePageLink = $scope.previousState;
        modalEntity = "Contract ";
        backText = $scope.previousStateTitle;
        editText = "Edited Successfully";
        createText = "Create Another Contract";
        grayBtnText = "";
    }

    function veriReportModelSetting() {
        //managePageLink = $scope.previousState;
        managePageLink = "VoBoard.ViewVerificationReport";
        modalEntity = "Delivery details for contract ";
        //backText = $scope.previousStateTitle;
        backText = "OK";
        editText = " has been updated successfully";
        createText = "";
        grayBtnText = "";
    }

    function priceDiffModelSetting() {
        managePageLink = $scope.previousState;
        modalEntity = "Verification ";
        backText = $scope.previousStateTitle;
        editText = "";
        createText = " View Verification Report ";
        grayBtnText = "";
    }

    function confirmModelSetting() {
        managePageLink = $scope.previousState;
        modalEntity = "Are you sure you want to save the cancellation request for contract ";
        backText = $scope.previousStateTitle;
        createText = "Yes";
        grayBtnText = "No";
        editText = "";
        confirmModal = true;
    }

    function confirmModelSettingReject() {
        managePageLink = $scope.previousState;
        modalEntity = "Are you sure you want to reject the cancellation request for contract ";
        backText = $scope.previousStateTitle;
        createText = "Yes";
        grayBtnText = "No";
        editText = "";
        confirmModal = true;
    }

    function cancelContractModelSetting() {
        managePageLink = $scope.previousState;
        modalEntity = "Contract cancellation request for contract ";
        backText = $scope.previousStateTitle;
        editText = " has been submitted successfully";
        createText = "";
        grayBtnText = "";
    }

    function rejectContractModelSetting() {
        managePageLink = $scope.previousState;
        modalEntity = "Contract cancellation request for contract ";
        backText = $scope.previousStateTitle;
        editText = " has been declined successfully";
        createText = "";
        grayBtnText = "";
    }

    function errorModelSetting() {
        managePageLink = $scope.previousState;
        modalEntity = "";
        backText = $scope.previousStateTitle;
        editText = "";
        createText = "";
        grayBtnText = "";
    }


    $scope.cancelThisContract = function() {
        $state.go("AdminBoard.CancelContract");
    }

    //setting cancellation Report tab data
    function initiateCancellationRequest() {
        //console.log($scope.vo.cancelData);
        if ($scope.isCO || $scope.isSalesRep)
            getServiceCall("cancelContract");
        else if ($scope.isAdmin||$scope.isCM)
            getServiceCall("approveCancelContract");
    }

    //Reject cancel request
    function rejectCancellationRequest() {
        if ($scope.isAdmin || $scope.isCM)
            getServiceCall("rejectCancelContract");
    }


    //Create Edit Functionalities
    $scope.enableRefremove = false;
    $scope.showCreateContract = false;
    $scope.editContractCustomerDetail = false;

    $scope.models = {};
    /*$scope.changeQuantity = function (i) {
        if ($scope.contract.productDetails[i].quantity >= 0)
            $scope.totalPoints = $scope.getTotalPoints (false);
    }*/
    $scope.incrementer = function(a, i) {
        if ($scope.contract.productDetails[i].productName !== '') {
            $scope.contract.productDetails[i].quantity = parseInt(a) + 1;
            $scope.contract.productDetails[i].productPoints = parseFloat(parseFloat($scope.contract.productDetails[i].productPoint) * $scope.contract.productDetails[i].quantity).toFixed(2);
            $scope.totalPoints = $scope.getTotalPoints(true);
            getServiceCall("fetchListOfPriceList");
        }
    }
    $scope.decrementer = function(a, i) {
        if ($scope.contract.productDetails[i].productName !== '') {
            if (parseInt(a) > 1) {
                $scope.contract.productDetails[i].quantity = parseInt(a) - 1;
                $scope.contract.productDetails[i].productPoints = parseFloat(parseFloat($scope.contract.productDetails[i].productPoint) * $scope.contract.productDetails[i].quantity).toFixed(2);
                $scope.totalPoints = $scope.getTotalPoints(true);
                getServiceCall("fetchListOfPriceList");
            }
        }

    }

    $scope.changeQuantity = function(a, d) {
        if ($scope.contract.productDetails[d].productName !== '') {
            $scope.contract.productDetails[d].productPoints = parseFloat(parseFloat($scope.contract.productDetails[d].productPoint) * $scope.contract.productDetails[d].quantity).toFixed(2);

            if ($scope.contract.productDetails[d].quantity >= 0) {
                $scope.totalPoints = $scope.getTotalPoints(false);
                getServiceCall("fetchListOfPriceList");
            }
        }
    }
    $scope.productList1 = [];
    $scope.productModel = [{
        "spinners": 0,
        "products": ''
    }, {
        "spinners": 0,
        "products": ''
    }, {
        "spinners": 0,
        "products": ''
    }];
    $scope.removeProductRow = function(index) {



        //stickey Header
        enableStickyHeader($(".sticky-block-view"));
        if ($scope.contract.productDetails.length > 1 /*&& ($scope.contract.productDetails[index].deliveredQty<=0||$scope.contract.productDetails[index].deliveredQty===undefined) && $scope.contract.productDetails[index].newlyAdded*/) {

            if($scope.upgradeDowngradeC){
                removedProductId.push($scope.contract.productDetails[index].itemId);
            }



            $scope.contract.productDetails.splice(index, 1);
            $scope.myprod.selProductDetails.splice(index, 1);
            $scope.totalPoints = $scope.getTotalPoints(false);

            //stickey Header
            enableStickyHeader($(".sticky-block"));
            getServiceCall("fetchListOfPriceList");
        }
    };
    $scope.checkContProdAddBtn = function(index) {
        if ($scope.contract.productDetails.length === (index + 1)) return true;
        return false;
    }
    $scope.addProductRow = function() {
        $scope.contract.productDetails.push({
            id: "",
            productId: "",
            productName: "",
            productPoint: "",
            quantity: 1,
            newlyAdded: true
        });

        //stickey Header
        enableStickyHeader($(".sticky-block-view"));
        $scope.totalPoints = $scope.getTotalPoints(false);
    };

    $scope.myprod.selProductDetails = [];
    $scope.selDetails = [];
    $scope.productDetailErr = [];
    $scope.selectProduct = function(selProduct, parentIndex, index) {
    $("#prodEditError").css('display', 'none');
        if (chkIsNewProduct(selProduct, $scope.myprod.selProductDetails, parentIndex)) {
            $scope.selDetails[parentIndex] = "";
            $scope.contract.productDetails[parentIndex].productId = selProduct.productId;
            $scope.contract.productDetails[parentIndex].productName = $scope.myprod.selProductDetails[parentIndex] = selProduct.productName;

            if (selProduct.hasOwnProperty("productPoints"))
                if (selProduct.productPoints !== null && selProduct.productPoints !== undefined && selProduct.productPoints !== "") {
                    $scope.contract.productDetails[parentIndex].productPoints = selProduct.productPoints;
                    $scope.contract.productDetails[parentIndex].productPoint = selProduct.productPoint;
                }


            if (selProduct.quantity !== null && selProduct.quantity !== undefined && $.trim(selProduct.quantity) !== "")
                $scope.contract.productDetails[parentIndex].quantity = selProduct.quantity;

            $scope.totalPoints = $scope.getTotalPoints(false);

            type = "fetchListOfPriceList";
            getServiceCall("fetchListOfPriceList");
        }
    }

    $scope.selectProductMobile = function(selProduct, parentIndex) {
    $("#prodEditError").css('display', 'none');
        if (chkIsNewProduct(selProduct, $scope.myprod.selProductDetails, parentIndex)) {
            $scope.selDetails[parentIndex] = "";
            $scope.contract.productDetails[parentIndex].productId = selProduct.productId;
            $scope.contract.productDetails[parentIndex].productName = selProduct.productName;

            if (selProduct.hasOwnProperty("productPoints"))
                if (selProduct.productPoints !== null && selProduct.productPoints !== undefined && selProduct.productPoints !== "")
                    $scope.contract.productDetails[parentIndex].productPoints = selProduct.productPoints;

            if (selProduct.quantity !== null && selProduct.quantity !== undefined && $.trim(selProduct.quantity) !== "")
                $scope.contract.productDetails[parentIndex].quantity = selProduct.quantity;

            $scope.totalPoints = $scope.getTotalPoints(false);

            type = "fetchListOfPriceList";
            getServiceCall("fetchListOfPriceList");
        }
    }

    function chkIsNewProduct(selProduct, selProductList, parentIndex) {
        var newProd = true;
        $("#prodEditError").css('display', 'none');
                var prevIndex=selProductList.indexOf(selProduct.productName);
                if(prevIndex>=0){
                    if(prevIndex===parentIndex){
                        return newProd;
                    }
                }
        $scope.showProductDetailErr = $scope.productDetailErr[parentIndex] = false;
        angular.forEach(selProductList, function(value, key) {
            //if (value === selProduct.productName) {
            if ((typeof value === 'object' && parentIndex !== key && value.productName === selProduct.productName) || (value === selProduct.productName)) {
                newProd = false;
                $scope.productDetailErr[parentIndex] = true;
                return true;
            }
            if (newProd) $scope.hideOption = true;
        });
        angular.forEach($scope.productDetailErr, function(value, key) {
            if (value) {
                $scope.showProductDetailErr = true;
                return true;
            }
        });

        return newProd;
    }

    $scope.referenceModel1 = [{
        "relation": "",
        "name": "",
        "phoneNo": "",
        "email": "",
        "addressLine1": "",
        "addressLine2": "",
        "cityName": "",
        "stateName": "",
        "pinCode": "",
        "newlyAdded": true
    }];

    if ($scope.referenceModel1.length > 1) $scope.enableRefremove = true;
    $scope.addReferenceDiv = function() {
        $scope.referenceModel1.push({
            "relation": "",
            "name": "",
            "phoneNo": "",
            "email": "",
            "addressLine1": "",
            "addressLine2": "",
            "cityName": "",
            "stateName": "",
            "pinCode": "",
            "newlyAdded": true
        });
        $scope.enableRefremove = true;
        //stickey Header
        enableStickyHeader($(".sticky-block-view"));
    }
    $scope.removeReferenceDiv = function(index) {
        if (index > -1) {
            $scope.referenceModel1.splice(index, 1);
        }
        if ($scope.referenceModel1.length < 2) $scope.enableRefremove = false;
        //stickey Header
        enableStickyHeader($(".sticky-block-view"));
    }


    $scope.nameEditError = true;
    $scope.icEditError = true;
    $scope.emailEditError = true;
    $scope.phoneEditError = true;
    $scope.mobileEditError = true;
    $scope.Homeaddr1EditError = true;
    $scope.HomecityEditError = true;
    $scope.HomeStateEditError = true;
    $scope.homePinEditError = true;
    $scope.companyEditNameError = true;
    $scope.companyDesgEditError = true;
    $scope.companyAdd1EditError = true;
    $scope.companyCityEditError = true;
    $scope.companyStateEditError = true;
    $scope.companyPincodeEditError = true;
    $scope.prodDetailsEditError = true;
    $scope.priceListCodeEditError = true;
    $scope.pricingPointsEditError = true;
    $scope.termsEditError = true;
    $scope.dp1EditError = true;
    $scope.methodOfDp1EditError = true;
    $scope.dp2EditMethodError = true;
    $scope.refError = true;
    $scope.contributionEditError = true;
    $scope.bankDp1EditError=true;
    $scope.bankDp2EditError=true;

    function validationForCustomerDetailsEdit() {
        $scope.nameEditError = validate.validateText($scope.contract.customerDetails.name);

        $scope.icEditError = validate.validateText($scope.contract.customerDetails.icNo);

        $scope.emailEditError = validate.validateEmail($scope.contract.customerDetails.email);

        $scope.phoneEditError = validate.phonenumber($scope.contract.customerDetails.phonoNo);

        $scope.mobileEditError = validate.phonenumber($scope.contract.customerDetails.altPhoneNo);

        $scope.Homeaddr1EditError = validate.validateText($scope.contract.customerDetails.homeAddressLine1);

        $scope.HomecityEditError = validate.validateText($scope.contract.customerDetails.homeCity);

        $scope.HomeStateEditError = validate.validateText($scope.contract.customerDetails.homeState);
        if ($scope.contract.customerDetails.homeState === "-State-")
            $scope.HomeStateEditError = false;

        $scope.homePinEditError = validate.validateText($scope.contract.customerDetails.homePinCode);

        $scope.companyEditNameError = validate.validateText($scope.contract.customerDetails.company);

        $scope.companyDesgEditError = validate.validateText($scope.contract.customerDetails.desg);

        $scope.companyAdd1EditError = validate.validateText($scope.contract.customerDetails.compAddressLine1);

        $scope.companyCityEditError = validate.validateText($scope.contract.customerDetails.compCity);

        $scope.companyStateEditError = validate.validateText($scope.contract.customerDetails.compState);
        if ($scope.contract.customerDetails.compState === "-State-")
            $scope.companyStateEditError = false;

        $scope.companyPincodeEditError = validate.validateText($scope.contract.customerDetails.compPinCode);

        var resultCustomerDetails = $scope.nameEditError && $scope.icEditError && $scope.emailEditError && $scope.phoneEditError && $scope.mobileEditError && $scope.Homeaddr1EditError
        $scope.HomecityEditError && $scope.HomeStateEditError && $scope.homePinEditError && $scope.companyEditNameError && $scope.companyDesgEditError &&
            $scope.companyAdd1EditError && $scope.companyCityEditError && $scope.companyStateEditError && $scope.companyPincodeEditError;
        return resultCustomerDetails;
    }

    function validationsProductDetailsEdit() {
        $scope.prodDetailsEditError = true;
        for (var i = 0; i < $scope.contract.productDetails.length; i++) {
            var er1 = validate.validateText($scope.contract.productDetails[i].productName);
            var er2 = true;
            if ($scope.contract.productDetails[i].quantity < 1)
                er2 = false;
            var res = er1 && er2;
            if (res === false){
            	$scope.prodDetailsEditError = false;
            	break;
            }


        }
        if ($scope.prodDetailsEditError === false)
            $("#prodEditError").css('display', 'block');
        else
            $("#prodEditError").css('display', 'none');
        return $scope.prodDetailsEditError;
    }

    function validationPricingDetailsEdit() {
        $scope.priceListCodeEditError = validate.validateText($scope.contract.priceListCode);
        $scope.pricingPointsEditError = validate.validateText($scope.contract.points);
        $scope.termsEditError = validate.validateText($scope.contract.terms);
        $scope.dp1EditError = validate.validateText($scope.contract.downPayment1);
        /*$scope.methodOfDp1EditError=validate.validateText($scope.contract.methodOfPaymentDP1);
        $scope.dp2EditMethodError=validate.validateText($scope.contract.methodOfPaymentDP2);*/

        if (parseFloat($scope.contract.downPayment1) != 0)
            $scope.methodOfDp1EditError = validate.validateText($scope.contract.methodOfPaymentDP1);

        if ($scope.contract.installmentAmt != undefined && $scope.contract.installmentAmt != null && parseFloat($scope.contract.installmentAmt) != 0)
            $scope.dp2EditMethodError = validate.validateText($scope.contract.methodOfPaymentDP2);

        if ($scope.contract.terms === 'COD')
            $scope.dp2EditMethodError = true;

        if (parseFloat($scope.contract.downPayment1) != 0)
            $scope.refError = validate.validateText($scope.contract.paymentRefNum);
        if ($scope.contract.salesRepContributionStatus === true)
            $scope.contributionEditError = validate.validateDecimal($scope.contract.salesRepContributionAmt);
        else
            $scope.contributionEditError = true;

        if($scope.contract.methodOfPaymentDP1==='Credit Card'){
            if($scope.contract.bankDp1!==undefined||$scope.contract.bankDp1!==null){
                $scope.bankDp1EditError=validate.validateText($scope.contract.bankDp1.bankId);
            }else if($scope.contract.bankDp1===undefined||$scope.contract.bankDp1===null){
                $scope.bankDp1EditError=false;
            }else{
                $scope.bankDp1EditError=true;
            }

        }

        /*if($scope.contract.methodOfPaymentDP2==='Credit Card'){
            if($scope.contract.bankDp2!==undefined||$scope.contract.bankDp2!==null){
                $scope.bankDp2EditError=validate.validateText($scope.contract.bankDp2.bankId);
            }else if($scope.contract.bankDp2===undefined||$scope.contract.bankDp2===null){
                $scope.bankDp2EditError=false;
            }else{
                $scope.bankDp2EditError=true;
            }
        }*/



        var result = $scope.priceListCodeEditError && $scope.pricingPointsEditError && $scope.termsEditError && $scope.dp1EditError && $scope.methodOfDp1EditError && $scope.dp2EditMethodError && $scope.refError && $scope.contributionEditError && $scope.bankDp1EditError && $scope.bankDp2EditError  ;
        return result;
    }
    $scope.generateAmount = function(ctype) {

        var dp1;

        if (parseFloat($scope.contract.downPayment1) === 0) {
            dp1 = 0;
        } else {
            dp1 = parseFloat($scope.contract.downPayment1) || undefined;
        }

        var dp2 = parseFloat($scope.contract.downPayment2) || 0;
        var saleRepAmt = parseFloat($scope.contract.salesRepContributionAmt) || undefined;
        var saleRepRlAmt = parseFloat($scope.contract.salesRepContributionAmt) || 0;
        var saleRepStatus = $scope.contract.salesRepContributionStatus;

        var total = dp1 + dp2 + saleRepRlAmt || undefined;
        var actualTotal = parseFloat(actualDownPayment1) + parseFloat(actualDownPayment2);

        if (angular.isDefined(ctype) && ctype === 'dp1') {
            if (dp1 === undefined || dp1 < 0 || dp1 > actualTotal || total > actualTotal) {

                $scope.contract.downPayment1 = actualDownPayment1;
                $scope.contract.downPayment2 = actualDownPayment2;
                $scope.contract.salesRepContributionAmt = undefined;

            } else {
                if (saleRepAmt === undefined || saleRepAmt < 0 || !saleRepStatus) {
                    $scope.contract.downPayment2 = actualTotal - dp1;
                    $scope.contract.salesRepContributionAmt = undefined;

                } else {
                    $scope.contract.downPayment2 = actualTotal - dp1 - saleRepAmt;
                }
            }

        } else {
            var editTotal = dp1 + saleRepAmt || undefined;
            if (saleRepAmt === undefined || saleRepAmt < 0 || saleRepAmt > actualTotal ||
                editTotal === undefined || editTotal > actualTotal || !saleRepStatus) {

                $scope.contract.downPayment2 = actualTotal - dp1;
                $scope.contract.salesRepContributionAmt = undefined;

            } else {
                $scope.contract.downPayment2 = actualTotal - dp1 - saleRepAmt;
            }
        }

        /*var dp1 = parseInt($scope.contract.downPayment1);
        var dp2 = parseInt($scope.contract.downPayment2);
        var saleRepAmt = parseInt($scope.contract.salesRepContributionAmt);
        var total = dp1 + dp2;
        var actualTotal = parseInt(actualDownPayment1) + parseInt(actualDownPayment2);

        if ($scope.contract.downPayment1 === undefined || $scope.contract.downPayment1 < 0 || $scope.contract.downPayment1 > actualTotal || $scope.contract.salesRepContributionAmt > actualTotal || $scope.contract.salesRepContributionAmt < 0) {
            $scope.contract.downPayment1 = actualDownPayment1;
            $scope.contract.downPayment2 = actualDownPayment2;
            $scope.contract.salesRepContributionAmt = undefined;
        } else if ($scope.contract.salesRepContributionAmt === undefined) {
            $scope.contract.downPayment2 = actualTotal - dp1;

        } else {
            if (saleRepAmt > dp2) {
                $scope.contract.downPayment2 = 0;
                $scope.contract.downPayment1 = actualTotal - saleRepAmt;

            } else {
                $scope.contract.downPayment2 = actualTotal - dp1 - saleRepAmt;
                if (ctype === 'dp1') {
                    $scope.contract.salesRepContributionAmt = undefined;
                    $scope.contract.downPayment2 = actualTotal - dp1;
                }
            }
        }*/
    }
    $scope.getPricingDetails = function() {
        if ($scope.contract.points !== "" && $scope.contract.points !== null) {
            if (validate.validateText($scope.contract.priceListCode) && validate.validateText($scope.contract.points) && validate.validateText($scope.contract.terms) && validate.validateDecimal($scope.contract.points)) {
                //vo can only change status to COD
                /*if ($scope.isVO && $scope.contract.terms === "COD") {
                    getServiceCall("getPricingDetails");
                } else if (!$scope.isVO) {
                    getServiceCall("getPricingDetails");
                }*/
                getServiceCall("getPricingDetails");

            }
            if (!validate.validateDecimal($scope.contract.points))
                $scope.pricingPointsEditError = false;
            else
                $scope.pricingPointsEditError = true;
        }

    }
    $(".allownumericwithdecimal").on("keypress keyup blur", function(event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which !== 46 || $(this).val().indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $scope.getTerms = function() {
        getServiceCall("getTerms");
        $scope.paymentDetailsShow = false;
        $scope.installmentList = null;
        actualDownPayment1 = 0;
        actualDownPayment2 = 0;

        $scope.contract.downPayment1 = null;
        $scope.contract.downPayment2 = null;
        $scope.contract.totalAmountOrder = null;
        $scope.contract.totalInstallmentAmount = null;
        $scope.contract.totalOutstandingBalance = null;
    }

    $scope.getPointList = function() {
        if ($scope.contract.terms === "CAD") {
            $scope.text.instalmentAmountLbl = "Order Value";
            $scope.text.paymentMethodDP2Lbl = "Method of Payment For CAD";
        } else {
            $scope.text.instalmentAmountLbl = "Monthly Instalment Amount";
            $scope.text.paymentMethodDP2Lbl = "Method of Payment For Instalment";
        }
        getServiceCall("getPointList");
        $scope.paymentDetailsShow = false;
        $scope.pointList = null;
        actualDownPayment1 = 0;
        actualDownPayment2 = 0;

        $scope.contract.downPayment1 = null;
        $scope.contract.downPayment2 = null;
        $scope.contract.totalAmountOrder = null;
        $scope.contract.totalInstallmentAmount = null;
        $scope.contract.totalOutstandingBalance = null;
    }
    $scope.getRow = function(rowId) {
        if (parseInt(rowId) % 2 === 0)
            return true;
        else return false;
    }
    $scope.getProductList = function(stateName) {
        stateNameForProduct = stateName;
        getServiceCall("getProduct");
    }
    $scope.toggleSalesRep = function() {
        if (!$scope.contributionEditError) {
            $scope.contributionEditError = true;
        }
    }
    $scope.readMore = function(pos) {
        for (var i = 0; i < $scope.remarkList.length; i++) {
            if (i === pos) {
                $scope.remarkList[i].showShortRemark = false;
                break;
            }

        }
    }

    $scope.readLess = function(pos) {
        for (var i = 0; i < $scope.remarkList.length; i++) {
            if (i === pos) {
                $scope.remarkList[i].showShortRemark = true;
                break;
            }

        }
    }
    $scope.shortRemark = function(text) {
        var shortText = '';
        if (text.length > 200) {
            shortText = text.substring(0, 200);
            return shortText;
        } else
            return text;
    }

    $scope.sendEmail = function(e) {
        e.preventDefault();
        /*var animation = true;
        var templateUrl = 'app/modals/utility-menu/email-config.html';
        var controller = 'emailConfigCtrl';
        $scope.openColumnConfig(animation, templateUrl, controller);*/
        generatePDF();
        var requestPdf="";
      setTimeout(function() {
        if (generatedPdf !== "") {
            var newGeneratedPdf = generatedPdf.replace("data:application/pdf;base64,", "");
            requestPdf = newGeneratedPdf;
            console.log(requestPdf);
             getServiceCall("sendEmail");
        }
             }, 1000);
        }







    $scope.printPage = function(e) {
        e.preventDefault();
        window.print();
        return false;
    }

    $scope.openColumnConfig = function(animation, templateUrl, controller) {
        var modalInstance = $uibModal.open({
            animation: animation,
            templateUrl: templateUrl,
            controller: controller,
            windowClass: 'center-modal',
            backdrop: 'static',
            size: 'lg'
        }).result.then(function(data) {
            if (data === "sendEmail") {

            }
        });
    };
    $scope.acceptByDM = function() {
        $scope.showDMTermsError = false;
        if($scope.isAdmin){
            getServiceCall('acceptedByDM');
        }
        else if ($scope.acceptResponsibility) {
            $scope.showDMTermsError = false;
            getServiceCall('acceptedByDM');
        } else
            $scope.showDMTermsError = true;
    }
    $scope.rejectedByDM = function() {
        $scope.showDMTermsError = false;
        getServiceCall('rejecetdByDM');
    }
    $scope.openDMModal = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/modals/role/modal.html',
            controller: 'modalCtrl',
            windowClass: 'center-modal',
            backdrop: 'static',
            keyboard: false,
            size: 'md'
        }).result.then(function(data) {
            if (data === "close") {
                if($state.current.name==='AdminBoard.ViewContract')
                    $state.go('AdminBoard.ManageContract');
                else
                $state.go('SalesRep.ManageContractSales');
            }
            if (data === "createAnother") {
                $state.reload();
            }
        });
    };

    function modalOpenDM(modalType) {
        if (errorName !== null && errorName !== "") {
            if (modalType === "success") {
                var modalObj = {
                    "modalEntity": modalEntity,
                    "body": modalBody,
                    "modalBtnText": "OK",
                    "backLink": "OK",
                    "singleBtn": true
                };
            } else {
                var modalObj = {
                    "modalEntity": "Failed!! ",
                    "body": errorMessage,
                    "modalBtnText": "",
                    "backLink": "OK",
                    "singleBtn": true
                };
            }

            localStorageService.set("modalObj",
                modalObj);
            $scope.openDMModal();
        }
    }

    function convertSysDate(date) {
        var dtArr = date.split("-");
        //var timeZone = "T00:00:00.000Z";
        var month = ('0' + (parseInt($.inArray(dtArr[1], shortMonthArr)) + 1)).slice(-2);
        var custDob = dtArr[2] + "-" + month + "-" + dtArr[0];
        return new Date(custDob);
    }
    $scope.newSelesRep = function() {
        //$scope.salesRepList;
        //$scope.adminCont.salesRepNameEdit;
        $scope.contract.salesRepName = $scope.salesRepList[$scope.adminCont.salesRepNameEdit];

    }
    $scope.withinThirtykm=true;
    $scope.changeLocationWithin30=function(){
        $scope.withinThirtykm=!$scope.withinThirtykm;
    }
    $scope.getMonthNo=function(monthName){
        var month;
            switch(monthName.toLowerCase()){
            case 'jan':
                month=0;
                break;
            case 'feb':
                month=1;
                break;
            case 'mar':
                month=2;
                break;
            case 'apr':
                month=3;
                break;
            case 'may':
                month=4;
                break;
            case 'jun':
                month=5;
                break;
            case 'jul':
                month=6;
                break;
            case 'aug':
                month=7;
                break;
            case 'sep':
                month=8;
                break;
            case 'oct':
                month=9;
                break;
            case 'nov':
                month=10;
                break;
            case 'dec':
                month=11;
                break;
        }
            return month;
    }
    $scope.approveOrRejectCancellation=function(opt){
        $scope.submitPressedApproveReject=true;
        if(opt==='y')
            $scope.vo.approveCancellation();
        else
            $scope.vo.rejectCancellation();
    }
    $scope.approveRejectionCommentDisable=function(){
        if(($scope.isAdmin || $scope.isCM) && $scope.contract.status.toLowerCase()==='cancellation initiated')
            return false;
        else if(($scope.isAdmin || $scope.isCM) && $scope.contract.status.toLowerCase()!=='cancellation initiated')
            return true;
        else if(!($scope.isAdmin || $scope.isCM))
            return true;
    }
    $scope.showProductDetails=function(){
        if($scope.isCO && ($scope.contract.status.toLowerCase()==='cancellation approved for product reprocessed' || $scope.contract.status.toLowerCase()==='cancelled'))
            return true;
        else if(($scope.isAdmin || $scope.isCM) && ($scope.contract.status.toLowerCase()==='product reprocessed' || $scope.contract.status.toLowerCase()==='cancelled'))
            return true;
    }
    $scope.showApproveRejectButton=function(){
        if(($scope.isAdmin || $scope.isCM) && ($scope.contract.status.toLowerCase()==='cancellation initiated'
                                || $scope.contract.status.toLowerCase()==='product reprocessed') )
            return true;
    }
    $scope.productDetailsLocationRequired=function(){
        if(($scope.isAdmin || $scope.isCM) && $scope.contract.status.toLowerCase()==='product reprocessed')
            return true;
    }
    $scope.showLocationForCancellation=function(){
        if($scope.isCO && $scope.contract.status.toLowerCase()==='cancelled')
            return true;
        else if(($scope.isAdmin || $scope.isCM) && ($scope.contract.status.toLowerCase()==='cancelled'
                                    || $scope.contract.status.toLowerCase()==='product reprocessed') )
            return true;

    }
    $scope.disableLocationForCancellation=function(){
        if($scope.isCO)
            return true;
        else if(($scope.isAdmin || $scope.isCM) && $scope.contract.status.toLowerCase()==='cancelled')
            return true;
    }
    $scope.showUpgradeDowngradeButton=function(){
        if($scope.isAdmin && $scope.contract.status.toLowerCase()!=='cancelled'
                          && $scope.contract.status.toLowerCase()!=='cancellation initiated'
                          && $scope.contract.status.toLowerCase()!=='closed'
                          && $scope.contract.status.toLowerCase()!=='delinquent'
                          && $scope.contract.status.toLowerCase()!=='suspensed'
                          && $scope.contract.status.toLowerCase()!=='cancellation approved for product reprocessed'
                          && $scope.contract.status.toLowerCase()!=='product reprocessed'
                          && $scope.contract.status.toLowerCase()!=='cancel request rejected' /*&& $scope.contract.modified!=='Y'*/)
            return true;
    }
    $scope.createNewContract=function(){
        if (validationForCustomerDetailsEdit() && validationPricingDetailsEdit() && validationsProductDetailsEdit()) {
            var paymentDetails = [];
            paymentDetails.push({
                paymentId: paymentId,
                paymentType: "Deposit1",
                paymentMethod: $scope.contract.methodOfPaymentDP1,
                orderDueAmt: $scope.contract.orderDueAmt,
                paymentAmt: prevDp1,
                paymentRefNum: $scope.contract.paymentRefNum,
                bankId:($scope.contract.bankDp1===undefined||$scope.contract.bankDp1===null)?null:$scope.contract.bankDp1.bankId

            });
            paymentDetails.push({
                paymentType: "Deposit2",
                paymentMethod: $scope.contract.methodOfPaymentDP2,
                orderDueAmt: $scope.contract.orderDueAmt,
                paymentAmt: $scope.contract.downPayment2,
                paymentRefNum: $scope.contract.paymentRefNum,
                bankId:($scope.contract.bankDp2===undefined||$scope.contract.bankDp2===null)?null:$scope.contract.bankDp2.bankId

            });
            if ($scope.contract.salesRepContributionStatus) {
                var srType = "";
                if ($scope.contract.commission) {
                    srType = "Commission"
                } else {
                    srType = "Cash"
                }
                paymentDetails.push({
                    paymentType: "SalesRepContribution",
                    paymentMethod: srType,
                    orderDueAmt: $scope.contract.orderDueAmt,
                    paymentAmt: $scope.contract.salesRepContributionAmt,
                    paymentRefNum: $scope.contract.paymentRefNum,
                    bankAccountNo: "5201258633",
                    bankName: "BankAbc"

                });
            }
            $scope.contract.paymentDetails = paymentDetails;
            if (adjustStatus) {
                $scope.contract.adjustment = true;
            } else {
                $scope.contract.adjustment = false;
            }
            console.log($scope.contract);
            getServiceCall("createNewContract");
        }

    }



    function chequeDateValidation(){
        if($scope.vo.response.itemDeliverPaymentInfo.paymentMethod==='Cheque'){

            $scope.cheque1Error=validate.validateText($scope.vo.chequeDate);
            if($scope.vo.chequeDate!=null && $scope.vo.chequeDate!=undefined){
                 var sixMonthDate=new Date();
                 sixMonthDate.setMilliseconds(0);
                 sixMonthDate.setMinutes(0);
                 sixMonthDate.setHours(0);
                 sixMonthDate.setMonth(sixMonthDate.getMonth()-6);
                 var currentDate=new Date();
                 currentDate.setMilliseconds(0);
                 currentDate.setMinutes(0);
                 currentDate.setHours(0);

                 var parts=$scope.vo.chequeDate.split('-');
                var selectedDate=new Date(parts[2],$scope.getMonthNo(parts[1]),parts[0]);
                var diff=selectedDate-sixMonthDate;
                var futureDiff=currentDate-selectedDate;
                /*if((selectedDate>sixMonthDate) && (currentDate>=selectedDate))*/
                if((selectedDate>sixMonthDate))
                $scope.cheque2Error=true;
                else
                 $scope.cheque2Error=false;
            }else{
                $scope.cheque1Error=false;
                $scope.cheque2Error=false;
            }


        }

        if($scope.cheque1Error && $scope.cheque2Error){
            return true;
        }else{
            return false;
        }


    }


    function generatePDF () {

        var html = document.getElementById("mailBody");

        document.getElementById("pdfIgnoreBtn").style.display = 'none';
        document.getElementById("pdfIgnoreBack").style.display = 'none';
        document.getElementById("pdfIgnoreSideBar").style.display = 'none';
        document.body.scrollTop = document.documentElement.scrollTop = 0;

        var pdf = new jsPDF('p', 'pt','a4',true);
        var canvas = pdf.canvas;

        html2canvas(html).then(function(canvas) {

            var pdfInternals = pdf.internal,
                pdfPageSize = pdfInternals.pageSize,
                pdfScaleFactor = pdfInternals.scaleFactor,
                pdfPageWidth = pdfPageSize.width,
                pdfPageHeight = pdfPageSize.height,
                totalPdfHeight = 0,
                htmlPageHeight = canvas.height,
                htmlScaleFactor = canvas.width / (pdfPageWidth * pdfScaleFactor);

            while(totalPdfHeight < htmlPageHeight){
                var newCanvas = canvasShiftImage(canvas, totalPdfHeight, pdfPageHeight * pdfScaleFactor);
                pdf.addImage(newCanvas, 'png', 0, 50, pdfPageWidth, 0, null, 'FAST'); //note the format doesn't seem to do anything... I had it at 'pdf' and it didn't care

                totalPdfHeight += (pdfPageHeight * pdfScaleFactor * htmlScaleFactor);

                if(totalPdfHeight < htmlPageHeight){ pdf.addPage(); }
            }

            pdf.output('dataurlnewwindow');

            document.getElementById("pdfIgnoreBtn").style.display = 'block';
            document.getElementById("pdfIgnoreBack").style.display = 'block';
            document.getElementById("pdfIgnoreSideBar").style.display = 'block';

            //console.log(pdf.output('datauristring'));
            generatedPdf = pdf.output('datauristring');

            //return generatedPdf;
        });

        var canvasShiftImage = function(oldCanvas, shiftAmt, realPdfPageHeight){
            shiftAmt = parseInt(shiftAmt) || 0;
            if(!shiftAmt){ return oldCanvas; }

            var newCanvas = document.createElement('canvas');
            //newCanvas.height = oldCanvas.height - shiftAmt;
            newCanvas.height = Math.min(oldCanvas.height - shiftAmt, realPdfPageHeight);
            newCanvas.width = oldCanvas.width;
            var ctx = newCanvas.getContext('2d');

            Pixastic.process(oldCanvas, "crop", {rect: {left: 0, top: shiftAmt, width: oldCanvas.width, height: (oldCanvas.height - shiftAmt)}}, function (newCanvas) {
                    ctx.drawImage(newCanvas, 0, 0, newCanvas.width, newCanvas.height, 0, 0, newCanvas.width, newCanvas.height);
             });

            return newCanvas;
        };
 }
}]);