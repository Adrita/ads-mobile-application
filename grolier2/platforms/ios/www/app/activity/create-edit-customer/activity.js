app
.controller(
		"CreateCustomerCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'validate',
		 'adapter',
		 'adjustHeight',
		 'Session',
		 function($scope, $state, $uibModal,
				 localStorageService, validate, adapter,
				 adjustHeight, Session) {
			 adjustHeight.adjustTopBar();
			 $scope.loader = false;
			 var customerName = "";
			 var errorMessage = "";
			 var customerCode = "";
			 /* MODAL IMPLEMENTATION */
			 var errorMessage = "";
			 $scope.animationsEnabled = true;

			 $scope.open = function() {
				 var modalInstance = $uibModal
				 .open({
					 animation : $scope.animationsEnabled,
					 templateUrl : 'app/modals/role/modal.html',
					 controller : 'modalCtrl',
					 windowClass : 'center-modal modal-custom-width',
					 backdrop : 'static',
					 keyboard : false,
					 size : 'md'
				 }).result.then(function(data) {
					 if (data === "close") {
						 $state.go('AdminBoard.ManageCustomer')
					 }
					 if (data === "createAnother") {
						 $state.reload();
					 }
				 });
			 };
			 function modalOpen(modalType) {
				 if (customerName !== null
						 && customerName !== "") {
					 if (modalType === "success") {
						 var modalObj = {
								 "modalEntity" : "Customer ",
								 "body" : "(" + customerName + " - "
								 + customerCode + ") ",
								 "backLink" : "Back to Manage Customers",
								 "modalBtnText" : "Create Another Customer"
						 };
					 } else if (modalType === "error") {
						 var modalObj = {
								 "modalEntity" : "Failed!! ",
								 "body" : errorMessage,
								 "backLink" : "Back to Manage Customers",
								 "modalBtnText" : "Create Another Customer"
						 };
					 }

					 localStorageService.set("modalObj",
							 modalObj);
					 $scope.open();
				 }
			 }
			 $scope.raceList = [ "Chinese", "Indian", "Malay","Others" ];
			 $scope.countryStateList = [];

			 $scope.part1 = true;
			 $scope.part2 = false;
			 $scope.part3 = false;
			 $scope.part4 = false;
			 $scope.submit1 = false;
			 $scope.submit2 = false;
			 $scope.submit3 = false;
			 $scope.submit4 = false;

			 $scope.proceed = function(pos) {
				 if (pos === '2') {
					 $scope.submit1 = true;
					 $scope.submit2 = false;
					 $scope.submit3 = false;

					 if ($scope.customerDetailsForm.$valid) {
						 $scope.part1 = false;
						 $scope.part2 = true;
						 $scope.part3 = false;
						 $scope.part4 = false;
					 }
				 } else if (pos === '3') {
					 $scope.submit1 = false;
					 $scope.submit2 = true;
					 $scope.submit3 = false;

					 if ($scope.addressForm.$valid) {
						 $scope.part1 = false;
						 $scope.part2 = false;
						 $scope.part3 = true;
						 $scope.part4 = false;
					 }

				 } else if (pos === '4') {
					 $scope.submit1 = false;
					 $scope.submit2 = false;
					 $scope.submit3 = true;
					 if ($scope.relationShipForm.$valid) {
						 createCustomer();
					 }

				 }
			 }
			 $scope.back = function(pos) {
				 $scope.part1 = false;
				 $scope.part2 = false;
				 $scope.part3 = false;
				 $scope.part4 = false;
				 if (pos === '1') {
					 $scope.part1 = true;
				 } else if (pos === '2') {
					 $scope.part2 = true;
				 }

			 }

			 function createCustomer() {
				 var relationships = [];
				 for ( var i = 0; i < $scope.relationShipList.length; i++) {
					 var singleObj = {
							 relation : $scope.relationShipList[i].relation,
							 name : $scope.relationShipList[i].name,
							 phoneNo : $scope.relationShipList[i].phone,
							 email : $scope.relationShipList[i].email,
							 addressLine1 : $scope.relationShipList[i].addrLine1,
							 addressLine2 : $scope.relationShipList[i].addrLine2,
							 cityName : $scope.relationShipList[i].cityName,
							 stateName : $scope.relationShipList[i].stateName,
							 pinCode : $scope.relationShipList[i].pinCode

					 }
					 relationships.push(singleObj);
				 }

				 $scope.customer.userId = Session.userId;
				 $scope.customer.relationships = relationships;

				 console.log($scope.customer);
				 type = "createCustomer";
				 getServiceCall();
			 }

			 $scope.pmtOptions = [ 'Debit Card', 'Credit Card',
			                       'Bank Transfer' ];
			 $scope.accTypeList = [ 'Savings', 'Current' ];
			 $scope.relationList = [ 'Mother', 'Father',
			                         'Sister', 'Brother', 'Spouse' ];
			 $scope.pmtList = [ {
				 paymentMethod : "",
				 bankName : "",
				 accType : "",
				 defPayment : "",
				 bankAddr1 : "",
				 bankAddr2 : ""
			 } ];
			 $scope.addPmt = function() {
				 $scope.pmtList.push({
					 paymentMethod : "",
					 bankName : "",
					 accType : "",
					 defPayment : "",
					 bankAddr1 : "",
					 bankAddr2 : ""
				 });
			 };
			 $scope.relationShipList = [ {
				 relation : '',
				 name : '',
				 phone : '',
				 email : '',
				 addrLine1 : '',
				 addrLine2 : '',
				 cityName : '',
				 stateName : '',
				 country : '',
				 pinCode : ''
			 } ];
			 $scope.addReference = function() {
				 $scope.relationShipList.push({
					 relation : '',
					 name : '',
					 phone : '',
					 email : '',
					 addrLine1 : '',
					 addrLine2 : '',
					 cityName : '',
					 stateName : '',
					 country : '',
					 pinCode : ''
				 });
				 $scope.submit3 = false;
			 }
			 $scope.delRelationship = function(index) {
				 $scope.relationShipList.splice(index, 1);
			 };
			 $scope.delPaymentMethod = function(index) {
				 $scope.pmtList.splice(index, 1);
			 };
			 var type = "getCustomer";
			 getServiceCall();

			 function getServiceCall() {
				 if (type === 'getCustomer') {
					 $scope.loader = true;
					 var data = {
							 userId : Session.userId
					 }
					 url = "CustomerController/getCustomer";
				 } else if (type === 'createCustomer') {
					 $scope.loader = true;
					 
					 var dob;

					 if ($scope.customer.dob !== null && $scope.customer.dob!==undefined) {
						 //old_dob = $scope.customer.dob;
						 var dtArr = $scope.customer.dob
						 .split("-");
						 var month = ('0' + (parseInt($.inArray(
								 dtArr[1], shortMonthArr)) + 1))
								 .slice(-2);
						 var custDob = dtArr[2] + "-" + month
						 + "-" + dtArr[0];

						 dob = new Date(custDob);
						 $scope.customer.dob = dob;
					 }
					 
					 var data = $scope.customer;
					 url = "CustomerController/createCustomer";
				 }

				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {

					 if (result.statusCode === 200) {
						 console.log(result);
						 if (type === 'createCustomer') {
							 $scope.loader = true;
							 console.log("Success");
							 customerName = $scope.customer.name;
							 customerCode = result.customerCode;
							 modalOpen("success");
						 }
						 if (type === 'getCustomer') {
							 $scope.loader = true;
							 $scope.customer = result;
							 $scope.countryStateList = result.stateList;
						 }
					 } else {
						 errorMessage = result.message;
						 modalOpen("error");
					 }
					 $scope.loader = false;
				 }
				 function error() {
					 $scope.loader = false;
					 console.log("Error block");
					 errorMessage = result.message;
					 modalOpen("error");

				 }

			 }
			 $scope.cancel = function() {
				 $state.go('AdminBoard.ManageCustomer');
			 }
			 $scope.goToTab = function(pos) {
				 if (pos === '1') {
					 $scope.part1 = true;
					 $scope.part2 = false;
					 $scope.part3 = false;
				 }
				 if (pos === '2') {
					 $scope.submit1 = true;
					 $scope.submit2 = false;
					 $scope.submit3 = false;
					 if ($scope.customerDetailsForm.$valid) {
						 $scope.part1 = false;
						 $scope.part2 = true;
						 $scope.part3 = false;
					 }

				 }
				 if (pos === '3') {
					 $scope.submit1 = false;
					 $scope.submit2 = true;
					 $scope.submit3 = false;
					 if ($scope.customerDetailsForm.$valid
							 && $scope.addressForm.$valid) {
						 $scope.part1 = false;
						 $scope.part2 = true;
						 $scope.part3 = false;
					 }

				 }
			 }
		 } ]);
