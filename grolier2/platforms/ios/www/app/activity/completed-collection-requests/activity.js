app.controller("CompletedCollectionRequestsCtrl", [
		'$scope',
		'$state',
		'$uibModal',
		'localStorageService',
		'adapter',
		'validate',
		'adjustHeight',
		'$compile',
		'ModalService',
		'commonServices',
		function($scope, $state, $uibModal, localStorageService, adapter,
				validate, adjustHeight, $compile, ModalService, commonServices) {
			adjustHeight.adjustTopBar();
			$scope.ShowContractPage = function(row, event) {
				localStorageService.set('contractRow', row);
				if (event.ctrlKey) window.open('#/AdminBoard/ViewContract', '_blank'); // in new tab
		        else $state.go('AdminBoard.ViewContract');
			}
			$scope.ShowCustomerPage = function(row, event) {
				localStorageService.set('customerCode', row.custCode);
				if (event.ctrlKey) window.open('#/AdminBoard/EditCustomer', '_blank'); // in new tab
		        else $state.go('AdminBoard.EditCustomer');
			}

			$scope.exportToCsv=function(e,data){
		    	e.preventDefault();

		    	var filterData=localStorageService.get('filteredContract');

		    	 var selectedColumns = localStorageService.get('selectedColumns');

		    	if(filterData!==null){

		    		var filterOutData=[];

		    		for(var i=0;i<filterData.length;i++){
		    		    angular.forEach(filterData[i], function(value, key) {
		    			  if(selectedColumns.hasOwnProperty(key)){
		    				  console.log(key);
		    			  }else{
		    				//  filterData.remove(key);
		    				delete filterData[i][key];
		    			  }
		    			});
		    		}

		    		commonServices.exportToCsvCommon(data, "Collection Report", true);

		    	}else{
		    		if(data == '')
		                return;
		    		for(var i=0;i<data.length;i++){
		    		    angular.forEach(data[i], function(value, key) {
		    			  if(selectedColumns.hasOwnProperty(key)){
		    				  console.log(key);
		    			  }else{
		    				//  filterData.remove(key);
		    				delete data[i][key];
		    			  }
		    			});
		    		}

		    		commonServices.exportToCsvCommon(data, "Collection Report", true);

		    		//JSONToCSVConvertor(data, "Contract Report", true);
		    	}




		    }

		} ]);