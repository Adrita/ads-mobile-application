app
    .controller(
        "SRTurnInCtrl", [
            '$scope',
            '$state',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'ModalService',
            'Session',
            '$cordovaNetwork',
            function($scope, $state, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, ModalService, Session, $cordovaNetwork) {

                adjustHeight.adjustTopBar();

                $scope.historyTab=true;
				$scope.Clearance=false;
				$scope.detailsView=false;
				var type="";

				$scope.isOnline=$cordovaNetwork.isOnline();

				if($scope.isOnline){
					type="historyTab";
				}else{

					$scope.historyTab=false;
					$scope.Clearance=true;
					type="pendingClearance";
				}


				$scope.$on('$cordovaNetwork:online', function(event, networkState){

					if(!$scope.isOnline){
						$state.reload();
						$scope.isOnline=true;
					}
				});

				$scope.$on('$cordovaNetwork:offline', function(event, networkState){
					$scope.isOnline=false;
					$state.reload();
                });


				getServiceCall(type);




                function getServiceCall(type) {

                    $scope.loader = true;
                    var url = "";
                    var data = {};
                    if (type === 'pendingClearance') {
                        url = "SalesRepTurnInController/salesRepTurnInPendingForClearanceRequest";
                        data = {
                            userId: Session.userId
                        };

                    }else if(type==='historyTab'){
						url = "SalesRepTurnInController/salesRepTurnInHistoryRequest";
						 data = {
								 userId : Session.userId
						 };
					}

                    adapter.getServiceData(url, data).then(success, error);


                    function success(response) {
                        $scope.loader = false;
                        if (response.statusCode === 200) {
                            $scope.cashSummary = response.cashSummary;
                            $scope.chequeSummary = response.chequeSummary;
                            $scope.creditCardSummary = response.creditCardSummary;

                            if(type==='pendingClearance'){
								$scope.cashDetailsList=response.cashDetailsList;
								$scope.chequeDetailsList=response.chequeDetailsList;
								app.saveDataIntoFile('saveSalesRepTurnIn',response);
							}else if(type==='historyTab'){
								$scope.listTurnIn=response.srTurnInHistoryRowList;
							}


                           // app.saveSalesRepTurnIn(response, successCallback, errorCallback);


                        } else {
                        $scope.loader = false;
                           // app.fetchSalesRepTurnIn(successCallbackFetch, errorCallbackFetch);
                           app.fetchDataFromFile('saveSalesRepTurnIn',successCallbackFetch,errorCallbackFetch);
                        }
                    }

                    function error() {
                    $scope.loader = false;
                        app.fetchDataFromFile('saveSalesRepTurnIn',successCallbackFetch, errorCallbackFetch);

                    }
                }



                $scope.openGrid=function(tabName){
					if(tabName==='history'){
						$scope.historyTab=true;
						$scope.Clearance=false;
						type="historyTab";
						getServiceCall(type);
					}else{
						$scope.historyTab=false;
						$scope.Clearance=true;
						type="pendingClearance";
						getServiceCall(type);
					}
				}

				$scope.viewDetails=function(row){
					$scope.detailsView=true;
					$scope.detailList=row;
				}
				$scope.backToTurnIn=function(){
					$scope.detailsView=false;
				}

                $scope.concatanateString = function(str) {
                    if (str) {
                        return str.replace(/\s+/g, '-').toLowerCase();
                    }

                }

                function successCallbackFetch(response) {
                    if (response.statusCode === 200) {
                        $scope.cashSummary = response.cashSummary;
                        $scope.chequeSummary = response.chequeSummary;
                        $scope.creditCardSummary = response.creditCardSummary;
                        $scope.cashDetailsList = response.cashDetailsList;
                        $scope.chequeDetailsList = response.chequeDetailsList;
                        $scope.$apply();


                    }
                }

                function errorCallbackFetch(message) {
                    alert(message);
                }


            }
        ]);