app.controller( "VoBankInCtrl", ['$scope','$state','$uibModal','localStorageService','adapter','validate','adjustHeight','$compile','ModalService','Session',function($scope,$state,$uibModal,localStorageService,adapter,validate,adjustHeight,$compile,ModalService,Session)
{
	  adjustHeight.adjustTopBar();
	  $scope.pageActionHeader="Bank-in-Collections";
	  $scope.CashBankIn=[];
	  $scope.ChequeBankIn=[];
	  $scope.CreditCardBankIn=[];
	  $scope.localData=localStorageService.get('voTurnInData');
	  $scope.cashTotal=0;
	  $scope.chequeTotal=0;
	  $scope.creditCardTotal=0;
	  $scope.submitData=[];
	  var obj={};
	  $scope.selectedBank=null;
	  $scope.chequeDateError=false;
	  $scope.chequePastDateError=false;
	  var paymentType="Cash";
	  var type='submit';
	  $scope.submitPressed=false;
	  var extraAmtInHand=0;

	  var loggedinUser = Session.userGlobalRole;

	  if($scope.localData==null)
		  $state.go('VoBoard.VoTurnIn');
	  else
		  {
		  	for(var i=0;i<$scope.localData.length;i++)
		  		{
		  			if($scope.localData[i].paymentMethod.toLowerCase()==='cash')
		  				{
		  				 $scope.CashBankIn.push($scope.localData[i]);
		  				 $scope.cashTotal+=parseFloat($scope.localData[i].amount);
		  				}
		  			else if($scope.localData[i].paymentMethod.toLowerCase()==='cheque')
		  				{
		  				
		  					paymentType="Cheque";
		  					$scope.ChequeBankIn.push($scope.localData[i]);
		  					$scope.chequeTotal+=parseFloat($scope.localData[i].amount);
		  				}
		  				 
		  			else if($scope.localData[i].paymentMethod.toLowerCase()==='credit card')
		  				{
		  				 	$scope.CreditCardBankIn.push($scope.localData[i]);
		  				 	$scope.creditCardTotal+=parseFloat($scope.localData[i].amount);
		  				}
		  			$scope.totalItemCount=$scope.localData.length;
		  			//$scope.cashAmount=$scope.cashTotal+$scope.chequeTotal+$scope.creditCardTotal;
		  				
		  		}
		  	for(var i=0;i<$scope.localData.length;i++)
		  		{
		  			obj={paymentMethod:$scope.localData[i].paymentMethod,amount:parseFloat($scope.localData[i].amount),paymentIdList:$scope.localData[i].paymentIdList};
		  			$scope.submitData.push(obj);
		  		}
		  	$scope.cashAmount=$scope.cashTotal;

		  }
	  type="getBankList";
	  getServiceCall();
	  function getServiceCall() {

			var data = {};
			var url = "";
			if (type === 'submit') {
				url = "VoBankInTurnInController/voBankInTurnInBankInRequest";
				data = {
					userId : Session.userId,
					slipNumber:$scope.slipNumber,
					selectedBank:$scope.selectedBank,
					remarks:$scope.remarks,
					actualCashAmount:parseFloat($scope.cashAmount),
//					+parseFloat(extraAmtInHand)
					receivedCashAmount:$scope.cashTotal,
					receiveInBankInDetails:$scope.submitData,
					bankedInDate:$scope.bankedInDate
				};
				$scope.loader = true;
			} 
			else if(type==="getBankList")
				{
					url="FetchListController/getBankList";
					data={userId:Session.userId};
					$scope.loader = true;
				}
			adapter.getServiceData(url, data).then(success,
					error);
			function success(result) {
				$scope.loader = false;
				if (result.statusCode === 200) {
					if (type === 'submit') {
						var total=0;
						if($scope.cashTotal>0)
						total+=parseFloat($scope.cashAmount);
						if($scope.chequeTotal>0)
						total+=parseFloat($scope.chequeTotal);
						if($scope.creditCardTotal>0)
						total+=parseFloat($scope.creditCardTotal);
						 modalEntity = total
						 + " "
						 + " has been received successfully.";
						 modalBody = "";
						 localStorageService.set(
								 "modalStatus", true);
						 modalOpen("success");
						$scope.loader = false;
					}else if(type==="getBankList")
						{
							$scope.bankNameList=result.bankList;
							$scope.loader = false;
						}
				} else if (result.statusCode === 401) {

				if(type==="getBankList"){

				data.countryId=loggedinUser.countryId;

				//app.fetchBankList(data,successBankList);

				}else{
					$scope.loader=false;
					//app.saveBankInDetailsVO(data,successCallback,errorCallback);

				}


				}
			}
			function error() {
			$scope.loader=false;
				if(type==="getBankList"){
				data.countryId=loggedinUser.countryId;
				app.fetchBankList(data,successBankList);

				}else{
//				$scope.loader = false;
//				modalOpen("error");
					app.saveBankInDetailsVO(data,successCallback,errorCallback);
				}
			}

		}
	  /* MODAL IMPLEMENTATION */
		 $scope.animationsEnabled = true;

		 $scope.open = function() {
			 var modalInstance = $uibModal.open({
				 animation : $scope.animationsEnabled,
				 templateUrl : 'app/modals/role/modal.html',
				 controller : 'modalCtrl',
				 windowClass : 'center-modal',
				 backdrop : 'static',
				 keyboard : false,
				 size : 'md'
			 }).result.then(function(data) {
				 if (data === "close") {
					 $state.go('VoBoard.VoTurnIn');
				 }
				 if (data === "createAnother") {
					 $state.reload();
				 }
			 });
		 };

		 var errorName = "Turn In";
		 var errorMessage= "Banked In Failed";

		 function modalOpen(modalType) {
			 if (errorName !== null && errorName !== "") {
				 if (modalType === "success") {
					 var modalObj = {
							 "modalEntity" : modalEntity,
							 "body" : modalBody,
							 "modalBtnText" : "OK",
							 "backLink" : "OK",
							 "singleBtn" : true
					 };
				 } else {
					 var modalObj = {
							 "modalEntity" : "Failed!! ",
							 "body" : errorMessage,
							 "modalBtnText" : "",
							 "backLink" : "OK",
							 "singleBtn" : true
					 };
				 }

				 localStorageService.set("modalObj",
						 modalObj);
				 $scope.open();
			 }
		 }
		 $scope.voBankIN={};
		 $scope.serviceHit=function()
		 {
			 $scope.submitPressed=true;
			 if($scope.voBankIN.$valid && chequeDateValidation())
				 {
				 	type='submit';
				 	getServiceCall();
				 }
			
		 };
		 
		 function chequeDateValidation(){
			 
			 var currentDate=new Date();
				currentDate.setHours(0);
				currentDate.setMinutes(0);
				currentDate.setSeconds(0);
				currentDate.setMilliseconds(0);
				
				
				var selectedDate=new Date($scope.bankedInDate.replace('-',' '));
				
				var past3Date=new Date();
				past3Date.setHours(-72);
				past3Date.setMinutes(0);
				past3Date.setSeconds(0);
				past3Date.setMilliseconds(0);
				
				
				var futureDiff=selectedDate-currentDate;
				
				
				if(futureDiff>0){
					$scope.chequeDateError=true;
					$scope.chequePastDateError=false;
					return false;
					
				}else if(selectedDate<past3Date && paymentType.toLowerCase()!=='cheque'){
					$scope.chequePastDateError=true;
					$scope.chequeDateError=false;
					return false;
					
					
				}else{
					$scope.chequeDateError=false;
					$scope.chequePastDateError=false;
					return true;
				}
			 
			 
		 }
		 
		 $scope.getSelectedBank=function()
		 {
			 
		 }
		 $scope.cancel=function()
		 {
			 $state.go('VoBoard.VoTurnIn');
		 }

		 function successBankList(result){
		 	$scope.bankNameList=result;
            $scope.loader = false;
            $scope.$apply();


           // turnInSync.fetchExtraAmt(Session.userId,successCallFetch,errorCallback);
		 }


		 function successCallFetch(result){
		 	extraAmtInHand=result.extraAmt;

		 }

		 function successCallback(result){
			var total=0;
			if($scope.cashTotal>0)
			total+=parseFloat($scope.cashAmount);
			if($scope.chequeTotal>0)
			total+=parseFloat($scope.chequeTotal);
			if($scope.creditCardTotal>0)
			total+=parseFloat($scope.creditCardTotal);
			 modalEntity = total
			 + " "
			 + " has been received successfully.";
			 modalBody = "";
			 localStorageService.set(
					 "modalStatus", true);
			 modalOpen("success");
			$scope.loader = false;
		 }

		 function errorCallback(message){

		 }


}]);
