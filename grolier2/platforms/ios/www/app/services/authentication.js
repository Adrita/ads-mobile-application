app.factory('authentication', ['$http', '$q', '$window', '$location', 'Session', 'localStorageService', '$base64', '$state','syncService','$timeout','commonServices', function($http, $q, $window, $location, Session, localStorageService, $base64, $state,syncService,$timeout,commonServices) {
    var authService = {};
    var userInfo = {};
    var loggedIndUser = {};
    var params = "adsqa.sintl.org:8080";
    var userRoleRouteMap = Session.userGlobalRole;

    authService.loginService = function(data, header) {
        var def = $q.defer();
        var req = {
            method: 'POST',
            headers: header.headers,
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: "http://" + params + "/j_security_check",
            data: data
        };




        $http(req)
            .success(function(result, status, headers, config) {
                if (angular.isDefined(result.statusCode) && result.statusCode === "200") {

                    //alert("In authentication success");
                    //Addition for Authentication
                    var hdr = headers(),
                        accessToken = hdr["accesstoken"];
                    var newConfig = {
                        headers: {
                            'AccessToken': accessToken
                        }
                    };

                    var sessionId = result.principal + result.userDetails.userId;
                    localStorageService.set('countryId',result.userDetails.countryId);
                    Session.create(sessionId, result.userDetails.userId, {});

                    commonServices.setTokenCreatedTime(new Date());


                    userRoleRouteMap = result.userDetails;
                    Session.createGlobalRoles(userRoleRouteMap);


                    Session.storeAuthData('configData', newConfig);
                    Session.storeAuthData('globalUserRoles', {
                        userInfo: result
                    });

                    app.storeUserRole(result,successCallback,errorCallback);


                    Session.createContractViewAccess(getUserScreenRoleAccess("AdminBoard.ViewContract"));
                    Session.createUserViewAccess(getUserScreenRoleAccess("AdminBoard.EditCustomer"));
                }

                def.resolve(result);
            })
            .error(function(error, status, headers, config) {
                // alert(req.data.userName);



                def.reject(error);

            });


        return def.promise;
    };




    authService.logout = function() {
        var def = $q.defer();
        var req = {
            method: 'POST',
            url: "http://" + params + "/j_security_logout",
            data: {}
        };
        var configuration = localStorageService.get('configData');
        if (angular.isDefined(configuration) && configuration !== null && configuration !== "")
            req.headers = configuration.headers;

        $http(req)
            .success(function(result, status, headers, config) {
            	Session.destroy();
                //$state.go('Login');
                def.resolve(result);
            })
            .error(function(error, status, headers, config) {
                Session.destroy();
                $state.go('Login');
                def.reject(error);
            });
        return def.promise;
    };

    authService.login = function(userId, password) {
        userInfo = {
            name: userId,
            password: password
        };
        this.loginService().then(function(result) {
            loggedIndUser = result;
        });
        return loggedIndUser;
    };

    function getLoggedInUser() {
        return loggedIndUser;
    }

    authService.isAuthenticated = function() {
        return !!Session.userId;
    }

    authService.isAuthorised = function(curState) {
        var isAuthorised = false,
            userRole = getUserScreenRoleAccess(curState);

        if (this.isAuthenticated() && userRole.stateName === curState && this.userHasRole(userRole)) {
            isAuthorised = true;
        }

        Session.create(Session.id, Session.userId, userRole);
        return isAuthorised;
    }

    authService.userHasRole = function(role) {
        var hasRole = false;
        if (this.hasSummaryAccess(role) || this.hasViewDetailsAccess(role) || this.hasEditDetailsAccess(role) || this.hasDraftAccess(role)) {
            hasRole = true;
        }
        return hasRole;
    }

    authService.hasSummaryAccess = function(role) {
        return (role.summaryAccess.toUpperCase() === "Y") ? true : false;
    }
    authService.hasViewDetailsAccess = function(role) {
        return (role.viewDetailsAccess.toUpperCase() === "Y") ? true : false;
    }
    authService.hasEditDetailsAccess = function(role) {
        return (role.editDetailsAccess.toUpperCase() === "Y") ? true : false;
    }
    authService.hasDraftAccess = function(role) {
        return (role.draftAccess.toUpperCase() === "Y") ? true : false;
    }

    var userSpecifiedRole = {
        stateName: "",
        summaryAccess: "N",
        viewDetailsAccess: "N",
        editDetailsAccess: "N",
        draftAccess: "N"
    };

    function getUserScreenRoleAccess(curState) {

        userSpecifiedRole.stateName = curState;
        var isFound = false,
            userRole = userSpecifiedRole;

        angular.forEach(userRoleRouteMap.screenAccess, function(value, key) {
            if (value.stateName === curState) {
                userRole = value;
                isFound = true;
                return true;
            }
        });
        //if (!isFound) {}
        return userRole;
    }

    function successCallback(response){

		app.sync(localStorageService.get('loggedUser'),localStorageService.get('password'),successLogin,errorLogin);

    }

    function successLogin(response){
    	//syncService.syncAllData({userId:Session.userId});

		localStorage.setItem('deviceId', device.uuid);

		syncService.checkLastUpdateTime({userId:Session.userId});
    	checkForLocation();

//    	syncService.checkLastUpdateTime({userId:Session.userId});
//
//		app.syncServiceData(success,error);
//
//		app.syncVerificationList(successVerification,errorVerification);
//
//		app.syncCollectionList(successCollection,errorCollection);
//
//
//		if(Session.userType.toLowerCase().indexOf("collection")>-1){
//			app.syncCollectorTurnIn(successCollectorTurnIn,errorCollectorTurnIn);
//         }

	   // app.syncNoteList(successNote,errorNote);

    }

    function errorLogin(response){
		console.log(response);

    }

    function checkForLocation(){
    	if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, {
                            timeout: 30000
                        });
                    } else {
                        alert("Geolocation is not supported");
                    }


                    function geolocationSuccess(position) {
        //				alert('Latitude: ' + position.coords.latitude + '\n' +
        //					'Longitude: ' + position.coords.longitude + '\n' +
        //					'Altitude: ' + position.coords.altitude + '\n' +
        //					'Accuracy: ' + position.coords.accuracy + '\n' +
        //					'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
        //					'Heading: ' + position.coords.heading + '\n' +
        //					'Speed: ' + position.coords.speed + '\n' +
        //					'Timestamp: ' + position.timestamp + '\n');


        				localStorage.setItem('lat',position.coords.latitude);
        				localStorage.setItem('lon',position.coords.longitude);

        				if(Session.userType.toLowerCase().indexOf("sales")>-1){
        					app.syncServiceData(successContract, errorContract);
        				}else if(Session.userType.toLowerCase().indexOf("verification")>-1){
        					app.syncVerificationList(successVerification, errorVerification);
        				}else if(Session.userType.toLowerCase().indexOf("collection") > -1){
        					app.syncCollectionList(successCollection, errorCollection);
        				}

        			}


        			function geolocationError(error) {
        				switch (error.code) {
        					case error.PERMISSION_DENIED:
        						alert("User denied the request for Geolocation.");
        						break;
        					case error.POSITION_UNAVAILABLE:
        						alert("Location information is unavailable.");
        						break;
        					case error.TIMEOUT:
        						alert("The request to get user location timed out.");
        						break;
        					case error.UNKNOWN_ERROR:
        						alert("An unknown error occurred.");
        						break;
        				}
        			}

    }


    function successNote(result){

    if(result.length===0){
    	app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
    }else{
    	for(var i=0;i<result.length;i++){
			syncService.noteSync(result[i],i,result.length);
		}
    }



        
    }

    function errorNote(result){
        console.log("Error");
        app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
    }


    function successCollectorTurnIn(result){
            	if (result.length > 0) {
    				//    		navigator.geolocation.getCurrentPosition(geolocationSuccess,geolocationError,[geolocationOptions]);
    //						$state.reload();
    				var data = {
    					deviceId: localStorage.getItem('deviceId'),
    					lat: "" + localStorage.getItem('lat'),
    					lon: "" + localStorage.getItem('lon'),
    					offlineBankInRecords: result
    				}
    				syncService.syncCollectorBankIn(data);
    			}else{
    				$state.reload();
    			}

            }

            function errorCollectorTurnIn(error){
            	$state.reload();
            }


    function successCollection(result){


        if(result.length===0){
            app.syncNoteList(successNote,errorNote);
        }else{
        	for(var i=0;i<result.length;i++){
                    syncService.collectionContract(result[i],i,result.length);
                }
        }



        
    }

    function errorCollection(result){
        console.log("Error");
        app.syncNoteList(successNote,errorNote);
    }


    function success(result){

        var data={
            userId:1,
            contractList:result
        }

        syncService.createContract(1,data);
        
    }

    function error(result){
        console.log("Error");
    }


    function successVerification(result){

        var count=0;

    for(var i=0;i<result.length;i++){        
        syncService.verifyContract(result[i],count,result.length);
        count=count+1;
    }

        
    }

    function errorVerification(result){
        console.log("Error");
    }

    function errorCallback(){
        
    }

    return authService;
}]);