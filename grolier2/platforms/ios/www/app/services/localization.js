app.factory('localize', ['$http', '$q','$rootScope', function ($http,$q,$rootScope) {
 var locale = {};
 function getLocalizedService() {
     var def = $q.defer();
     var req = {
    	 method:'GET',
    	 url:"distribution/data/localization.json"
     };
     $http(req)
         .success(function(data) {
             def.resolve(data);
         })
         .error(function() {
             def.reject("Failed to get Localize String");
         });
     return def.promise;
 };
 
 function setLocalizedResources(){
	getLocalizedService().then(function(result) {
    	locale = result;
	 }, function(data) {
	     console.log('Strings retrieval failed.');
	 });
 };

 function getLocalizedResources(){
    if(locale == null){
        setLocalizedResources();
    }
    return locale;
 }
return {
    setLocalizedResources:setLocalizedResources,
    getLocalizedResources:getLocalizedResources
}
}]);



