angular.module('adjustPageHeight',[]);
angular.module('adjustPageHeight').service('adjustHeight', ['$state', function ($state) {
	this.adjustTopBar=function()
	{
		//no height calculation for mobile manage page
		var mobileScreenList = [ 'AdminBoard.ManageContract', 'AdminBoard.CreateEditContract', 'AdminBoard.ViewContract', 'CoBoard.ManageCollectorCollectionRequest', 'CoBoard.CoCollectPayment', 'SalesRep.SalesRepRegistration', 'VoBoard.VoCollectPayment' ];

		// check if current state matches link
		var noHeightCalculation = function() {
			if( mobileScreenList.indexOf($state.current.name) > -1 && window.innerWidth < 768 ) return true;
			return false; //for all other state
		};		
		
		setTimeout(function() {
			var x=$("nav").outerHeight();
			var y=$("header").outerHeight();
			var z=$("#main-nav .sub-menu-wrapper:not('.ng-hide')").outerHeight();
			if (z === null || z === undefined) z=0;

			var buffer=10;
			var height=(x+y+z)+"px";
			
			if ( noHeightCalculation() ) $('.mainContent').css('padding-top', y);
			else $('.mainContent').css('padding-top', height);
			
		}, 250);
	
	}
	this.adjustFixed=function(x)
	{
		

	}
}]);
			
		   