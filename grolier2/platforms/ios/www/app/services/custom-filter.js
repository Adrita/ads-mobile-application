app.filter('searchByOrderNo', function() {
  return function(input, search) {
    if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = [];
    angular.forEach(input, function(value, key) {
      var actual = ('' + value.or).toLowerCase();
      if (actual.indexOf(expected) !== -1) {
        result.push(value);
      }
    });
    return result;
  }
});


app.filter('filterBySR',function(){
  return function(input,search){
    if(!input) return input;
    if(!search) return input;
    var result=[];
    angular.forEach(input,function(value,key){
      if(value.srName.indexOf(search)!==-1){
        result.push(value);
      }
    });
    return result;

  }

});