app.factory('userRole', ['Session', function (Session) {
	var roleService = {};	 
	var contractNav = [];
	var mainMenu = [];
	var newMainMenu = [];
	 var menuIndex = [];
	var _isInit = false;
	
	var removeUserLink = true;
	var gridClass = "col-xs-4";
	//var defaultLangingPage = "AdminBoard.ManageUser";
	var defaultLangingPage = "";
	
	function init () {
		_isInit = true;
		
		mainMenu = Session.userGlobalRole.mainMenu;
		//if (angular.isDefined(Session.userGlobalRole.defaultLangingPage)) defaultLangingPage = Session.userGlobalRole.defaultLangingPage;
		
		newMainMenu = menuGenerator ();
		
		var menuLen = newMainMenu.length;
		if (menuLen === 4) {
			removeUserLink = false;
			gridClass = "col-xs-12 col-sm-3";
		} 
		else if (menuLen === 3) gridClass = "col-xs-12 col-sm-4";
		else if (menuLen === 2) gridClass = "col-xs-12 col-sm-6";
		else if (menuLen === 1 || menuLen === 0) gridClass = "col-xs-12";
		else if(menuLen===5) gridClass="col-xs-12 col-sm-2 five-div";

		
	}
	
	function sortByKey(array, key) {
		if(array!==undefined){
			return array.sort(function(a, b) {
						var x = a[key]; var y = b[key];
						return ((x < y) ? -1 : ((x > y) ? 1 : 0));
					});
		}
		return 0;
	}
	
	function showMobileNav(array) {
		if( window.innerWidth < 960 ) {
			var newArr = [];
			angular.forEach(array, function(value, key) {
				var newSubArr = [];
				//Menu for mobile screen
				if( value.hasOwnProperty("appOnly") && value.appOnly === "Y") {
					 //Sub-menu for mobile screen
					 if (value.hasOwnProperty("subMenu") && value.subMenu.length > 0) {
						 angular.forEach(value.subMenu, function(subValue, subKey) {
							 if( subValue.hasOwnProperty("appOnly") && subValue.appOnly === "Y") {
								 newSubArr.push(subValue);
							 }
						 });
						 value.subMenu = newSubArr;
					 }
					 newArr.push(value);
				}
			});
			return newArr;
			
		} else {
			return array;
		}
	}
	
	function menuGenerator () {
		 var nav = [];
		 var isSetLangingPage = false;
		 
		 mainMenu = sortByKey(mainMenu, 'menuId');
		 mainMenu = showMobileNav(mainMenu);

		 angular.forEach(mainMenu, function(value, key) {
			 
			 var index = key;
			 var stateName = null;
			 
			 //for default landing page
			 /*if (value.hasOwnProperty("stateName") && value.stateName != null && value.stateName != "") {
				 stateName = defaultLangingPage = value.stateName;
			 }*/
			 if (!isSetLangingPage && value.hasOwnProperty("subMenu") && value.subMenu.length > 0) {
				 defaultLangingPage = value.subMenu[0].stateName;
				 isSetLangingPage = true;
			 } 
			 if (!isSetLangingPage) {
				 defaultLangingPage = "AdminBoard.ManageUser";
			 }
			 
			 if (value.menuId === 1) {
				 stateName = value.hasOwnProperty("stateName")?value.stateName:null;
				 value.icon = "distribution/images/dashboard_icon.png";
				 value.click = "ShowSecondNav('dashboard', "+index+")";
				 value.removeLink = false;
				 value.menuClass = "dashboard_menu";
				 value.showArrow = false;
				 
			 } else if (value.menuId === 2) {
				 stateName = value.hasOwnProperty("stateName")?value.stateName:null;
				 value.icon = "distribution/images/user_roles.png";
				 value.click = "ShowSecondNav('userRole', "+index+")";
				 value.removeLink = false;
				 value.menuClass = "user_role";
				 value.showArrow = true;
				 
			 } else if (value.menuId === 3) {
				 stateName = value.hasOwnProperty("stateName")?value.stateName:null;
				 value.icon = "distribution/images/contracts_icon.png";
				 value.click = "ShowSecondNav('contract', "+index+")";
				 value.removeLink = false;
				 value.menuClass = "contract_menu";
				 value.showArrow = true;
				 
			 } else if (value.menuId === 4) {
				 stateName = value.hasOwnProperty("stateName")?value.stateName:null;
				 value.icon = "distribution/images/customers_icon.png";
				 value.click = "ShowSecondNav('customer', "+index+")";
				 value.removeLink = false;
				 value.menuClass = "customer_menu";
				 value.showArrow = false;
			 }
			 else if (value.menuId === 5) {
				 stateName = value.hasOwnProperty("stateName")?value.stateName:null;
				 value.icon = "distribution/images/customers_icon.png";
				 value.click = "ShowSecondNav('team', "+index+")";
				 value.removeLink = false;
				 value.menuClass = "customer_menu";
				 value.showArrow = true;
			 }
			 menuIndex[index] = false;
			 nav[index] = value;
		 });
		 
		 return nav;
	}
	
	roleService.getNavigation = function () {
		if(!_isInit) init();
		var nav = {
				mainMenu: newMainMenu,
				menuIndex: menuIndex,
				removeUserLink: removeUserLink,
				gridClass: gridClass
		}		 
		return nav;
	}
	 
	roleService.defaultLangingPage = function () {
		init();	 
		return defaultLangingPage;
	}
	
	roleService.getUserHomePage = function () {
		return defaultLangingPage;
	}
	
	return roleService;
	
}]);