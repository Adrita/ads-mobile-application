app.factory('grid', ['$http', '$q','$state','localize',function ($http,$q,$state,localize) {
function getColMappingData() {
    var array = $state.current.name.split(".");
    var gridDataMapURL = 'distribution/'+'data'+'/'+array[1]+'Map.json';
     var def = $q.defer();
     var req = {
    	 method:'GET',
    	 url:gridDataMapURL
     };
     $http(req)
         .success(function(data) {
             def.resolve(data);
         })
         .error(function() {
             def.reject("Failed to get Localize String");
         });
     return def.promise;
 };
 function getPagingData(page) {
    return page.data.slice((page.pageNo - 1) * page.pageSize, page.pageNo * page.pageSize);
 };
 function rowTemplate(){ 
    return  '<div ng-class="{ \'merge_row\': row.entity.merge === true}">' +
    '  <div ng-if="row.entity.merge === true"><span>{{row.entity.name}}({{row.entity.UserId}})</span> is now part of sales team <button class="btn btn-sm btn-danger" ng-click="grid.appScope.rowFormatter( row )">ok</button></div>' +
    '  <div ng-if="!row.entity.merge === true" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"  ui-grid-cell></div>' +
    '</div>';

 }
  function loadMore($scope){
    return $scope.quantity+2;
  }
  return {	  
    getColMappingData:getColMappingData,
    getPagingData:getPagingData,
    rowTemplate:rowTemplate,
    loadMore:loadMore
  };

}]);



