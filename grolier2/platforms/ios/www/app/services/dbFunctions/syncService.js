app.factory('syncService', ['$http', '$q','insertData','$timeout','$state','localStorageService', function ($http, $q,insertData,$timeout,$state,localStorageService) {

var sERVICE_ENDPOINT = "http://adsqa.sintl.org:8080/rest/",
	countryUrl = sERVICE_ENDPOINT + "CountryController/manageCountry",
    bankUrl = sERVICE_ENDPOINT + "BankController/manageBank",
    paymentMethodUrl=sERVICE_ENDPOINT+"FetchListController/getPaymentMethodList",
    stateUrl = sERVICE_ENDPOINT + "CountryController/getStatesForMobile",
    locationUrl = sERVICE_ENDPOINT + "LocationController/manageLocationForMobile",
    dutyFreePincodeUrl = sERVICE_ENDPOINT + "DutyFreePincodeController/getDutyFreePincodeListForMobile",
    itemDetailsUrl = sERVICE_ENDPOINT + "ItemController/manageItemForMobile",
    priceListUrl = sERVICE_ENDPOINT + "PriceListController/managePriceList",
    lastUpdatedTimeUrl = sERVICE_ENDPOINT + "CheckLastUpdatedTimeController/checkLastUpdatedTimeForMobile",
    verificationRequestSyncUrl = sERVICE_ENDPOINT + "VerificationController/manageAssignedVerificationRequestForMobile",

    createContractUrl = sERVICE_ENDPOINT + "ContractController/createContractForMobile",
    verifyContractUrl = sERVICE_ENDPOINT + "VerificationController/verifyContract",
    verifyPaymentUrl = sERVICE_ENDPOINT + "VerificationController/collectPaymentSubmitForMobile",
    collecPaymentUrl = sERVICE_ENDPOINT + "CollectionController/collectPaymentSubmit",
    saveNoteUrl = sERVICE_ENDPOINT+ "CollectionController/saveNote";

    collectorBankInUrl= sERVICE_ENDPOINT +"CollectorManageTurnInController/bankInCollectorForMobile";
    verificationBankInUrl= sERVICE_ENDPOINT +"VoBankInTurnInController/voBankInRequestSyncForMobile";
    verificationReceiveInUrl= sERVICE_ENDPOINT +"VoBankInTurnInController/voReceiveInRequestSyncForMobile";

    var anyTableExists=false;
    var successFullSync=0;
    var callingAll=0;

    var tableUrlMap={};

    tableUrlMap[constants.country_table]=countryUrl;
    tableUrlMap[constants.state_table]=stateUrl;
    tableUrlMap[constants.location_table]=locationUrl;
    tableUrlMap[constants.duty_free_zone_table]=dutyFreePincodeUrl;
    tableUrlMap[constants.item_master_table]=itemDetailsUrl;
    tableUrlMap[constants.price_list_master_table]=priceListUrl;
    tableUrlMap[constants.last_updated_time_table]=lastUpdatedTimeUrl;
    tableUrlMap[constants.bank_master_table]=bankUrl;
    tableUrlMap[constants.payment_method_table]=paymentMethodUrl;

    var deleteDataOfTables=["last_updated_time_tb"];




var service_url_list=[countryUrl,bankUrl,paymentMethodUrl,stateUrl,locationUrl,dutyFreePincodeUrl,itemDetailsUrl,priceListUrl,lastUpdatedTimeUrl];

var syncService={};

var deleteVerificationList=[];

var collectionContractList=[];

var noteDeletionList=[];

var options = { dimBackground: true };

var db_name_list=["ads_country_master_tb","ads_state_master_tb","ads_location_master_tb","ads_duty_free_pincode_tb","ads_item_master_tb","ads_price_list_master_tb","last_updated_time_tb","ads_bank_master_tb","ads_payment_method_tb"];

var lastUpdatedData={};



syncService.syncAllData=function(data){

for(var i=0;i<service_url_list.length;i++){

	SpinnerPlugin.activityStart("Sync in Progress....", options);

    switch(service_url_list[i]){

        case countryUrl:
                syncService.syncCountryData(data);
                break;

        case locationUrl:
                syncService.syncLocationData(data);
                break;

        case stateUrl:
                syncService.syncStateData(data);
                break;

        case bankUrl:
                syncService.syncBankData(data);
                break;

        case paymentMethodUrl:
        		syncService.syncPaymentMethod(data);
        		break;

        case dutyFreePincodeUrl:
                syncService.syncDutyFreePinCode(data);
                break;

        case itemDetailsUrl:
                syncService.syncItemDetails(data);
                break;

        case priceListUrl:
                syncService.syncPriceListData(data);
                break;

        case lastUpdatedTimeUrl:
                syncService.syncLastUpdatedTimeData(data,"type1");
                break;

    }



}


}


syncService.checkLastUpdateTime=function(data){


	var db = window.sqlitePlugin.openDatabase({name: "Grolier.db"});
    		db.transaction(function(tx) {
                        tx.executeSql("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+"last_updated_time_tb"+"'", [], function(tx, rs) {
                          ////console.log('Record length: ' + rs.rows.length);
                          if(rs.rows.length>0){
                          	anyTableExists=true;

                          	syncService.syncLastUpdatedTimeData(data,"type2");

                          }else{
                          //callingAll++;
							  //if(callingAll===1){
								fileSync.createTable(successDb,errorDb);
							//  }
                          }
                        }, function(tx, error) {
                          console.log('SELECT error: ' + error.message);

                        });
                      });
}

function successDb(result){

	callingAll++;

	var reqData={
		userId:1
	}

	if(callingAll===1){
		syncService.syncAllData(reqData);
	}

}

function errorDb(result){

}





syncService.syncCountryData=function(data){
	var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: countryUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...Country Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

            	if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
            		successFullSync++;

            		if(successFullSync===8){
            			SpinnerPlugin.activityStop();
            		}
                    insertData.insertCountryData(result.countries);


            	}

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncBankData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: bankUrl,
            data: data
        };
        //SpinnerPlugin.activityStart("Sync in Progress...Bank Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                    successFullSync++;
                    if(successFullSync===8){
                                			SpinnerPlugin.activityStop();
                                		}

                  //  console.log("bank:::::" + result.bankList.length);
                    insertData.insertBankData(result.bankList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncStateData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: stateUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...State Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                    successFullSync++;
                    if(successFullSync===8){
                                			SpinnerPlugin.activityStop();
                                		}

                    //console.log("states:::::" + result.stateList.length);
                    insertData.insertStateData(result.stateList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncPaymentMethod=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: paymentMethodUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...State Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                    successFullSync++;
                    if(successFullSync===8){
                                			SpinnerPlugin.activityStop();
                                		}

                    //console.log("states:::::" + result.stateList.length);
                    insertData.insertPaymentMethod(result.paymentMethodList);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncDutyFreePinCode=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: dutyFreePincodeUrl,
            data: data
        };
        //SpinnerPlugin.activityStart("Sync in Progress...Pincode Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                    successFullSync++;
                    if(successFullSync===8){
                                			SpinnerPlugin.activityStop();
                                		}

                  //  console.log("pincode:::::" + result.dutyFreePincodes.length);
                    insertData.insertPincodeData(result.dutyFreePincodes);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncPriceListData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: priceListUrl,
            data: data
        };




        //SpinnerPlugin.activityStart("Sync in Progress...Pricelist Details", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                    successFullSync++;
                    //console.log("price list:::::" + result.priceList.length);
                    insertData.insertPriceListData(result.priceList);
					if(successFullSync===8){
                    	//alert("closing in price list");
						SpinnerPlugin.activityStop();
					}

                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncItemDetails=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: itemDetailsUrl,
            data: data
        };
//SpinnerPlugin.activityStart("Sync in Progress...Item Details", options);

        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode && result.statusCode === 200)) {
                successFullSync++;


                    insertData.insertItemData(result.itemList);
                    if(successFullSync===8){
						//alert("closing in item list");
						SpinnerPlugin.activityStop();
					}


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

syncService.syncLastUpdatedTimeData=function(data,type){
    var def = $q.defer();
        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: lastUpdatedTimeUrl,
            data: data
        };

        //SpinnerPlugin.activityStart("Sync in Progress...", options);


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                successFullSync++;



					if(type==="type1"){
						insertData.insertLastUpdatedRow(result);
						//SpinnerPlugin.activityStop();
					}else{
						lastUpdatedData=result;
						fileSync.fetchLastUpdatedRow(successCallLastUpdatedRow,errorCallLastUpdatedRow);
					}
					if(successFullSync===8){
						SpinnerPlugin.activityStop();
						//alert("Clocing in last updated")
					}




                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}

function successCallLastUpdatedRow(result){
	//alert(JSON.stringify(result));

	if(result.length!==0){
		syncService.compareOldToNew(result,lastUpdatedData);
	}else{
		var reqData={
        		userId:1
        	}
		syncService.syncAllData(reqData);
	}


}

function errorCallLastUpdatedRow(result){

}


syncService.compareOldToNew=function(oldData,newData){

for(var i=0;i<oldData.length;i++){

	switch(oldData[i].tableName){

            case constants.country_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastCountryMasterUpdateTime);
                    break;

            case constants.state_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastStateMasterUpdateTime);
                    break;

            case constants.location_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastLocationMasterUpdateTime);
                    break;

            case constants.duty_free_zone_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastDutyFreePincodeUpdateTime);
                    break;

            case constants.item_master_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastItemMasterUpdateTime);
                    break;

            case constants.price_list_master_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastPriceListMasterUpdateTime);
                    syncService.syncAgain();
                    break;

//            case constants.last_updated_time_table:
//                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastCountryMasterUpdateTime);
//                    break;
//
            case constants.bank_master_table:
                    syncService.updateTableUrlMap(oldData[i].tableName,oldData[i].lastUpdatedTime,newData.lastBankMasterUpdateTime);
                    break;

        }


}


}

syncService.syncAgain=function(){
//	if(deleteDataOfTables.length>0){
//		fileSync.deleteTables(deleteDataOfTables,successDeletion,errorDeletion);
//	}

for(var i=0;i<deleteDataOfTables.length;i++){
		fileSync.deleteTables(deleteDataOfTables[i],successDeletion,errorDeletion);
}

}

function errorDeletion(error){
	console.log(error);
}

function successDeletion(result){

deleteDataOfTables=["last_updated_time_tb"];

var data={
	userId:1
};

//for (var i in tableUrlMap){
//		if(tableUrlMap[i]!==null){
			switch(tableUrlMap[result]){

                    case countryUrl:
                            syncService.syncCountryData(data);
                            break;

                    case locationUrl:
                            syncService.syncLocationData(data);
                            break;

                    case stateUrl:
                            syncService.syncStateData(data);
                            break;

                    case bankUrl:
                            syncService.syncBankData(data);
                            break;

                    case dutyFreePincodeUrl:
                            syncService.syncDutyFreePinCode(data);
                            break;

                    case itemDetailsUrl:
                            syncService.syncItemDetails(data);
                            break;

                    case priceListUrl:
                            syncService.syncPriceListData(data);
                            break;

                    case lastUpdatedTimeUrl:
                            syncService.syncLastUpdatedTimeData(data,"type1");
                            break;

                }
		//}
//}


}

syncService.updateTableUrlMap=function(tableName,oldUpdateTime,lastUpdateTime){
	if (oldUpdateTime == "null" && lastUpdateTime == null) {
                // no need to update
                if (tableName in tableUrlMap) {
                    //tableUrlMap.put(tableName, null);
                    tableUrlMap[tableName]=null;
                }
            } else if (oldUpdateTime == "null" && lastUpdateTime != null) {
                // need to update
                deleteTableData(tableName);
            } else if (oldUpdateTime != null && oldUpdateTime != "null" && lastUpdateTime == null) {
                // need to update
                deleteTableData(tableName);
            } else if (oldUpdateTime==lastUpdateTime) {
                // no need to update
                if (tableName in tableUrlMap) {
                    tableUrlMap[tableName]=null;
                }
                console.log("Table = " + tableName + " no update require");
            } else {
                // need to update
                deleteTableData(tableName);
                console.log(tableName + " update require");
            }


}

function deleteTableData(tableName){
	console.log("Delete Table "+tableName);

	deleteDataOfTables.push(tableName);
}

syncService.syncLocationData=function(data){
    var def = $q.defer();
        var req = {
            method: 'POST',
            //*url:"http://"+params[2]+"/rest/LoginController/login",*//*
            url: locationUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                    //alert(JSON.stringify(result));
                  //  console.log("location:::::" + result.locationList.length);
                  successFullSync++;

                    insertData.insertLocationData(result.locationList);
					if(successFullSync===8){
					//alert("closing in location data");
						SpinnerPlugin.activityStop();
					}

                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


return def.promise;


}


syncService.createContract=function(mobileRowId,data){

    console.log("Create Contract::::"+ JSON.stringify(data));

    SpinnerPlugin.activityStart("Sync in progress.... for draft Contract", options);

var def = $q.defer();

    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: createContractUrl,
            data: data
        };





        $http(req)
            .success(function(result, status, headers, config) {

                var date=new Date();

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {

                    var responseCodeList=result.mobileRowIdResponseCodeList;

                    var mobileRowIdList=[];

                    for(var i=0;i<responseCodeList.length;i++){

                        if(responseCodeList[i].responseCode===200){

                            mobileRowIdList.push(responseCodeList[i].mobileRowId);

                        }
                    }


                    app.deleteContractText(mobileRowIdList);
                    SpinnerPlugin.activityStop();


                    if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                        $state.reload();
                    }


                }else{
                	SpinnerPlugin.activityStop();
                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);
                SpinnerPlugin.activityStop();

            });
        // },60000);
    // }

    


return def.promise;

}


syncService.verifyContract=function(data,index,resultLength){

  //  console.log("Verify contract vo:::::"+JSON.stringify(data));

  SpinnerPlugin.activityStart("Sync in progress....", options);

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: verifyContractUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

               // console.log(result);

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {

                   // app.deleteFromVerifyFile(data.contractId);

                    deleteVerificationList.push(data.contractId);



                    if(index===resultLength-1){

                    SpinnerPlugin.activityStop();
                        app.collectPaymentVo(deleteVerificationList,successCallbackPayment,errorCallbackPayment);

                        deleteVerificationList=[];



                    }

                    if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                                            $state.reload();
                                        }
                   
                   


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                SpinnerPlugin.activityStop();
                def.reject(error);

            });


}


function successCallbackPayment(result){

                        $state.reload();


                        var data={
                            userId:1,
                            verifyContractList:result
                        };

                    
                        syncService.syncPaymentVO(data);
                    
                    

                   }

                   function errorCallbackPayment(message){

                    console.log("Error");
                   }

syncService.syncPaymentVO=function(data){

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

    SpinnerPlugin.activityStart("Sync in progress....", options);

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: verifyPaymentUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

             //   console.log(result);

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                   
                   var responseCodeList=result.contractIdResponseCodeList;
                   var contractIdList=[];

                   for(var i=0;i<responseCodeList.length;i++){

                    if(responseCodeList[i].responseCode===200){
                        contractIdList.push(responseCodeList[i].contractId);
                    }

                   }

                   SpinnerPlugin.activityStop();

                   app.deleteFromVerificationFile(contractIdList,successVerifyPaymentDelete,errorVerifyPaymentDelete);




                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);
                SpinnerPlugin.activityStop();

            });


}


function successVerifyPaymentDelete(success){
		app.syncVerificationReceiveTurnIn(successVerificationReceiveTurnIn, errorVerificationReceiveTurnIn);
}

function errorVerifyPaymentDelete(error){
		app.syncVerificationReceiveTurnIn(successVerificationReceiveTurnIn, errorVerificationReceiveTurnIn);
}

function successVerificationReceiveTurnIn(response){
	if(result.length>0){
                            		var data = {
    									deviceId: localStorage.getItem('deviceId'),
    									lat: "" + localStorage.getItem('lat'),
    									lon: "" + localStorage.getItem('lon'),
    									offlineReceiveInBankInRecords: result
    								}
    								syncService.syncVerificationReceive(data);
                            	}else{
                            		app.syncVerificationBankInTurnIn(successVerificationBankInCallback,errorVerificationBankInCallback);
                            	}

}

function errorVerificationReceiveTurnIn(result){

                        	app.syncVerificationBankInTurnIn(successVerificationBankInCallback,errorVerificationBankInCallback);

                        }


syncService.fetchVerificationData=function(data){

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            /*url:"http://"+params[2]+"/rest/LoginController/login",*/
            url: verificationRequestSyncUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

              //  console.log(result);

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                   
                   app.saveVerificationRequestData(result);


                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);
                def.reject(error);

            });


}


syncService.collectionContract= function(data,index,arrayLength){

    var def = $q.defer();
    // for(var i=0;i<data.length;i++){

    SpinnerPlugin.activityStart("Sync in progress....", options);


        var req = {
            method: 'POST',
            url: collecPaymentUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {

                   // app.deleteCollectionRecord(data.contractId);



                   collectionContractList.push(data.contractId);

                   if(index===arrayLength-1){
                        app.deleteCollectionRecord(collectionContractList,successDeletionCollectionRecord);

                        SpinnerPlugin.activityStop();
                        collectionContractList=[];
                        if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                                                                    $state.reload();
                                                                }

                   }

                   SpinnerPlugin.activityStop();


                }else{
                    app.deleteCollectionRecord(collectionContractList,successDeletionCollectionRecord);
                    collectionContractList=[];
                    SpinnerPlugin.activityStop();
                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);

                app.deleteCollectionRecord(collectionContractList,successDeletionCollectionRecord);

                SpinnerPlugin.activityStop();

                collectionContractList=[];

                def.reject(error);

            });

}


function successDeletionCollectionRecord(result){
    app.syncNoteList(successNote,errorNote);
}

function successNote(result){



if(result.length<=0){
	app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
}else{
	for(var i=0;i<result.length;i++){
		syncService.noteSync(result[i],i,result.length);
	}
}



        
    }

    function errorNote(result){
        console.log("Error");
        app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
    }



    function successCollectorTurnIn(result){
            	if (result.length > 0) {
    				//    		navigator.geolocation.getCurrentPosition(geolocationSuccess,geolocationError,[geolocationOptions]);
    //						$state.reload();
    				var data = {
    					deviceId: localStorage.getItem('deviceId'),
    					lat: "" + localStorage.getItem('lat'),
    					lon: "" + localStorage.getItem('lon'),
    					offlineBankInRecords: result
    				}
    				syncService.syncCollectorBankIn(data);
    			}else{
    				$state.reload();
    			}

            }

            function errorCollectorTurnIn(error){
            	$state.reload();
            }

syncService.noteSync=function(data,index,arrayLength){

var def = $q.defer();
SpinnerPlugin.activityStart("Sync in progress....", options);
    // for(var i=0;i<data.length;i++){

        var req = {
            method: 'POST',
            url: saveNoteUrl,
            data: data
        };


        $http(req)
            .success(function(result, status, headers, config) {

                if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                   
                   //console.log(result);
                   noteDeletionList.push(data.contractId);

                   if(index===arrayLength-1){

                     app.deleteNoteRecord(noteDeletionList);

                     SpinnerPlugin.activityStop();

                     noteDeletionList=[];

//                    if($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='VoBoard.VoVerificationRequest'||$state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
//                        $state.reload();
//                    }

					app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);

                   }


                }else{
                    app.deleteNoteRecord(noteDeletionList);
                    app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
					SpinnerPlugin.activityStop();

                     noteDeletionList=[];
                }

            })

            .error(function(error, status, headers, config) {
                // alert(req.data.userName);

                app.deleteNoteRecord(noteDeletionList);
                app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);

                SpinnerPlugin.activityStop();

                noteDeletionList=[];

                console.log(error);
                def.reject(error);

            });


}



syncService.syncCollectorBankIn=function(data){

	console.log("CO Bank in data::::"+ JSON.stringify(data));

        SpinnerPlugin.activityStart("Sync in progress.... for bank-in records ", options);

    var def = $q.defer();

        // for(var i=0;i<data.length;i++){

            var req = {
                method: 'POST',
                /*url:"http://"+params[2]+"/rest/LoginController/login",*/
                url: collectorBankInUrl,
                data: data
            };

            $http(req)
                .success(function(result, status, headers, config) {
                    if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                        var responseCodeList=result.mobileRowIdResponseCodeList;
                        var mobileRowIdList=[];
                        for(var i=0;i<responseCodeList.length;i++){

                            if(responseCodeList[i].responseCode===200){
                                mobileRowIdList.push(responseCodeList[i].mobileRowId);
                            }
                        }

                        app.deleteCollectorTurnInText(mobileRowIdList);
                        SpinnerPlugin.activityStop();

                        if($state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
                            $state.reload();
                        }

                    }else{
                    	SpinnerPlugin.activityStop();
                    }
                })
                .error(function(error, status, headers, config) {
                    // alert(req.data.userName);
                    def.reject(error);
                    SpinnerPlugin.activityStop();

                });
            // },60000);
        // }
    return def.promise;
}

syncService.syncVerificationReceive=function(data){
	console.log("VO Receive::::"+ JSON.stringify(data));

            SpinnerPlugin.activityStart("Sync in progress.... for bank-in records ", options);

        var def = $q.defer();

            // for(var i=0;i<data.length;i++){

                var req = {
                    method: 'POST',
                    /*url:"http://"+params[2]+"/rest/LoginController/login",*/
                    url: verificationReceiveInUrl,
                    data: data
                };

                var configuration = localStorageService.get('configData');
                			if (angular.isDefined(configuration) && configuration !== null
                					&& configuration !== "")
                				req.headers = {
                					'AccessToken' : configuration.headers.AccessToken,
                					//'Content-type' : 'application/pdf'

                					/*'Content-Type: application/vnd.android.package-archive';
                						'Content-Disposition: attachment; filename="Grolier.apk"';*/
                				};

                $http(req)
                    .success(function(result, status, headers, config) {
                        if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                            var responseCodeList=result.mobileRowIdResponseCodeList;
                            var mobileRowIdList=[];
                            for(var i=0;i<responseCodeList.length;i++){

                                if(responseCodeList[i].responseCode===200){
                                    mobileRowIdList.push(responseCodeList[i].mobileRowId);
                                }
                            }

                            app.deleteVerificationReceivedata(mobileRowIdList,successReceiveDelete,errorReceiveDelete);
                            SpinnerPlugin.activityStop();

//                            if($state.current.name==='CoBoard.ManageCollectorCollectionRequest'){
//                                $state.reload();
//                            }

                        }else{
                        	SpinnerPlugin.activityStop();
                        }
                    })
                    .error(function(error, status, headers, config) {
                        // alert(req.data.userName);
                        def.reject(error);
                        SpinnerPlugin.activityStop();

                    });
                // },60000);
            // }
        return def.promise;

}


syncService.syncVerificationBankIn=function(data){
	console.log("VO Bank In::::"+ JSON.stringify(data));

                SpinnerPlugin.activityStart("Sync in progress.... for bank-in records ", options);

            var def = $q.defer();

                // for(var i=0;i<data.length;i++){

                    var req = {
                        method: 'POST',
                        /*url:"http://"+params[2]+"/rest/LoginController/login",*/
                        url: verificationBankInUrl,
                        data: data
                    };

                    var configuration = localStorageService.get('configData');
                    			if (angular.isDefined(configuration) && configuration !== null
                    					&& configuration !== "")
                    				req.headers = {
                    					'AccessToken' : configuration.headers.AccessToken,
                    					//'Content-type' : 'application/pdf'

                    					/*'Content-Type: application/vnd.android.package-archive';
                    						'Content-Disposition: attachment; filename="Grolier.apk"';*/
                    				};

                    $http(req)
                        .success(function(result, status, headers, config) {
                            if (angular.isDefined(result.statusCode) && result.statusCode === 200) {
                                var responseCodeList=result.mobileRowIdResponseCodeList;
                                var mobileRowIdList=[];
                                for(var i=0;i<responseCodeList.length;i++){

                                    if(responseCodeList[i].responseCode===200){
                                        mobileRowIdList.push(responseCodeList[i].mobileRowId);
                                    }
                                }

                                app.deleteVerificationBankIndata(mobileRowIdList,successBankInDelete,errorBankInDelete);
                                SpinnerPlugin.activityStop();

                                if($state.current.name==='VoBoard.VoTurnIn'){
                                    $state.reload();
                                }

                            }else{
                            	SpinnerPlugin.activityStop();
                            }
                        })
                        .error(function(error, status, headers, config) {
                            // alert(req.data.userName);
                            def.reject(error);
                            SpinnerPlugin.activityStop();

                        });
                    // },60000);
                // }
            return def.promise;

}

function successReceiveDelete(result){
		app.syncVerificationBankInTurnIn(successVerificationBankInCallback,errorVerificationBankInCallback);
}

function errorReceiveDelete(error){
		alert("Error in deletion");
}

function successBankInDelete(result){

}

function errorBankInDelete(error){

}

function successVerificationBankInCallback(result){

	if(result.length>0){
		var data = {
			deviceId: localStorage.getItem('deviceId'),
			lat: "" + localStorage.getItem('lat'),
			lon: "" + localStorage.getItem('lon'),
			offlineBankInRecords: result
		}
		syncService.syncVerificationBankIn(data);
	}

}

function errorVerificationBankInCallback(result){

}





syncService.checkState=function(){

    //alert($state.current.name);

}

return syncService;



}])