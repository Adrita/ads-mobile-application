app.factory('filterService', ['$http', '$q','$rootScope','localStorageService','$state', function ($http,$q,$rootScope,localStorageService,$state) {
 var locale = {};

 function sideBarFilter($scope,arg){
	 var countryRecord;
     var statusRecord;
     var textRecord;
     var userTypeRecord;
     var salesDesigRecord;
     var divcodeRecord;
     var paymentTypeRecord;
     var shipmentStatusList;
     var campaignCodeRecord;
     var dateRanageRecord;
     var stateRecord;
     var coRecord;
     var voRecord;
     var SalesRepRecord;
     var HouseSaleRecord;
     var TurnInRecord;
     var requestTypeRecord;
     var deductionTypeRecord;
     var delinquentMonthRecord;
     var vmFilterRecord;
     var cmFilterRecord;
     //var SalesRepRecord;

     //console.log($scope.serviceData);

     if(arg.country.length !== 0)
     {
      countryRecord= $scope.serviceData.filter(function (item)
         {
          var result = false;
          $.each(arg.country,function(index,ut)
          {
              if (arg.currentState ==='AdminBoard.ManageUserType') {
            	  if ($.inArray(ut, item.countryNameList) >= 0) {
                      result=true;
                  };
              }
        	  else if(arg.currentState ==='AdminBoard.ManageUser' && item.country === ut){
                result=true;
              }
        	  else if(arg.currentState ==='AdminBoard.ManageCustomer' && item.country === ut){
                  result=true;
        	  }
              else if(arg.currentState !=='AdminBoard.ManageUser' && item.countryName === ut){
                 result=true;
              }

          });
          return result===true?item:null;
        });
     }
     else {
      countryRecord = $scope.serviceData;

     }
     if(arg.status.length !== 0){
      statusRecord= countryRecord.filter(function (item)
         {
          var result = false;
          $.each(arg.status,function(index,ut)
          {
        	  if(ut.toLowerCase()==='confirmed' && item.confirmContractDt!==undefined && item.confirmContractDt!==null && item.status!=null && item.status.toLowerCase()!='cancelled'){
        		  result=true;
        	  }
        	  else if(arg.currentState ==='AdminBoard.ManageContract' && item.status.toLowerCase() === ut.toLowerCase()){
                  result=true;
              }
        	  else if(arg.currentState ==='CoBoard.CompletedCollectionContracts' && item.status.toLowerCase()===ut.toLowerCase()){
    		  result=true;
    		  }
        	  else if(arg.currentState ==='VoBoard.ManageVoCompleteRequest' && item.orderStatus.toLowerCase()===ut.toLowerCase()){
        		  result=true;
        	  }
    		  else if(arg.currentState ==='VoBoard.VoCompleteRequest' && item.orderStatus.toLowerCase()===ut.toLowerCase() && ut.toLowerCase()!='confirmed'){
        		  result=true;
    		  }
        	  else if(arg.currentState ==='VoBoard.VoCompleteRequest' &&  ut.toLowerCase()=='confirmed' && item.confirmContractDt!=null){
        		  result=true;
    		  }
        	  else if(arg.currentState ==='SalesRep.ManageContractSales' && item.status.toLowerCase()===ut.toLowerCase()){
        		result=true;
        	  }
        	  else if(arg.currentState ==='AdminBoard.ManageAdvanceRequests' && item.status!==null && item.status.toLowerCase()===ut.toLowerCase()){
        		result = true;
        	  }
        	  else if(arg.currentState ==='AdminBoard.ManageAdvanceRequestsAdmin' && item.status!==null && item.status.toLowerCase()===ut.toLowerCase()){
        		result=true;
        	  }
        	  else if(arg.currentState ==='VoBoard.ManageVoManagerVerification' && item.status!==null && item.status.toLowerCase()===ut.toLowerCase()){
        	  	result=true;
        	  }
        	  else if(arg.currentState ==='CoBoard.CollectionContracts' && item.status!==null && item.status.toLowerCase()===ut.toLowerCase()){
        	  	result=true;
        	  }
        	  else if(arg.currentState ==='AdminBoard.ManageCommissionDetails' && item.statusPayment!==null && item.statusPayment.toLowerCase()===ut.toLowerCase()){
        	  	result=true;
        	  }
        	  else if(item.active === ut){
                result=true;
              };
          });
          return result===true?item:null;
        });

     }
     else{
      statusRecord = countryRecord;
     }
     if(arg.name !== undefined && arg.name !== "") {
        nameRecord = statusRecord.filter(function (item)
         {
        	var filterResp = false;
         	angular.forEach(item, function(value, key) {

         		if(value !== undefined && value !== "" && value !== null && value !== "null" && value !== "undefined") {
         			stringVal = String(value);
         			//console.log(key + ': ' + value);
         			if (key !== "priceCodeList"){
         				// exclude priceCodeList array of Manage Price List, as it contains all the list of table

         				if($state.current.name==='AdminBoard.ManageTurnIn'||$state.current.name==='VoBoard.VoTurnIn'){
         					if ( value === arg.name || stringVal.toLowerCase()===arg.name.toLowerCase()) {
    	         				filterResp = true;
    	         				return filterResp;
    	         			}
         				}else{
         					if ( value === arg.name || stringVal.toLowerCase().search(arg.name.toLowerCase()) !== -1 ) {
    	         				filterResp = true;
    	         				return filterResp;
    	         			}
         				}
         			}


         		}

        	});
         	return filterResp;
         });
      }
      else{
        nameRecord =statusRecord;
      }
      if(arg.usertype.length !== 0){
        userTypeRecord= nameRecord.filter(function (item)
         {
          var result = false;
          $.each(arg.usertype,function(index,ut)
          {
        	  switch(arg.currentState) {
	              case 'AdminBoard.ManageCommissionRate':{
	            	  if(item.userTypeName === ut){
	                      result=true;
	                  };
	                  break;
	              }
	              case 'AdminBoard.ManageScreenAccess':{
	            	  if ($.inArray(ut, item.user_type) >= 0) {
	                      result=true;
	                  };
	                  break;
	              }
	              case 'AdminBoard.ManageUser':{
	            	  if(item.userTypeDesg.userType === ut){
	                      result=true;

	                  };
	                  break;
	              }
	              case 'AdminBoard.ManageUserType':{
	            	  if(item.userTypeName === ut){
	                      result=true;

	                  };
	                  break;
	              }
	              default: {
	            	  if(item.userType === ut){
	                      result=true;
	                  };
	                  break;
	              }
	    	  }
        	  /*if(item.userType == ut){
                result=true;
              };*/
          });
          return result===true?item:null;
        });
      }
      else{
        userTypeRecord = nameRecord;
      }
      if(arg.salesdesig.length !== 0){
        salesDesigRecord= userTypeRecord.filter(function (item)
         {
          var result = false;
          $.each(arg.salesdesig,function(index,ut)
          {
              if(item.designation!==null && item.designation.desgName === ut){
                result=true;
              };
          });
          return result===true?item:null;
        });
      }
      else{
        salesDesigRecord = userTypeRecord;
      }
      if(arg.divcode.length !== 0){
	      divcodeRecord= salesDesigRecord.filter(function (item) {
			var result = false;
			$.each(arg.divcode,function(index,ut) {
			    if(arg.currentState ==='AdminBoard.ManageUser' && item.division!==null && item.division.divisionName === ut){
			      result=true;
			    }
			    else if(item.divisionName === ut)
			    {
			    	result=true;
			    }
			});
			return result===true?item:null;
	      });

      } else {
    	  divcodeRecord = salesDesigRecord;
      }
      if(arg.paymentType.length !== 0){
    	  paymentTypeRecord= divcodeRecord.filter(function (item) {

			var result = false;
			$.each(arg.paymentType,function(index,ut) {

				switch(arg.currentState) {
	              case 'AdminBoard.ManageContract':{
	            	 /* if ($.inArray(ut, item.paymentMethodList) >= 0) {
	                      result=true;
	                  };*/
	            	  if(item.paymentMethodList!==null && item.paymentMethodList.length>0 && item.paymentMethodList[0]===ut){
	                      result=true;
	                  };
	                  break;
	              }
	              case 'CoBoard.CollectionTurnIn':{
	            	  if(item.typePayment!==undefined && item.typePayment.toUpperCase() === ut.toUpperCase()){
	                      result=true;
	                  };
	                  break;
	              }
	              case 'SalesRep.ManageTurnIn':{
	            	  if(item.paymentType!==undefined && item.paymentType.toUpperCase() === ut.toUpperCase()){
	                      result=true;
	                  };

	                  break;
	              }
	              case 'AdminBoard.AdminTurnIn':{
	            	  if(item.typePayment!==undefined && item.typePayment.toUpperCase() === ut.toUpperCase()){
	                      result=true;
	                  };
	                  break;
	              }
	              case 'VoBoard.VoTurnIn':{
	            	  if(item.paymentMethod!==undefined && item.paymentMethod.toUpperCase() === ut.toUpperCase()){
	                      result=true;
	                  };
	                  break;
	              }
	              default: {
	            	  if(item.paymentType === ut){
	                      result=true;
	                  };
	              }
	    	    }
			});
			return result===true?item:null;
	      });

      } else {
    	  paymentTypeRecord = divcodeRecord;
      }
      //ShipmentStatusFilter
      if(arg.shipmentStatus.length !== 0){
    	  shipmentStatusList= paymentTypeRecord.filter(function (item) {
			var result = false;
			$.each(arg.shipmentStatus,function(index,ut) {
			    if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase() === ut.toLowerCase() &&
			    		item.status.toLowerCase() !== 'rejected' && item.status.toLowerCase() !== 'dm rejected' &&
			    			item.status.toLowerCase() !== "cancelled" && item.status.toLowerCase() !== "cancellation initiated"){

			    	result=true;
			    };
			});
			return result===true?item:null;
	      });

      } else {
    	  shipmentStatusList = paymentTypeRecord;
      }

      if(arg.campaignCode.length !== 0){
    	  campaignCodeRecord= shipmentStatusList.filter(function (item) {
			var result = false;
			$.each(arg.campaignCode,function(index,ut) {
			    if(item.campaignCode === ut){
			      result=true;
			    };
			});
			return result===true?item:null;
	      });

      } else {
    	  campaignCodeRecord = shipmentStatusList;
      }

      //State Filter

      if(arg.dateRanage.length !== 0){
    	  dateRanageRecord= campaignCodeRecord.filter(function (item) {
			var result = false;
			$.each(arg.dateRanage,function(index,ut) {
				var containsConfirmed=false;
				for(var k=0;k<arg.status.length;k++){
					if(arg.status[k].toLowerCase()==='confirmed'){
						containsConfirmed=true;
						break;
					}

				}
				//var dateArr = ut.split(",");
				if(arg.status.length===0 && arg.shipmentStatus.length===0)
					{
						if(arg.currentState==='VoBoard.VoVerificationRequest'){
							if(compareDate(ut, item.assignVOContractDt)){
					      	result=true;
							}
						}

						else{
							if(compareDate(ut, item.orderDate)){
					      result=true;
						}
						}


					}
				else if(arg.shipmentStatus.length===0)
				{

					if(item.status!=null)
							{
								if(containsConfirmed && item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt))
									result=true;
								else if(item.status.toLowerCase()==='confirmed' && item.confirmContractDt!=null && compareDate(ut,item.confirmContractDt))
									result=true;
								else if(item.status.toLowerCase()==='pending vo allocation' && item.pendingVOAllocationContractDt!=null && compareDate(ut,item.pendingVOAllocationContractDt))
									result=true;
								else if(item.status.toLowerCase()==='pending verification' && item.assignVOContractDt!==null && compareDate(ut,item.assignVOContractDt) )
									result=true;
								else if(item.status.toLowerCase()==='cancellation initiated' && item.cancelContractInitiatedDt!==null && compareDate(ut,item.cancelContractInitiatedDt))
									result=true;
								else if(item.status.toLowerCase()==='cancelled' && item.cancelContractDt!==null && compareDate(ut,item.cancelContractDt))
									result=true;
								else if(item.status.toLowerCase()==='pending collection' && item.assignCOContractDt!==null && compareDate(ut,item.assignCOContractDt))
									result=true;
								else if(item.status.toLowerCase()==='pending co allocation' && item.pendingCOAllocationContractDt!==null && compareDate(ut,item.pendingCOAllocationContractDt))
									result=true;
								else if(item.status.toLowerCase()==='closed' && item.closeContractDt!==null && compareDate(ut,item.closeContractDt))
									result=true;
								else if(item.status.toLowerCase()==='pending vo allocation' && item.pendingVOAllocationContractDt!=null && compareDate(ut,item.pendingVOAllocationContractDt))
									result=true;
								else if(arg.currentState!=='VoBoard.VoCompleteRequest' && item.status.toLowerCase()==='confirmed' && item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt))
									result=true;
								else if(arg.currentState==='VoBoard.VoCompleteRequest' && item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt))
									result=true;
								else if(item.status.toLowerCase()==='rejected' && item.orderRejectionDate!==null && compareDate(ut,item.orderRejectionDate))
									result=true;
								else if(item.status.toLowerCase()==='dm rejected' && item.orderRejectionDate!==null && compareDate(ut,item.orderRejectionDate))
									result=true;
								else if(item.status.toLowerCase()==='cancellation approved for product reprocessed' && item.cancelContractDt!==null && compareDate(ut,item.cancelContractDt))
									result=true;
								else if(item.status.toLowerCase()==='product reprocessed' && item.cancelContractDt!==null && compareDate(ut,item.cancelContractDt))
									result=true;
							}

				}
				else if(arg.status.length===0){
					if(arg.currentState!=='VoBoard.ManageVoManagerVerification'){
						if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='delivered' && item.deliveredContractDt!==null && compareDate(ut,item.deliveredContractDt))
								result=true;
						else if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='partially delivered' && item.partiallyDeliveredContractDt!==null && compareDate(ut,item.partiallyDeliveredContractDt))
								result=true;
						else if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='pending shipment' && item.assignVOContractDt!==null && compareDate(ut,item.assignVOContractDt))
						 	result=true;
					}
					else{
						 if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='partially delivered' && item.assignVOContractDt!==null && compareDate(ut,item.assignVOContractDt))
								result=true;
						 else if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='pending shipment' && item.assignVOContractDt!==null && compareDate(ut,item.assignVOContractDt))
						 	result=true;
					}


				}
				else
					{


					 if(item.status!=null)
						{
							if(containsConfirmed && item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt))
								result=true;
							else if(item.status.toLowerCase()==='confirmed' && item.confirmContractDt!=null && compareDate(ut,item.confirmContractDt))
								result=true;
							else if(item.status.toLowerCase()==='pending vo allocation' && item.pendingVOAllocationContractDt!=null && compareDate(ut,item.pendingVOAllocationContractDt))
								result=true;
							else if(item.status.toLowerCase()==='pending verification' && item.assignVOContractDt!==null && compareDate(ut,item.assignVOContractDt) )
								result=true;
							else if(item.status.toLowerCase()==='cancellation initiated' && item.cancelContractInitiatedDt!==null && compareDate(ut,item.cancelContractInitiatedDt))
								result=true;
							else if(item.status.toLowerCase()==='cancelled' && item.cancelContractDt!==null && compareDate(ut,item.cancelContractDt))
								result=true;
							else if(item.status.toLowerCase()==='pending collection' && item.assignCOContractDt!==null && compareDate(ut,item.assignCOContractDt))
								result=true;
							else if(item.status.toLowerCase()==='pending co allocation' && item.pendingCOAllocationContractDt!==null && compareDate(ut,item.pendingCOAllocationContractDt))
								result=true;
							else if(item.status.toLowerCase()==='closed' && item.closeContractDt!==null && compareDate(ut,item.closeContractDt))
								result=true;
							else if(arg.currentState!=='VoBoard.VoCompleteRequest' && item.status.toLowerCase()==='confirmed' && item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt))
								result=true;
							else if(arg.currentState==='VoBoard.VoCompleteRequest' && item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt))
								result=true;
							else if(item.confirmContractDt!==null && compareDate(ut,item.confirmContractDt) && arg.currentState==='VoBoard.VoCompleteRequest')
								result=true;
							else if(item.status.toLowerCase()==='rejected' && item.orderRejectionDate!==null && compareDate(ut,item.orderRejectionDate))
								result=true;
							else if(item.status.toLowerCase()==='cancellation approved for product reprocessed' && item.cancelContractDt!==null && compareDate(ut,item.cancelContractDt))
								result=true;
							else if(item.status.toLowerCase()==='product reprocessed' && item.cancelContractDt!==null && compareDate(ut,item.cancelContractDt))
								result=true;
						}

						if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='delivered' && item.deliveredContractDt!==null && compareDate(ut,item.deliveredContractDt))
							result=true;
						else if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='partially delivered' && item.partiallyDeliveredContractDt!==null && compareDate(ut,item.partiallyDeliveredContractDt))
							result=true;
						else if(item.shipmentStatus!==null && item.shipmentStatus.toLowerCase()==='pending shipment' && item.assignVOContractDt!==null && compareDate(ut,item.assignVOContractDt))
						 	result=true;
					}
			});
			return result===true?item:null;
	      });

      } else {
    	  dateRanageRecord = campaignCodeRecord;
      }
      if(arg.state.length!==0)
	  {
    	  stateRecord= dateRanageRecord.filter(function (item) {
			var result = false;
			if(arg.currentState==="CoBoard.CollectionContracts")
				{
					$.each(arg.state,function(index,ut) {
					if(item.custState === ut){
						result=true;
						}
				    });
				}
			if(arg.currentState==="CoBoard.CompletedCollectionContracts")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="VoBoard.ManageVoManagerVerification")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="VoBoard.ManageVoCompleteRequest")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="VoBoard.ManageVoPriceDifference")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="VoBoard.VoVerificationRequest")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="VoBoard.VoCompleteRequest")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="CoBoard.ManageCollectorCollectionRequest")
			{
				$.each(arg.state,function(index,ut) {
				if(item.custState === ut){
					result=true;
					}
			    });
			}
			if(arg.currentState==="AdminBoard.ManageCustomer")
			{
				$.each(arg.state,function(index,ut) {
				if(item.homeState === ut){
					result=true;
					}
			    });
			}

				return result===true?item:null;
			});

	      }
	  else{
		  stateRecord =dateRanageRecord;
	  }
      if(arg.vmFilter.length!==0){

    	  vmFilterRecord= stateRecord.filter(function (item) {
  			var result = false;
  			if(arg.currentState==="VoBoard.ManageVoCompleteRequest")
  				{
  					$.each(arg.vmFilter,function(index,ut) {
  					if(ut.toLowerCase()==='verified' && ((item.orderRejectionDate!==null && compareDate('This Month',item.orderRejectionDate))
  							|| (item.confirmContractDt!==null && compareDate('This Month',item.confirmContractDt))
  							|| (item.reconsiderContractDt!==null && compareDate('This Month',item.reconsiderContractDt)))){
  						result=true;
  						}
  					else if(ut.toLowerCase()==='completed' && (item.orderStatus.toLowerCase()==='confirmed'
  																|| item.orderStatus.toLowerCase()==='rejected'
  																|| item.orderStatus.toLowerCase()==='dm rejected'
  																|| item.orderStatus.toLowerCase()==='cancelled'
  																|| item.orderStatus.toLowerCase()==='cancellation initiated'
  																|| item.orderStatus.toLowerCase()==='closed'
  																|| item.orderStatus.toLowerCase()==='pending co allocation'
  																|| item.shipmentStatus.toLowerCase()==='delivered'
  																|| item.shipmentStatus.toLowerCase()==='partially delivered') && compareDate('This Month',item.lastUpdateDt)){
  					result=true;
  					}

  				    });
  				}
  			else if(arg.currentState==="VoBoard.ManageVoManagerVerification"){
  				$.each(arg.vmFilter,function(index,ut) {
  					 if(ut.toLowerCase()==='assigned' && (item.orderStatus.toLowerCase()==='confirmed' || item.orderStatus.toLowerCase()==='pending verification'
							  || item.shipmentStatus.toLowerCase()==='partially delivered')
							  && item.verificationOfficerCode!==null
							  && (item.assignVOContractDt!==null && compareDate('This Month',item.assignVOContractDt))){
  						result=true;
  					}
  				});

  			}
  			else if(arg.currentState==="VoBoard.VoCompleteRequest"){
  				$.each(arg.vmFilter,function(index,ut) {
 					 if(ut.toLowerCase()==='verified' && ((item.confirmContractDt!==null && compareDate('This Month',item.confirmContractDt))
 							 							 || (item.orderRejectionDate!==null && compareDate('This Month',item.orderRejectionDate))
 							 							 || (item.reconsiderContractDt!==null && compareDate('This Month',item.reconsiderContractDt)))){
 						result=true;
 					}
 				});
  			}
  			return result===true?item:null;
    	  });



      }
      else{
    	  vmFilterRecord=stateRecord;
      }
      if(arg.cmFilter.length!==0){

    	  cmFilterRecord= vmFilterRecord.filter(function (item) {
  			var result = false;
  			if(arg.currentState==="CoBoard.CompletedCollectionContracts" || arg.currentState==="CoBoard.CollectionContracts" )
  				{
  					$.each(arg.cmFilter,function(index,ut) {
  					if(ut.toLowerCase()==='uncollected' && (item.status.toLowerCase()==='pending collection'
  																|| item.status.toLowerCase()==='delinquent'
  																|| item.status.toLowerCase()==='current'
  																|| item.status.toLowerCase()==='pay ahead'
  																|| item.status.toLowerCase()==='suspend') && item.statusDate!==null){
  						var currentDate=new Date();
  						currentDate.setHours(0);
  						currentDate.setMinutes(0);
  						currentDate.setSeconds(0);
  						currentDate.setMilliseconds(0);
  						var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
  						//var lastDay = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
  						var statusDate=new Date(item.statusDate.replace('-',' '));
  						if(statusDate<firstDay)
  						result=true;
  						}
  					else if(ut.toLowerCase()==='collected' && (item.status.toLowerCase()==='pending collection'
  																|| item.status.toLowerCase()==='delinquent'
  																|| item.status.toLowerCase()==='pay ahead'
  																|| item.status.toLowerCase()==='closed'
  																|| item.status.toLowerCase()==='suspend')
  																&& compareDate('This Month',item.lastPaymentDt) && item.statusDate!==null){
  					var currentDate=new Date();
  					currentDate.setHours(0);
					currentDate.setMinutes(0);
					currentDate.setSeconds(0);
					currentDate.setMilliseconds(0);
					var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
					var statusDate=new Date(item.statusDate.replace('-',' '));
					if(statusDate>=firstDay)
  					result=true;
  					}
  					else if(ut.toLowerCase()==='completed' && (item.status.toLowerCase()==='pending collection'
															|| item.status.toLowerCase()==='closed'
															|| item.status.toLowerCase()==='cancelled')
															&& item.lastPaymentDt!==null
															&& compareDate('This Month',item.lastUpdateDt)){
  						/*var currentDate=new Date();
  	  					currentDate.setHours(0);
  						currentDate.setMinutes(0);
  						currentDate.setSeconds(0);
  						currentDate.setMilliseconds(0);
  						var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
  						var statusDate=new Date(item.statusDate.replace('-',' '));
  						if(statusDate>=firstDay)*/
  		  				result=true;

  					}
  					else if(ut.toLowerCase()==='partially_collected' && (item.status.toLowerCase()==='pending collection'
															|| item.status.toLowerCase()==='delinquent'
															|| item.status.toLowerCase()==='current'
															|| item.status.toLowerCase()==='pay ahead'
															|| item.status.toLowerCase()==='closed'
															|| item.status.toLowerCase()==='suspend')
															&& item.lastPaymentDt!==null
															&& compareDate('This Month',item.lastPaymentDt) && item.statusDate!==null){
						var currentDate=new Date();
  	  					currentDate.setHours(0);
  						currentDate.setMinutes(0);
  						currentDate.setSeconds(0);
  						currentDate.setMilliseconds(0);
  						var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
  						var statusDate=new Date(item.statusDate.replace('-',' '));
  						if(statusDate<firstDay)
  						result=true;

  					}
  					else if(ut.toLowerCase()==='reassigned' && item.reassignCOContractDt!==null && compareDate('This Month',item.reassignCOContractDt)){
  						result=true;
  					}

  				    });
  				}
  			return result===true?item:null;
    	  });



      }
      else{
    	  cmFilterRecord=vmFilterRecord;
      }
      if(arg.co.length!==0)
	  {
    	  coRecord= cmFilterRecord.filter(function (item) {
			var result = false;
			if(arg.currentState==="CoBoard.CollectionContracts")
				{
				$.each(arg.co,function(index,ut) {
					if(item.collectionOfficerCode === ut){
						result=true;
						}

				      //result=true;
				    });
				}
			if(arg.currentState==="CoBoard.CompletedCollectionContracts")
			{
				$.each(arg.co,function(index,ut) {
					if(item.collectionOfficerCode === ut){
						result=true;
						}

				      //result=true;
				    });
				}

				return result===true?item:null;
			});

	      }
	  else{
		  coRecord =cmFilterRecord;
	  }
      if(arg.vo.length!==0)
	  {
    	  voRecord= coRecord.filter(function (item) {
			var result = false;
			if(arg.currentState==="CoBoard.CollectionContracts")
				{
			$.each(arg.vo,function(index,ut) {
				if(item.verificationOfficerName === ut){
					result=true;
					}

			      //result=true;
			    });
				}
			if(arg.currentState==="VoBoard.ManageVoCompleteRequest")
			{
				$.each(arg.vo,function(index,ut) {
					if(item.verificationOfficerCode === ut){
				result=true;
				}

		      //result=true;
		    });
			}
			if(arg.currentState==="VoBoard.ManageVoManagerVerification")
			{
				$.each(arg.vo,function(index,ut) {
					if(item.verificationOfficerCode === ut){
				result=true;
				}

		      //result=true;
		    });
			}
				return result===true?item:null;
			});

	      }
	  else{
		  voRecord =coRecord;
	  }
      if(arg.salesRep.length!==0)
	  {
    	  SalesRepRecord= voRecord.filter(function (item) {
			var result = false;
			if(arg.currentState==="VoBoard.ManageVoPriceDifference")
				{
				$.each(arg.salesRep,function(index,ut) {
					if(item.salesRepName === ut){
						result=true;
						}

				      //result=true;
				    });
				}
				else if(arg.currentState==="AdminBoard.ManageCommissionDetails"){
					$.each(arg.salesRep,function(index,ut) {
					if(item.salesRepName.toLowerCase()=== ut.toLowerCase()){
						result=true;
						}
				    });
				}
				return result===true?item:null;
			});

	      }
	  else{
		  SalesRepRecord =voRecord;
	  }

      if(angular.isDefined(arg.fiscalWeek) && arg.fiscalWeek!==null && angular.isDefined(arg.fiscalYear) && arg.fiscalYear!==null){
    	  HouseSaleRecord=SalesRepRecord.filter(function(item){
    		  var result=false;
    		  if(arg.currentState==="AdminBoard.HouseSales"){
					if(item.fiscalWeek === arg.fiscalWeek && item.fiscalYear=== arg.fiscalYear){
						result=true;
						}

				}
				else if(arg.currentState==="AdminBoard.ManageAdvanceRequests"){
					if(item.startDt!=null && new Date(item.startDt).getWeek() === arg.fiscalWeek && new Date(item.startDt).getFullYear()=== arg.fiscalYear){
						result=true;
						}
				}
				else if(arg.currentState==="AdminBoard.ManageAdvanceRequestsAdmin"){
					if(item.startDt!=null && new Date(item.startDt).getWeek() === arg.fiscalWeek && new Date(item.startDt).getFullYear()=== arg.fiscalYear){
						result=true;
						}
				}
    		  return result===true?item:null;
    	  })
      }
      else{
    	  HouseSaleRecord =SalesRepRecord;
	  }

      //Adding this Part--Adrita
      if(arg.turnInStatus.length!==0)
	  {
    	  TurnInRecord= HouseSaleRecord.filter(function (item) {
			var result = false;
			if(arg.currentState==="CoBoard.CollectionTurnIn"||arg.currentState==="AdminBoard.AdminTurnIn")
				{
				$.each(arg.turnInStatus,function(index,ut) {
					if(item.statusPayment === ut){
						result=true;
						}

				      //result=true;
				    });
				}
				return result===true?item:null;
			});

	      }
	  else{
		  TurnInRecord =HouseSaleRecord;
	  }
      if(arg.requestType.length!==0)
	  {
    	  requestTypeRecord= TurnInRecord.filter(function (item) {
			var result = false;

				$.each(arg.requestType,function(index,ut) {
					if(item.requestType.toLowerCase() === ut.toLowerCase()){
						result=true;
						}

				      //result=true;
				    });
				return result===true?item:null;
			});

	      }
	  else{
		  requestTypeRecord =TurnInRecord;
	  }
      if(arg.deductionTypeList.length!==0)
	  {
    	  deductionTypeRecord= requestTypeRecord.filter(function (item) {
			var result = false;

				$.each(arg.deductionTypeList,function(index,ut) {
					if(item.deductionMode!==null && item.deductionMode.toLowerCase() === ut.toLowerCase()){
						result=true;
						}

				      //result=true;
				    });
				return result===true?item:null;
			});

	      }
	  else{
		  deductionTypeRecord =requestTypeRecord;
	  }
      if(arg.delinquentMonthList.length!==0)
	  {
    	  delinquentMonthRecord= deductionTypeRecord.filter(function (item) {
			var result = false;

				$.each(arg.delinquentMonthList,function(index,ut) {
					console.log(item.delinquentPeriod);
					if(item.delinquentPeriod!==null && ut.toLowerCase()==='non delinquent' && parseInt(item.delinquentPeriod) === 0){
						result=true;
					}
					else if(item.delinquentPeriod!==null && parseInt(item.delinquentPeriod) === parseInt(ut)){
						result=true;
						}

				      //result=true;
				    });
				return result===true?item:null;
			});

	      }
	  else{
		  delinquentMonthRecord =deductionTypeRecord;
	  }

      locale = delinquentMonthRecord;

      localStorageService.set('filteredContract',locale);
      return locale;
 };
 function compareDate (dateRanage, dateCheck) {
	 /*var dateFrom = "02-05-2013";
	var dateTo = "02-09-2013";
	var dateCheck = "02-May-2013";

	 var d1 = dateFrom.split("-");
	 var d2 = dateTo.split("-");
	 var c = dateCheck.split("-");

	 var from = new Date(d1[2], d1[1]-1, d1[0]);  // -1 because months are from 0 to 11
	 var to   = new Date(d2[2], d2[1]-1, d2[0]);*/

	 var now = from = to = new Date();
	 switch(dateRanage) {
		 case 'This Week': {
			 var thisWeek = rangeWeek();
			 from = thisWeek.start;
			 to = thisWeek.end;
			 break;
		 }
		 case 'Last Week': {
			 var lastWeek = rangeWeek(new Date(new Date(now).setDate(now.getDate() - 7)));
			 from = lastWeek.start;
			 to = lastWeek.end;
			 break;
		 }
		 case 'This Month': {
			 var thisMonth = rangeMonth(now.getMonth(), now.getFullYear());
			 from = thisMonth.start;
			 to = thisMonth.end;
			 break;
		 }
		 case 'Last Month': {
			 var month = now.getMonth() - 1;
			 var year = now.getFullYear();
			 if (month  < 0) {
				 month = month + 12;
				 year = now.getFullYear() - 1;
			 }
			 var lastMonth = rangeMonth(month, year);
			 from = lastMonth.start;
			 to = lastMonth.end;
			 break;
		 }
		 case 'This Quarter': {
			 var thisQuarter = rangeQuarter(now, now.getMonth(), now.getFullYear());
			 from = thisQuarter.start;
			 to = thisQuarter.end;
			 break;
		 }
		 case 'Last Quarter': {
			 var month = now.getMonth() - 3;
			 var year = now.getFullYear();
			 if (month  < 0) {
				 month = month + 12;
				 year = now.getFullYear() - 1;
			 }
			 var lastQuarter = rangeQuarter(now, month, year);
			 from = lastQuarter.start;
			 to = lastQuarter.end;
			 break;
		 }
		 case 'This Year': {
			 var thisYear = rangeYear(now.getFullYear());
			 from = thisYear.start;
			 to = thisYear.end;
			 break;
		 }
		 case 'Last Year': {
			 var lastYear = rangeYear(now.getFullYear() - 1);
			 from = lastYear.start;
			 to = lastYear.end;
			 break;
		 }
	 }
	 if(dateCheck!==null)
		 {
		 	var c = dateCheck.split("-");
		 	var month = shortMonthArr.indexOf(c[1]);	 //in global js
		 	var check = new Date(c[2], month, c[0]);
		 	return (check >= from && check <= to);
		 }

 };

 function rangeQuarter (now, month, year) {
	 var start, end;
	 var quarter = Math.floor((month + 3) / 3);
	 switch(quarter) {
	 	case 1: {
	 		return { start: new Date(year+"-1-1"), end: new Date(year+"-3-31") };
	 		break;
	 	}
	 	case 2: {
	 		return { start: new Date(year+"-4-1"), end: new Date(year+"-6-30") };
	 		break;
	 	}
	 	case 3: {
	 		return { start: new Date(year+"-7-1"), end: new Date(year+"-9-30") };
	 		break;
	 	}
	 	case 4: {
	 		return { start: new Date(year+"-10-1"), end: new Date(year+"-12-31") };
	 		break;
	 	}
	 }
 };

 function rangeYear (yearStr) {
	 return { start: new Date(yearStr+"-1-1"), end: new Date(yearStr+"-12-31") };
 };

 function rangeMonth (month, year) {
     return { start: new Date(year, month, 1), end: new Date(year, month + 1, 0) };
 };

 function rangeWeek (dateStr) {
     if (!dateStr) dateStr = new Date().getTime();
     var dt = new Date(dateStr);
     dt = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
     dt = new Date(dt.getTime() - (dt.getDay() > 0 ? (dt.getDay() - 1) * 1000 * 60 * 60 * 24 : 6 * 1000 * 60 * 60 * 24));
     return { start: dt, end: new Date(dt.getTime() + 1000 * 60 * 60 * 24 * 7 - 1) };
 };

return {
    sideBarFilter:sideBarFilter
}
}]);