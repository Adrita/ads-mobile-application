// Hide Header on on scroll down
$(document).ready(function(){
var didScroll;
var lastScrollTop = 0;
/*var delta = 5;*/
var delta=170;
var navbarHeight = $('header').outerHeight() + $('nav').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {

    var st = $(this).scrollTop();
    if($('nav').outerHeight()===0)
    		var delta=44;
    else
    	var delta=170;
    if((st-delta)<0)
    	{
    		$('header').removeClass('hideBar').addClass('header-up');
    		$('nav').removeClass('hideBar').addClass('navbar-up');
    		return;
    	}
    
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('header-up').addClass('hideBar');
        $('nav').removeClass('navbar-up').addClass('hideBar');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
           $('header').removeClass('hideBar').addClass('header-up');
           $('nav').removeClass('hideBar').addClass('navbar-up');
        }
    }
    lastScrollTop = st;
}
});