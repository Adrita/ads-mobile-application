app.controller("ManageGridCtrl", ['$scope', '$filter', '$timeout', '$state', '$rootScope', 'localStorageService', 'localize',
    'adapter', 'grid', 'filterService', 'adjustHeight', 'Session', '$uibModal', 'authentication', 'syncService', '$cordovaNetwork', '$base64', 'commonServices', '$http',
    function($scope, $filter, $timeout, $state, $rootScope, localStorageService, localize,
        adapter, grid, filterService, adjustHeight, Session, $uibModal, authentication, syncService, $cordovaNetwork, $base64, commonServices, $http) {
        //Test Comment
        $scope.loading = true;
        $scope.showPagination = true;
        localStorageService.set('currentRecord', null);
        localStorageService.set('lastPage', null);
        localStorageService.set('lastData', null);
        localStorageService.set('backLinkText', null);
        localStorageService.set('pageActionHeader', null);
        var currentTypeId = null;
        $scope.loader = false;
        $scope.isOffline = false;
        $scope.isDesktop = true;

        //alert(device.uuid);

        localStorage.setItem('deviceId', device.uuid);

        var geolocationOptions = {
            maximumAge: 3000,
            timeout: 50000,
            enableHighAccuracy: true
        };


        //		navigator.geolocation.getCurrentPosition(geolocationSuccess,
        //                                                         geolocationError,
        //                                                         [geolocationOptions]);




        var internetConnected = true;

        if (window.innerWidth < 960) $scope.isDesktop = false;

        var offline = false;

        var filterData = null;
        var mappedURL = adapter.getMappedUrls();
        switch ($state.current.name) {
            case 'AdminBoard.ManageUser':
            case 'AdminBoard.ManageRole':
            case 'AdminBoard.ManageRegistration':
            case 'AdminBoard.ManageCustomer':
            case 'AdminBoard.ManageSequenceNumber':
            case 'AdminBoard.ManageCountry':
            case 'AdminBoard.ManageLocation':
            case 'AdminBoard.ManageRegion':
            case 'AdminBoard.ManageDivision':
            case 'AdminBoard.ManageScreenAccess':
            case 'AdminBoard.ManageCommissionRate':
            case 'AdminBoard.ManagePriceList':
            case 'AdminBoard.ManageSequenceGenerator':
            case 'AdminBoard.ManageItem':
            case 'VoBoard.ManageVoManagerVerification':
            case 'VoBoard.ManageVoCompleteRequest':
            case 'VoBoard.ManageVoPriceDifference':
            case 'VoBoard.VoVerificationRequest':
            case 'VoBoard.VoCompleteRequest':
            case 'CoBoard.CollectionContracts':
            case 'CoBoard.CompletedCollectionContracts':
            case 'CoBoard.ManageCollectorCollectionRequest':
            case 'CoBoard.CollectionTurnIn':
            case 'AdminBoard.AdminTurnIn':
            case 'AdminBoard.HouseSales':
                {
                    //$rootScope.$broadcast('showCollapseMenu', true);
                    $rootScope.$broadcast('TrackNavBar', true);
                    $rootScope.$broadcast('AdminStateTrack', $state.current.name);
                    collapseNav();
                    break;
                }
            case 'AdminBoard.ManageContract':
                {
                    $rootScope.$broadcast('TrackNavBar', true);
                    $rootScope.$broadcast('AdminStateTrack', $state.current.name);
                    //activateService();
                    collapseNav();
                    break;
                }
            default:
                {
                    $rootScope.$broadcast('showCollapseMenu', false);
                }
        }


        // check if current state matches link
        function collapseNav() {
            //navigation close for mobile manage page
            var mobileScreenList = ['AdminBoard.ManageContract', 'AdminBoard.CreateEditContract', 'AdminBoard.ViewContract', 'CoBoard.ManageCollectorCollectionRequest', 'CoBoard.CoCollectPayment', 'SalesRep.SalesRepRegistration', 'VoBoard.VoCollectPayment', 'VoBoard.VoVerifyContract', 'VoBoard.VoVerificationRequest'];

            if (mobileScreenList.indexOf($state.current.name) > -1 && window.innerWidth < 768) $rootScope.$broadcast('showCollapseMenu', false);
            else $rootScope.$broadcast('showCollapseMenu', true);
        };

        $rootScope.$broadcast('HideMobileNav', true);
        $rootScope.$broadcast('AdminStateTrack', $state.current.name);

        $scope.MainGridWidth = "col-xs-10 desktop hidden-xs filter-on";
        $scope.SidebarWidth = "filter-on";
        $scope.MobilePageWidth = "col-xs-10 visible-xs container-xs manage-xs filter-off";

        if ($state.current.name === "AdminBoard.ManageRegistration") {
            $scope.MainGridWidth = "col-xs-12 desktop hidden-xs";
        }

        if ($state.current.name === "AdminBoard.AdminReceiveBankIn") {
            $scope.MainGridWidth = "col-xs-10 desktop hidden-xs";
        }

        if (window.innerWidth <= 959 && window.innerWidth >= 768) {
            $scope.MainGridWidth = "col-xs-10 grid_custom_width desktop hidden-xs filter-off";
        }
        if (window.innerWidth <= 959) $scope.SidebarWidth = "filter-off";

        $scope.$on('ModifiedGridWidth', function(event, arg) {
            $scope.MainGridWidth = arg;
            refreshGrid();
        });
        $scope.$on('SidebarWidth', function(event, arg) {
            $scope.SidebarWidth = arg;
        });
        $scope.$on('ModifiedMobileWidth', function(event, arg) {
            $scope.MobilePageWidth = arg;
        });

        $scope.HideNavBar = function() {
            $rootScope.$broadcast('TrackNavBar', false);
        };
        $scope.gridOptions = {};
        $scope.serviceData = null;
        $scope.quantity = 5; /*for mobile loading*/
        $scope.colInfo = null;
        $scope.colData = null;
        /*Initialization with default values*/
        $scope.selectedCnt = 10;
        $scope.currentPage = 1;
        showingRecord();
        $scope.recordsPerPage = [10, 20, 30, 40, 60];
        $scope.maxSize = 5;
        var pageOptions = {
            data: null,
            pageNo: 1,
            pageSize: 10,
            totalCnt: 0
        };
        $scope.gridOptions = {
            columnDefs: null,
            data: null,
            rowHeight: 50,
            enableColumnMenus: false,
            enableVerticalScrollbar: 0,
            enablePaging: true,
            pagingOptions: null,
            rowTemplate: grid.rowTemplate()
        };
        activateService();


        /*service call*/
        //$scope.isEmptyManageData = true;
        $scope.hidePaginationOnNoData = false;

        function activateService() {
            adjustHeight.adjustTopBar();

            /*// enumerate routes that don't need authentication
            var stateThatDontCallAgain = [ 'CoBoard.CollectionContracts', 'VoBoard.ManageCollectorCollectionRequest', 'CoBoard.ManageCollectorCollectionRequest', 'VoBoard.ManageVoManagerVerification', 'AdminBoard.EditCustomer', 'VoBoard.VoVerifyContract', 'VoBoard.ViewVerificationReport' ];

            // check if current state matches link
            var linkClean = function(state) {
            	if( stateThatDontCallAgain.indexOf(state) === -1 ) return true;
            	return false; //for stateThatDontCallAgain list
            };

            if (linkClean($state.current.name)) {

            }*/

            if ($state.current.name === 'CoBoard.CollectionContracts' || $state.current.name === 'VoBoard.ManageCollectorCollectionRequest' || $state.current.name === 'CoBoard.ManageCollectorCollectionRequest' || $state.current.name === 'VoBoard.ManageVoManagerVerification' ||
                $state.current.name === 'AdminBoard.EditCustomer' || $state.current.name === 'VoBoard.VoVerifyContract' || $state.current.name === 'VoBoard.ViewVerificationReport' || $state.current.name === 'VoBoard.VoTurnIn' || $state.current.name === 'CoBoard.CollectionTurnIn' || $state.current.name === 'AdminBoard.AdminTurnIn' || $state.current.name === "SalesRep.ManageTurnIn" || $state.current.name === "AdminBoard.HouseSales" || $state.current.name === "SalesRep.ManageContractSales" ||
                $state.current.name === 'AdminBoard.ManageAdvanceRequests' || $state.current.name === 'AdminBoard.ManageAdvanceRequestsAdmin') {
                console.log("URL");
            } else {
                adapter.getMappedUrls().then(function(result) {
                    $scope.loading = true;
                    var url = adapter.getCurrentUrl(result);
                    console.log(url);
                    var data = {
                        "userId": Session.userId
                    };

                    if ($state.current.name === 'AdminBoard.ManageCountry') {
                        data = {}
                    }
                    if ($state.current.name === 'AdminBoard.ManageUser') {
                        //  data = {"userId" :localStorageService.get('userId')}
                        data = {
                            "userId": Session.userId,
                            "pageSize": $scope.selectedCnt,
                            "currentPage": $scope.currentPage,
                            "filtered": localStorageService.get('isFiltered'),
                            "filterObj": localStorageService.get('filterObj')
                        }
                    }
                    if ($state.current.name === 'VoBoard.VoCompleteRequest') {
                        data = {
                            "userId": Session.userId
                        }
                    }

                    if ($state.current.name === 'AdminBoard.ManageContract') {
                        data = {
                            "userId": Session.userId,
                            "pageSize": $scope.selectedCnt,
                            "currentPage": $scope.currentPage,
                            "filtered": localStorageService.get('isFiltered'),
                            "filterObj": localStorageService.get('filterObj')
                        }
                    }

                    if ($state.current.name === 'AdminBoard.ManagePriceList') {
                        data = {
                            "userId": Session.userId,
                            "pageSize": $scope.selectedCnt,
                            "currentPage": $scope.currentPage,
                            "filtered": localStorageService.get('isFiltered'),
                            "filterObj": localStorageService.get('filterObj')
                        }
                    }

                    if ($state.current.name === 'VoBoard.VoVerificationRequest') {
                        data = {
                            "userId": Session.userId,
                            "pageSize": $scope.selectedCnt,
                            "currentPage": $scope.currentPage,
                            "filtered": localStorageService.get('isFiltered'),
                            "filterObj": localStorageService.get('filterObj')
                        }

                        if ($scope.serviceType === "scroll") {
                            $scope.loading = false;
                            $scope.scrollLoad = true;
                            $scope.gridOptions.data = null;

                            $scope.serviceType = "";

                        }

                        /*if(localStorageService.get('filterObj')!==null && localStorageService.get('filterObj')!==undefined){
                        	url="VerificationController/manageAssignedVerification";
                        }*/
                    }
                    if (url !== undefined && typeof url !== "object") {

                        adapter.getServiceData(url, data).then(success, error);



                        function success(result) {

                            //internetConnected=true;

                            localStorageService.set("countryList", result.countryList);
                            if (result.statusCode === 200) {
                                $scope.loading = false;
                                $scope.isEmptyManageData = false;
                                switch ($state.current.name) {
                                    case 'AdminBoard.ManageUser':
                                        {
                                            $scope.serviceData = result.userList;
                                            $scope.colData = result.userList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            $scope.totalItemCount = result.totalNumberOfRecords;
                                            localStorageService.set('isFiltered', result.filtered);
                                            if (result.currentPage === 1) {
                                                $scope.currentPage = 1;
                                            }

                                            break;
                                        }
                                    case 'AdminBoard.ManageDivision':
                                        {
                                            $scope.serviceData = result.divisionList;
                                            $scope.colData = result.divisionList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageCountry':
                                        {
                                            $scope.serviceData = result.countries;
                                            $scope.colData = result.countries;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageBank':
                                        {

                                            $scope.serviceData = result.bankList;
                                            $scope.colData = result.bankList;
                                            $scope.isEmptyManageData = false;



                                            break;
                                        }
                                    case 'AdminBoard.ManageLocation':
                                        {
                                            $scope.serviceData = result.locationList;
                                            $scope.colData = result.locationList;
                                            if ($scope.colData === null || $scope.colData === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageRegion':
                                        {

                                            var regData = [];

                                            for (var i = 0; i < result.regionList.length; i++) {
                                                var areaList = [];
                                                for (var j = 0; j < result.regionList[i].areaList.length; j++) {
                                                    areaList.push(result.regionList[i].areaList[j].areaName);
                                                }
                                                regData.push({
                                                    regionCode: result.regionList[i].regionCode,
                                                    regionId: result.regionList[i].regionId,
                                                    regionName: result.regionList[i].regionName,
                                                    areaList: areaList,
                                                    countryName: result.regionList[i].countryName,
                                                    active: result.regionList[i].active
                                                })
                                            }
                                            $scope.serviceData = regData;
                                            $scope.colData = regData;
                                            localStorageService.set("regionData", result.regionList);
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageUserType':
                                        {
                                            $scope.serviceData = result.userTypeList;
                                            var countryArr = [];
                                            for (var i = 0; i < result.userTypeList.length; i++) {
                                                countryArr = [];
                                                for (var j = 0; j < result.userTypeList[i].countryList.length; j++)
                                                    countryArr.push(result.userTypeList[i].countryList[j].countryName);
                                                result.userTypeList[i].countryNameList = countryArr;
                                            }
                                            $scope.colData = result.userTypeList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageItem':
                                        {
                                            $scope.serviceData = result.itemList;
                                            $scope.colData = result.itemList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageRole':
                                        {
                                            $scope.serviceData = result.roleList;
                                            localStorageService.set("userTypeList", result.userTypeList);
                                            // localStorageService.set("accessList",result.screenAccessDetailsList);
                                            $scope.colData = result.roleList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageScreenAccess':
                                        {

                                            $scope.colData = result.screenList;
                                            $scope.serviceData = result.screenList;
                                            if ($scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            } else {
                                                $scope.isEmptyManageData = false;
                                            }

                                            break;
                                        }
                                    case 'AdminBoard.ManagePriceList':
                                        {
                                            $scope.serviceData = result.priceList;
                                            localStorageService.set("userTypeList", result.priceList);
                                            localStorageService.set("priceCodeList", result.priceCodeList);
                                            localStorageService.set("installmentList", result.installmentList);
                                            $scope.colData = result.priceList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }

                                            $scope.totalItemCount = result.totalNumberOfRecords;
                                            localStorageService.set('isFiltered', result.filtered);
                                            if (result.currentPage === 1) {
                                                $scope.currentPage = 1;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageCommissionRate':
                                        {
                                            $scope.serviceData = result.commissionRateList;
                                            localStorageService.set("countryList", result.countryList);
                                            localStorageService.set("userTypeList", result.commissionRateList);
                                            localStorageService.set("designationList", result.designationList);
                                            $scope.colData = result.commissionRateList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageSequenceGenerator':
                                        {
                                            $scope.serviceData = result.sequenceNumberList;
                                            console.log(result.sequenceNumberList);
                                            localStorageService.set("dataElementList", result.dataElementList);
                                            $scope.colData = result.sequenceNumberList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'AdminBoard.ManageContract':
                                        {

                                            $scope.colData = result.manageContractList;
                                            $scope.serviceData = result.manageContractList;
                                            localStorageService.set('isFiltered', result.filtered);
                                            //if (localStorageService.get('manageData') === '' || localStorageService.get('manageData') === null || localStorageService.get('manageData') === undefined)
                                            localStorageService.set('manageData', $scope.colData);

                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }

                                            $scope.topTotalPoints = result.totalPoint;
                                            $scope.topTotalOrderAmount = result.totalOrderAmt;
                                            $scope.topOutstandingAmount = result.totalOutstandingAmt;
                                            $scope.totalItemCount = result.totalNumberOfRecords;

                                            if (result.currentPage === 1) {
                                                $scope.currentPage = 1;
                                            }
                                            /*if(localStorageService.get('dashBoardContractsSelected')!==null && localStorageService.get('dashBoardContractsSelected')!==undefined && localStorageService.get('dashBoardContractsSelected')!=="")
                                            	{
                                            		$rootScope.$broadcast('getSelectedContracts', { type:localStorageService.get('dashBoardContractsSelected')  });
                                            		localStorageService.set('dashBoardContractsSelected',null);

                                            	}*/
                                            break;

                                        }
                                    case 'AdminBoard.ManageCustomer':
                                        {

                                            $scope.colData = result.manageCustomerList;
                                            $scope.serviceData = result.manageCustomerList;
                                            if ($scope.colData.length === 0 || $scope.colData === null) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;

                                        }

                                    case 'AdminBoard.ManageRegistration':
                                        {
                                            $scope.colData = result.salesRepRegList;
                                            $scope.serviceData = result.salesRepRegList;
                                            $scope.roleList = result.roleList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;

                                        }
                                    case 'VoBoard.ManageVoCompleteRequest':
                                        {
                                            $scope.colData = result.verificationList;
                                            $scope.serviceData = result.verificationList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'VoBoard.ManageVoPriceDifference':
                                        {
                                            $scope.colData = result.verificationList;
                                            $scope.serviceData = result.verificationList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'VoBoard.VoVerificationRequest':
                                        {
                                            $scope.colData = result.verificationList;
                                            $scope.serviceData = result.verificationList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }

                                            $scope.totalItemCount = result.totalNumberOfRecords;
                                            localStorageService.set('isFiltered', result.filtered);
                                            if (result.currentPage === 1) {
                                                $scope.currentPage = 1;
                                            }
                                            //      break;
                                            // app.saveVerificationRequestData(data);
                                            syncService.fetchVerificationData(data);
                                            $scope.scrollLoad = false;
                                            break;
                                        }
                                    case 'VoBoard.VoCompleteRequest':
                                        {
                                            $scope.colData = result.verificationList;
                                            $scope.serviceData = result.verificationList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'CoBoard.CompletedCollectionContracts':
                                        {
                                            $scope.hidePaginationOnNoData = true;
                                            $scope.colData = result.collectionList;
                                            $scope.serviceData = result.collectionList;
                                            if ($scope.colData === null || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }
                                    case 'SalesRep.ManageContractSales':
                                        {
                                            $scope.colData = result.manageContractList;
                                            $scope.serviceData = result.manageContractList;
                                            localStorageService.set('manageData', $scope.colData);
                                            if ($scope.colData === null || $scope.colData === undefined || $scope.colData.length === 0) {
                                                $scope.isEmptyManageData = true;
                                            }
                                            break;
                                        }


                                        $scope.totalList = $scope.colData.length;

                                }



                                // syncService.syncAllData({userId:Session.userId});

                                gridPopulation();
                                //adjustHeight.adjustTopBar();
                            } else {
                                $scope.loading = false;
                                $scope.isEmptyManageData = true;
                                console.log('service not resolved');
                                //adjustHeight.adjustTopBar();
                            }
                        }

                        function error() {

                            internetConnected = false;


                            $scope.loading = false;
                            $scope.isEmptyManageData = true;
                            switch ($state.current.name) {
                                case 'AdminBoard.ManageContract':
                                    {
                                        var data = "manageContract";

                                        app.fetchDraftContract(data, callbackManageContract, callbackError);
                                        break;
                                    }

                                case 'VoBoard.VoVerificationRequest':
                                    {
                                        var data = "verificationRequest";

                                        offline = true;

                                        app.fetchVerificationRequest(data, callbackVerificationRequest);
                                        break;

                                    }
                            }


                        }
                    }

                });


            }


        };




        $scope.$on('gridData', function(event, gridObj) {
            $scope.colData = gridObj;
            $scope.serviceData = gridObj;
            if ($state.current.name === 'SalesRep.ManageContractSales') {
                localStorageService.set('manageData', $scope.colData);
            }

            gridPopulation();
        });


        function callbackVerificationRequest(result) {
            if (result.statusCode === 200) {
                $scope.colData = result.verificationList;
                $scope.serviceData = result.verificationList;
                if ($scope.colData === null || $scope.colData.length === 0) {
                    $scope.isEmptyManageData = true;
                } else {
                    $scope.isEmptyManageData = false;
                }
                $scope.totalList = $scope.colData.length;
                $scope.totalItemCount = $scope.colData.length

                $scope.$apply();

                gridPopulation();
            }
        }

        function callbackError(result) {
            console.log(result + "In Error");
            $scope.isEmptyManageData = true;
            $scope.totalList = 0;
            $scope.colData = [];
            $scope.serviceData = [];

            $scope.showPagination = false;

            localStorageService.set('manageData', $scope.colData);
            $scope.gridOptions.data = [];

            $scope.$apply();
            gridPopulation();



        }

        function callbackManageContract(resultJson) {

            console.log(resultJson + "In success");
            $scope.colData = resultJson;
            $scope.serviceData = resultJson;
            //if (localStorageService.get('manageData') === '' || localStorageService.get('manageData') === null || localStorageService.get('manageData') === undefined)
            localStorageService.set('manageData', $scope.colData);

            if ($scope.colData === null || $scope.colData.length === 0) {
                $scope.isEmptyManageData = true;
            } else {
                $scope.isEmptyManageData = false;
            }
            if (localStorageService.get('dashBoardContractsSelected') !== null && localStorageService.get('dashBoardContractsSelected') !== undefined && localStorageService.get('dashBoardContractsSelected') !== "") {
                $rootScope.$broadcast('getSelectedContracts', {
                    type: localStorageService.get('dashBoardContractsSelected')
                });

            }

            var totalPoints = 0;
            var totalOrderAmt = 0;
            var totalOutstandingAmt = 0;

            angular.forEach($scope.colData, function(value, key) {
                if (value.totalAmountOrder && value.totalOutstandingBalance && value.points) {
                    totalPoints = totalPoints + parseFloat(value.points);
                    totalOrderAmt = totalOrderAmt + parseFloat(value.totalAmountOrder);
                    totalOutstandingAmt = totalOutstandingAmt + parseFloat(value.totalOutstandingBalance);
                }

            });
            $scope.topTotalPoints = totalPoints;
            $scope.topTotalOrderAmount = totalOrderAmt;
            $scope.topOutstandingAmount = totalOutstandingAmt;
            $scope.totalList = $scope.colData.length;
            $scope.totalItemCount = $scope.colData.length;

            $scope.$apply();

            gridPopulation();

        }

        function gridPopulation() {
            if ($state.current.name === 'AdminBoard.HouseSales' ||
                $state.current.name === 'CoBoard.ManageCollectorCollectionRequest' ||
                $state.current.name === 'CoBoard.CollectionContracts') {
                var result = localStorageService.get('manageHouseSales');
                var cols = result.defaultColumns;
                var allCols = result.allColumns;
                var selctedColoumnConfig = localStorageService.get('selctedColoumnConfig');
                if (selctedColoumnConfig !== null && selctedColoumnConfig !== undefined && selctedColoumnConfig[$state.current.name] !== undefined && selctedColoumnConfig[$state.current.name] !== null) {
                    $scope.colInfo = selctedColoumnConfig[$state.current.name];
                    $scope.gridOptions.columnDefs = selctedColoumnConfig[$state.current.name];
                } else {
                    $scope.colInfo = cols;
                    $scope.gridOptions.columnDefs = cols;
                }

                $scope.configGrid();
                localStorageService.set('selectedColumns', cols);
                localStorageService.set('allColumns', allCols);
            } else
                grid.getColMappingData($scope.serviceData).then(function(result) {

                    if ($state.current.name === 'AdminBoard.ManageContract') { //For column configuration

                        var cols = result.defaultColumns;
                        var allCols = result.allColumns;
                        if (window.innerWidth < 960) {
                            cols = result.tabColumns;
                        }
                        var selctedColoumnConfig = localStorageService.get('selctedColoumnConfig');
                        if (selctedColoumnConfig !== null && selctedColoumnConfig !== undefined && selctedColoumnConfig[$state.current.name] !== undefined && selctedColoumnConfig[$state.current.name] !== null) {
                            $scope.colInfo = selctedColoumnConfig[$state.current.name];
                            $scope.gridOptions.columnDefs = selctedColoumnConfig[$state.current.name];
                        } else {
                            $scope.colInfo = cols;
                            $scope.gridOptions.columnDefs = cols;
                        }
                        $scope.configGrid();
                        localStorageService.set('selectedColumns', cols);
                        localStorageService.set('allColumns', allCols);


                    } else if ($state.current.name === 'VoBoard.VoCompleteRequest' ||
                        $state.current.name === 'VoBoard.VoVerificationRequest' ||
                        $state.current.name === 'VoBoard.ManageVoCompleteRequest' ||
                        $state.current.name === 'CoBoard.CompletedCollectionContracts') {
                        var cols = result.defaultColumns;
                        var allCols = result.allColumns;
                        var selctedColoumnConfig = localStorageService.get('selctedColoumnConfig');
                        if (selctedColoumnConfig !== null && selctedColoumnConfig[$state.current.name] !== undefined && selctedColoumnConfig[$state.current.name] !== null) {
                            $scope.colInfo = selctedColoumnConfig[$state.current.name];
                            $scope.gridOptions.columnDefs = selctedColoumnConfig[$state.current.name];
                        } else {
                            $scope.colInfo = cols;
                            $scope.gridOptions.columnDefs = cols;
                        }
                        $scope.configGrid();
                        localStorageService.set('selectedColumns', cols);
                        localStorageService.set('allColumns', allCols);

                    } else {
                        $scope.colInfo = result;
                        $scope.configGrid();
                        $scope.gridOptions.columnDefs = result;
                    }
                    /*$scope.gridOptions.data = manageVoManagerData();*/
                });
        }
        $scope.configGrid = function() {


            var selectedColoumns = localStorageService.get('selctedColoumnConfig');
            if (selectedColoumns !== null && selectedColoumns[$state.current.name] !== null && selectedColoumns[$state.current.name] !== undefined) $scope.gridOptions.columnDefs = selectedColoumns[$state.current.name];
            else
                $scope.gridOptions.columnDefs = $scope.colInfo;

            if ($scope.colData != null) {
                if ($state.current.name === 'AdminBoard.ManageContract' || $state.current.name === 'AdminBoard.ManageUser' || $state.current.name === 'AdminBoard.ManagePriceList' || $state.current.name === 'VoBoard.ManageVoManagerVerification' || $state.current.name === 'CoBoard.CollectionContracts' || $state.current.name === 'VoBoard.VoVerificationRequest' || $state.current.name === 'CoBoard.ManageCollectorCollectionRequest') {
                    $scope.totalItemCount = $scope.totalItemCount;
                    $scope.totalItems = $scope.totalItemCount;
                } else {
                    $scope.totalItemCount = $scope.colData.length;
                    $scope.totalItems = $scope.colData.length;
                }

                if ($state.current.name === 'CoBoard.CollectionTurnIn' || $state.current.name === 'AdminBoard.AdminTurnIn' || $state.current.name === 'VoBoard.VoTurnIn' || $state.current.name === 'SalesRep.ManageTurnIn' /*||$state.current.name==='AdminBoard.ManageCommissionDetails'*/ )
                    setPagingData($scope.colData, $scope.currentPage, $scope.colData.length);
                else
                    setPagingData($scope.colData, $scope.currentPage, $scope.selectedCnt);




                //Mobile screen pagination
                mobilePagination();
            } else {
                setPagingData($scope.colData, $scope.currentPage, $scope.selectedCnt);
            }

        }
        var setPagingData = function(data, page, pageSize) {
            if ($state.current.name !== 'AdminBoard.ManageContract' && $state.current.name !== 'AdminBoard.ManageUser' && $state.current.name !== 'AdminBoard.ManagePriceList' && $state.current.name !== 'VoBoard.ManageVoManagerVerification' && $state.current.name !== 'CoBoard.CollectionContracts' && $state.current.name !== 'VoBoard.VoVerificationRequest' && $state.current.name !== 'CoBoard.ManageCollectorCollectionRequest') {
                var pagedData = data.slice((page - 1) * pageSize, page * pageSize);

                if (pagedData !== null && pagedData.length > 0) {
                    $scope.gridOptions.data = pagedData;
                } else {
                    $scope.gridOptions.data = [];
                }
            } else {
                var pagedData = data;

                if (pagedData !== null && pagedData.length > 0) {
                    $scope.gridOptions.data = pagedData;
                } else {
                    $scope.gridOptions.data = [];
                }
            }



            showingRecord();

            refreshGrid();
        };


        //For mobile screen listing
        $scope.listing = {};

        $scope.listing.sortedByList = [{
            value: "lastPaymentDate",
            text: "Last Payment Date"
        }, {
            value: "status",
            text: "Status"
        }];

        if ($state.current.name === 'CoBoard.ManageCollectorCollectionRequest') {
            $scope.listing.sortedByList = [{
                value: "nextPaymentDueDt",
                text: "Payment Due Date"
            }, {
                value: "orderStatus",
                text: "Status"
            }];
        } else if ($state.current.name === 'VoBoard.VoVerificationRequest' || $state.current.name === 'VoBoard.VoCompleteRequest') {
            $scope.listing.sortedByList = [{
                value: "points",
                text: "Points"
            }, {
                value: "totalOrderAmt",
                text: "Total Amount"
            }];
        }

        function mobilePagination() {

            var limit = 10;
            $scope.listing.loadMore = function() {
                $scope.selectedCnt = $scope.selectedCnt + limit;
                $scope.PageCntChange();
            }
        }

        function getColMapData() {
            $scope.colData = grid.getPagingData(pageOptions);
        }

        function refreshGrid() {
            $scope.refresh = true;
            $timeout(function() {
                $scope.refresh = false;
            }, 0);
        };

        // Modified for Pagination 30-May
        function showingRecord() {

            if ($state.current.name !== 'AdminBoard.ManageContract' && $state.current.name !== 'AdminBoard.ManageUser' && $state.current.name !== 'AdminBoard.ManagePriceList' && $state.current.name !== 'VoBoard.ManageVoManagerVerification' && $state.current.name !== 'CoBoard.CollectionContracts' && $state.current.name !== 'VoBoard.VoVerificationRequest' && $state.current.name !== 'CoBoard.ManageCollectorCollectionRequest') {
                localStorageService.set('isFiltered', false);
            }
            $scope.showingStart = (($scope.currentPage - 1) * $scope.selectedCnt) + 1;
            $scope.showingEnd = $scope.currentPage * $scope.selectedCnt;
            if ($scope.showingEnd > $scope.totalItemCount) {
                $scope.showingEnd = $scope.totalItemCount;
                /*if(($state.current.name==='AdminBoard.ManageContract'||$state.current.name==='AdminBoard.ManageUser') && localStorageService.get('isFiltered')){
        		 		$scope.showingStart=1;
        			 	$scope.currentPage=1;
        		 	}*/

            }
            if ($scope.selectedCnt >= $scope.totalItemCount) $scope.showPagination = false;
            else $scope.showPagination = true;
            if (filterData !== null && filterData !== undefined)
                if (filterData.length === 0) {
                    $scope.showingStart = 0;
                }
        }

        $scope.PageCntChange = function() {
            $scope.currentPage = 1;
            showingRecord();

            var itemsPerPage = $scope.selectedCnt;


            if ($state.current.name !== 'AdminBoard.ManageContract' && $state.current.name !== 'AdminBoard.ManageUser' && $state.current.name !== 'AdminBoard.ManagePriceList' && $state.current.name !== 'VoBoard.ManageVoManagerVerification' && $state.current.name !== 'CoBoard.CollectionContracts' && $state.current.name !== 'VoBoard.VoVerificationRequest' && $state.current.name !== 'CoBoard.ManageCollectorCollectionRequest') {

                if (filterData !== null) {
                    setPagingData(filterData, $scope.currentPage, $scope.selectedCnt);
                    if (filterData.length % itemsPerPage === 0)
                        $scope.totalItems = Math.floor((filterData.length / itemsPerPage)) * 10;
                    else
                        $scope.totalItems = (Math.floor((filterData.length / itemsPerPage) + 1)) * 10;
                    console.log($scope.totalItems);
                    setPagingData(filterData, 1, itemsPerPage);
                } else {
                    setPagingData($scope.serviceData, $scope.currentPage, $scope.selectedCnt);
                    if ($scope.serviceData.length % itemsPerPage === 0)
                        $scope.totalItems = Math.floor(($scope.serviceData.length / itemsPerPage)) * 10;
                    else
                        $scope.totalItems = (Math.floor(($scope.serviceData.length / itemsPerPage) + 1)) * 10;
                    console.log($scope.totalItems);
                    setPagingData($scope.serviceData, 1, itemsPerPage);
                }
            } else if ($state.current.name === 'VoBoard.ManageVoManagerVerification') {
                $scope.$broadcast('ManageVoManagerVerification', true);

            } else if ($state.current.name === 'CoBoard.CollectionContracts') {
                $scope.$broadcast('CollectionContracts', true);
            } else if ($state.current.name === 'CoBoard.ManageCollectorCollectionRequest') {
                $scope.$broadcast('ManageCollectorCollectionRequest', true);
            } else {

                if (!appInUse) {
                    activateService();
                }

            }



        };



        $scope.setPage = function(pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function(e) {
            e.stopImmediatePropagation();
            if (filterData !== null) {
                setPagingData(filterData, $scope.currentPage, $scope.selectedCnt);
            } else {
                setPagingData($scope.serviceData, $scope.currentPage, $scope.selectedCnt);
            }
        };




        $scope.ShowEditPage = function(row) {
            var currentRecord;
            var isEditItem;
            switch ($state.current.name) {
                case 'AdminBoard.ManageUser':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.userId === row.userId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageCountry':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.countryId === row.countryId && record.stateName === row.stateName;
                        });
                        break;
                    }
                case 'AdminBoard.ManageDivision':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.divisionId === row.divisionId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageDivision':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.countryId === row.countryId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageBank':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.bankId === row.bankId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageLocation':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.locationId === row.locationId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageRegion':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.regionId === row.regionId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageUserType':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.userTypeId === row.userTypeId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageItem':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return (record.itemId === row.itemId) &&
                                (record.priceListInd === row.priceListInd) &&
                                (record.countryName === row.countryName);
                        });
                        isEditItem = true;
                        break;
                    }
                case 'AdminBoard.ManageRole':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.roleId === row.roleId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageScreenAccess':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.screenCode === row.screenCode;
                        });
                        break;
                    }
                case 'AdminBoard.ManagePriceList':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.id === row.id;
                        });
                        break;
                    }
                case 'AdminBoard.ManageSequenceGenerator':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.sequenceId === row.sequenceId;
                        });
                        break;
                    }
                case 'AdminBoard.ManageCommissionRate':
                    {
                        currentRecord = $scope.serviceData.filter(function(record) {
                            return record.commissionRateCode === row.commissionRateCode;
                        });
                        break;
                    }
            }
            if (!isEditItem) {
                console.log(currentRecord);
                localStorageService.set('currentRecord', currentRecord[0]);
                var array = $state.current.name.split(".");
                var url = array[0] + '.CreateEdit' + array[1].slice(6);
                $state.go(url);
            } else {
                localStorageService.set('currentRecord', currentRecord[0]);
                $state.go('AdminBoard.EditItem');
            }
        }

        $scope.loadMore = grid.loadMore($scope);

        $scope.rowFormatter = function(row) {
            row.entity.merge = false;
            var uniqID = row.entity[Object.keys(row.entity)[0]];

            var newRecord = $scope.colData.filter(function(item) {
                //This function is called from gridPopulation.js which is not getting called anymore
                return item[[Object.keys(item)[0]]] !== uniqID;
            });
        };

        $scope.cumulative = function(grid, row) {
            row.entity.merge = true;
        };
        $scope.$on('filterObj', function(event, arg) {

            if ($state.current.name !== 'AdminBoard.ManageContract' && $state.current.name !== 'AdminBoard.ManageUser' && $state.current.name !== 'AdminBoard.ManagePriceList' && $state.current.name !== 'VoBoard.ManageVoManagerVerification' && $state.current.name !== 'CoBoard.CollectionContracts' && $state.current.name !== 'VoBoard.VoVerificationRequest' && $state.current.name !== 'CoBoard.ManageCollectorCollectionRequest') {

                if (arg !== null) {
                    filterData = filterService.sideBarFilter($scope, arg);
                    //$scope.totalItems = filterData.length;
                    $scope.totalItems = (filterData !== null) ? filterData.length : 0;
                    $scope.selectedCnt = 10;
                    $scope.totalItemCount = $scope.totalItems;
                    if ($state.current.name === 'VoBoard.VoTurnIn' || $state.current.name === 'AdminBoard.AdminTurnIn' || $state.current.name === 'CoBoard.CollectionTurnIn' || $state.current.name === 'SalesRep.ManageTurnIn')
                        setPagingData(filterData, 1, filterData.length);
                    else
                        setPagingData(filterData, 1, 10);
                } else {

                    filterData = $scope.serviceData;
                    //gridPopulation();
                    $scope.configGrid();
                }
                localStorageService.set('manageData', filterData);
            } else if ($state.current.name === 'VoBoard.ManageVoManagerVerification') {
                if (arg === null) {
                    localStorageService.set('filterObj', null);
                } else {
                    localStorageService.set('isFiltered', true);
                }
                $scope.$broadcast('ManageVoManagerVerification', true);
            } else if ($state.current.name === 'CoBoard.CollectionContracts') {
                if (arg === null) {
                    localStorageService.set('filterObj', null);
                } else {
                    localStorageService.set('isFiltered', true);
                }
                $scope.$broadcast('CollectionContracts', true);
            } else if ($state.current.name === 'CoBoard.ManageCollectorCollectionRequest') {
                if (arg === null) {
                    localStorageService.set('filterObj', null);
                } else {
                    localStorageService.set('isFiltered', true);
                }
                $scope.$broadcast('ManageCollectorCollectionRequest', true);
            } else {
                if (arg === null) {
                    localStorageService.set('filterObj', null);
                } else {
                    localStorageService.set('isFiltered', true);
                }
                activateService();
            }



        });


        $scope.activeColumn = function(index) {
            gridActiveColumn(index);
        }
        $scope.deActiveColumn = function(predicate, index) {
            if ($scope.predicate === predicate && $scope.reverse === false) gridActiveColumn(index);
            else gridInactiveColumn(index);
        }

        var prvSelectIndex;
        $scope.reverse = false;
        $scope.order = function(predicate, index) {
            $scope.predicate = predicate;
            if (prvSelectIndex !== undefined) {
                if (notInStateList($state.current.name) && prvSelectIndex !== index) gridInactiveColumn(prvSelectIndex);
            }
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;

            if ($scope.reverse === false) {
                if (notInStateList($state.current.name)) gridActiveColumn(index);
            } else {
                if (notInStateList($state.current.name)) gridInactiveColumn(index);
            }
            prvSelectIndex = index;
        };

        function gridActiveColumn(index) {
            $timeout(function() {
                $("table tr:nth-of-type(odd) > td:nth-child(" + index + ")").addClass('active-odd');
                $("table tr:nth-of-type(even) > td:nth-child(" + index + ")").addClass('active-even');
                $("table tr > th:nth-child(" + index + ")").addClass('active');
            }, 0);
        }

        function gridInactiveColumn(index) {
            $timeout(function() {
                $("table tr:nth-of-type(odd) > td:nth-child(" + index + ")").removeClass('active-odd').removeClass('active-even');
                $("table tr:nth-of-type(even) > td:nth-child(" + index + ")").removeClass('active-even').removeClass('active-odd');
                $("table tr > th:nth-child(" + index + ")").removeClass('active');
            }, 0);
        }

        //subtable
        var prvSelectIndexSubTable;
        $scope.subTableReverse = false;
        $scope.orderSubTable = function(predicate, index) {
            $scope.subTablePredicate = predicate;
            prvSelectIndexSubTable = index;
            $scope.subTableReverse = ($scope.subTablePredicate === predicate) ? !$scope.subTableReverse : false;
        };

        function notInStateList(name) {
            if (name !== 'VoBoard.VoTurnIn' && name !== 'AdminBoard.AdminTurnIn' &&
                name !== 'CoBoard.CollectionTurnIn' && name !== 'SalesRep.ManageTurnIn')
                return true;

            return false;
        }

        $scope.concatanateString = function(str) {
            if (str !== null)
                return str.replace(/\s+/g, '-').toLowerCase();
        }


        /******Column Configuration******/
        $scope.columnConfig = function(e) {
            e.preventDefault();
            var animation = true;
            var templateUrl = 'app/modals/utility-menu/column-config.html';
            var controller = 'columnConfigCtrl';
            $scope.openColumnConfig(animation, templateUrl, controller);
        }
        $scope.sendEmail = function(e) {
            e.preventDefault();
            var animation = true;
            var templateUrl = 'app/modals/utility-menu/email-config.html';
            var controller = 'emailConfigCtrl';
            $scope.openColumnConfig(animation, templateUrl, controller);
        }
        $scope.printPage = function(e) {
            e.preventDefault();
            window.print();
            return false;
        }

        $scope.openColumnConfig = function(animation, templateUrl, controller) {
            var modalInstance = $uibModal.open({
                animation: animation,
                templateUrl: templateUrl,
                controller: controller,
                windowClass: 'center-modal',
                backdrop: 'static',
                size: 'lg'
            }).result.then(function(data) {
                if (data === "done") {
                    $scope.gridOptions.columnDefs = localStorageService.get('selectedColumns');
                    refreshGrid();
                }
            });
        };
        /*END*/

        //		document.addEventListener("online",onOnline,false);
        //        document.addEventListener("offline",onOffline,false);

        document.addEventListener('backbutton', onBackKeyDown, false);


        function onBackKeyDown(event) {
            // Handle the back button



            if ($state.current.name === 'AdminBoard.ManageContract' || $state.current.name === 'VoBoard.VoVerificationRequest' || $state.current.name === 'CoBoard.ManageCollectorCollectionRequest') {
                authentication.logout().then(success, error);

                function success(result) {
                    if (result.errorCode === "200") {
                        Session.destroy();
                        $state.go('Login');

                    }
                }

                function error() {
                    Session.destroy();
                    $state.go('Login');
                }


            } else if ($state.current.name === 'AdminBoard.CreateEditContract' || $state.current.name === 'AdminBoard.ViewContract') {

                //                $state.go('AdminBoard.ManageContract');
                if (Session.userRoleName.toLowerCase() === "collection officer") {
                    $state.go('CoBoard.ManageCollectorCollectionRequest');

                } else {
                    //            				$scope.previousStateTitle = "Manage Contract"
                    //            				$scope.previousState = "AdminBoard.ManageContract";
                    $state.go('AdminBoard.ManageContract');

                }

            } else if ($state.current.name === 'VoBoard.VoVerifyContract') {

                $state.go('VoBoard.VoVerificationRequest');



            } else if ($state.current.name === 'VoBoard.VoCollectPayment') {
                if (offline) {
                    $state.go('VoBoard.VoVerificationRequest');
                } else {
                    $state.go('VoBoard.VoCompleteRequest');
                }


            } else if ($state.current.name === 'CoBoard.CoCollectPayment' || $state.current.name === 'AdminBoard.EditCustomer') {

                if (Session.userRoleName.toLowerCase() === "collection officer") {
                    $state.go('CoBoard.ManageCollectorCollectionRequest');
                } else {
                    $state.go('AdminBoard.ManageCustomer');
                }
                // $state.go('CoBoard.ManageCollectorCollectionRequest');

            } else if ($state.current.name === 'AdminBoard.CancelContract') {
                if (Session.userRoleName.toLowerCase() === "collection officer") {
                    $state.go('CoBoard.ManageCollectorCollectionRequest');
                } else {
                    //            				$scope.previousStateTitle = "Manage Contract"
                    //            				$scope.previousState = "AdminBoard.ManageContract";
                    $state.go('AdminBoard.ManageContract');
                }


            } else if ($state.current.name === 'Login') {
                navigator.app.exitApp();

            }



        }
        $(window).scroll(function() {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                if ($state.current.name !== 'CoBoard.ManageCollectorCollectionRequest') {

                    var limit = 5;
                    $scope.selectedCnt = $scope.selectedCnt + limit;
                    if ($scope.gridOptions.data !== undefined && $scope.gridOptions.data !== null) {
                        if ($scope.gridOptions.data.length < $scope.totalItemCount) {
                            $scope.serviceType = "scroll";
                            activateService();
                        }
                    }

                } else {
                    $rootScope.$broadcast('scrollCollectionRequest', {});
                }
            }

        });



        var type = $cordovaNetwork.getNetwork();

        var isOnline = $cordovaNetwork.isOnline();

        var isOffline = $cordovaNetwork.isOffline();




        // listen for Online event
        $scope.$on('$cordovaNetwork:online', function(event, networkState) {

            //alert("Online");

            if ($state.current.name === 'CoBoard.ManageCollectorCollectionRequest' || $state.current.name === 'CoBoard.CollectionTurnIn') {


                internetConnected = localStorageService.get('internetConnected');
                if (internetConnected === "true") {
                    internetConnected = true;
                } else {
                    internetConnected = false;
                }
            }



            if (internetConnected) {
                return;
            }
            internetConnected = true;

            //$scope.loading = true;

            var tokenCreatedTime = commonServices.getTokenCreatedTime();
            var currentDate = new Date();

            if (currentDate - tokenCreatedTime > 1, 800, 000) {

                alert("Your Sesson has expired!");
                $scope.loading = true;
                app.readLogFile(successRead, errorRead);

            } else {

                checkForLocation();
                // $scope.loading = true;

            }
            // app.readLogFile(successRead,errorRead);

            var onlineState = networkState;
        });


        function checkForLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, {
                    timeout: 30000
                });
            } else {
                alert("Geolocation is not supported");
            }


            function geolocationSuccess(position) {
                //				alert('Latitude: ' + position.coords.latitude + '\n' +
                //					'Longitude: ' + position.coords.longitude + '\n' +
                //					'Altitude: ' + position.coords.altitude + '\n' +
                //					'Accuracy: ' + position.coords.accuracy + '\n' +
                //					'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
                //					'Heading: ' + position.coords.heading + '\n' +
                //					'Speed: ' + position.coords.speed + '\n' +
                //					'Timestamp: ' + position.timestamp + '\n');


                localStorage.setItem('lat', position.coords.latitude);
                localStorage.setItem('lon', position.coords.longitude);

                if (Session.userType.toLowerCase().indexOf("sales") > -1) {
                    app.syncServiceData(successContract, errorContract);
                } else if (Session.userType.toLowerCase().indexOf("verification") > -1) {
                    app.syncVerificationList(successVerification, errorVerification);
                } else if (Session.userType.toLowerCase().indexOf("collection") > -1) {
                    app.syncCollectionList(successCollection, errorCollection);
                }

            }


            function geolocationError(error) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        alert("User denied the request for Geolocation.");
                        break;
                    case error.POSITION_UNAVAILABLE:
                        alert("Location information is unavailable.");
                        break;
                    case error.TIMEOUT:
                        alert("The request to get user location timed out.");
                        break;
                    case error.UNKNOWN_ERROR:
                        alert("An unknown error occurred.");
                        break;
                }
            }

        }

        function successContract(result) {

            var data = {
                userId: 1,
                contractList: result
            }
            if (result.length !== 0) {
                syncService.createContract(1, data);
            } else {
                $state.reload();
            }


        }

        function errorContract(result) {
            console.log("Error");
            $state.reload();
        }


        function successVerification(result) {

            var count = 0;

            for (var i = 0; i < result.length; i++) {
                syncService.verifyContract(result[i], count, result.length);
                count = count + 1;
            }

            if (result.length == 0) {

                $state.reload();
            }


        }

        function errorVerification(result) {
            console.log("Error");
            //if(result.length==0){
            $state.reload();

        }

        function successNote(result) {



            if (result.length == 0) {

                app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
                // $state.reload();
            } else {
                for (var i = 0; i < result.length; i++) {
                    syncService.noteSync(result[i], i, result.length);
                }
            }


        }

        function errorNote(result) {
            console.log("Error");
            app.syncCollectorTurnIn(successCollectorTurnIn, errorCollectorTurnIn);
        }

        function successCollectorTurnIn(result) {
            if (result.length > 0) {
                //    		navigator.geolocation.getCurrentPosition(geolocationSuccess,geolocationError,[geolocationOptions]);
                //						$state.reload();
                var data = {
                    deviceId: localStorage.getItem('deviceId'),
                    lat: "" + localStorage.getItem('lat'),
                    lon: "" + localStorage.getItem('lon'),
                    offlineBankInRecords: result
                }
                syncService.syncCollectorBankIn(data);
            } else {
                $state.reload();
            }

        }

        function errorCollectorTurnIn(error) {
            $state.reload();
        }


        function successCollection(result) {


            if (result.length === 0) {
                app.syncNoteList(successNote, errorNote);
            } else {
                for (var i = 0; i < result.length; i++) {
                    syncService.collectionContract(result[i], i, result.length);
                }
            }




        }

        function errorCollection(result) {
            console.log("Error");
            app.syncNoteList(successNote, errorNote);
        }


        // listen for Offline event
        $scope.$on('$cordovaNetwork:offline', function(event, networkState) {

            //  alert("Offline");

            var offlineState = networkState;

            if ($state.current.name === 'CoBoard.ManageCollectorCollectionRequest' || $state.current.name === 'CoBoard.CollectionTurnIn') {
                localStorageService.set('internetConnected', false);
                $state.reload();
            }


            //      if(!internetConnected){
            //      //Session.destroy();
            //            return;
            //      }

            internetConnected = false;
            // $state.go('Login');
            //$scope.$apply();




        });

        function successRead(result) {

            var userName = result[0].userName;
            var passWord = result[0].password;
            $scope.EncryptAuthInfo = $base64.encode(userName + ":" + passWord);
            config = {
                headers: {
                    'Authorization': $scope.EncryptAuthInfo
                }
            };

            var req = {
                userName: $scope.UserName,
                password: $scope.Password
            };

            authentication.loginService(req, config).then(success, error);

            function success(result) {
                $scope.loading = false;


                if ($state.current.name === 'AdminBoard.ManageContract' || $state.current.name === 'VoBoard.VoVerificationRequest' || $state.current.name === 'CoBoard.ManageCollectorCollectionRequest') {
                    $state.reload();
                }

                $scope.$apply();
            }

            function error(result) {
                $scope.loading = false;
                //alert(result);
            }


        }

        function errorRead(result) {
            $scope.loading = false;
            //alert(result);
        }

    }
]);