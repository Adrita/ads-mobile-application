var fileSync = {
    syncDevice: function(userName, password, successCallback, errorCallback) {
        var userNamePassword = [{
            "userName": userName,
            "password": password
        }];

        // alert(userNamePassword);



        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("log.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                console.log("fileEntry is file?" + fileEntry.isFile.toString());
                writeFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    console.log("Successful file write... log txt");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    errorCallback("Failed file write: " + e.toString());
                };

                // If data object is not passed in,
                // create a new Blob instead.
                if (!dataObj) {
                    dataObj = new Blob([JSON.stringify(userNamePassword)], {
                        type: 'text/plain'
                    });
                }

                fileWriter.write(dataObj);
            });
        }


    },


    createDataBase: function(successCallback, errorCallback) {
        /**cordova.exec(successCallback,
         errorCallback,
         'FileStoragePlugin',
         'createDatabase',
         null
         )**/

        var db = sqlitePlugin.openDatabase(constants.dbName, constants.dbVersion, '', 1);

    },

    createTable: function(successCallback, errorCallback) {
        var db = window.sqlitePlugin.openDatabase({
            name: "Grolier.db"
        });
        db.transaction(function(transaction) {

            //   transaction.executeSql("DROP TABLE IF EXISTS ads_country_master_tb");

            transaction.executeSql(constants.cREATE_COUNTRY_MASTER_TB, [],
                function(tx, result) {},
                function(error) {});
        });

        db.transaction(function(transaction) {

            //  transaction.executeSql("DROP TABLE IF EXISTS ads_state_master_tb");


            transaction.executeSql(constants.cREATE_STATE_MASTER_TB, [],
                function(tx, result) {
                    ////console.log("State table created");
                },
                function(error) {});
        });

        db.transaction(function(transaction) {


            //   transaction.executeSql("DROP TABLE IF EXISTS ads_location_master_tb");
            transaction.executeSql(constants.cREATE_LOCATION_MASTER_TB, [],
                function(tx, result) {},
                function(error) {});
        });

        db.transaction(function(transaction) {

            //   transaction.executeSql("DROP TABLE IF EXISTS ads_duty_free_pincode_tb");
            transaction.executeSql(constants.cREATE_DUTY_FREE_PINCODE_TB, [],
                function(tx, result) {},
                function(error) {});
        });

        db.transaction(function(transaction) {

            //transaction.executeSql("DROP TABLE IF EXISTS ads_item_master_tb");
            //transaction.executeSql("DROP TABLE IF EXISTS ads_item_details_master_tb");
            transaction.executeSql(constants.cREATE_ITEM_MASTER_TB, [],
                function(tx, result) {},
                function(error) {});
        });

        //   db.transaction(function(transaction) {

        // //  transaction.executeSql("DROP TABLE IF EXISTS ads_item_details_master_tb");
        //   transaction.executeSql(constants.cREATE_ITEM_DETAILS_MASTER_TB, [],
        //   function(tx, result) {},
        //   function(error) {});
        //   });

        db.transaction(function(transaction) {

            // transaction.executeSql("DROP TABLE IF EXISTS ads_price_list_master_tb");
            transaction.executeSql(constants.cREATE_PRICE_LIST_MASTER_TB, [],
                function(tx, result) {},
                function(error) {});
        });

        db.transaction(function(transaction) {
            //   transaction.executeSql("DROP TABLE IF EXISTS ads_bank_master_tb");
            transaction.executeSql(constants.cREATE_BANK_MASTER, [],
                function(tx, result) {},
                function(error) {});
        });

        db.transaction(function(transaction) {

            //   transaction.executeSql("DROP TABLE IF EXISTS last_updated_time_tb");
            transaction.executeSql(constants.cREATE_LAST_UPDATED_TIME_TB, [],
                function(tx, result) {
                    successCallback("Success");
                },
                function(error) {});
        });

        db.transaction(function(transaction) {

            //   transaction.executeSql("DROP TABLE IF EXISTS last_updated_time_tb");
            transaction.executeSql(constants.cREATE_PAYMENT_METHOD_TB, [],
                function(tx, result) {
                    successCallback("Success");
                },
                function(error) {});
        });
    },


    syncLocalData: function(data, successCallback, errorCallback) {

        // alert(constants.countryUrl);

        cordova.plugin.http.post(constants.countryUrl, {
            userId: data
        }, {}, function(response) {
            // prints 200
            ////console.log(response.status);
            try {
                response.data = JSON.parse(response.data);
                // prints test
                ////console.log(response.data.message);
            } catch (e) {
                console.error("JSON parsing error");
            }
        }, function(response) {});




    },




    readFromFile: function(successCallback, errorCallback, userName, password) {
        var requestData = [{
            "userName": userName,
            "password": password
        }];
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("log.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);
                    result = result[0];

                    var passwordFile = result.password;
                    if (password.toLowerCase() === passwordFile.toLowerCase()) {
                        ////console.log("Password matched");

                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFileSystem, fail);

                        function gotFileSystem(fileSystem) {
                            fileSystem.root.getDirectory("grolier", {
                                create: false
                            }, gotDirectory);
                        }

                        function gotDirectory(dirEntry) {

                            ////console.log('file system open: ' + dirEntry.name);
                            dirEntry.getFile("userRoleJson.txt", {
                                create: false,
                                exclusive: false
                            }, function(fileEntry) {

                                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                                readUserRoleFile(fileEntry, null);

                            }, onErrorCreateFile);

                        }



                        function readUserRoleFile(fileEntry) {

                            fileEntry.file(function(file) {
                                var reader = new FileReader();

                                reader.onloadend = function() {
                                    ////console.log("Successful file read: " + this.result);
                                    // displayFileData(fileEntry.fullPath + ": " + this.result);

                                    var result = JSON.parse(this.result);
                                    successCallback(result[0]);
                                };

                                reader.readAsText(file);

                            }, onErrorReadFile);


                        }



                    } else {
                        errorCallback("Only last logged in user can log in");
                    }
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }


        /*cordova.exec(successCallback,
         errorCallback,
         'FileStoragePlugin',
         'readFromFile',
         requestData
         )*/
    },
    storeUserRole: function(successCallback, errorCallback, data) {


        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: true
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("userRoleJson.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                writeFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    console.log("Successful file write... userRoleJson");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };

                // If data object is not passed in,
                // create a new Blob instead.
                if (!dataObj) {
                    dataObj = new Blob([JSON.stringify(requestData)], {
                        type: 'text/plain'
                    });
                }

                fileWriter.write(dataObj);
            });
        }


        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'storeUserRole', requestData);

    },

    fetchState: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {
            tx.executeSql('SELECT state_name AS stateName  FROM ads_state_master_tb WHERE country_id = ' + data.countryId, [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);
                var stateNameArray = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    stateNameArray.push(rs.rows.item(i).stateName);
                }

                stateNameArray.sort();

                successCallback(stateNameArray);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });


        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchState', requestData);

    },

    fetchLastUpdatedRow: function(successCallback, errorCallback) {
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });
        db.transaction(function(tx) {
            tx.executeSql('SELECT table_name AS tableName, last_update_time AS lastUpdatedTime  FROM last_updated_time_tb', [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);
                var lastUpdatedTimeArray = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    var singleObj = {
                        "tableName": rs.rows.item(i).tableName,
                        "lastUpdatedTime": rs.rows.item(i).lastUpdatedTime
                    };

                    lastUpdatedTimeArray.push(singleObj);
                }

                // stateNameArray.sort();

                successCallback(lastUpdatedTimeArray);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });

    },

    fetchProduct: function(successCallback, errorCallback, data) {
        //        var requestData = [];
        //        requestData.push(data);

        //alert(data.countryId);

        //alert("In fetch Product List Method");



        var successRegion = function(region) {
            var db = window.sqlitePlugin.openDatabase({
                name: constants.dbName
            });

            var dbField = "";

            //alert("Region name is>>>>>"+region);

            db.transaction(function(tx) {
                tx.executeSql('SELECT item_id AS itemId,points AS points,product_code AS productCode,item_desc AS itemDesc,available_for_em AS availableEm, available_for_wm AS availableWm  FROM ads_item_master_tb ORDER BY item_desc', [], function(tx, rs) {
                    console.log('Record length: ' + rs.rows.length);
                    var productList = [];

                    for (var i = 0; i < rs.rows.length; i++) {

                        var singleObj = {
                            itemId: rs.rows.item(i).itemId,
                            productId: rs.rows.item(i).productCode,
                            productPoint: rs.rows.item(i).points,
                            productPoints: rs.rows.item(i).points,
                            productName: rs.rows.item(i).itemDesc,
                            quantity: 1,
                            newlyAdded: false
                        };

                        if (region === 'East' && rs.rows.item(i).availableEm)
                            productList.push(singleObj);
                        else if (region === 'West' && rs.rows.item(i).availableWm)
                            productList.push(singleObj);


                    }

                    successCallback(productList);


                }, function(tx, error) {
                    ////console.log('SELECT error: ' + error.message);
                    errorCallback(error.message);
                });



            });

        }

        var errorRegion = function(message) {
            alert(message);
        }

        fileSync.fetchRegion(successRegion, errorRegion, data);

        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchProduct', requestData);
    },

    fetchPointsTotal: function(successCallback, errorCallback, data) {
        var productDetails = data.productDetails;

        var productId = [];

        for (var i = 0; i < productDetails.length; i++) {
            productId.push(productDetails[i].productId);

        }


        var totalPoint = 0;
        var totalNonTaxPoint = 0;
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {
            tx.executeSql('SELECT tax_percentg AS taxPercentage, points AS points, product_code AS productCode FROM ads_item_master_tb WHERE product_code in (' + productId + ')', [], function(tx, rs) {
                // console.log('Record length item: ' + rs.rows.length);

                for (var i = 0; i < rs.rows.length; i++) {

                    if (rs.rows.item(i).taxPercentage !== 0) {

                        for (var j = 0; j < productDetails.length; j++) {
                            if (productDetails[j].productId === rs.rows.item(i).productCode) {
                                totalPoint = parseFloat(totalPoint) + (rs.rows.item(i).points * productDetails[j].quantity);
                                totalNonTaxPoint = parseFloat(totalNonTaxPoint) + (rs.rows.item(i).points * productDetails[j].quantity);

                                break;
                            }
                        }


                    } else {
                        for (var j = 0; j < productDetails.length; j++) {
                            if (productDetails[j].productId === rs.rows.item(i).productCode) {
                                totalPoint = parseFloat(totalPoint) + (rs.rows.item(i).points * productDetails[j].quantity);
                                // totalNonTaxPoint=parseFloat(totalNonTaxPoint)+(rs.rows.item(i).points*productDetails[j].quantity);

                                break;
                            }
                        }
                    }


                }

                var returnObj = {
                    itemPoint: totalPoint,
                    totalItemPoint: totalNonTaxPoint
                }

                successCallback(returnObj);


            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });



        });



    },

    checkPincode: function(successCallback, errorCallback, data) {
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {
            tx.executeSql('SELECT duty_free_zone AS dutyFreeZone  FROM ads_duty_free_pincode_tb WHERE pincode = ' + data.homePinCode, [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);

                var pin = false;

                if (rs.rows.length > 0) {
                    pin = true;
                }

                successCallback(pin);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });

    },

    fetchRegion: function(successCallback, errorCallback, data) {

        //alert("But first we will fetch region of the state");
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });
        //alert(data.stateName);

        db.transaction(function(tx) {
            tx.executeSql("SELECT pricing_area AS regionName FROM ads_state_master_tb WHERE state_name = '" +
                data.stateName + "'", [],
                function(tx, rs) {
                    // console.log('region length: ' + rs.rows.length);

                    var region = rs.rows.item(0).regionName;

                    successCallback(region);
                },
                function(tx, error) {
                    console.log('SELECT error: ' + error.message);
                    errorCallback(error.message);
                });




        });
    },

    fetchListPriceList: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);

        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {


            if (!data.excempted) {

                if (data.itemPoint == 0.0) {

                    tx.executeSql("SELECT price_list_code AS priceListCode FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from = " + null + " AND gst_points_to = " + null + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint, [], function(tx, rs) {
                        ////console.log('Record length: ' + rs.rows.length);
                        var priceList = [];

                        for (var i = 0; i < rs.rows.length; i++) {

                            if (priceList.indexOf(rs.rows.item(i).priceListCode) < 0) {
                                priceList.push(rs.rows.item(i).priceListCode);
                            }

                        }

                        successCallback(priceList);
                    }, function(tx, error) {
                        console.log('SELECT error: ' + error.message);
                        errorCallback(error.message);
                    });

                } else {
                    tx.executeSql("SELECT price_list_code AS priceListCode FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from <= " + data.totalItemPoint + " AND gst_points_to >= " + data.totalItemPoint + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint, [], function(tx, rs) {
                        ////console.log('Record length: ' + rs.rows.length);

                        var priceList = [];

                        for (var i = 0; i < rs.rows.length; i++) {

                            if (priceList.indexOf(rs.rows.item(i).priceListCode) < 0) {
                                priceList.push(rs.rows.item(i).priceListCode);
                            }

                        }

                        successCallback(priceList);
                    }, function(tx, error) {
                        ////console.log('SELECT error: ' + error.message);
                        errorCallback(error.message);
                    });



                }


            } else {

                tx.executeSql("SELECT price_list_code AS priceListCode FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from <= " + 0.0 + " AND gst_points_to >= " + 0.75 + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint, [], function(tx, rs) {
                    ////console.log('Record length: ' + rs.rows.length);

                    var priceList = [];

                    for (var i = 0; i < rs.rows.length; i++) {

                        if (priceList.indexOf(rs.rows.item(i).priceListCode) < 0) {
                            priceList.push(rs.rows.item(i).priceListCode);
                        }

                    }

                    successCallback(priceList);
                }, function(tx, error) {
                    ////console.log('SELECT error: ' + error.message);
                    errorCallback(error.message);
                });

            }

        });




        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchListPriceList', requestData);
    },

    fetchTerms: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);

        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {


            if (!data.excempted) {

                if (data.itemPoint == 0.0) {

                    tx.executeSql("SELECT installment_no AS terms FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from = " + null + " AND gst_points_to = " + null + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint + " AND price_list_code = '" + data.priceListCode + "'", [], function(tx, rs) {
                        ////console.log('Record length: ' + rs.rows.length);
                        var termList = [];

                        for (var i = 0; i < rs.rows.length; i++) {

                            if (termList.indexOf(rs.rows.item(i).terms) < 0) {
                                termList.push(rs.rows.item(i).terms);
                            }

                        }

                        successCallback(termList);
                    }, function(tx, error) {
                        console.log('SELECT error: ' + error.message);
                        errorCallback(error.message);
                    });

                } else {
                    tx.executeSql("SELECT installment_no AS terms FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from <= " + data.totalItemPoint + " AND gst_points_to >= " + data.totalItemPoint + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint + " AND price_list_code = '" + data.priceListCode + "'", [], function(tx, rs) {
                        ////console.log('Record length: ' + rs.rows.length);

                        var termList = [];

                        for (var i = 0; i < rs.rows.length; i++) {

                            if (termList.indexOf(rs.rows.item(i).terms) < 0) {
                                termList.push(rs.rows.item(i).terms);
                            }

                        }

                        successCallback(termList);
                    }, function(tx, error) {
                        ////console.log('SELECT error: ' + error.message);
                        errorCallback(error.message);
                    });



                }


            } else {

                tx.executeSql("SELECT installment_no AS terms FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from <= " + 0.0 + " AND gst_points_to >= " + 0.75 + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint + " AND price_list_code = '" + data.priceListCode + "'", [], function(tx, rs) {
                    ////console.log('Record length: ' + rs.rows.length);

                    var termList = [];

                    for (var i = 0; i < rs.rows.length; i++) {

                        if (termList.indexOf(rs.rows.item(i).terms) < 0) {
                            termList.push(rs.rows.item(i).terms);
                        }

                    }

                    successCallback(termList);
                }, function(tx, error) {
                    ////console.log('SELECT error: ' + error.message);
                    errorCallback(error.message);
                });

            }

        });

        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchTerms', requestData);
    },

    fetchPoints: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);



        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {


            if (!data.excempted) {

                if (data.itemPoint == 0.0) {

                    tx.executeSql("SELECT points AS points FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from = " + null + " AND gst_points_to = " + null + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint + " AND price_list_code = '" + data.priceListCode + "' AND installment_no = '" + data.terms + "'", [], function(tx, rs) {
                        ////console.log('Record length: ' + rs.rows.length);
                        var termList = [];

                        for (var i = 0; i < rs.rows.length; i++) {

                            if (termList.indexOf(rs.rows.item(i).points) < 0) {
                                termList.push(rs.rows.item(i).points);
                            }

                        }

                        successCallback(termList);
                    }, function(tx, error) {
                        console.log('SELECT error: ' + error.message);
                        errorCallback(error.message);
                    });

                } else {
                    tx.executeSql("SELECT points AS points FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from <= " + data.totalItemPoint + " AND gst_points_to >= " + data.totalItemPoint + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint + " AND price_list_code = '" + data.priceListCode + "' AND installment_no = '" + data.terms + "'", [], function(tx, rs) {
                        ////console.log('Record length: ' + rs.rows.length);

                        var termList = [];

                        for (var i = 0; i < rs.rows.length; i++) {

                            if (termList.indexOf(rs.rows.item(i).points) < 0) {
                                termList.push(rs.rows.item(i).points);
                            }

                        }

                        successCallback(termList);
                    }, function(tx, error) {
                        ////console.log('SELECT error: ' + error.message);
                        errorCallback(error.message);
                    });



                }


            } else {

                tx.executeSql("SELECT points AS points FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND gst_points_from <= " + 0.0 + " AND gst_points_to >= " + 0.75 + " AND region_name = '" + data.regionName + "' AND points <= " + data.itemPoint + " AND price_list_code = '" + data.priceListCode + "' AND installment_no = '" + data.terms + "'", [], function(tx, rs) {
                    ////console.log('Record length: ' + rs.rows.length);

                    var termList = [];

                    for (var i = 0; i < rs.rows.length; i++) {

                        if (termList.indexOf(rs.rows.item(i).points) < 0) {
                            termList.push(rs.rows.item(i).points);
                        }

                    }

                    successCallback(termList);
                }, function(tx, error) {
                    ////console.log('SELECT error: ' + error.message);
                    errorCallback(error.message);
                });

            }

        });
        //cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchPoints', requestData);
    },

    fetchPricingDetails: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);

        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });
        db.transaction(function(tx) {
            tx.executeSql("SELECT id,first_down_pymnt,sec_down_paymnt,installment_amt,total_sale_amt FROM ads_price_list_master_tb WHERE country_id = " + data.countryId + " AND price_list_code = '" + data.priceListCode + "' AND installment_no = '" + data.terms + "' AND points =" + data.points, [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);
                var result = {
                    downPayment1: rs.rows.item(0).first_down_pymnt,
                    downPayment2: rs.rows.item(0).sec_down_paymnt,
                    totalAmountOrder: rs.rows.item(0).total_sale_amt,
                    installmentAmt: rs.rows.item(0).installment_amt,
                    totalOutstandingBalance: rs.rows.item(0).total_sale_amt

                };

                if (data.terms !== 'COD' && data.terms !== 'CAD') {
                    result.totalInstallmentAmount = rs.rows.item(0).installment_amt * parseInt(data.terms);

                } else {
                    result.totalInstallmentAmount = rs.rows.item(0).installment_amt;
                }

                successCallback(result);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });



        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchPricing', requestData);
    },

    saveContractData: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("contract.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());

                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    errorCallback("Failed file write: " + e.toString());
                };


                if (isAppend) {
                    try {

                        fileEntry.file(function(file) {
                            var reader = new FileReader();

                            reader.onloadend = function() {
                                console.log("Successful file read: " + this.result);


                                if (this.result !== null && this.result !== '' && this.result !== 'null') {
                                    var saveContractData = JSON.parse(this.result);



                                    var newContract = data;
                                    if (!dataObj) {

                                        if (saveContractData && saveContractData.length > 0) {
                                            newContract.mobileRowId = saveContractData[saveContractData.length - 1].mobileRowId + 1;
                                        } else {
                                            newContract.mobileRowId = 0;
                                        }



                                        saveContractData.push(newContract);

                                        console.log(saveContractData);

                                        // console.log("stringify"+ JSON.stringify(saveContractData));


                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }

                                } else {
                                    if (!dataObj) {

                                        requestData[0].mobileRowId = 0;

                                        // saveContractData.push(newContract);
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }

                                }


                                fileWriter.write(dataObj);




                            };

                            reader.readAsText(file);

                        }, onErrorReadFile);

                    } catch (e) {
                        errorCallback("file doesn't exist!");
                    }
                }

                // If data object is not passed in,
                // create a new Blob instead.



            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }


        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'saveContractData', requestData);
    },

    fetchDraftContract: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("contract.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    var contractList = [];

                    for (var i = 0; i < result.length; i++) {

                        var orderDate = result[i].orderDate;
                        orderDate = orderDate.substring(0, orderDate.indexOf('T'));

                        var dt = new Date(orderDate);

                        var year = dt.getFullYear();
                        var month = dt.getMonth();
                        month = month + 1;

                        dt = dt.getDate() + "/" + month + "/" + year;




                        //resultContract.contractCode=year+"0000"+data.mobileRowId+"MY";

                        var singleObj = {
                            contractCode: year + "000" + result[i].mobileRowId + "MY",
                            customerName: result[i].customerDetails.name,
                            status: "Draft",
                            points: result[i].points,
                            mobileRowId: i,
                            orderDate: dt,
                            lastPaymentDate: dt,
                            terms: result[i].terms,
                            shipmentStatus: 'Pending Delivery',
                            totalOutstandingBalance: result[i].totalAmountOrder - result[i].paymentDetails[0].paymentAmt,
                            totalAmountOrder: result[i].totalAmountOrder,
                            // orderDate:orderDate,
                            isSync: false

                        };

                        contractList.push(singleObj);


                    }




                    successCallback(contractList);
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }


        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchDraftContract', requestData);

    },

    viewContract: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);



        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("contract.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }


        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    var resultContract = result[data.mobileRowId];

                    // var ={};

                    resultContract.status = "Draft";

                    var dt = new Date();

                    var year = dt.getFullYear();

                    year = year.toString().substr(-2);

                    resultContract.contractCode = year + "0000" + data.mobileRowId + "MY";


                    var pricingDetails = resultContract.paymentDetails;

                    var downPayment1 = 0;
                    var downPayment2 = 0;
                    var salesRepContributionAmt = 0;
                    var salesRepContributionStatus = false;
                    var methodOfPaymentDP1 = "";
                    var methodOfPaymentDP2 = "";

                    var index = 0;

                    for (var i = 0; i < pricingDetails.length; i++) {
                        var paymentObj = pricingDetails[i];
                        var paymentType = paymentObj.paymentType;
                        if (paymentType.toLowerCase() === "deposit1") {
                            downPayment1 = parseFloat(paymentObj.paymentAmt);
                            methodOfPaymentDP1 = paymentObj.paymentMethod;
                        } else if (paymentType.toLowerCase() === "deposit2") {
                            downPayment2 = parseFloat(paymentObj.paymentAmt);
                            methodOfPaymentDP2 = paymentObj.paymentMethod;
                            index = i;
                        } else {
                            salesRepContributionAmt = parseFloat(paymentObj.paymentAmt);
                            salesRepContributionStatus = true;
                        }

                    }

                    pricingDetails.splice(index, 1);

                    resultContract.paymentDetails = pricingDetails;

                    resultContract.review_terms = true;
                    resultContract.methodOfPaymentDP1 = methodOfPaymentDP1;
                    resultContract.methodOfPaymentDP2 = methodOfPaymentDP2;



                    var priceListCode = resultContract.priceListCode;
                    var terms = resultContract.terms;
                    var points = parseFloat(resultContract.points);


                    var requestData = {
                        priceListCode: priceListCode,
                        terms: terms,
                        points: points,
                        countryId: data.countryId
                    };


                    fileSync.fetchPricingDetails(success, error, requestData);


                    function success(result) {

                        //requiredContract.put("priceListId", priceList.getId());

                        resultContract.downPayment1 = downPayment1;

                        resultContract.totalAmountOrder = result.totalAmountOrder;
                        resultContract.downPayment2 = downPayment2;
                        resultContract.salesRepContribution = salesRepContributionAmt;
                        resultContract.salesRepContributionStatus = salesRepContributionStatus;

                        resultContract.installmentAmt = result.installmentAmt;

                        resultContract.totalInstallmentAmount = result.totalInstallmentAmount;

                        resultContract.totalOutstandingBalance = parseFloat(result.totalAmountOrder) - (downPayment1);

                        successCallback(resultContract);

                    };

                    function error() {

                        console.log("Could not fetch");
                    }




                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }




        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'viewContract', requestData);
    },


    saveVerificationRequestData: function(successCallback, errorCallback, data) {

        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: true
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("verificationRequest.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                writeFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    ////console.log("Failed file write: " + e.toString());
                };

                // If data object is not passed in,
                // create a new Blob instead.
                if (!dataObj) {
                    dataObj = new Blob([JSON.stringify(requestData)], {
                        type: 'application/json'
                    });
                }

                fileWriter.write(dataObj);
            });
        }

        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'saveVerificationRequestData', requestData);

    },

    fetchVerificationRequest: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("verificationRequest.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    successCallback(result[0]);
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }




        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchVerificationRequest', requestData);

    },

    verifyContract: function(successCallback, errorCallback, data) {

        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("verifyRequests.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());

                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };


                if (isAppend) {
                    try {

                        fileEntry.file(function(file) {
                            var reader = new FileReader();

                            reader.onloadend = function() {
                                console.log("Successful file read: " + this.result);


                                if (this.result !== null && this.result !== '') {
                                    var saveContractData = JSON.parse(this.result);



                                    var newContract = data;
                                    if (!dataObj) {

                                        saveContractData.push(newContract);

                                        console.log(saveContractData);

                                        // console.log("stringify"+ JSON.stringify(saveContractData));


                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }

                                } else {
                                    if (!dataObj) {

                                        // saveContractData.push(newContract);
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }

                                }


                                fileSync.deleteVerificationRequest(data.contractId);


                                fileWriter.write(dataObj);




                            };

                            reader.readAsText(file);

                        }, onErrorReadFile);

                    } catch (e) {
                        errorCallback("file doesn't exist!");
                    }
                }

                // If data object is not passed in,
                // create a new Blob instead.



            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }


        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'verifyContract', requestData);
    },

    fetchLocation: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchLocation', requestData);
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {
            tx.executeSql('SELECT location_id AS locationId,location_name AS locationName FROM ads_location_master_tb', [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);
                var locationList = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    locationList.push({
                        locationId: rs.rows.item(i).locationId,
                        locationName: rs.rows.item(i).locationName
                    });
                }

                // stateNameArray.sort();

                successCallback(locationList);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });


    },

    submitPayment: function(successCallback, errorCallback, data) {

        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("submitPayment.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());

                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };


                if (isAppend) {
                    try {

                        fileEntry.file(function(file) {
                            var reader = new FileReader();

                            reader.onloadend = function() {
                                console.log("Successful file read: " + this.result);


                                if (this.result !== null && this.result !== '') {
                                    var saveContractData = JSON.parse(this.result);



                                    var newContract = data;
                                    if (!dataObj) {

                                        saveContractData.push(newContract);

                                        console.log(saveContractData);

                                        // console.log("stringify"+ JSON.stringify(saveContractData));


                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }

                                } else {
                                    if (!dataObj) {

                                        // saveContractData.push(newContract);
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }

                                }


                                fileSync.deleteVerificationRequest(data.contractId);


                                fileWriter.write(dataObj);




                            };

                            reader.readAsText(file);

                        }, onErrorReadFile);

                    } catch (e) {
                        errorCallback("file doesn't exist!");
                    }
                }

                // If data object is not passed in,
                // create a new Blob instead.



            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }



        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'submitPayment', requestData);
    },

    saveCollectionRequest: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: true
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("collectionRequests.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                writeFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    ////console.log("Failed file write: " + e.toString());
                };

                // If data object is not passed in,
                // create a new Blob instead.
                if (!dataObj) {
                    dataObj = new Blob([JSON.stringify(requestData)], {
                        type: 'application/json'
                    });
                }

                fileWriter.write(dataObj);
            });
        }


        //  cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'saveCollectionRequest', requestData)
    },

    fetchCollectionRequest: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("collectionRequests.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    successCallback(result[0]);
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }



        // cordova.exec(successCallback, errorCallback, 'FileStoragePlugin', 'fetchCollectionRequest', requestData)


    },

    fetchBankList: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM ads_bank_master_tb WHERE country_id = ' + data.countryId, [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);
                var bankList = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    bankList.push({
                        bankId: rs.rows.item(i).bank_id,
                        accountName: rs.rows.item(i).account_name,
                        accountNumber: rs.rows.item(i).account_number,
                        bankName: rs.rows.item(i).bank_name

                    });
                }

                // stateNameArray.sort();

                successCallback(bankList);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });


        //  cordova.exec(successCallback,errorCallback,'FileStoragePlugin','fetchBankList',requestData);
    },

    fetchPaymentMethodList: function(successCallback, errorCallback) {
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {
            tx.executeSql('SELECT * FROM ads_payment_method_tb ', [], function(tx, rs) {
                ////console.log('Record length: ' + rs.rows.length);
                var paymentMethodList = [];
                for (var i = 0; i < rs.rows.length; i++) {
                    paymentMethodList.push({
                        paymentMethodId: rs.rows.item(i).payment_id,
                        paymentMethodName: rs.rows.item(i).payment_method

                    });
                }

                // stateNameArray.sort();

                successCallback(paymentMethodList);
            }, function(tx, error) {
                ////console.log('SELECT error: ' + error.message);
                errorCallback(error.message);
            });
        });


    },

    savePaymentCo: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("submitPaymentCo.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());

                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };


                if (isAppend) {
                    try {

                        fileEntry.file(function(file) {
                            var reader = new FileReader();

                            reader.onloadend = function() {
                                console.log("Successful file read: " + this.result);


                                if (this.result !== null && this.result !== '') {
                                    var saveContractData = JSON.parse(this.result);



                                    var newContract = data;
                                    if (!dataObj) {

                                        saveContractData.push(newContract);

                                        console.log(saveContractData);

                                        //console.log("stringify"+ JSON.stringify(saveContractData));


                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }

                                } else {
                                    if (!dataObj) {

                                        // saveContractData.push(newContract);
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }

                                }


                                fileSync.deleteCollectionRequest(data.contractId);


                                fileWriter.write(dataObj);




                            };

                            reader.readAsText(file);

                        }, onErrorReadFile);

                    } catch (e) {
                        errorCallback("file doesn't exist!");
                    }
                }

                // If data object is not passed in,
                // create a new Blob instead.



            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }



        //cordova.exec(successCallback,errorCallback,'FileStoragePlugin','savePaymentCo',requestData);

    },


    saveNote: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);


        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("note.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                ////console.log("fileEntry is file?" + fileEntry.isFile.toString());

                var isAppend = true;
                writeFile(fileEntry, null, isAppend);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    ////console.log("Successful file write...");

                    successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };


                if (isAppend) {
                    try {

                        fileEntry.file(function(file) {
                            var reader = new FileReader();

                            reader.onloadend = function() {
                                console.log("Successful file read: " + this.result);


                                if (this.result !== null && this.result !== '') {
                                    var saveContractData = JSON.parse(this.result);



                                    var newContract = data;
                                    if (!dataObj) {

                                        saveContractData.push(newContract);

                                        console.log(saveContractData);




                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }

                                } else {
                                    if (!dataObj) {

                                        // saveContractData.push(newContract);
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }

                                }


                                // fileSync.deleteCollectionRequest(data.contractId);


                                fileWriter.write(dataObj);




                            };

                            reader.readAsText(file);

                        }, onErrorReadFile);

                    } catch (e) {
                        errorCallback("file doesn't exist!");
                    }
                }

                // If data object is not passed in,
                // create a new Blob instead.



            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }
        // cordova.exec(successCallback,errorCallback,'FileStoragePlugin','saveNote',requestData);

    },

    fetchCountryList: function(successCallback, errorCallback, data) {
        var requestData = [];
        requestData.push(data);
        // cordova.exec(successCallback,errorCallback,'FileStoragePlugin','fetchCountryList',requestData);
    },

    insertCountryRow: function(data) {
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        //progress.show("Sync in Progress...Country Details");
        var options = {
            dimBackground: true
        };


        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_country_master_tb (country_id, country_code, country_name,max_cashup_discount) VALUES (?,?,?,?)", [data.countryId, data.countryCode, data.countryName, data.maxCashUpDiscount], fileSync.onSuccess, fileSync.onError);


        });
        //progress.hide();

    },

    insertPaymentMethod: function(data) {
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        //progress.show("Sync in Progress...Country Details");
        var options = {
            dimBackground: true
        };


        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_payment_method_tb (payment_id, payment_method) VALUES (?,?)", [data.paymentMethodId, data.paymentMethodName], fileSync.onSuccess, fileSync.onError);


        });

    },

    insertBankRow: function(data) {


        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_bank_master_tb (bank_id, country_id, bank_name,account_name,account_number) VALUES (?,?,?,?,?)", [data.bankId, data.countryId, data.bankName, data.accountName, data.accountNumber], fileSync.onSuccess, fileSync.onError);


        });
        // SpinnerPlugin.activityStop();

    },
    insertStateRow: function(data) {
        var options = {
            dimBackground: true
        };


        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {

            ////console.log("inserting state:::: "+data.stateName);

            tx.executeSql("INSERT INTO ads_state_master_tb (state_id, state_code, state_name, pricing_area, country_id, point_conv_rate, price_list_price) VALUES (?,?,?,?,?,?,?)", [data.stateId, data.stateCode, data.stateName, data.pricingArea, data.countryId, data.pointConvRate, data.priceListPrice], function(tx, result) {
                    ////console.log("Data inserted");
                },
                function(error) {
                    ////console.log("Error::::"+error.message);
                });


        });
        // SpinnerPlugin.activityStop();

    },
    insertPincodeRow: function(data) {
        var options = {
            dimBackground: true
        };


        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_duty_free_pincode_tb (id, pincode, duty_free_zone) VALUES (?,?,?)", [data.id, data.pincode, data.dutyFreeZone]);


        });
        // SpinnerPlugin.activityStop();

    },
    insertPriceListRow: function(data) {
        var options = {
            dimBackground: true
        };
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });



        db.transaction(function(tx) {

            ////console.log(JSON.stringify(data));

            tx.executeSql("INSERT INTO ads_price_list_master_tb (id, price_list_code, country_id,points,installment_no,first_down_pymnt,sec_down_paymnt,installment_amt,total_sale_amt,gst_points_from,gst_points_to,region_name) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", [data.id, data.priceListCode, data.countryId, data.points, data.noInstallments, data.firstDownPayment, data.secondDownPayment, data.installmentAmount, data.totalSales, data.gstPointsFrom, data.gstPointsTo, data.regionName]);


        });
        SpinnerPlugin.activityStop();

    },

    insertItemListRow: function(data) {
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });
        // var options = { dimBackground: true };

        console.log("Item>>> " + data.itemId + " " + data.productCode + " " + data.itemDesc + " " + data.taxPercentage + " " + data.points);

        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_item_master_tb (item_id, product_code, item_desc,tax_percentg,points,available_for_em,available_for_wm ) VALUES (?,?,?,?,?,?,?)", [data.itemId, data.productCode, data.itemDesc, data.taxPercentage, data.points, data.availableForEM, data.availableForWM], fileSync.onSuccess, fileSync.onError);


        });

        //SpinnerPlugin.activityStop();

    },

    onSuccess: function(tx, r) {
        console.log("Query successful");

    },

    onError: function(tx, e) {

        console.log("SQLite Error: " + e.message);

    },

    insertItemDetailsRow: function(data) {
        var options = {
            dimBackground: true
        };


        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });
        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_item_details_master_tb (item_id, product_code, country_id, tax_percentg, points) VALUES (?,?,?,?,?)", [data.itemId, data.productCode, data.countryId, data.taxPercentage, data.points]);


        });
        // SpinnerPlugin.activityStop();
    },

    insertLastUpdatedRow: function(data) {
        var options = {
            dimBackground: true
        };

        //SpinnerPlugin.activityStart("Sync in Progress...Last updated details", options);


        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });
        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_country_master_tb", data.lastCountryMasterUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_state_master_tb", data.lastStateMasterUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_location_master_tb", data.lastLocationMasterUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_duty_free_pincode_tb", data.lastDutyFreePincodeUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_item_master_tb", data.lastItemMasterUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_item_details_master_tb", data.lastItemDetailsMasterUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_price_list_master_tb", data.lastPriceListMasterUpdateTime]);
            tx.executeSql("INSERT INTO last_updated_time_tb (table_name, last_update_time) VALUES (?,?)", ["ads_bank_master_tb", data.lastBankMasterUpdateTime]);


        });

        //SpinnerPlugin.activityStop();




    },

    insertLocationListRow: function(data) {
        // progress.update("Sync in Progress... Location details");
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        db.transaction(function(tx) {

            tx.executeSql("INSERT INTO ads_location_master_tb (location_id, location_name,default_location) VALUES (?,?,?)", [data.locationId, data.locationName, data.defaultLocation], fileSync.onSuccess, fileSync.onError);


        })
    },



    syncServiceData: function(successCallback, errorCallback) {

        // progress.show("Checking for file...");

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir, onfailDirectory);
        }

        function gotDir(dirEntry) {

            dirEntry.getFile("contract.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);

            }, onErrorCreateFile);
        }

        function fail() {
            errorCallback("Failed to load file system");
            // progress.hide();
        }

        function onErrorCreateFile() {
            errorCallback("file does not exists");
            //progress.hide();
        }

        function onfailDirectory() {
            errorCallback("Directory does not exists");
            //progress.hide();

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    var result = JSON.parse(this.result);

                    console.log(result);
                    var contractList = [];
                    for (var i = 0; i < result.length; i++) {
                        delete result[i].totalAmountOrder;
                        contractList.push(result[i]);
                    }


                    // progress.hide();

                    // alert(JSON.stringify(contractList));

                    successCallback(contractList);

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }


        function onErrorReadFile() {

           errorCallback("Failed to read file");
            // progress.hide();
        }




    },

    syncVerificationList: function(successCallback, errorCallback) {

        console.log("Here");

        //  progress.show("Checking for file...");

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir, onfailDirectory);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("verifyRequests.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);

            }, onErrorCreateFile);




        }

        function fail() {
            errorCallback("Failed to load file system");
            // progress.hide();
        }

        function onErrorCreateFile() {
            errorCallback("file does not exists");
            //   progress.hide();
        }

        function onfailDirectory() {
            errorCallback("Directory does not exists");
            //  progress.hide();

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    var result = JSON.parse(this.result);




                    // progress.hide();

                    // alert(JSON.stringify(result));

                    successCallback(result);

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }


        function onErrorReadFile() {

            errorCallback("Failed to read file");
            // progress.hide();
        }




    },

    syncCollectionList: function(successCallback, errorCallback) {

        // progress.show("Checking for file...");

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir, onfailDirectory);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("submitPaymentCo.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);

            }, onErrorCreateFile);




        }

        function fail() {
            errorCallback("Failed to load file system");
            // progress.hide();
        }

        function onErrorCreateFile() {
            errorCallback("file does not exists");
            //  progress.hide();
        }

        function onfailDirectory() {
            errorCallback("Directory does not exists");
            // progress.hide();

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    var result = JSON.parse(this.result);
                    // progress.hide();

                    // alert(JSON.stringify(result));

                    successCallback(result);

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }


        function onErrorReadFile() {

            errorCallback("Failed to read file");
            // progress.hide();
        }
    },


    syncNoteList: function(successCallback, errorCallback) {

        //  progress.show("Checking for file...");

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir, onfailDirectory);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("note.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);

            }, onErrorCreateFile);
        }

        function fail() {
            errorCallback("Failed to load file system");
            // progress.hide();
        }

        function onErrorCreateFile() {
            errorCallback("file does not exists");
            //  progress.hide();
        }

        function onfailDirectory() {
            errorCallback("Directory does not exists");
            //  progress.hide();

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    var result = JSON.parse(this.result);
                    //progress.hide();

                    successCallback(result);

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }


        function onErrorReadFile() {

            errorCallback("Failed to read file");
            // progress.hide();
        }

    },
    collectPaymentVo: function(data, successCallback, errorCallback) {

        console.log("Here");

        // progress.show("Checking for file...");


        //for(var i=0;i<data.length;i++){
        fileSync.deleteFromVerifyFile(data);
        //}

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir, onfailDirectory);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("submitPayment.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);

            }, onErrorCreateFile);




        }

        function fail() {
            errorCallback("Failed to load file system");
            //  progress.hide();
        }

        function onErrorCreateFile() {
            errorCallback("file does not exists");
            // progress.hide();
        }

        function onfailDirectory() {
            errorCallback("Directory does not exists");
            //  progress.hide();

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    var result = JSON.parse(this.result);

                    console.log(result);
                    var contractList = [];

                    for (var i = 0; i < result.length; i++) {
                        if (data.indexOf(result[i].contractId) >= 0) {
                            contractList.push(result[i]);

                        }

                    }



                    successCallback(contractList);

                }


                // progress.hide();

                // alert(JSON.stringify(contractList));



                // };

                reader.readAsText(file);

            }, onErrorReadFile);
        }


        function onErrorReadFile() {

            errorCallback("Failed to read file");
            // progress.hide();
        }


    },

    deleteVerificationRequest: function(data) {

        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("verificationRequest.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    result = result[0];

                    for (var i = 0; i < result.verificationList.length; i++) {
                        if (result.verificationList[i].contractId === data) {
                            result.verificationList.splice(i, 1);
                            break;
                        }

                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");


                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        var resultArray = [];
                        resultArray.push(result);

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(resultArray)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }

    },


    deleteCollectionRequest: function(data) {
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("collectionRequests.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    result = result[0];

                    for (var i = 0; i < result.collectionList.length; i++) {
                        if (result.collectionList[i].contractId === data) {
                            result.collectionList.splice(i, 1);
                            break;
                        }

                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");


                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        var resultArray = [];
                        resultArray.push(result);

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(resultArray)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });




                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }
    },
    deleteContractText: function(mobileRowId) {


        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("contract.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    /*for(var i=mobileRowId.length-1;i>=0;i--){
                     result.splice(mobileRowId[i],1);
                     }*/

                    for (var i = 0; i < mobileRowId.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].mobileRowId === mobileRowId[i]) {
                                result.splice(j, 1);
                            }
                        }
                    }


                    if (result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            result[i].mobileRowId = i;
                        }
                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");


                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }


    },

    deleteFromVerificationFile: function(data, successCallback, errorCallback) {

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

            function gotFS(fileSystem) {
                fileSystem.root.getDirectory("grolier", {
                    create: false
                }, gotDir);
            }

            function gotDir(dirEntry) {

                ////console.log('file system open: ' + dirEntry.name);
                dirEntry.getFile("submitPayment.txt", {
                    create: false,
                    exclusive: false
                }, function(fileEntry) {

                    // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                    readFile(fileEntry);

                }, onErrorCreateFile);

            }

            function readFile(fileEntry) {

                fileEntry.file(function(file) {
                    var reader = new FileReader();

                    reader.onloadend = function() {
                        ////console.log("Successful file read: " + this.result);
                        var result = JSON.parse(this.result);

                        for (var i = 0; i < data.length; i++) {
                            for (var j = 0; j < result.length; j++) {
                                if (result[j].contractId === data[i]) {
                                    result.splice(j, 1);
                                    break;
                                }
                            }

                        }



                        fileEntry.createWriter(function(fileWriter) {

                            fileWriter.onwriteend = function() {
                                ////console.log("Successful file write...");


                                // readFile(fileEntry);
                            };

                            fileWriter.onerror = function(e) {
                                successCallback("Failed file write: " + e.toString());
                            };

                            var dataObj = null;

                            // If data object is not passed in,
                            // create a new Blob instead.

                            if (!dataObj) {
                                dataObj = new Blob([JSON.stringify(result)], {
                                    type: 'application/json'
                                });
                            }

                            fileWriter.write(dataObj);
                        });

                    };

                    reader.readAsText(file);

                }, onErrorReadFile);
            }

            function onErrorCreateFile() {
                errorCallback("Failed to create file");
            }

            function fail() {
                errorCallback("Failed to load file system");
            }

            function onErrorReadFile() {
                errorCallback("Failed to read File")
            }


        },

    deleteFromVerifyFile: function(data) {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("verifyRequests.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);


                    for (var i = 0; i < data.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].contractId === data[i]) {
                                result.splice(j, 1);
                                break;
                            }
                        }

                    }

                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {

                        };

                        fileWriter.onerror = function(e) {};

                        var dataObj = null;

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }

    },

    deleteCollectionRecord: function(data, successCallback) {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("submitPaymentCo.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    for (var i = 0; i < data.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].contractId === data[i]) {
                                result.splice(j, 1);
                                break;
                            }
                        }
                    }




                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");

                            successCallback("Success");
                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }


    },

    deleteNoteRecord: function(data) {


        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("note.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            //  progress.show("Checking for note sync");

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    for (var i = 0; i < data.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].contractId === data[i]) {
                                result.splice(j, 1);
                                break;
                            }
                        }

                    }




                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");
                            // progress.hide();

                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("Failed to create file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }

    },

    readLogFile: function(successCallback, errorCallback) {

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("log.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {
            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    var result = JSON.parse(this.result);
                    successCallback(result);

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }


    },

    deleteTables: function(tableName, successCallback, errorCallback) {


        //for(var i=0;i<tableNames.length;i++){
        var db = window.sqlitePlugin.openDatabase({
            name: constants.dbName
        });

        //var tableName=tableNames[i];

        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM ' + tableName, [], function(tx, rs) {
                successCallback(tableName);
            }, function(tx, error) {
                errorCallback(error.message);
                console.log('SELECT error: ' + error.message);
            });
        });
        // }



    }




}