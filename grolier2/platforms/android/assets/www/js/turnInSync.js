var turnInSync = {
    saveDataIntoFile: function(fileName, data) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile(fileName + ".txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {

                console.log("fileEntry is file?" + fileEntry.isFile.toString());
                writeFile(fileEntry, null);

            }, onErrorCreateFile);

        }

        function onErrorCreateFile() {
            //errorCallback("Failed to create file");
        }

        function fail() {
            //errorCallback("Failed to load file system");
        }


        function writeFile(fileEntry, dataObj) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {

                fileWriter.onwriteend = function() {
                    console.log("Successful file write..." + fileName + " txt");

                    //                            successCallback("200");
                    // readFile(fileEntry);
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };

                // If data object is not passed in,
                // create a new Blob instead.
                if (!dataObj) {
                    dataObj = new Blob([JSON.stringify(data)], {
                        type: 'text/plain'
                    });
                }

                fileWriter.write(dataObj);
            });
        }

    },

    fetchDataFromFile: function(fileName, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile(fileName + ".txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);
            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);
                    successCallback(result);
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to show from file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },

    saveVOReceiveData: function(data, successCallback, errorCallback) {
        turnInSync.saveVOReceiveDataIntoFile('saveVOReceiveData', data);
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveVerificationTurnInReceive.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);
            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    var cashTotal = 0;
                    var chequeTotal = 0;
                    var creditCardTotal = 0;
                    var deleteRowIndex=[];

                    if (result) {
//                        for (var i = 0; i < result.voBankInTurnInReceiveRows.length; i++) {
//                            if (result.voBankInTurnInReceiveRows[i].srName === data.srName) {
//                                if (result.voBankInTurnInReceiveRows[i].paymentMethod === 'Cash') {
//                                    cashTotal = cashTotal + parseFloat(result.voBankInTurnInReceiveRows[i].amount);
//                                } else if (result.voBankInTurnInReceiveRows[i].paymentMethod === 'Cheque') {
//                                    chequeTotal = chequeTotal + parseFloat(result.voBankInTurnInReceiveRows[i].amount);
//                                } else {
//                                    creditCardTotal = creditCardTotal + parseFloat(result.voBankInTurnInReceiveRows[i].amount);
//                                }
//
//                                result.voBankInTurnInReceiveRows.splice(i, 1);
//                            }
//                        }

					for(var i=result.voBankInTurnInReceiveRows.length-1;i>=0;i--){
						if (result.voBankInTurnInReceiveRows[i].srName === data.srName){
							if (result.voBankInTurnInReceiveRows[i].paymentMethod === 'Cash') {
								cashTotal = cashTotal + parseFloat(result.voBankInTurnInReceiveRows[i].amount);
							}else if (result.voBankInTurnInReceiveRows[i].paymentMethod === 'Cheque'){
								chequeTotal = chequeTotal + parseFloat(result.voBankInTurnInReceiveRows[i].amount);
							}else{
								creditCardTotal = creditCardTotal + parseFloat(result.voBankInTurnInReceiveRows[i].amount);
							}
							result.voBankInTurnInReceiveRows.splice(i, 1);
						}
					}

                    }

                    data.srName=undefined;

                    turnInSync.saveDataIntoFile('saveVerificationTurnInReceive', result);

                    turnInSync.updateBankInVOAfterReceive(data);
                    turnInSync.updateSummary('receive', cashTotal, chequeTotal, creditCardTotal, successCallback, errorCallback);



                    // successCallback(result)
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to save into file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }


    },

    updateSummary: function(type, cashTotal, chequeTotal, creditCardTotal, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveVOSummary.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);
            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);


                    if (type === 'receive') {
                        result.cashSummary.receivedAmt = parseFloat(result.cashSummary.receivedAmt) + cashTotal;
                        result.chequeSummary.receivedAmt = parseFloat(result.chequeSummary.receivedAmt) + chequeTotal;
                        result.cashSummary.outstandingAmt = parseFloat(result.cashSummary.outstandingAmt) + cashTotal;
                        result.chequeSummary.outstandingAmt = parseFloat(result.chequeSummary.outstandingAmt) + chequeTotal;
                        result.creditCardSummary.receivedAmt= parseFloat(result.creditCardSummary.receivedAmt)+creditCardTotal
                    } else if (type === 'bankedIn') {

                        if (cashTotal <= result.cashSummary.outstandingAmt){
							result.cashSummary.outstandingAmt = parseFloat(result.cashSummary.outstandingAmt) - cashTotal;
							result.cashSummary.bankedInAmt = parseFloat(result.cashSummary.bankedInAmt)+cashTotal;
						}

						if(chequeTotal <= result.chequeSummary.outstandingAmt){
							result.chequeSummary.outstandingAmt = parseFloat(result.chequeSummary.outstandingAmt) - chequeTotal;
							result.chequeSummary.bankedInAmt = parseFloat(result.chequeSummary.bankedInAmt)+chequeTotal;
						}

                    }


                    //result.creditCardSummary.receivedAmt=parseFloat(result.creditCardSummary.receivedAmt)+creditCardTotal;

                    turnInSync.saveDataIntoFile('saveVOSummary', result);

                    successCallback("200");

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to save");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },
    updateSummaryCO: function(type, cashTotal, chequeTotal, creditCardTotal, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveCOSummary.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);
            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);


                    if (type === 'receive') {
                        result.cashSummary.receivedAmt = parseFloat(result.cashSummary.receivedAmt) + cashTotal;
                        result.chequeSummary.receivedAmt = parseFloat(result.chequeSummary.receivedAmt) + chequeTotal;
                        result.cashSummary.outstandingAmt = parseFloat(result.cashSummary.outstandingAmt) + cashTotal;
                        result.chequeSummary.outstandingAmt = parseFloat(result.chequeSummary.outstandingAmt) + chequeTotal;
                    } else if (type === 'bankedIn') {

                        if (cashTotal <= result.cashSummary.outstandingAmt){
							result.cashSummary.outstandingAmt = parseFloat(result.cashSummary.outstandingAmt) - cashTotal;
							result.cashSummary.bankedInAmt = parseFloat(result.cashSummary.bankedInAmt)+cashTotal;
                        }

                        if(chequeTotal <= result.chequeSummary.outstandingAmt){
                        	result.chequeSummary.outstandingAmt = parseFloat(result.chequeSummary.outstandingAmt) - chequeTotal;
                            							result.chequeSummary.bankedInAmt = parseFloat(result.chequeSummary.bankedInAmt)+chequeTotal;
                        }


                    }


                    //result.creditCardSummary.receivedAmt=parseFloat(result.creditCardSummary.receivedAmt)+creditCardTotal;

                    turnInSync.saveDataIntoFile('saveCOSummary', result);

                    successCallback("200");

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to save");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },
    saveVOReceiveDataIntoFile: function(fileName, data) {
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {
            dirEntry.getFile(fileName + ".txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                var isAppend = true;
                writeFile(fileEntry, null, isAppend);
            }, onErrorCreateFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to save");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {
                fileWriter.onwriteend = function() {};
                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };
                if (isAppend) {
                    try {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                if (this.result !== null && this.result !== '' && this.result !== 'null' && this.result !== '[]') {
                                    var saveContractData = JSON.parse(this.result);
                                    var newContract = data;
                                    if (!dataObj) {
                                        newContract.mobileRowId = saveContractData[saveContractData.length - 1].mobileRowId + 1;
                                        saveContractData.push(newContract);
                                        console.log(saveContractData);
                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }
                                } else {
                                    if (!dataObj) {
                                        requestData[0].mobileRowId = 0;
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }
                                }
                                fileWriter.write(dataObj);
                            };
                            reader.readAsText(file);
                        }, onErrorReadFile);
                    } catch (e) {
                        console.log("file doesn't exist!");
                    }
                }

            });
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }

    },

    updateBankInVOAfterReceive: function(data) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveVerificationTurnInPending.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);
            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);



                    if (this.result !== null && this.result !== "" && this.result !== "null" && this.result !== "[]") {
                        var result = JSON.parse(this.result);


                        var cashList = [];
                        var chequeList = [];

                        var cashTotal = 0;

                        for (var i = 0; i < data.receiveInBankInDetails.length; i++) {
                            if (data.receiveInBankInDetails[i].paymentMethod === 'Cash') {
                                cashList.push(data.receiveInBankInDetails[i]);
                                cashTotal = parseFloat(cashTotal) + data.receiveInBankInDetails[i].amount;
                            } else if (data.receiveInBankInDetails[i].paymentMethod === 'Cheque') {
                                chequeList.push(data.receiveInBankInDetails[i]);
                            }
                        }

                        var gotDateEntry = false;
                        var dateIndex = 0;

                        if (result.voBankInTurnInPendingClearanceRows) {
                            for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++) {
                                var value = result.voBankInTurnInPendingClearanceRows[i];

                                var m_names = new Array("Jan", "Feb", "Mar",
                                    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                                    "Oct", "Nov", "Dec");

                                var d = new Date();
                                var curr_date = d.getDate();
                                var curr_month = d.getMonth();
                                var curr_year = d.getFullYear();

                                d = curr_date + "-" + m_names[curr_month] + "-" + curr_year;

                                if (value.paymentMethod === 'Cash' && value.date === d) {
                                    gotDateEntry = true;
                                    dateIndex = i;
                                    break;
                                }
                            }

                        }



                        if (gotDateEntry) {

                            result.voBankInTurnInPendingClearanceRows[dateIndex].amount = parseFloat(result.voBankInTurnInPendingClearanceRows[dateIndex].amount) + cashTotal;
                            for (var i = 0; i < cashList.length; i++) {
                                for (var j = 0; j < cashList[i].paymentIdList.length; j++) {
                                	if(result.voBankInTurnInPendingClearanceRows[dateIndex].paymentIdList){
                                		result.voBankInTurnInPendingClearanceRows[dateIndex].paymentIdList.push(cashList[i].paymentIdList[j]);
                                	}else{
                                		result.voBankInTurnInPendingClearanceRows[dateIndex].paymentIdList=[];
                                		result.voBankInTurnInPendingClearanceRows[dateIndex].paymentIdList.push(cashList[i].paymentIdList[j]);

                                	}

                                }
                            }


                        } else {
                            if (cashList && cashList.length > 0) {
                                var paymentIdList = [];

                                for (var i = 0; i < cashList.length; i++) {
                                    for (var j = 0; j < cashList[i].paymentIdList.length; j++) {
                                        paymentIdList.push(cashList[i].paymentIdList[j]);
                                    }
                                }

                                var singleObj = {
                                    paymentMethod: 'Cash',
                                    date: d,
                                    amount: cashTotal,
                                    chequeDate: null,
                                    chequeDt: null,
                                    chequeNo: null,
                                    bankSlipNo: null,
                                    remarks: null,
                                    bankedInBy: 'With VO',
                                    paymentIdList: paymentIdList

                                };
                                result.voBankInTurnInPendingClearanceRows.unshift(singleObj);

                            } else if (chequeList && chequeList.length > 0) {

                                for (var i = 0; i < chequeList.length; i++) {
                                    var singleObj = {
                                        paymentMethod: 'Cheque',
                                        date: chequeList[i].chequeDate,
                                        amount: chequeList[i].amount,
                                        chequeDate: chequeList[i].chequeDate,
                                        chequeDt: chequeList[i].chequeDate,
                                        chequeNo: chequeList[i].chequeNo,
                                        bankSlipNo: null,
                                        remarks: null,
                                        bankedInBy: 'With VO',
                                        paymentIdList: chequeList[i].paymentIdList

                                    };
                                    result.voBankInTurnInPendingClearanceRows.unshift(singleObj);

                                }


                            }

                        }


                        turnInSync.saveDataIntoFile('saveVerificationTurnInPending', result);

                    } else {
                        alert("Bank In file not created yet");
                        errorCallback("Bank In file not created yet");
                    }



                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to save");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },

    saveBankInDetailsVO: function(data, successCallback, errorCallback) {
        turnInSync.saveVOReceiveDataIntoFile('saveVOBankInData', data);
        turnInSync.updateBankInVOAfterBankIn(data, successCallback, errorCallback);
    },

    saveBankInDetailsCO: function(data, successCallback, errorCallback) {
        turnInSync.saveVOReceiveDataIntoFile('saveCOBankInData', data);
        turnInSync.updateBankInCOAfterBankIn(data, successCallback, errorCallback);
    },

    updateBankInVOAfterBankIn: function(data, successCallback, errorCallback) {
        var successFetch = function(result) {

            var actualCashToBankIn = parseFloat(data.actualCashAmount);




            if (actualCashToBankIn && actualCashToBankIn !== 0) {
                for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++) {
                    if (result.voBankInTurnInPendingClearanceRows[i].paymentMethod === 'Cash' && result.voBankInTurnInPendingClearanceRows[i].bankedInBy === 'With VO') {
                        if (result.voBankInTurnInPendingClearanceRows[i].amount === actualCashToBankIn) {
                            //paying exact amount
                            //result.voBankInTurnInPendingClearanceRows.splice(i,1);
                            result.voBankInTurnInPendingClearanceRows[i].bankedInBy = 'Banked In By VO';

                            //						var extraData={
                            //							userId:data.userId,
                            //							extraAmt:0
                            //						};
                            //						turnInSync.saveExtraAmt(extraData);
                            actualCashToBankIn = 0;
                            break;
                        } else if (parseFloat(result.voBankInTurnInPendingClearanceRows[i].amount) > actualCashToBankIn) {
                            //paying less
                            result.voBankInTurnInPendingClearanceRows[i].amount = result.voBankInTurnInPendingClearanceRows[i].amount - actualCashToBankIn;
                            var singleObj = {
                                paymentMethod: 'Cash',
                                date: data.bankedInDate,
                                amount: result.voBankInTurnInPendingClearanceRows[i].amount,
                                chequeDate: null,
                                chequeDt: null,
                                chequeNo: null,
                                bankSlipNo: null,
                                remarks: null,
                                bankedInBy: 'With VO',
                                paymentIdList: result.voBankInTurnInPendingClearanceRows[i].paymentIdList

                            };

                            var payedObj = {
                                paymentMethod: 'Cash',
                                date: data.bankedInDate,
                                amount: actualCashToBankIn,
                                chequeDate: null,
                                chequeDt: null,
                                chequeNo: null,
                                bankSlipNo: null,
                                remarks: null,
                                bankedInBy: 'Banked In By VO',
                                paymentIdList: result.voBankInTurnInPendingClearanceRows[i].paymentIdList
                            }

                            result.voBankInTurnInPendingClearanceRows[i] = singleObj;
                            result.voBankInTurnInPendingClearanceRows.unshift(payedObj);

                            //						var extraAmt=parseFloat(actualCashToBankIn);
                            //
                            //						var extraData={
                            //							userId:data.userId,
                            //							extraAmt:extraAmt
                            //						};
                            //
                            //						turnInSync.saveExtraAmt(extraData);
                            //
                            actualCashToBankIn = 0;
                            break;
                        } else if (actualCashToBankIn > result.voBankInTurnInPendingClearanceRows[i].amount) {
                            //paying more
                            //result.voBankInTurnInPendingClearanceRows.splice(i,1);
                            result.voBankInTurnInPendingClearanceRows[i].bankedInBy = 'Banked In By VO';
                            actualCashToBankIn = actualCashToBankIn - result.voBankInTurnInPendingClearanceRows[i].amount;

                            if (actualCashToBankIn === 0) {
                                //							var extraData={
                                //								userId:data.userId,
                                //								extraAmt:0
                                //							};
                                //							turnInSync.saveExtraAmt(extraData);
                                break;
                            }
                        }
                    }

                }
            }



            var cashTotal = data.actualCashAmount;
            var chequeTotal = 0;
            var chequeList = [];

            for (var i = 0; i < data.receiveInBankInDetails.length; i++) {
                if (data.receiveInBankInDetails[i].paymentMethod === 'Cheque') {
                    chequeTotal = parseFloat(chequeTotal) + parseFloat(data.receiveInBankInDetails[i].amount);
                    chequeList.push(data.receiveInBankInDetails[i]);
                }
            }

            for (var i = 0; i < chequeList.length; i++) {
                for (var j = 0; j < result.voBankInTurnInPendingClearanceRows.length; j++) {
                    if (result.voBankInTurnInPendingClearanceRows[j].paymentMethod === 'Cheque' && result.voBankInTurnInPendingClearanceRows[j].chequeNo === chequeList[i].chequeNo) {
                        result.voBankInTurnInPendingClearanceRows[j].bankedInBy = 'Banked In By VO';
                    }
                }

            }

            console.log(JSON.stringify(result));

            turnInSync.saveDataIntoFile('saveVerificationTurnInPending', result);

            turnInSync.updateSummary('bankedIn', cashTotal, chequeTotal, 0, successCallback, errorCallback);

            ///For Cheque

        }

        var errorFetch = function(message) {
            alert(result);
        }
        turnInSync.fetchDataFromFile('saveVerificationTurnInPending', successFetch, errorFetch);

    },


    updateBankInCOAfterBankIn: function(data, successCallback, errorCallback) {


        var successFetch = function(result) {

            var actualCashToBankIn = parseFloat(data.actualCashAmount);




            if (actualCashToBankIn && actualCashToBankIn !== 0) {
                for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++) {
                    if (result.voBankInTurnInPendingClearanceRows[i].paymentMethod === 'Cash' && result.voBankInTurnInPendingClearanceRows[i].bankedInBy === 'With CO') {
                        if (parseFloat(result.voBankInTurnInPendingClearanceRows[i].amount) === actualCashToBankIn) {
                            //paying exact amount
                            //result.voBankInTurnInPendingClearanceRows.splice(i,1);
                            result.voBankInTurnInPendingClearanceRows[i].bankedInBy = 'Banked In By CO';

                            //						var extraData={
                            //							userId:data.userId,
                            //							extraAmt:0
                            //						};
                            //						turnInSync.saveExtraAmt(extraData);
                            actualCashToBankIn = 0;
                            break;
                        } else if (parseFloat(result.voBankInTurnInPendingClearanceRows[i].amount) > actualCashToBankIn) {
                            //paying less
                            result.voBankInTurnInPendingClearanceRows[i].amount = parseFloat(result.voBankInTurnInPendingClearanceRows[i].amount) - actualCashToBankIn;
                            var singleObj = {
                                paymentMethod: 'Cash',
                                date: data.bankedInDate,
                                amount: result.voBankInTurnInPendingClearanceRows[i].amount,
                                chequeDate: null,
                                chequeDt: null,
                                chequeNo: null,
                                bankSlipNo: null,
                                remarks: null,
                                bankedInBy: 'With CO',
                                paymentIdList: result.voBankInTurnInPendingClearanceRows[i].paymentIdList

                            };

                            var payedObj = {
                                paymentMethod: 'Cash',
                                date: data.bankedInDate,
                                amount: parseFloat(actualCashToBankIn),
                                chequeDate: null,
                                chequeDt: null,
                                chequeNo: null,
                                bankSlipNo: null,
                                remarks: null,
                                bankedInBy: 'Banked In By CO',
                                paymentIdList: result.voBankInTurnInPendingClearanceRows[i].paymentIdList
                            }

                            result.voBankInTurnInPendingClearanceRows[i] = singleObj;
                            result.voBankInTurnInPendingClearanceRows.unshift(payedObj);

                            //						var extraAmt=parseFloat(actualCashToBankIn);
                            //
                            //						var extraData={
                            //							userId:data.userId,
                            //							extraAmt:extraAmt
                            //						};
                            //
                            //						turnInSync.saveExtraAmt(extraData);
                            //
                            actualCashToBankIn = 0;
                            break;
                        } else if (actualCashToBankIn > parseFloat(result.voBankInTurnInPendingClearanceRows[i].amount)) {
                            //paying more
                            //result.voBankInTurnInPendingClearanceRows.splice(i,1);
                            result.voBankInTurnInPendingClearanceRows[i].bankedInBy = 'Banked In By CO';
                            actualCashToBankIn = actualCashToBankIn - parseFloat(result.voBankInTurnInPendingClearanceRows[i].amount);

                            if (actualCashToBankIn === 0) {
                                //							var extraData={
                                //								userId:data.userId,
                                //								extraAmt:0
                                //							};
                                //							turnInSync.saveExtraAmt(extraData);
                                break;
                            }
                        }
                    }

                }
            }



            var cashTotal = data.actualCashAmount;
            var chequeTotal = 0;
            var chequeList = [];

            for (var i = 0; i < data.receiveInBankInDetails.length; i++) {
                if (data.receiveInBankInDetails[i].paymentMethod === 'Cheque') {
                    chequeTotal = parseFloat(chequeTotal) + parseFloat(data.receiveInBankInDetails[i].amount);
                    chequeList.push(data.receiveInBankInDetails[i]);
                }
            }

            for (var i = 0; i < chequeList.length; i++) {
                for (var j = 0; j < result.voBankInTurnInPendingClearanceRows.length; j++) {
                    if (result.voBankInTurnInPendingClearanceRows[j].paymentMethod === 'Cheque' && result.voBankInTurnInPendingClearanceRows[j].chequeNo === chequeList[i].chequeNo) {
                        result.voBankInTurnInPendingClearanceRows[j].bankedInBy = 'Banked In By VO';
                    }
                }

            }

            console.log(JSON.stringify(result));

            turnInSync.saveDataIntoFile('saveCollectorTurnIn', result);

            turnInSync.updateSummaryCO('bankedIn', cashTotal, chequeTotal, 0, successCallback, errorCallback);

            ///For Cheque

        }

        var errorFetch = function(message) {
            alert(result);
        }
        turnInSync.fetchDataFromFile('saveCollectorTurnIn', successFetch, errorFetch);


    },


    fetchExtraAmt: function(userId, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveExtraAmt.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {
                readFile(fileEntry);
            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {
            fileEntry.file(function(file) {
                var reader = new FileReader();
                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    if (this.result !== null && this.result !== '' && this.result !== "null" && this.result !== "[]") {
                        var result = JSON.parse(this.result);
                        var count = 0;
                        if (result.length == 0) {
                            errorCallback("NO RECORD");
                        }
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].userId == userId) {
                                var response = result[i];
                                result.splice(i, 1);
                                successCallback(response);
                            } else {
                                count++;
                            }
                        }
                        if (count === result.length) {
                            errorCallback("NO RECORD");
                        }
                    } else {
                        errorCallback("NO RECORD");
                    }
                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("Failed to create file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File")
        }

    },
    saveExtraAmt: function(data) {
        var requestData = [];
        requestData.push(data);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {
            dirEntry.getFile("saveExtraAmt.txt", {
                create: true,
                exclusive: false
            }, function(fileEntry) {
                var isAppend = true;
                writeFile(fileEntry, null, isAppend);
            }, onErrorCreateFile);
        }

        function onErrorCreateFile() {
            // errorCallback("Failed to create file");
        }

        function fail() {
            //errorCallback("Failed to load file system");
        }

        function writeFile(fileEntry, dataObj, isAppend) {
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function(fileWriter) {
                fileWriter.onwriteend = function() {};
                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };
                if (isAppend) {
                    try {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                if (this.result !== null && this.result !== '' && this.result !== 'null' && this.result!=='[]') {
                                    var saveContractData = JSON.parse(this.result);
                                    var newContract = data;
                                    if (!dataObj) {

                                        var count = 0;

                                        if (saveContractData.length > 0) {
                                            for (var i = 0; i < saveContractData.length; i++) {
                                                if (saveContractData[i].userId === data.userId) {
                                                    saveContractData[i].extraAmt = data.extraAmt;
                                                    break;
                                                } else {
                                                    count++;
                                                }
                                            }

                                            if (count === saveContractData.length) {
                                                saveContractData.push(newContract);
                                            }

                                        } else {
                                            saveContractData.push(newContract);
                                        }

                                        console.log(saveContractData);
                                        dataObj = new Blob([JSON.stringify(saveContractData)], {
                                            type: 'application/json'
                                        });
                                    }
                                } else {
                                    if (!dataObj) {
                                        dataObj = new Blob([JSON.stringify(requestData)], {
                                            type: 'application/json'
                                        });
                                    }
                                }
                                fileWriter.write(dataObj);
                            };
                            reader.readAsText(file);
                        }, onErrorReadFile);
                    } catch (e) {
                        console.log("file doesn't exist!");
                    }
                }

            });
        }

        function onErrorReadFile() {
            //errorCallback("Failed to read File");
        }


    },

    syncCollectorTurnIn: function(successCallback, errorCallback) {
        turnInSync.fetchDataFromFile('saveCOBankInData', successCallback, errorCallback);
    },

    syncVerificationReceiveTurnIn: function(successCallback, errorCallback) {
        turnInSync.fetchDataFromFile('saveVOReceiveData', successCallback, errorCallback);
    },

    syncVerificationBankTurnIn: function(successCallback, errorCallback) {
        turnInSync.fetchDataFromFile('saveVOBankInData', successCallback, errorCallback);
    },

    deleteCollectorTurnInText: function(mobileRowId) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveCOBankInData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    for (var i = 0; i < mobileRowId.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].mobileRowId === mobileRowId[i]) {
                                result.splice(j, 1);
                            }
                        }
                    }


                    if (result!==null && result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            result[i].mobileRowId = i;
                        }
                    }else{
                    	result=null;
                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");


                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            console.log("No record to delete from file");
        }

        function fail() {
            console.log("Failed to load file system");
        }

        function onErrorReadFile() {
            console.log("Failed to read File")
        }

    },
    deleteVerificationReceivedata: function(mobileRowIdList, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveVOReceiveData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    for (var i = 0; i < mobileRowIdList.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].mobileRowId === mobileRowIdList[i]) {
                                result.splice(j, 1);
                            }
                        }
                    }


                    if (result!==null && result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            result[i].mobileRowId = i;
                        }
                    }else{
                    	result=null;
                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");
                            successCallback("Successful");

                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                            errorCallback("Error Occurred");
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("No record to delete from file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }
    },
    deleteVerificationBankIndata: function(mobileRowIdList, successCallback, errorCallback) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.root.getDirectory("grolier", {
                create: false
            }, gotDir);
        }

        function gotDir(dirEntry) {

            ////console.log('file system open: ' + dirEntry.name);
            dirEntry.getFile("saveVOBankInData.txt", {
                create: false,
                exclusive: false
            }, function(fileEntry) {

                // ////console.log("fileEntry is file?" + fileEntry.isFile.toString());
                readFile(fileEntry);

            }, onErrorCreateFile);

        }

        function readFile(fileEntry) {

            fileEntry.file(function(file) {
                var reader = new FileReader();

                reader.onloadend = function() {
                    ////console.log("Successful file read: " + this.result);
                    var result = JSON.parse(this.result);

                    for (var i = 0; i < mobileRowIdList.length; i++) {
                        for (var j = 0; j < result.length; j++) {
                            if (result[j].mobileRowId === mobileRowIdList[i]) {
                                result.splice(j, 1);
                            }
                        }
                    }


                    if (result!==null && result.length > 0) {
                        for (var i = 0; i < result.length; i++) {
                            result[i].mobileRowId = i;
                        }
                    }else{
                    	result=null;
                    }



                    fileEntry.createWriter(function(fileWriter) {

                        fileWriter.onwriteend = function() {
                            ////console.log("Successful file write...");
                            successCallback("Successful");

                            // readFile(fileEntry);
                        };

                        fileWriter.onerror = function(e) {
                            ////console.log("Failed file write: " + e.toString());
                            errorCallback("Error Occurred");
                        };

                        var dataObj = null;

                        // If data object is not passed in,
                        // create a new Blob instead.

                        if (!dataObj) {
                            dataObj = new Blob([JSON.stringify(result)], {
                                type: 'application/json'
                            });
                        }

                        fileWriter.write(dataObj);
                    });

                };

                reader.readAsText(file);

            }, onErrorReadFile);
        }

        function onErrorCreateFile() {
            errorCallback("There are no record to delete from file");
        }

        function fail() {
            errorCallback("Failed to load file system");
        }

        function onErrorReadFile() {
            errorCallback("Failed to read File");
        }
    }


}