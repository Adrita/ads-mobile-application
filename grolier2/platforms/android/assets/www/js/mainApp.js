var app = angular.module("ScholasticGrolier", [ 'ui.router','ngPageTitle','ngTouch',
		'ngResource', 'ui.grid', 'ui.grid.cellNav', 'ui.grid.selection',
		'Header', 'Navigator', 'SideFilterBar', 'LocalStorageModule',
		'ui.grid.autoResize', 'ngAnimate', 'ui.bootstrap', 'Login',
		'MyAccount', 'ngCookies', 'validation', 'adjustPageHeight','base64','ngCordova','ngIdle']);

app.config(['$stateProvider', '$urlRouterProvider','IdleProvider', function($stateProvider, $urlRouterProvider,IdleProvider) {

	function currentRecord()
	{
		var data = localStorageService.get('currentRecord');
		return data===null?true:false;
	}

	if(window.location.href.indexOf('login')===-1){
		IdleProvider.idle(60);
		IdleProvider.timeout(1740);
	}

	$stateProvider
		.state('Login', {
			url : "/Login",
			data: {
				pageTitle: "Login | Grolier Asia Direct Sales"
            },
			views : {
				'header' : {
					templateUrl : "app/modules/header/login-view.html"
					//templateUrl : "app/modules/header/view.html",
				},
				'main' : {
					templateUrl : "app/modules/login/view.html",
					controller : 'LoginCtrl'
				},
				'footer' : {
					templateUrl : "app/modules/footer/view.html"
				}
			}
		})


		.state('SalesRep', {
			url : "/SalesRep",
			views : {
				'header' : {
					templateUrl : "app/modules/header/view.html",
					controller : 'HeaderCtrl'
				},
				'navigator' : {
					templateUrl : "app/modules/navbar/view.html",
					controller : 'NavMenuCtrl'
				},
				'main' : {
					templateUrl : "app/modules/sales-rep/view.html"
				},
				'footer' : {
					templateUrl : "app/modules/footer/view.html"
				}
			}
		})
		.state('SalesRep.ManageTurnIn',{
			data: {
				pageTitle: "Sales Rep Turn-In | Grolier Asia Direct Sales"
            },
			url : "/ManageTurnIn",
			templateUrl : 'app/activity/sales-rep-turn-in/view.html'
		})
		.state('SalesRep.SalesRepRegistration', {
			data: {
				pageTitle: "Sales Rep Registration | Grolier Asia Direct Sales"
            },
			url : "/SalesRepRegistration",
			templateUrl : 'app/activity/sales-rep-registration/view-sales.html',
			controller : 'SalesRepRegistrationCtrl'
		})
		.state('SalesRep.Dashboard',{
			data: {
				pageTitle: "Dashboard | Grolier Asia Direct Sales"
            },
			url : "/Dashboard",
			templateUrl : 'app/activity/salesRepDashBoard/view.html'
		})
		.state('SalesRep.LoginSales', {
			url : "/LoginSales",
			data: {
				pageTitle: "Login Sales Rep| Grolier Asia Direct Sales"
            },
            templateUrl : 'app/modules/login/view.html',
			controller : 'LoginSalesCtrl'
			/*views : {
				'header' : {
					templateUrl : "app/modules/header/login-view.html",
					//templateUrl : "app/modules/header/view.html",
					controller : 'HeaderCtrl'
				},
				'main' : {
					templateUrl : "app/modules/login-sales/view.html",
					controller : 'LoginSalesCtrl'
				},
				'footer' : {
					templateUrl : "app/modules/footer/view.html"
				}
			}*/
		})
		.state('SalesRep.ManageContractSales', {
			data: {
				pageTitle: "Manage Contract Sales | Grolier Asia Direct Sales"
            },
        	url: "/ManageContractSales",
        	templateUrl: 'app/activity/sales-officer-manage-contract/view.html'
        })
        .state('SalesRep.ViewContract', {
        	data: {
				pageTitle: "Sales Rep View Contract | Grolier Asia Direct Sales"
            },
        	url: "/ViewContract",
        	templateUrl: 'app/activity/sales-officer-view-contract/view.html',
        	controller:'ViewContractCtrl'
        })

		.state('VoBoard', {
			url : "/VoBoard",
			views : {
				'header' : {
					templateUrl : "app/modules/header/view.html",
					controller : 'HeaderCtrl'
				},
				'navigator' : {
					//templateUrl : "app/modules/navbar/other-navbar-view.html",
					templateUrl : "app/modules/navbar/view.html",
					controller : 'NavMenuCtrl'
				},
				'main' : {
					templateUrl : "app/modules/vo-board/view.html"
				},
				'footer' : {
					templateUrl : "app/modules/footer/view.html"
				}
			}
		})
		.state('VoBoard.ManageVoManagerVerification', {
			data: {
				pageTitle: "Manage Vo Manager Verification | Grolier Asia Direct Sales"
            },
			url : '/ManageVoManagerVerification',
			templateUrl : 'app/activity/manage-vo-manager-verification/view.html'
		})
		.state('VoBoard.ManageVoCompleteRequest', {
			data: {
				pageTitle: "Manage Vo Complete Request | Grolier Asia Direct Sales"
            },
			url : '/ManageVoCompleteRequest',
			templateUrl : 'app/activity/manage-vo-complete-request/view.html'
		//	controller : 'ManageVoCompleteCtrl'
		})
		.state('VoBoard.ManageVoPriceDifference', {
			data: {
				pageTitle: "Manage Vo Price Difference | Grolier Asia Direct Sales"
            },
			url : '/ManageVoPriceDifference',
			templateUrl : 'app/activity/manage-vo-price-difference/view.html',
			controller : 'ManageVoPriceDifferenceCtrl'
		})
		.state('VoBoard.VoVerificationRequest', {
			data: {
				pageTitle: "Vo Verification Request | Grolier Asia Direct Sales"
            },
			url : '/VoVerificationRequest',
			templateUrl : 'app/activity/vo-verification-request/view.html'
			//controller : 'VoRequestCtrl'
		})
		/*.state('VoBoard.ManageVoTurnIn', {
			url : '/ManageVoTurnIn',
			templateUrl : 'app/activity/vo-turn-in/view.html',
			controller : 'ManageVoTurnInCtrl'
		})*/
		.state('VoBoard.VoCompleteRequest', {
			data: {
				pageTitle: "Vo Complete Request | Grolier Asia Direct Sales"
            },
			url : '/VoCompleteRequest',
			templateUrl : 'app/activity/vo-complete-request/view.html',
			controller : 'VoCompletedCtrl'
		})
		.state('VoBoard.VoVerifyContract', {
			data: {
				pageTitle: "Vo Verify Contract | Grolier Asia Direct Sales"
            },
			url : '/VoVerifyContract',
			templateUrl : 'app/activity/vo-verify-contract/view.html',
			controller : 'VoVerifyContractCtrl'
		})
		.state('VoBoard.UpdateVoPriceDifference', {
			data: {
				pageTitle: "Update Vo Price Difference | Grolier Asia Direct Sales"
            },
			url : '/UpdateVoPriceDifference',
			templateUrl : 'app/activity/update-vo-price-difference/view.html',
			controller : 'ViewContractCtrl'
		})
		.state('VoBoard.ViewVerificationReport', {
			data: {
				pageTitle: "View Verification Report | Grolier Asia Direct Sales"
            },
			url : '/ViewVerificationReport',
			//templateUrl : 'app/activity/view-verification-report/view.html',
			templateUrl : 'app/activity/view-contract/view.html',
			controller : 'ViewContractCtrl'
		})
		.state('VoBoard.VoCollectPayment', {
			data: {
				pageTitle: "Vo Collect Payment | Grolier Asia Direct Sales"
            },
			url : '/VoCollectPayment',
			templateUrl : 'app/activity/vo-collect-payment/view.html',
			controller : 'VoCollectPaymentCtrl'
		})
		.state('VoBoard.VoTurnIn', {
			data: {
				pageTitle: "Vo Turn-in | Grolier Asia Direct Sales"
            },
			url : '/VoTurnIn',
			templateUrl : 'app/activity/vo-turn-in/view.html'
		})
		.state('VoBoard.VoBankIn',{
			data: {
				pageTitle: "Vo Bank-in | Grolier Asia Direct Sales"
            },
			url:'/VoBankIn',
			templateUrl: 'app/activity/vo-bank-in/view.html'
		})
		.state('VoBoard.VoReceive',{
			data: {
				pageTitle: "Vo Receive | Grolier Asia Direct Sales"
            },
			url:'/VoReceive',
			templateUrl: 'app/activity/vo-receive/view.html'
		})
		.state('CoBoard', {
			url: "/CoBoard",
			views: {
				'header': {
					templateUrl: "app/modules/header/view.html",
					controller: 'HeaderCtrl'
				},
				'navigator': {
					//templateUrl : "app/modules/navbar/other-navbar-view.html",
					templateUrl : "app/modules/navbar/view.html",
					controller: 'NavMenuCtrl'
				},
				'main': {
					templateUrl: "app/modules/vo-board/view.html"
				},
				'footer': {
					templateUrl: "app/modules/footer/view.html"
            	}
			}
		})
        .state('CoBoard.CollectionContracts',{
        	data: {
				pageTitle: "Collection Contracts | Grolier Asia Direct Sales"
            },
        	url:'/CollectionContracts',
        	templateUrl:'app/activity/manage-co-requests/view.html'
        })
        .state('CoBoard.CompletedCollectionContracts',{
        	data: {
				pageTitle: "Completed Collection Contracts | Grolier Asia Direct Sales"
            },
        	url:'/CompletedCollectionContracts',
        	templateUrl:'app/activity/completed-collection-requests/view.html'
        })
        .state('CoBoard.ManageCollectorCollectionRequest', {
        	data: {
				pageTitle: "Manage Collector Collection Request | Grolier Asia Direct Sales"
            },
        	url: '/ManageCollectorCollectionRequest',
            templateUrl: 'app/activity/manage-collector-collection-request/view.html'
        })
        .state('CoBoard.CoCollectPayment', {
        	data: {
				pageTitle: "Co Collect Payment | Grolier Asia Direct Sales"
            },
            url: '/CoCollectPayment',
            templateUrl: 'app/activity/co-collect-payment/view.html'
        })
        .state('CoBoard.CollectionTurnIn', {
        	data: {
				pageTitle: "Collection Turn-in | Grolier Asia Direct Sales"
            },
            url: '/CollectionTurnIn',
            templateUrl: 'app/activity/collection-turn-in/view.html'
        })
        .state('CoBoard.CollectorBankIn', {
        	data: {
				pageTitle: "Collector Bank-in | Grolier Asia Direct Sales"
            },
            url: '/CollectorBankIn',
            templateUrl: 'app/activity/collector-bank-in/view.html'
        })


		.state('AdminBoard', {
			url : "/AdminBoard",
			views : {
				'header' : {
					templateUrl : "app/modules/header/view.html",
					controller : 'HeaderCtrl'
				},
				'navigator' : {
					templateUrl : "app/modules/navbar/view.html",
					controller : 'NavMenuCtrl'
				},
				'main' : {
					templateUrl : "app/modules/admin-board/view.html"

				  /*resolve: {
					  auth: ["$q", "authentication",'$state', function($q, authentication,$state) {
						  var isAuthenticated = authentication.isAuthenticated();

						  if (isAuthenticated) {
							  return $q.when(isAuthenticated);
						  } else {
							  return $q.reject($state.go('Login'));
						  }
					  }]
				  }*/

				},
				'footer' : {
					templateUrl : "app/modules/footer/view.html"
				}
			}
		})
		.state('AdminBoard.ManageAdvanceRequests',{
			data: {
				pageTitle: "Manage Advance Requests | Grolier Asia Direct Sales"
            },
			url : '/ManageAdvanceRequests',
			templateUrl : 'app/activity/manage-advance-request/view.html'
		})
		.state('AdminBoard.ManageUser', {
			data: {
				pageTitle: "Manage User | Grolier Asia Direct Sales"
            },
			url : '/ManageUser',
			templateUrl : 'app/activity/manage-user/view.html'
		})
		.state('AdminBoard.ManageRole', {
			data: {
				pageTitle: "Manage Role | Grolier Asia Direct Sales"
            },
			url : "/ManageRole",
			templateUrl : 'app/activity/manage-role/view.html'
		})
		.state('AdminBoard.SRRegRequest', {
			data: {
				pageTitle: "Registration Request | Grolier Asia Direct Sales"
            },
			url : "/SRRegRequest",
			templateUrl : 'app/activity/sr-registration-request/view.html'
		})
		.state('AdminBoard.ManageSequenceNumber', {
			data: {
				pageTitle: "Manage Sequence Generator | Grolier Asia Direct Sales"
            },
			url : '/ManageSequenceNumber',
			templateUrl : 'app/activity/manage-sequence/view.html'
		})
		.state('AdminBoard.ManageBank', {
			url : "/ManageBank",
			templateUrl : 'app/activity/manage-bank/view.html'
		})
		.state('AdminBoard.ManageAdvanceRequestsAdmin',{
			data: {
				pageTitle: "Manage Advanced Requests | Grolier Asia Direct Sales"
            },
			url : "/ManageAdvanceRequestsAdmin",
			templateUrl : 'app/activity/admin-manage-advanced-requests/view.html'
		})
		.state('AdminBoard.ManageCountry', {
			data: {
				pageTitle: "Manage Country | Grolier Asia Direct Sales"
            },
			url : "/ManageCountry",
			templateUrl : 'app/activity/manage-country/view.html'
		})
		.state('AdminBoard.ManageLocation', {
			data: {
				pageTitle: "Manage Location | Grolier Asia Direct Sales"
            },
			url : "/ManageLocation",
			templateUrl : 'app/activity/manage-location/view.html'
		})
		.state('AdminBoard.ManageRegion', {
			url : "/ManageRegion",
			templateUrl : 'app/activity/manage-region/view.html'
		})
		.state('AdminBoard.ManageDivision', {
			data: {
				pageTitle: "Manage Division | Grolier Asia Direct Sales"
            },
			url : "/ManageDivision",
			templateUrl : 'app/activity/manage-division/view.html'
		})
		.state('AdminBoard.ManageScreenAccess', {
			data: {
				pageTitle: "Manage Screen Access | Grolier Asia Direct Sales"
            },
			url : "/ManageScreenAccess",
			templateUrl : 'app/activity/manage-screen-access/view.html'
		})
		.state('AdminBoard.ManageCommissionRate', {
			data: {
				pageTitle: "Manage Commission Rate | Grolier Asia Direct Sales"
            },
			url : "/ManageCommissionRate",
			templateUrl : 'app/activity/manage-commission/view.html'
		})
		.state('AdminBoard.ManagePriceList', {
			data: {
				pageTitle: "Manage Price List | Grolier Asia Direct Sales"
            },
			url : "/ManagePriceList",
			templateUrl : 'app/activity/manage-price-list/view.html'
		})
		.state('AdminBoard.ManageSequenceGenerator', {
			data: {
				pageTitle: "Manage Sequence Genarator | Grolier Asia Direct Sales"
            },
			url : "/ManageSequenceGenerator",
			templateUrl : 'app/activity/manage-sequence/view.html'
		})
		.state('AdminBoard.ManageItem', {
			data: {
				pageTitle: "Manage Item | Grolier Asia Direct Sales"
            },
			url : "/ManageItem",
			templateUrl : 'app/activity/manage-item/view.html'
		})
		.state('AdminBoard.MyAccount', {
			data: {
				pageTitle: "My Account | Grolier Asia Direct Sales"
            },
			url : "/MyAccount",
			templateUrl : 'app/activity/account-settings/view.html'
		})
		.state('AdminBoard.CreateEditUser', {
			data: {
				pageTitle: "User | Grolier Asia Direct Sales"
            },
			url : "/CreateEditUser",
			templateUrl : 'app/activity/create-edit-user/view.html',
			controller : 'CreateEditUserCtrl'
		})
		.state('AdminBoard.CreateEditRole', {
			data: {
				pageTitle: "Role | Grolier Asia Direct Sales"
            },
			url : "/CreateEditRole",
			templateUrl : 'app/activity/create-edit-role/view.html',
			controller : 'CreateEditRoleCtrl'
		})
		.state('AdminBoard.CreateEditCountry', {
			data: {
				pageTitle: "Country | Grolier Asia Direct Sales"
            },
			url : "/CreateEditCountry",
			templateUrl : 'app/activity/create-edit-country/view.html',
			controller : 'CreateEditCountryCtrl'
		})
		.state('AdminBoard.CreateEditSequenceGenerator', {
			data: {
				pageTitle: "Sequence Generator | Grolier Asia Direct Sales"
            },
			url : "/CreateEditSequenceGenerator",
			templateUrl : 'app/activity/create-edit-sequence/view.html',
			controller : 'CreateEditSequenceNumberCtrl'
		})
		.state('AdminBoard.CreateEditLocation', {
			data: {
				pageTitle: "Location | Grolier Asia Direct Sales"
            },
			url : "/CreateEditLocation",
			templateUrl : 'app/activity/create-edit-location/view.html',
			controller : 'CreateEditLocationCtrl'
		})
		.state('AdminBoard.CreateEditBank', {
			data: {
				pageTitle: "Bank | Grolier Asia Direct Sales"
            },
			url : "/CreateEditBank",
			templateUrl : 'app/activity/create-edit-bank/view.html',
			controller : 'CreateEditBankCtrl'
		})
		.state('AdminBoard.CreateEditPriceList', {
			data: {
				pageTitle: "Price List | Grolier Asia Direct Sales"
            },
			url : "/CreateEditPriceList",
			templateUrl : 'app/activity/create-edit-price-list/view.html',
			controller : 'CreateEditPriceListCtrl'
		})
		.state('AdminBoard.EditItem', {
			data: {
				pageTitle: "Item | Grolier Asia Direct Sales"
            },
			url : "/EditItem",
			templateUrl : 'app/activity/edit-item/view.html',
			controller : 'EditItemCtrl'
		})
		.state('AdminBoard.Notification', {
			data: {
				pageTitle: "Notification | Grolier Asia Direct Sales"
            },
			url : "/Notification",
			templateUrl : 'app/activity/notification/view.html',
			controller : 'NotificationCtrl'
		})
		.state('AdminBoard.CreateEditRegion', {
			data: {
				pageTitle: "Region | Grolier Asia Direct Sales"
            },
			url : "/CreateEditRegion",
			templateUrl : 'app/activity/create-edit-region/view.html',
			controller : 'CreateEditRegionCtrl'
		})
		.state('AdminBoard.CreateEditDivision', {
			data: {
				pageTitle: "Division" +
						" | Grolier Asia Direct Sales"
            },
			url : "/CreateEditDivision",
			templateUrl : 'app/activity/create-edit-division/view.html',
			controller : 'CreateEditDivisionCtrl'
		})
		.state('AdminBoard.CreateEditCommissionRate', {
			data: {
				pageTitle: "Comission Rate | Grolier Asia Direct Sales"
            },
			url : "/CreateEditCommissionRate",
			templateUrl : 'app/activity/create-edit-commission/view.html',
			controller : 'CreateEditCommissionRateCtrl'
		})
		.state('AdminBoard.CreateEditScreenAccess', {
			data: {
				pageTitle: "Screen Access | Grolier Asia Direct Sales"
            },
			url : "/CreateEditScreenAccess",
			templateUrl : 'app/activity/create-edit-screen/view.html',
			controller : 'CreateEditScreenAccessCtrl'
		})
		.state('AdminBoard.CreateEditUserType', {
			data: {
				pageTitle: "User Type | Grolier Asia Direct Sales"
            },
			url : "/CreateEditUserType",
			templateUrl : 'app/activity/create-edit-user-type/view.html',
			controller : 'CreateEditUserTypeCtrl'
		})
		.state('AdminBoard.ManageUserType', {
			data: {
				pageTitle: "Manage User | Grolier Asia Direct Sales"
            },
			url : '/ManageUserType',
			templateUrl : 'app/activity/manage-user-type/view.html'
		})
		.state('AdminBoard.ManageContract', {
			data: {
				pageTitle: "Manage Contracts | Grolier Asia Direct Sales"
            },
			url : '/ManageContract',
			templateUrl : 'app/activity/manage-contract/view.html'
		})
		.state('AdminBoard.CreateEditContract', {
			data: {
				pageTitle: "Contract | Grolier Asia Direct Sales"
            },
			url : '/CreateEditContract',
			templateUrl : 'app/activity/create-edit-contract/view.html',
			controller : 'CreateEditContractCtrl'
		})
		.state('AdminBoard.ViewContract', {
			data: {
				pageTitle: "Contract | Grolier Asia Direct Sales"
            },
			url : '/ViewContract',
			templateUrl : 'app/activity/view-contract/view.html',
			controller : 'ViewContractCtrl'
		})
		.state('AdminBoard.CreateEditCustomer', {
			data: {
				pageTitle: "Customer | Grolier Asia Direct Sales"
            },
			url : '/CreateEditCustomer',
			templateUrl : 'app/activity/create-edit-customer/view.html',
			controller : 'CreateCustomerCtrl'
		})
		.state('AdminBoard.EditCustomer', {
			data: {
				pageTitle: "Customer | Grolier Asia Direct Sales"
            },
			url : '/EditCustomer',
			templateUrl : 'app/activity/edit-customer/view.html'
		})
		.state('AdminBoard.ManageCustomer', {
			data: {
				pageTitle: "Manage Customer | Grolier Asia Direct Sales"
            },
			url : '/ManageCustomer',
			templateUrl : 'app/activity/manage-customer/view.html'
		})
		.state('AdminBoard.ManageRegistration', {
			data: {
				pageTitle: "Manage Registration | Grolier Asia Direct Sales"
            },
			url : "/ManageRegistration",
			templateUrl : 'app/activity/manage-registration/view.html'
		})
		.state('AdminBoard.WelcomeGrolier', {
			data: {
				pageTitle: "Welcome Grolier | Grolier Asia Direct Sales"
            },
			url : "/WelcomeGrolier",
			templateUrl : 'app/activity/welcome-grolier/view.html',
			controller : 'WelcomeGrolierCtrl'
		})
		.state('AdminBoard.NewSalesRep', {
			data: {
				pageTitle: "New Sales Rep | Grolier Asia Direct Sales"
            },
			url : '/NewSalesRep',
			templateUrl : 'app/activity/new-sales-rep/view.html',
			controller : 'SalesRepCtrl'
		})
		.state('AdminBoard.AdminTurnIn', {
			data: {
				pageTitle: "Admin Turn In | Grolier Asia Direct Sales"
            },
			url : '/AdminTurnIn',
			templateUrl : 'app/activity/admin-turn-in/view.html'
		})
		.state('AdminBoard.AdminClearBankIn',{
			data: {
				pageTitle: "Admin Clear Bank-in | Grolier Asia Direct Sales"
            },
			url:'/AdminClearBankIn',
			templateUrl: 'app/activity/admin-clear-bank-in/view.html'
		})
		.state('AdminBoard.AdminReceiveBankIn',{
			data: {
				pageTitle: "Admin Receive Bank-in | Grolier Asia Direct Sales"
            },
			url:'/AdminReceiveBankIn',
			templateUrl: 'app/activity/admin-receive-bank-in/view.html'
		})
		.state('AdminBoard.CancelContract',{
			data: {
				pageTitle: "Cancel Contract | Grolier Asia Direct Sales"
            },
			url:'/CancelContract',
			templateUrl: 'app/activity/cancel-contract/view.html',
			controller : 'CancelContractCtrl'
		})
		.state('AdminBoard.HouseSales',{
			data: {
				pageTitle: "House Sales | Grolier Asia Direct Sales"
            },
			url:'/HouseSales',
			templateUrl: 'app/activity/house-sales/view.html'
		})
		.state('AdminBoard.CreateHouseSales',{
			data: {
				pageTitle: "Create House Sales | Grolier Asia Direct Sales"
            },
			url:'/CreateHouseSales',
			templateUrl: 'app/activity/create-house-sales/view.html',
			controller : 'HouseSalesCtrlCreate'
		})
		.state('AdminBoard.CreateAdvanceRequest',{
			data: {
				pageTitle: "Create Advance Request | Grolier Asia Direct Sales"
            },
            url:'/CreateAdvanceRequest',
            templateUrl: 'app/activity/advanceReequestForm/view.html'
		})
		.state('AdminBoard.ManageCommissionDetails',{
			data: {
				pageTitle: "Manage Commission Details | Grolier Asia Direct Sales"
            },
            url:'/ManageCommissionDetails',
            templateUrl: 'app/activity/manage-commission-details/view.html'
		})
		.state('AdminBoard.ViewHouseSale',{
			data: {
				pageTitle: "View House Sale | Grolier Asia Direct Sales"
            },
			url:'/ViewHouseSale',
			templateUrl: 'app/activity/view-house-sale/view.html',
			controller : 'HouseSalesCtrlEdit'
		})


	$urlRouterProvider.otherwise('Login');
} ]);

app.run(['$timeout', '$rootScope', '$location', '$state', 'authentication', 'localize', 'localStorageService', 'commonServices','userRole','Idle', function($timeout, $rootScope, $location, $state, authentication, localize, localStorageService, commonServices,userRole,Idle) {

	localize.setLocalizedResources();

	$rootScope.previousStateTitle;
	$rootScope.previousState;
	$rootScope.currentState;

	// enumerate routes that don't need authentication
	var routesThatDontRequireAuth = [ 'Login','SalesRep.LoginSales','SalesRep.SalesRepRegistration','AdminBoard.CreateHouseSales'];

	// check if current location matches route
	var routeClean = function(route) {
		if(route.name !== undefined && routesThatDontRequireAuth.indexOf(route.name) === -1) return true;
		return false; //for routesThatDontRequireAuth list
	};

	var startChanged=false;
    var changeSuccess=false;

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

		//alert("$stateChangeSuccess");
		//Previous Manage page heading


		var title = $(".raleway_heading:not(.print_only):eq(0)").text();
		$rootScope.previousStateTitle = (title === "")?commonServices.getTitle(fromState.name):title;
		$rootScope.previousState = fromState.name;
	    $rootScope.currentState = toState.name;
	    localStorageService.set('filterObj',null);

	    if(startChanged && $rootScope.currentState==='Login'){
                    return;
                 }
                  startChanged=true;

		// if route requires auth and user is not logged in
		if (routeClean(toState) && !authentication.isAuthorised(toState.name)) {
			event.preventDefault();

			localStorageService.set('unauthorisedAccess', true);
			$rootScope.$broadcast('unauthorisedAccess', true);

			// redirect back to login
			$state.go('Login');
		}else if (!routeClean(toState) && authentication.isAuthenticated()) {
         			event.preventDefault();

         			var a = userRole.defaultLangingPage();

         			if(a!==null && a!==undefined && a!=""){
         			$state.go(a);
         			}

         		}

		/*if(toState.name==='SalesRep.LoginSales'){
			$state.go('SalesRep.LoginSales');
		}*/
	});

	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		//alert("$stateChangeSuccess");

		if(changeSuccess && toState.name==='Login'){
                    return;
                    }
                changeSuccess=true;
		$timeout(function() {
			  $rootScope.title = 'Default title';
			  if (toState.data && toState.data.pageTitle) {
				  //var title = toState.data.pageTitle;
				  //var chTitle = title.replace('Asia', $rootScope.regionName);
				  $rootScope.title = toState.data.pageTitle;

				  if($state.current.name.indexOf('Login')<0){
				  	Idle.watch();
				  }

			  }
		  });
	});

} ]);

app.factory('ModalService', function() {
	var regArr = [];

	return {
		regArr : regArr
	}
});
app.factory('RequestService', function() {
	var dataArr = [];

	return {
		dataArr : dataArr
	}
});
