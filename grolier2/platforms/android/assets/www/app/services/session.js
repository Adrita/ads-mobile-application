app.service('Session', function ($cookies, localStorageService) {
	
	//function setPageRefreshData () {
		var userSes = $cookies.getObject('globalUser');
		if (angular.isDefined(userSes)) {
			this.id = userSes.id;
			this.userId = userSes.userId;
			this.userRole = userSes.userRole;
		};
		
		//var userRoleRouteMap = $cookies.getObject('globalUserRoles');
		var userRoleRouteMap = localStorageService.get('globalUserRoles');
		if (angular.isDefined(userRoleRouteMap) && userRoleRouteMap !== null && userRoleRouteMap !== "") {
			
			this.userGlobalRole = userRoleRouteMap.userInfo.userDetails;
			this.userRoleName = userRoleRouteMap.userInfo.userDetails.roleName;
			this.userType = userRoleRouteMap.userInfo.userDetails.userType;
			
		} else this.userGlobalRole = {};
		
		var contractViewAccess = $cookies.getObject('contractViewAccess');
		if (angular.isDefined(contractViewAccess)) {
			this.contractViewAccess = contractViewAccess;
		};

		var userViewAccess = $cookies.getObject('userViewAccess');
		if (angular.isDefined(userViewAccess)) {
			this.userViewAccess = userViewAccess;
		};
	//};
	//setPageRefreshData ();
	
	this.create = function(sessionId, userId, userRole) {
		this.id = sessionId;
		this.userId = userId;
		this.userRole = userRole;
		
		$cookies.putObject('globalUser', {id: sessionId, userId: userId, userRole: userRole});
	};
	this.createGlobalRoles = function (userRole) {
		this.userGlobalRole = userRole;
		this.userRoleName = userRole.roleName;
		this.userType = userRole.userType;
		
		//$cookies.putObject('globalUserRoles', userRole);
	};
	this.createContractViewAccess = function (roleData) {
		this.contractViewAccess = roleData;
		$cookies.putObject('contractViewAccess', roleData);
	};
	this.createUserViewAccess = function (roleData) {
		this.userViewAccess = roleData;
		$cookies.putObject('userViewAccess', roleData);
	};
	this.destroy = function() {
		this.id = null;
		this.userId = null;
		this.userRole = null;
		
		$cookies.remove('globalUser');
		$cookies.remove('contractViewAccess');
		$cookies.remove('userViewAccess');
		
		localStorageService.set('configData', null);
		localStorageService.set('globalUserRoles', null);
	};
	this.storeAuthData = function (storeKey, data) {
		localStorageService.set(storeKey, data);
	};
});