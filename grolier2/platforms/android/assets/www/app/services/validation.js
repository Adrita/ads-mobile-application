angular.module('validation',[]);
angular.module('validation').service('validate', function() {
			
		    this.validateText = function(text) {
		    		if(text === "" || text === undefined || text === null)
		            return false;
		    		else
		    		return true;
		        };
		    this.validateEmail= function(email){
		    	var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
		    	return pattern.test(email);
		    };
		    this.validateAlphaNumeric =function(ic){
		    	var pattern=/^[A-Z0-9]+$/;
		    	return pattern.test(ic);
		    };
		    this.validateDate=function(currVal){
		    	  if (currVal === '' || currVal===undefined || currVal===null) 
		    		  return false;

		    	   
		    	   var rxDatePattern = /^(\d{1,2})(\/|-)([a-zA-Z]{3})(\/|-)(\d{4})$/;

		    	   var dtArray = currVal.match(rxDatePattern); // is format OK?

		    	   if (dtArray === null) return false;

		    	   var dtDay = parseInt(dtArray[1]);
		    	   var dtMonth = dtArray[3];
		    	   var dtYear = parseInt(dtArray[4]);
		    	   
		    	   
		    	   switch (dtMonth.toLowerCase()) {
		    	       case 'jan':
		    	           dtMonth = '01';
		    	           break;
		    	       case 'feb':
		    	           dtMonth = '02';
		    	           break;
		    	       case 'mar':
		    	           dtMonth = '03';
		    	           break;
		    	       case 'apr':
		    	           dtMonth = '04';
		    	           break;
		    	       case 'may':
		    	           dtMonth = '05';
		    	           break;
		    	       case 'jun':
		    	           dtMonth = '06';
		    	           break;
		    	       case 'jul':
		    	           dtMonth = '07';
		    	           break;
		    	       case 'aug':
		    	           dtMonth = '08';
		    	           break;
		    	       case 'sep':
		    	           dtMonth = '09';
		    	           break;
		    	       case 'oct':
		    	           dtMonth = '10';
		    	           break;
		    	       case 'nov':
		    	           dtMonth = '11';
		    	           break;
		    	       case 'dec':
		    	           dtMonth = '12';
		    	           break;
		    	   }

		    	   
		    	   dtMonth = parseInt(dtMonth);
		    	   
		    	   if (isNaN(dtMonth)) return false;
		    	   else if (dtMonth < 1 || dtMonth > 12) return false;
		    	   else if (dtDay < 1 || dtDay > 31) return false;
		    	   else if ((dtMonth === 4 || dtMonth === 6 || dtMonth === 9 || dtMonth === 11) && dtDay === 31) return false;
		    	   else if (dtMonth === 2) {
		    	       var isleap = (dtYear % 4 === 0 && (dtYear % 100 !== 0 || dtYear % 400 === 0));
		    	       if (dtDay > 29 || (dtDay === 29 && !isleap)) return false;
		    	   }

		    	   return true;
		    };
		    this.validateDecimal=function(num)
		    {
		    	num=parseFloat(num);
		    	return ((num - 0) === num && (''+num).trim().length > 0);

		    };
		    this.validateInteger=function(value) {
		    	  var x;
		    	  if(value === "" || value === undefined || value === null)
			            return false;
		    	  for (i = 0 ; i < value.length ; i++) { 
		    		     if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false 
		    		   } 
		    		  return true; 
		    };
		    this.validateMobile=function(number)
		    {
		    	if(number === "" || number === undefined || number === null)
		    	return false;
		    	var res = number.charAt(0);
		    	if(res!=='+')
		    		return false;
		    	for(var i=1;i<number.length;i++)
		    		{
		    			res=number.charAt(i);
		    			if(res==='-' || res==='0' ||res==='1' ||res==='2' ||res==='3' ||res==='4' ||res==='5' ||res==='6' ||res==='7' ||res==='8' ||res==='9')
		    				return true;
		    			else
		    				return false;
		    		}
		    	return true;
		    }
		   this.mobileNumber=function(no)
		   {
			   if(no === "" || no === undefined || no === null)
			    	return false;
			   var phone=/^\+\(?([0-9]*)\)?[-]([0-9]{1,})$/;
			   if(no.match(phone))
			 return true;
			 else return false;
		   }
		   this.mobileNumber2=function(no)
		   {
			   if(no === "" || no === undefined || no === null)
			    	return false;
			   var phone=/^[0-9]+$/;
			   var phone1=/^(?!0*$).*$/;
			   if(no.match(phone) && no.match(phone1))
			 return true;
			 else return false;
		   }
		   this.phonenumber= function(inputtxt) {
			   if(inputtxt === "" || inputtxt === undefined || inputtxt === null)
			    	return false;
		    	  /*var phoneno = /^\+\(?([0-9]*)\)?[-]([0-9]{1,})[-]([0-9]{1,})$/;
		    	  if(inputtxt.match(phoneno)) {
		    	    return true;
		    	  }
		    	  else {
		    	    return false;
		    	  }*/
			   	/*for(var i=0;i<inputtxt.length;i++)
			   		{
			   			var x=inputtxt.charAt(i);
			   			if(x=='+'||x=='-'||x=='1'||x=='2'||x=='3'||x=='4'||x=='5'||x=='6'||x=='7'||x=='8'||x=='9'||x=='0')
			   			{}
			   			else
			   				return false;
			   		}
			   	return true;*/
			   		var phoneNO=inputtxt;
			   		var result=/^[0-9]+$/.test(phoneNO);
			   		var result1=/^(?!0*$).*$/.test(phoneNO);
			   		return result && result1;
		    	}
		});