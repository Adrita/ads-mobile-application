app.factory('stickyHeader', ['$http', '$q', function ($http, $q) {
	var $window = $(window),
      $stickies,
      $navSticky = $(".sticky-nav"),
      $stickyNavPosition,
      isNavPosSet = false;

	var load = function(stickies, heightArr) {
		
	  if (!isNavPosSet && typeof $navSticky === "object" && $navSticky instanceof jQuery && $navSticky.length > 0) {
		  $stickyNavPosition = $navSticky.offset().top;
		  isNavPosSet = true;
	  }
	  
	  if (typeof stickies === "object" && stickies instanceof jQuery && stickies.length > 0) {
	
	      $stickies = stickies.each(function(key, value) {
	    	
	    	var $thisSticky = $(this);
	    	var width = $thisSticky.parents('.review-container').width()+"px";
	    	
	    	if ($thisSticky.parent().attr("class") !== "followWrap")
	    		$thisSticky = $(this).wrap('<div class="followWrap" />');
	    	
	        $thisSticky.removeClass("fixed").removeAttr("style");
	        $thisSticky.removeClass("absolute").removeAttr("style");
	        
	        $thisSticky
	            .data('originalPosition', $thisSticky.offset().top)
	            .data('originalHeight', $thisSticky.outerHeight())
	            .data('containerHeight', $thisSticky.parents('.review-container').outerHeight(true))
	              /*.css("width", width)*/
	              .parent()
	              .height($thisSticky.outerHeight());
	      });
	
	      $window.off("scroll.stickies").on("scroll.stickies", function() {
			  _whenScrolling();		
	      });
	    }
	};

	var _whenScrolling = function() {
		
		if ($stickyNavPosition <= $window.scrollTop()) {
			$navSticky.addClass("fixed");
			$(".sticky-nav").addClass("fixed");
			
		}
	    else {
	    	$navSticky.removeClass("fixed");
	    	$(".sticky-nav").removeClass("fixed");
	    }
		
	    $stickies.each(function(i) {			
	
	      var $thisSticky = $(this),
	          $stickyPosition = $thisSticky.data('originalPosition');
	
	      if ($stickyPosition <= $window.scrollTop()) {        
	        
	        var $nextSticky = $stickies.eq(i + 1),
	            $nextStickyPosition = $nextSticky.data('originalPosition') - $thisSticky.data('originalHeight');
	
	        $thisSticky.addClass("fixed");
	
	        if ($nextSticky.length > 0 && $thisSticky.offset().top >= $nextStickyPosition) {
	
	        	//$thisSticky.addClass("absolute").css("top", $nextStickyPosition);
	        	$stickySetPosition = $thisSticky.data('containerHeight') - $thisSticky.data('originalHeight') - 4;
	        	$thisSticky.addClass("absolute").css("top", $stickySetPosition);
	        }
	
	      } else {
	        
	        var $prevSticky = $stickies.eq(i - 1);
	
	        $thisSticky.removeClass("fixed");
	
	        if ($prevSticky.length > 0 && $window.scrollTop() <= $thisSticky.data('originalPosition') - $thisSticky.data('originalHeight')) {
	
	          $prevSticky.removeClass("absolute").removeAttr("style");
	        }
	      }
	    });
	};

	return {
	  enableStickyHeader: load
	};
}]);

