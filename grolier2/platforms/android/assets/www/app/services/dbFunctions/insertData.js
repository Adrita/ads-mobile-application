app.factory('insertData', [function () {

var insertData={};

var options = { dimBackground: true };


insertData.insertCountryData=function(countries){



for(var i=0;i<countries.length;i++){

//if(i==countries.length-1){
//SpinnerPlugin.activityStop();
//}

	app.insertCountryRow(countries[i]);

}

}

insertData.insertBankData=function(banks){

for(var i=0;i<banks.length;i++){
//if(i==0){
//SpinnerPlugin.activityStart("Sync in Progress...Bank Details", options);
//}
//if(i==banks.length-1){
// SpinnerPlugin.activityStop();
// }

	app.insertBankRow(banks[i]);

}

}

insertData.insertStateData=function(states){

for(var i=0;i<states.length;i++){

//if(i==0){
//SpinnerPlugin.activityStart("Sync in Progress...State Details", options);
//
//}else
//if(i==states.length-1){
// SpinnerPlugin.activityStop();
// }

	app.insertStateRow(states[i]);

}

}

insertData.insertPincodeData=function(pincodes){

for(var i=0;i<pincodes.length;i++){

//if(i==0){
//SpinnerPlugin.activityStart("Sync in Progress...Pincode Details", options);
//
//}
//else
//if(i==pincodes.length-1){
// SpinnerPlugin.activityStop();
// }

	app.insertPincodeRow(pincodes[i]);

}

}


insertData.insertPriceListData=function(priceList){
	for(var i=0;i<priceList.length;i++){

	//console.log("Price list"+ priceList[i].priceListCode);

//	if(i==0){
//             SpinnerPlugin.activityStart("Sync in Progress...Pricelist Details", options);
//
//    }
//    else
//if(i==priceList.length-1){
//     SpinnerPlugin.activityStop();
//     }

	app.insertPriceListRow(priceList[i]);

}
}

insertData.insertItemData=function(itemList){

	for(var i=0;i<itemList.length;i++){
		app.insertItemListRow(itemList[i]);
	}


}

insertData.insertPaymentMethod=function(paymentMethodList){
	for(var i=0;i<paymentMethodList.length;i++){
		app.insertPaymentMethod(paymentMethodList[i]);
	}
}

insertData.insertItemDetailsData=function(itemDetails){

	for(var i=0;i<itemDetails.length;i++){
		app.insertItemDetailsRow(itemDetails[i]);
	}


}

insertData.insertLastUpdatedRow=function(result){
	app.insertLastUpdatedRow(result);
	//SpinnerPlugin.activityStop();

}

insertData.insertLocationData=function(locationList){
	for(var i=0;i<locationList.length;i++){
		if(i==locationList.length-1){
     		//SpinnerPlugin.activityStop();
     	}

	app.insertLocationListRow(locationList[i]);

	}
}

insertData.isTableExists=function(tableName){
		var db = window.sqlitePlugin.openDatabase({name: "Grolier.db"});
		db.transaction(function(tx) {
                    tx.executeSql("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", [], function(tx, rs) {
                      ////console.log('Record length: ' + rs.rows.length);
                      if(rs.rows.length>0){
                      	return true;
                      }else{
                      	return false;
                      }
                    }, function(tx, error) {
                      console.log('SELECT error: ' + error.message);

                    });
                  });

}


return insertData;
}])