app.controller( "LoginSalesCtrl", ['$scope','$rootScope','$state','authentication','localStorageService','$cookies','$cookieStore','$window','validate','$base64','adapter', function($scope, $rootScope, $state,authentication,localStorageService,$cookies,$cookieStore,$window,validate,$base64,adapter)
{
	$scope.userNameError=true;
	$scope.passwordError=true;
	$scope.unAuth=true;
	$('.mainContent').css('padding-top','44px');
	$scope.login="Login";
	if($cookieStore!=null){
		$scope.UserName=$cookieStore.get('loggedUser');
		$scope.Password=$cookieStore.get('password');
		$scope.rememberMe=true;
		$scope.login="Login";
		$scope.loginName=true;
	}
	$scope.myFunc=function()
	{
		if(checkValidations())
		login();
	}
	 $scope.LoginAuthentication=function(){
	    	$scope.loading=false;
	    	$scope.loginName=true;
	    	$scope.loader = false;
			$scope.unAuth=true;
	    	if(checkValidations()){

				$scope.loading=true;
				$scope.loginName=false;
	    		
	    	type="salesRepLogin";
	    		getServiceCall();
	    	}
	    	
		}
	 
	 function checkValidations()
	    {
	    	$scope.userNameError=validate.validateText($scope.UserName);
	    	$scope.passwordError=validate.validateText($scope.Password);
	    	/*$scope.loginName=true;
	    	$scope.loading=false;*/
	    	var result=$scope.userNameError && $scope.passwordError;
	    	return  result;
	    	
	    	/*if(result.checkValidations==true){
	        	$scope.loading=true;
	         	$scope.loginName=false;
	        }*/
	    }
	 function getServiceCall(){
		   if(type==='salesRepLogin'){
				var data={
						userName : $scope.UserName,
	    			    password: $scope.Password
				}
				url="SalesRepController/salesRepLogin";
		   }
		   
		   $scope.EncryptAuthInfo=$base64.encode($scope.UserName+":"+$scope.Password);				
   		config = {		
   			headers: {		
   				'Authorization': $scope.EncryptAuthInfo		
   			}		
           };
   		localStorageService.set('configData',config);
		   
		   adapter.getServiceData(url,data).then(success,error);
		   function success(result){
		        if(result.statusCode === 200){
		        	console.log(result);
		        	if(type==='salesRepLogin'){
		        		if($scope.rememberMe){
    						$cookies.loggedUser = $scope.UserName;
    						$cookies.password = $scope.Password;
    						$cookieStore.put('loggedUser',$cookies.loggedUser);
    						$cookieStore.put('password',$cookies.password);
    						/*localStorageService.set('loggedUser',$scope.UserName);
    						localStorageService.set('password',$scope.Password);*/
    						
    					} else if ($scope.UserName === $cookieStore.get('loggedUser')) {
    						$cookies.remove('loggedUser');
    						$cookies.remove('password');
    						$cookieStore.remove('loggedUser');
    						$cookieStore.remove('password');
    					}
		        		/*$scope.sales.srName=result.srLoginResponse.srName;
		        		$scope.sales.email=result.srLoginResponse.srEmail;
		        		$scope.imdSupVisorName=result.srLoginResponse.imdSupVisorName;
		        		$scope.imdSupVisorCode=result.srLoginResponse.imdSupVisorCode;
		        		$scope.sales.divisionCode=result.srLoginResponse.divisionCode;
		        		$scope.sales.divisionalManagerCode=result.srLoginResponse.divisionalManagerCode;
		        		$scope.sales.country=result.srLoginResponse.countryName;
		        		$scope.sales.idCard="data:image/png;base64iVBORw0KGgoAAAANSUhEUgAAAHgAAABaCAYAAABzAJ ... ynyP/WPlf+c+Rf6z8r/znyD9W/lf+I0QH/wenqneXabJbJwAAAABJRU5ErkJggg==";
		        		$scope.sales.profilePic="data:image/png;base64iVBORw0KGgoAAAANSUhEUgAAAHgAAABaCAYAAABzAJ ... ynyP/WPlf+c+Rf6z8r/znyD9W/lf+I0QH/wenqneXabJbJwAAAABJRU5ErkJggg==";
		        		var stateNameList=[];
		        		for(var i=0;i<result.srLoginResponse.states.length;i++){
		        			stateNameList.push(result.srLoginResponse.states[i].stateName);
		        		}
		        		$scope.stateList=stateNameList;*/
		        		
		        		$state.go('SalesRep.SalesRepRegistration');
		        	}
		        }else{
		        	$scope.unAuth=false;
		        	$scope.loginErMsg=result.message;
		        }
		   }function error(){
		   	   console.log("Service not resolved");
			   
		      }
	}
}]);