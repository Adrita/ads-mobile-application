app.controller( "footerController", ['$scope','$rootScope','$state','authentication','localStorageService','$cookies','$cookieStore','$window','validate','$base64','adapter','$timeout','$cordovaFileTransfer', function($scope, $rootScope, $state,authentication,localStorageService,$cookies,$cookieStore,$window,validate,$base64,adapter,$timeout,$cordovaFileTransfer)
{

var options = { dimBackground: true };


$scope.getPrivacyPolicy=function(){

	//var url="file:///android_asset/www/distribution/data/Contract-Terms-&-Conditions.pdf";
	var url="file:///android_asset/www/distribution/data/PersonalDataProtection.html";
	//var url="http://cdn.wall-pix.net/albums/art-space/00030109.jpg";
	var targetPath=cordova.file.externalRootDirectory+"PersonalDataProtection.html";
	//var targetPath=cordova.file.externalRootDirectory+"testImage.jpg";

	var trustHosts= true;
	var options={};


	var permissions=cordova.plugins.permissions;


	permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE,function(status){
		if(status.hasPermission){
		$scope.downloadStart=true;
			$cordovaFileTransfer.download(url,targetPath,options,trustHosts).then(function(results){
            	SpinnerPlugin.activityStop();

            	$scope.downloadStart=false;
            	alert("Privacy policy successfully downloaded in sd card as a html file");
            		console.log("success");
            	},function (err){
            	$scope.downloadStart=false;
            		SpinnerPlugin.activityStop();
            		console.log("Error");

            	},function(progress){
            		$timeout(function(){
            			$scope.downloadProgress= (progress.loaded/progress.total)*100;

            			if($scope.downloadStart){
            				SpinnerPlugin.activityStart("Downloading....", options);
            			}

            		});
            	});

		}else{
			permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE,success,error);

			function error(){
				alert("permission not there");
			}

			function success(status){
				if(!status.hasPermission){
					error();
				}
				$scope.downloadStart=true;
				$cordovaFileTransfer.download(url,targetPath,options,trustHosts).then(function(results){
                            	SpinnerPlugin.activityStop();

                            	$scope.downloadStart=false;
                            	alert("Privacy Policy has been successfully downloaded in sd card");
                            		console.log("success");
                            	},function (err){
                            	$scope.downloadStart=false;
                            		SpinnerPlugin.activityStop();
                            		console.log("Error");

                            	},function(progress){
                            		$timeout(function(){
                            			$scope.downloadProgress= (progress.loaded/progress.total)*100;

                            			if($scope.downloadStart){
                            				SpinnerPlugin.activityStart("Downloading....", options);
                            			}

                            		});
                            	});

			}
		}

	})
}


$scope.getTermsnUse=function(){
//var url="file:///android_asset/www/distribution/data/Contract-Terms-&-Conditions.pdf";
	var url="file:///android_asset/www/distribution/data/GrolierTnC.html";
	//var url="http://cdn.wall-pix.net/albums/art-space/00030109.jpg";
	var targetPath=cordova.file.externalRootDirectory+"GrolierTnC.html";
	//var targetPath=cordova.file.externalRootDirectory+"testImage.jpg";

	var trustHosts= true;
	var options={};


	var permissions=cordova.plugins.permissions;


	permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE,function(status){
		if(status.hasPermission){
		$scope.downloadStart=true;
			$cordovaFileTransfer.download(url,targetPath,options,trustHosts).then(function(results){
            	SpinnerPlugin.activityStop();

            	$scope.downloadStart=false;
            	alert("Terms of use has been successfully downloaded in sd card as a html file");
            		console.log("success");
            	},function (err){
            	$scope.downloadStart=false;
            		SpinnerPlugin.activityStop();
            		console.log("Error");

            	},function(progress){
            		$timeout(function(){
            			$scope.downloadProgress= (progress.loaded/progress.total)*100;

            			if($scope.downloadStart){
            				SpinnerPlugin.activityStart("Downloading....", options);
            			}

            		});
            	});

		}else{
			permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE,success,error);

			function error(){
				alert("permission not there");
			}

			function success(status){
				if(!status.hasPermission){
					error();
				}
				$scope.downloadStart=true;
				$cordovaFileTransfer.download(url,targetPath,options,trustHosts).then(function(results){
                            	SpinnerPlugin.activityStop();

                            	$scope.downloadStart=false;
                            	alert("Contract-Terms-&-Conditions.pdf successfully downloaded in sd card");
                            		console.log("success");
                            	},function (err){
                            	$scope.downloadStart=false;
                            		SpinnerPlugin.activityStop();
                            		console.log("Error");

                            	},function(progress){
                            		$timeout(function(){
                            			$scope.downloadProgress= (progress.loaded/progress.total)*100;

                            			if($scope.downloadStart){
                            				SpinnerPlugin.activityStart("Downloading....", options);
                            			}

                            		});
                            	});

			}
		}

	})

}






}]);