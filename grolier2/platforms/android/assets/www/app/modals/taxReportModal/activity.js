app.controller('taxStatementModalCtrl',['$scope','$http','$location','$uibModalInstance','localStorageService','adapter',function ($scope,$http,$location,$uibModalInstance,localStorageService,adapter) {
  
  $scope.submitPressed=false;
  $scope.customerStatement={};
  var reqURL="";
  $scope.salesRepList=[];
  $scope.salesRepSuggestionList=[];
  $scope.showSalesRepSuggestion=false;
  adapter.getServiceData("ReportController/getSalesRepDetails",{}).then(success,error);
  function success(result){
	  $.each(result.salesRepMAP, function(i, val) {
		  $scope.salesRepList.push(val)
		});
  }
  function error(){
	  $uibModalInstance.dismiss();
  }
  $scope.close= function () {
	  $scope.submitPressed=true;
	if($scope.taxStatementForm.$valid)
		{
			reqURL="ReportController/getSalesManCommissionTax?salesPersonName="+$scope.salesRepName;
			adapter.getReport(reqURL);
			$uibModalInstance.close($scope.contractCode);
		}
    
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };
  $scope.getsalesRepSuggestionList=function(){
	  $scope.showSalesRepSuggestion=false;
	  $scope.salesRepSuggestionList=[];
	  if($scope.salesRepName!==null && $scope.salesRepName!==undefined && $scope.salesRepName!=="")
		  {
		  		for(var i=0;i<$scope.salesRepList.length;i++)
		  			{
		  				if($scope.salesRepList[i].toLowerCase().indexOf($scope.salesRepName.toLowerCase())!==-1)
		  					$scope.salesRepSuggestionList.push($scope.salesRepList[i]);
		  			}
		  		if($scope.salesRepSuggestionList.length>0)
		  			$scope.showSalesRepSuggestion=true;
		  }
  }
  $scope.setSalesrepName=function(SalesRepName)
  {
	  $scope.salesRepName=SalesRepName;
	  $scope.showSalesRepSuggestion=false;
  }
}]);