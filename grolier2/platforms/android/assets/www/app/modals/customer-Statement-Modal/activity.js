app.controller('adminDashBoardModals',['$scope','$uibModalInstance','adapter',function ($scope,$uibModalInstance,adapter) {
  
  $scope.contractCodeList=[];
  $scope.submitPressed=false;
  $scope.contractSuggestion=false;
  $scope.customerStatement={};
  $scope.contractList=[];
  $scope.contractSuggestionList=[];
  adapter.getServiceData("ReportController/getContractDetails",{}).then(success,error);
	function success(result){
		$scope.contractList=result.contractLst;
	}
	function error()
	{
		$uibModalInstance.dismiss();
	}
  $scope.close= function () {
	  $scope.submitPressed=true;
	if($scope.customerStatement.$valid)
		{
			reqURL="ReportController/getCustomerStatementOnRequest?contractNo="+$scope.contractCode;
			adapter.getReport(reqURL);
			$uibModalInstance.close($scope.contractCode);
		}
    
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss();
  };
  $scope.getContractSuggestionList=function()
  {
	  $scope.contractSuggestionList=[];
	  $scope.contractSuggestion=false;
	  if($scope.contractCode!==null && $scope.contractCode!==undefined && $scope.contractCode!=="")
		  {
		  	for(var i=0;i<$scope.contractList.length;i++)
		  		{
		  		if($scope.contractList[i].contractCode.toLowerCase().indexOf($scope.contractCode.toLowerCase())!==-1)
					{
						$scope.contractSuggestionList.push($scope.contractList[i]);
					}
		  		}
		  		if($scope.contractSuggestionList.length>0)
	  			$scope.contractSuggestion=true;
		  }
  }
  $scope.getSelectedContract=function(contract)
  {
	  	$scope.contractCode=contract.contractCode;
	  	$scope.contractSuggestion=false;
  }
  
}]);