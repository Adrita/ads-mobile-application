(function () {
	'use strict';
	
	app.controller ("columnConfigCtrl", columnConfig);
	columnConfig.$inject = ['$scope', '$uibModalInstance', '$state', 'localStorageService'];
	
	function columnConfig ($scope, $uibModalInstance, $state, localStorageService) {

		$scope.selectedColumns = localStorageService.get('selectedColumns');
		$scope.allColumns = getAllColumns();
				
		$scope.addColumns = addColumns;
		$scope.removeColumns = removeColumns;
		$scope.selectColumn = selectColumn;
		$scope.key = null;
		$scope.column = null;
		$scope.done = doneSelection;
		$scope.cancel = dismissModal;
		
		/////////////
		
		
		function addColumns () {
			if ($scope.key !== null && $scope.column === 'left') {
				var cols = $scope.allColumns;
				$scope.selectedColumns[$scope.key] = cols[$scope.key];
				delete cols[$scope.key];
				$scope.allColumns = cols;
				$scope.key = null;
			}
		}
		function removeColumns () {
			if ($scope.key !== null && $scope.column === 'right') {
				var cols = $scope.selectedColumns;
				$scope.allColumns[$scope.key] = cols[$scope.key];
				delete cols[$scope.key];
				$scope.selectedColumns = cols;
				$scope.key = null;
			}
		}
		function selectColumn( key, column ) {
			$scope.key = key;
			$scope.column = column;
		}
		
		function doneSelection () {
			$scope.key = null;
			localStorageService.set('selectedColumns', $scope.selectedColumns);
			$uibModalInstance.close("done");
		}
		function dismissModal () {
			$scope.key = null;
			$uibModalInstance.dismiss();
		}
		
		function getAllColumns() {
			var obj1 = localStorageService.get('allColumns');
			var obj2 = $scope.selectedColumns;
		    var result = {};
		    
		    for(var key in obj1) {
		    	if (!obj2.hasOwnProperty(key)) result[key] = obj1[key];
		    }
		    return result;
		}
		
	}
	
})();