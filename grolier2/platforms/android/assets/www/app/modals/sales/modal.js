app.controller( "modalSalesCtrl", ['$scope','$uibModalInstance','adapter','$state','localStorageService','ModalService','Session',function($scope,$uibModalInstance,adapter,$state,localStorageService,ModalService,Session)
{
    var modalObj = localStorageService.get('modalRegistration');
    var index;
    if(modalObj !== null){
        $scope.name = modalObj.name;
        $scope.code = modalObj.code;
        
        index= modalObj.id;
    }
     $scope.yes = function (str) {
       localStorageService.set('modalRegistration',null);
       $uibModalInstance.dismiss();
     };

     $scope.no = function () {
       $uibModalInstance.dismiss();
     };
 
     
     $scope.deleteData = function (productIndex,type){
    	 
    	 /*$scope.$emit('update_parent_controller',index);*/
    	
    	
    	 
    	 var dataReq=ModalService.regArr[index];
		  console.log(dataReq);
		 
		  url="SalesRepController/approveOrRejectSalesRepRegistrationRequests";
		  if(type==="approve"){
			  var data={
					  userId:Session.userId,
					  recId:dataReq.recId,
					  imdSuperVisorCode:dataReq.immediateSupervisorCode,
					  approveRejectRequest:true
			  }
		  }else{
			  var data={
					  userId:Session.userId,
					  recId:dataReq.recId,
					  imdSuperVisorCode:dataReq.immediateSupervisorCode,
					  approveRejectRequest:false
			  }
		  }
		 
		  adapter.getServiceData(url,data).then(success,error);
		  function success(result){
		        if(result.statusCode === 200){
		        	 $uibModalInstance.dismiss();
		        	$state.reload();
		        	 /*ModalService.regData.splice(index,1);*/
		        }
		  }function error(){
		   	   console.log("Service not resolved");
		   	   
	      }
		  
	  };
   
}]);