app
.controller(
		"VoVerifyContractCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'validate',
		 'adapter',
		 'adjustHeight',
		 'Session',
		 '$cordovaNetwork',
		 function($scope, $state, $uibModal,
				 localStorageService, validate, adapter,
				 adjustHeight, Session, $cordovaNetwork) {
			 $scope.buttonText = "Collect Payment";
			 $scope.customerDetails = "Customer Details";
			 $scope.companyDetails = "Company's Order";
			 $scope.commentsDetails = "Verification Officer Comments";
			 adjustHeight.adjustTopBar();
			 $scope.loading = false;

			 $scope.selectedLanguage=[];
			 $scope.selectedCompany=[];

			 $scope.custNameError=false;
			 $scope.languageError=false;
			 $scope.companyError=false;
			 $scope.otherError=false;
			 $scope.otherCompanyError=false;

			 var confirmModal=false;
			 var isOnline = $cordovaNetwork.isOnline();

			 $scope.contract = {};
			 $scope.contract = {
					 contractCode : null,
					 customerCode : null,
					 customerDetails : {
						 name : null
					 }
			 };
			 $scope.vo = {};
			 $scope.common = {};
			 $scope.common.currency = currency; // from global
			 // js

			 $scope.contract.contractCode = localStorageService
			 .get('contractCode');
			 // $scope.contract.contractCode = "160049mm";

			 /* MODAL IMPLEMENTATION */
			 $scope.animationsEnabled = true;

			 $scope.open = function() {

                              var template='';
                              var controllerVar="";

                              if(confirmModal){
                                  template='app/modals/confirm-modal/modal.html';
                                  controllerVar='modalCtrlConfirm';
                              }else{
                                  template='app/modals/role/modal.html';
                                  controllerVar='modalCtrl';
                              }
                              var modalInstance = $uibModal.open({
                                  animation : $scope.animationsEnabled,
                                  templateUrl : template,
                                  controller : controllerVar,
                                  windowClass : 'center-modal',
                                  backdrop : 'static',
                                  keyboard : false,
                                  size : 'md'
                              }).result.then(function(data) {
                                  if (data === "close") {
                                          if(!confirmModal){
                                              $state.go($scope.backlink);
                                          }else{
                                               confirmModal = false;
                                               $scope.vo.report.data.recommendContract="";
                                          }

                                  }
                                  if (data === "createAnother") {

                                      if(!confirmModal){
                                      $state.reload();
                                      }else{
                                          confirmModal=false;
                                          getServiceCall("verifyContract");
                                      }
                                  }
                              });
                          };

			 var errorName = "Contract";
			 var errorCode = "Test1234";
			 var contractCode = $scope.contract.contractCode;
			 var errorMessage = "";
			 var successMessage = "";

			 var modalEntity = "";
			 var modalBody = "";
			 var modalMessge = "";

			 function modalOpen(modalType) {
				 if (errorName !== null && errorName !== "") {
					 if (modalType === "success") {
						 var modalObj = {
								 "modalEntity" : modalEntity,
								 "modalMessge" : modalMessge,
								 "body" : modalBody,
								 "backLink" : "Back to Complete Request",
								 "modalBtnText" : "",
								 "singleBtn" : true
						 };
						 $scope.backlink = "VoBoard.VoCompleteRequest";
					 } else {
						 var modalObj = {
								 "modalEntity" : "Failed!! ",
								 "body" : errorMessage,
								 "backLink" : "Back to Verification Request",
								 "modalBtnText" : "",
								 "singleBtn" : true
						 };
						 $scope.backlink = "VoBoard.VoVerificationRequest";
					 }

					 localStorageService.set("modalObj",
							 modalObj);
					 $scope.open();
				 }
			 }
			 /* END */

			 $scope.pageCount = 0;
			 $scope.count = 0;
			 $scope.next = function(isProgress) {


			 if($scope.pageCount===2 && !$scope.vo.report.data.custName){
			 	$scope.custNameError=true;
			 	return;
			 }else if($scope.pageCount === 2 && $scope.vo.report.data.custName){
			 	$scope.custNameError=false;
			 }else if($scope.pageCount===4 && $scope.selectedLanguage.length===0){
			 	$scope.languageError=true;
			 	return;
			 }else if($scope.pageCount===4 && $scope.selectedLanguage.length>0){
			 	$scope.languageError=false;
			 	if($scope.selectedLanguage.indexOf("Other")>-1 && $scope.vo.report.langSelected['Other']===""){
			 		$scope.otherError=true;
			 		return;
			 	}else{
			 		$scope.otherError=false;
			 	}
			 }else if($scope.pageCount===6 && $scope.selectedCompany.length===0){
				$scope.companyError=true;
				return;
			 }else if($scope.pageCount===6 && $scope.selectedCompany.length>0){
				$scope.companyError=false;
				if($scope.selectedCompany.indexOf("Other")>-1 && $scope.vo.report.operationSelected['Other']===""){
					$scope.otherCompanyError=true;
					return;
				}else{
					$scope.otherCompanyError=false;
				}
			 }



				 if ($scope.pageCount < 10)
					 $scope.pageCount += 1;

				 if ($scope.pageCount === 3
						 || $scope.pageCount === 4
						 || $scope.pageCount === 5
						 || $scope.pageCount === 7
						 || $scope.pageCount === 9) {
					 $scope.count += 1;

					 // console.log($scope.vo.report);
					 // console.log($scope.vo.report.langSelected);
				 }
				 if ($scope.pageCount === 6) {
					 $scope.vo.report.langString = getStringfromArr(
							 $scope.vo.report.langSelected,
							 $scope.vo.report.langList);

				 } else if ($scope.pageCount === 8) {
					 $scope.vo.report.operationString = getStringfromArr(
							 $scope.vo.report.operationSelected,
							 $scope.vo.report.operationList);

				 }
			 }
			 $scope.previous = function(isProgress) {
				 if ($scope.pageCount === 3
						 || $scope.pageCount === 4
						 || $scope.pageCount === 5
						 || $scope.pageCount === 7
						 || $scope.pageCount === 9) {
					 $scope.count -= 1;

				 }
				 if ($scope.pageCount > 0)
					 $scope.pageCount -= 1;
			 }


			 $scope.selectLanguage=function(language){
			 	if($scope.selectedLanguage.indexOf(language)<0){
			 		$scope.selectedLanguage.push(language);
			 		$scope.languageError=false;
			 	}else{
			 		var index = $scope.selectedLanguage.indexOf(language);
			 		$scope.selectedLanguage.splice(index,1);
			 		if($scope.selectedLanguage.length===0){
			 			$scope.languageError=true;
			 		}
			 	}

			 }

			 $scope.selectCompany=function(company){
				if($scope.selectedCompany.indexOf(company)<0){
					$scope.selectedCompany.push(company);
					$scope.companyError=false;
				}else{
					var index = $scope.selectedCompany.indexOf(company);
					$scope.selectedCompany.splice(index,1);
					if($scope.selectedCompany.length===0){
						$scope.companyError=true;
					}
				}

			 }

			 function getStringfromArr(Arr, compareArr) {
				 var selArr = [];
				 angular.forEach(compareArr,
						 function(value, key) {
					 if (Arr[value])
						 selArr.push(value);
				 });
				 if (Arr['Other'] !== undefined
						 && Arr['Other'] !== null
						 && Arr['Other'] !== "") {
					 selArr.push(Arr['Other']);
				 }
				 return selArr.join(", ");
			 }

			 $scope.vo = {
					 report : {
						 deliveryDetails : {
							 orderNo : "DO1234",
							 location : "Location001"
						 },
						 incomeList : [ "< 1000", "1001 - 2000",
						                "2001 - 3000", "> 3000" ],
						                langList : [ "Malay", "Tamil", "Cantonese",
						                             "Mandarin", "English" ],
						                             operationList : [ "Construction",
						                                               "Trading", "Direct Selling",
						                                               "Finance / Banking","NA" ],
						                                               langSelected : [],
						                                               operationSelected : [],
						                                               data : {
						                                            	   contractId : $scope.contract.contractId,
						                                            	   custName : $scope.contract.customerDetails.name,
						                                            	   salaryIncome : "1001 - 2000",
						                                            	   languageSpeak : "Malay|Y,Tamil|N,Cantonese|Y,Mandarin|N,ENGLISH|N,NilLang|Others",
						                                            	   compOperating : "Construction|Y,Trading|N,Direct Selling|Y,Finance / Banking|N, NA|N, NilLang|Others",
						                                            	   verificationOfficerCode : $scope.contract.salesRepCode,
						                                            	   recommendContract : "",
						                                            	   remarks : ""
						                                               }
					 }
			 };

			 $scope.vo.report.data.installmentLocation="Home";

			 $scope.vo.productColList = [ {
				 col : "S.No",
				 width : "15%"
			 }, {
				 col : "Products",
				 width : "70%"
			 }, {
				 col : "Ordered Qty",
				 width : "15%"
			 } ];

			 var contractStatus = "";
			 $scope.contractAccepted = $scope.contractReconsider = $scope.contractReject = false;
			 $scope.changeContractRecommend = function(status) {

				 contractStatus = status;

				 $scope.vo.report.data.languageSpeak = encryptLangValuePair(
						 $scope.vo.report.langSelected,
						 $scope.vo.report.langList);
				 $scope.vo.report.data.compOperating = encryptLangValuePair(
						 $scope.vo.report.operationSelected,
						 $scope.vo.report.operationList);

						 if(status==="Accept_Contract"){
							  status="accept";
						  }else{
							  status="reject";
						  }

						  confirmModelSetting(status);
						  localStorageService.set("modalStatus",true);
							modalOpenConfirm("success");

				 // console.log($scope.vo.report.data);


			 }
			 $scope.submitReconsidered = function() {
				 $scope.vo.report.data.remarks = $scope.vo.report.reconsidered.message;
				 getServiceCall("verifyContract");

				 // successMessage =
				 // $scope.contract.salesRepName+"
				 // ("+$scope.contract.salesRepCode+") has been
				 // successfully notified about reconsideration
				 // of "+$scope.contract.contractCode;
				 modalEntity = $scope.contract.salesRepName;
				 modalBody = " (" + $scope.contract.salesRepCode
				 + ")";
				 modalMessge = "has been successfully notified about reconsideration of "
					 + $scope.contract.contractCode;

				 localStorageService.set("modalStatus", true);
				 modalOpen("success");
			 }
			 $scope.gotoManagePage = function() {
				 $state.go('VoBoard.VoVerificationRequest');
			 }
			 $scope.collectPayment = function() {
				 localStorageService.set('contractId',
						 $scope.contract.contractId);
				 $state.go('VoBoard.VoCollectPayment');
			 }
			 $scope.gotoContractDetail = function(event) {
			    if (event.ctrlKey) {
		            localStorageService.set('contractRow', $scope.contract);
		            window.open('#/AdminBoard/ViewContract', '_blank'); // in new tab

			    } else {
			    	if(isOnline){
			    		localStorageService.set('contractRow', $scope.contract);
						$state.go('AdminBoard.ViewContract');
			    	}

		        }
			 }

			 $scope.$on('$cordovaNetwork:online', function(event, networkState) {
			 	if(isOnline){
			 		return;
			 	}

			 	isOnline=true;

			 });

			 $scope.$on('$cordovaNetwork:offline', function(event, networkState) {
			 	isOnline=false;
			 });

			 $scope.ShowCustomerPage = function(customerCode) {
				 localStorageService.set('customerCode',
						 customerCode);
				 // $state.go('AdminBoard.EditCustomer');
			 }
			 $scope.gotoUserDetail = function() {
				 localStorageService.set('currentRecord',
						 $scope.contract);
				 $state.go('AdminBoard.CreateEditUser');
			 }
			 function closeStatusPanel() {
				 $scope.contractAccepted = false;
				 $scope.contractReconsider = false;
				 $scope.contractReject = false;
			 }

			 function decryptLangValuePair(data, type) {

				 var langArr = data.split(",");
				 var finalLangArr = [];
				 angular
				 .forEach(
						 langArr,
						 function(value, key) {
							 var temp = value.split("|");
							 if (temp[1] === "Y") {
								 finalLangArr
								 .push(temp[0]);
								 if (type === "language")
									 $scope.vo.report.langSelected[temp[0]] = true;
								 else if (type === "compOperating")
									 $scope.vo.report.operationSelected[temp[0]] = true;

							 } else if (temp[1] === "Others"
								 && temp[0] !== "NilLang") {
								 finalLangArr
								 .push(temp[0]);

								 if (type === "language") {
									 $scope.vo.report.otherLanguageSpeak = true;
									 $scope.showOtherLang = true;
									 $scope.vo.report.langSelected['Other'] = temp[0];

								 } else if (type === "compOperating") {
									 $scope.vo.report.otherCompOperating = true;
									 $scope.showOtherOperation = true;
									 $scope.vo.report.operationSelected['Other'] = temp[0];
								 }

							 }
						 });
				 return finalLangArr.join(", ");
			 }
			 function encryptLangValuePair(Arr, compareArr) {
				 var selArr = [];
				 angular.forEach(compareArr,
						 function(value, key) {
					 if (Arr[value]) {
						 selArr.push(value + "|Y");
					 } else {
						 selArr.push(value + "|N");
					 }
				 });
				 if (Arr['Other'] !== undefined
						 && Arr['Other'] !== null
						 && Arr['Other'] !== "") {
					 selArr.push(Arr['Other'] + "|Others");
				 } else {
					 selArr.push("NilLang|Others");
				 }
				 return selArr.join(",");
			 }

			 $scope.reverify = function() {
				 $scope.pageCount = 1;
				 $scope.count = 0;
				 closeStatusPanel();

				 // localStorageService.set('contractCode',
				 // $scope.contract.contractCode);
				 // $state.reload();
			 }

			 getServiceCall("viewContract");

			 function getServiceCall(type) {

				 if (type === 'viewContract') {
					 $scope.loading = true;
					 var data = {
							 userId : Session.userId,
							 contractCode : $scope.contract.contractCode
					 }, url = "ContractController/viewContract";

				 } else if (type === 'verifyContract') {
					 $scope.loading = true;
					 $scope.vo.report.data.userId = Session.userId;
					 var data = $scope.vo.report.data, url = "VerificationController/verifyContract";

				 } else if (type === 'getContract') {
					 $scope.loading = true;
					 var data = {
							 "contractId" : $scope.vo.report.data.contractId
					 }, url = "VerificationController/getContractDetail";

				 }
				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {
					 if (result.statusCode === 200) {
						 if (result.message.toLowerCase() === "success") {
							 // console.log(result);
							 if (type === "viewContract") {

								 $scope.contract = result;
								 $scope.vo.report.data.userId = Session.userId;
								 $scope.vo.report.data.contractId = $scope.contract.contractId;
								 $scope.vo.report.data.custName = $scope.contract.customerDetails.name;
								 $scope.vo.report.data.verificationOfficerCode = $scope.contract.salesRepCode;
								 if (result.downPayment2 === '0')
									 $scope.buttonText = "Deliver Items";
								 var data = $scope.contract.terms;
								 if (data === "COD"
									 || data === "CAD")
									 $scope.contract.paymentPeriod = data;
								 else
									 $scope.contract.paymentPeriod = data
									 + " Months";

							 } else if (type === "verifyContract") {

							 app.deleteVerificationRequest($scope.vo.report.data.contractId);

								 if (contractStatus === 'Accept_Contract') {
									 closeStatusPanel();
									 $scope.contractAccepted = true;

								 } else if (contractStatus === 'Reconsider_Contract') {
									 closeStatusPanel();
									 $scope.contractReconsider = true;

								 } else if (contractStatus === 'Reject_Contract') {
									 closeStatusPanel();
									 $scope.contractReject = true;
								 }



							 } else if (type === "getContract") {
								 $scope.vo.report.data.salaryIncome = result.salaryIncome;
								 $scope.vo.report.data.languageSpeak = result.languageSpeak;
								 $scope.vo.report.data.compOperating = result.compOperating;
								 $scope.vo.report.data.remarks = result.remarks;

								 //select language
								 decryptLangValuePair(
										 $scope.vo.report.data.languageSpeak,
										 'language');
								 //select company operating
								 decryptLangValuePair(
										 $scope.vo.report.data.compOperating,
										 'compOperating');

							 }

						 } else {
							 console.log("Else");
						 }
					 } else {
						 console.log("Service Not Resolved");
						 $scope.loading = false;


                         					 var appInUse = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;

                         					 if(!appInUse){
                         					 console.log("Service not resolved");
                         					 console.log(result);

                         					 errorCode = result.statusCode;
                         					 contractCode = $scope.contract.contractCode;
                         					 errorMessage = result;
                         					 localStorageService.set("modalStatus",
                         							 false);
                         					 modalOpen("error");
                         					 }

                         					 if(appInUse){

                         					 if(type==='viewContract'){

                         								$scope.contract = localStorageService.get("requestDetails");

                         								console.log($scope.contract);

                         								$scope.contract.customerCode=$scope.contract.custCode;

                         								 $scope.vo.report.data.userId = Session.userId;
                         								 $scope.vo.report.data.contractId = $scope.contract.contractId;
                         								 $scope.vo.report.data.custName = $scope.contract.custName;
                         								 $scope.vo.report.data.verificationOfficerCode = $scope.contract.salesRepCode;
                         								 if($scope.contract.customerDownPayment2===null ||$scope.contract.customerDownPayment2===undefined){
                         								 $scope.contract.customerDownPayment2=0;
                         								 }

                         								 if ($scope.contract.customerDownPayment2 === '0'||$scope.contract.customerDownPayment2 === 0)
                         									 $scope.buttonText = "Deliver Items";
                         								 var data = $scope.contract.terms;
                         								 if (data === "COD"
                         									 || data === "CAD")
                         									 $scope.contract.paymentPeriod = data;
                         								 else
                         									 $scope.contract.paymentPeriod = data
                         									 + " Months";

                         								$scope.contract.totalAmountOrder=$scope.contract.totalOrderAmt;
                         								$scope.contract.downPayment1=$scope.contract.customerDownPayment1;
                         								$scope.contract.downPayment2=$scope.contract.customerDownPayment2;


                         								if(data==="COD"||data==="CAD"){

                         								$scope.contract.installmentAmt=$scope.contract.outstandingInstallments;
                         								}else{
                         								$scope.contract.installmentAmt=parseFloat($scope.contract.outstandingInstallments)/parseFloat(data);

                         								}
                         								}else if(type==='verifyContract'){
                         								$scope.vo.report.data.userId = Session.userId;
                                                         var data = $scope.vo.report.data;
                                                         data.isSync=false;

                                                         app.verifyContract(data,callbackVerifyContract);



                         								}
                         					 }
					 }
					 $scope.loading = false;

					 //console.log(type);
					 //console.log(result);
				 }
				 ;
				 function error(result) {
					 $scope.loading = false;


					 var appInUse = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;

					 if(!appInUse){
					 console.log("Service not resolved");
					 console.log(result);

					 errorCode = result.statusCode;
					 contractCode = $scope.contract.contractCode;
					 errorMessage = result;
					 localStorageService.set("modalStatus",
							 false);
					 modalOpen("error");
					 }

					 if(appInUse){

					 if(type==='viewContract'){

								$scope.contract = localStorageService.get("requestDetails");

								console.log($scope.contract);

								$scope.contract.customerCode=$scope.contract.custCode;

								 $scope.vo.report.data.userId = Session.userId;
								 $scope.vo.report.data.contractId = $scope.contract.contractId;
								 $scope.vo.report.data.custName = $scope.contract.custName;
								 $scope.vo.report.data.verificationOfficerCode = $scope.contract.salesRepCode;
								 if($scope.contract.customerDownPayment2===null ||$scope.contract.customerDownPayment2===undefined){
								 $scope.contract.customerDownPayment2=0;
								 }

								 if ($scope.contract.customerDownPayment2 === '0'||$scope.contract.customerDownPayment2 === 0)
									 $scope.buttonText = "Deliver Items";
								 var data = $scope.contract.terms;
								 if (data === "COD"
									 || data === "CAD")
									 $scope.contract.paymentPeriod = data;
								 else
									 $scope.contract.paymentPeriod = data
									 + " Months";

								$scope.contract.totalAmountOrder=$scope.contract.totalOrderAmt;
								$scope.contract.downPayment1=$scope.contract.customerDownPayment1;
								$scope.contract.downPayment2=$scope.contract.customerDownPayment2;


								if(data==="COD"||data==="CAD"){

								$scope.contract.installmentAmt=$scope.contract.outstandingInstallments;
								}else{
								$scope.contract.installmentAmt=parseFloat($scope.contract.outstandingInstallments)/parseFloat(data);

								}
								}else if(type==='verifyContract'){
								$scope.vo.report.data.userId = Session.userId;
                                var data = $scope.vo.report.data;
                                data.isSync=false;

                                app.verifyContract(data,callbackVerifyContract);



								}
					 }
				 }
			 }


			 function confirmModelSetting (type) {
				  //managePageLink = $scope.previousState;
				 modalEntity = "Are you sure you want to "+type+" this contract?";
				 //backText = $scope.previousStateTitle;
				 createText = "Yes";
				 grayBtnText = "No";
				 editText = "";
				 confirmModal = true;
			  }


			  function modalOpenConfirm(modalType) {
			   if (errorName !== null && errorName !== "") {
				   if (modalType === "success") {
					   var modalObj = {
							   "modalEntity" : modalEntity,
							   "modalBtnText" : "",
							   "singleBtn" : true,
							   "grayBtnText" : grayBtnText,
							   "confirmModal": confirmModal,
							   "createText":createText
					   };
					   //$scope.backlink = "VoBoard.VoCompleteRequest";
				   } else {
					   var modalObj = {
							   "modalEntity" : "Failed!! ",
							   "body" : errorMessage,
							   "modalBtnText" : "",
							   "singleBtn" : true
					   };
					  // $scope.backlink = "VoBoard.VoVerificationRequest";
				   }

				   localStorageService.set("modalObj",
						   modalObj);
				   $scope.open();
			   }
		   }

			 function callbackVerifyContract(message){

			 if (contractStatus === 'Accept_Contract') {
					 closeStatusPanel();
					 $scope.contractAccepted = true;

				 } else if (contractStatus === 'Reconsider_Contract') {
					 closeStatusPanel();
					 $scope.contractReconsider = true;

				 } else if (contractStatus === 'Reject_Contract') {
					 closeStatusPanel();
					 $scope.contractReject = true;
				 }

				 $scope.$apply();

			 }

			 $scope.concatanateString = function(str) {
				 if (str !== undefined && str !== null
						 && str !== "")
					 return str.replace(/\s+/g, '-')
					 .toLowerCase();
				 return str;
			 }

		 } ]);