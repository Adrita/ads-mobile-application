app
    .controller(
        "VoCollectPaymentCtrl", [
            '$scope',
            '$state',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'Session',
            function ($scope, $state, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, Session) {

             $scope.contract = {};
             $scope.submitPressed=false;
             $scope.codDeliveryError=false;
             var dwnPayment2Amount = 0;
             adjustHeight.adjustTopBar();
             $scope.showPaymentSection = true;
             $('.sub-container').css('min-height', '400px');
             $scope.contract.contractId = localStorageService
             .get('contractId');
             $scope.showDeliveryError = false;
             $scope.sixMonthError=true;
             $scope.binError=true;
             var loggedinUser = Session.userGlobalRole;

                $scope.vo = {};
                $scope.collect = {};
                $scope.common = {};
                $scope.common.currency = currency; // from global
                // js
                $scope.vo.deliveryDetails = {
                    orderNo: "DO1234",
                    location: "Location001"
                };
                $scope.vo.productDeliveredQty = [];
                $scope.loading = false;

                var offline = false;

                /* MODAL IMPLEMENTATION */
                $scope.animationsEnabled = true;

                $scope.open = function () {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'app/modals/role/modal.html',
                        controller: 'modalCtrl',
                        windowClass: 'center-modal',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'md'
                    }).result.then(function (data) {
                        if (data === "close") {
                            //                            if (window.innerWidth < 960) $state.go('VoBoard.VoVerificationRequest');
                            //                            else
                            if (!offline)
                                $state.go('VoBoard.VoCompleteRequest');
                            else
                                $state.go('VoBoard.VoVerificationRequest');
                        }
                        if (data === "createAnother") {
                            $state.reload();
                        }
                    });
                };

                var errorName = "Contract";
                var errorCode = "Test1234";
                var contractCode = $scope.contract.contractCode;
                var errorMessage = "";
                var successMessage = "";

                var modalEntity = "";
                var modalBody = "";
                var modalMessge = "";
                var appInUse = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;


                function modalOpen(modalType) {
                    if (errorName !== null && errorName !== "") {
                        if (modalType === "success") {

                            if (!offline) {
                                var modalObj = {
                                    "modalEntity": modalEntity,
                                    "modalMessge": modalMessge,
                                    "body": modalBody,
                                    //"backLink": (window.innerWidth < 960) ? "Back to Verification Request" : "Back to Completed Request",
                                    "backLink": "Back to Completed Request",
                                    "modalBtnText": "",
                                    "singleBtn": true
                                };
                            } else {
                                var modalObj = {
                                    "modalEntity": modalEntity,
                                    "modalMessge": modalMessge,
                                    "body": modalBody,
                                    //"backLink": (window.innerWidth < 960) ? "Back to Verification Request" : "Back to Completed Request",
                                    "backLink": "Back to Verfication Request",
                                    "modalBtnText": "",
                                    "singleBtn": true
                                };

                            }

                        } else {
                            var modalObj = {
                                "modalEntity": "Failed!! ",
                                "body": errorMessage,
                                "backLink": "Back to Verification Request",
                                "modalBtnText": "",
                                "singleBtn": true
                            };
                        }

                        localStorageService.set("modalObj",
                            modalObj);
                        $scope.open();
                    }
                }
                /* END */

                $scope.vo.productColList = [{
                    col: "S.No",
                    width: "10%"
                }, {
                    col: "Products",
                    width: "50%"
                }, {
                    col: "Ordered Qty",
                    width: "12%"
                }, {
                    col: "Delivered Qty",
                    width: "12%"
                }];

                $scope.backtoVerifyContract = function () {
                    localStorageService.set('contractCode',
                        $scope.contract.contractCode);
                    $state.go('VoBoard.VoVerifyContract');
                }
                $scope.gotoManagePage = function () {
                    $state.go('VoBoard.VoCompleteRequest');
                }
                $scope.ShowCustomerPage = function (customerCode) {
                    localStorageService.set('customerCode',
                        customerCode);
                }

             $scope.gotoContractDetail = function(event) {
                localStorageService.set('contractRow', $scope.contract);
                if (event.ctrlKey) window.open('#/AdminBoard/ViewContract', '_blank'); // in new tab
                else $state.go('AdminBoard.ViewContract');
             }
             $scope.submitPayment = function() {
                 $scope.submitPressed=true;
                 $scope.contract.data.locationId = $scope.selectedLocation.locationId;
                 if($scope.selectedPayment!=undefined && $scope.selectedPayment.paymentMethodId!=undefined)
                 $scope.contract.data.paymentTypeId = $scope.selectedPayment.paymentMethodId;
                 if($scope.chequeDate!==null && $scope.chequeDate!==undefined && $scope.selectedPayment.paymentMethodName.toLowerCase()==='cheque')
                 {
                    var sixMonthDate=new Date();
                    sixMonthDate.setHours(0);
                    sixMonthDate.setMinutes(0);
                    sixMonthDate.setSeconds(0);
                    sixMonthDate.setMilliseconds(0);
                    sixMonthDate.setMonth(sixMonthDate.getMonth()-6);
                    var currentDate=new Date();
                    currentDate.setHours(0);
                    currentDate.setMinutes(0);
                    currentDate.setSeconds(0);
                    currentDate.setMilliseconds(0);
                    var selectedDate=new Date($scope.chequeDate.replace('-',' '));
                    var diff=selectedDate-sixMonthDate;
                    var futureDateDiff=currentDate-selectedDate;
                    /*if(selectedDate>sixMonthDate && currentDate>=selectedDate)*/
                    if(selectedDate>sixMonthDate)
                    $scope.sixMonthError=true;
                    else
                    $scope.sixMonthError=false;

                 }
                 else
                     $scope.sixMonthError=true;

                 if($scope.vo.voVerificationForm.$valid && $scope.selectedLocation!==undefined && $scope.selectedLocation.locationId!==undefined){

                 $scope.binError=false;

                 if($scope.selectedPayment!==undefined && $scope.selectedPayment!==null && $scope.selectedPayment.paymentMethodName!==undefined && $scope.selectedPayment.paymentMethodName.toLowerCase()==='cheque' && $scope.sixMonthError && !$scope.binError)
                 getServiceCall("submitPayment");
                 else if($scope.selectedPayment===null || $scope.selectedPayment===undefined ||$scope.selectedPayment.paymentMethodName==undefined || $scope.selectedPayment.paymentMethodName.toLowerCase()!=='cheque' && !$scope.binError)
                 getServiceCall("submitPayment");

                 }else if($scope.selectedLocation===undefined||$scope.selectedLocation.locationId===undefined){

                 	$scope.binError=true;

                 }

             }

                getServiceCall("collectPayment");

                function getServiceCall(type) {

                    if (type === 'collectPayment') {
                        $scope.loading = true;
                        var data = {
                                "userId": Session.userId,
                                "contractId": $scope.contract.contractId
                            },
                            url = "VerificationController/collectPayment";

                 } else if (type === 'submitPayment') {
                     $scope.loading = true;
                     if($scope.selectedBank!=null && $scope.selectedBank.bankId!=undefined)
                     $scope.contract.data.bankId=$scope.selectedBank.bankId;

                     if($scope.selectedPayment!==undefined && $scope.selectedPayment.paymentMethodName!==undefined){
                        if($scope.selectedPayment.paymentMethodName.toLowerCase()==='cheque'){
                            
                            var chequeDate=new Date($scope.chequeDate).toLocaleDateString("en-GB");
                             //chequeDate=new Date(chequeDate);
                             //chequeDate=chequeDate.getDate()+"/"+(chequeDate.getMonth()+1)+"/"+chequeDate.getFullYear();

                            $scope.contract.data.chequeDt=chequeDate;

                        }
                     
                     }
                     

                     $scope.contract.data.distance=$scope.distance;
                     $scope.contract.data.amtToBeCollected=$scope.paymentMade;
                     var data = $scope.contract.data, url = "VerificationController/collectPaymentSubmit";


                    }
                    adapter.getServiceData(url, data).then(success,
                        error);

                    function success(result) {
                        if (result.statusCode === 200) {
                            if (result.message.toLowerCase() === "success") {
                                if (type === "collectPayment") {
                                    $scope.showPaymentSection = true;
                                    $scope.contract = result;
                                    $scope.bankList=result.bankList;
                                    $scope.contract.data = {
                                        userId: Session.userId,
                                        contractId: $scope.contract.contractId,
                                        amtToBeCollected: $scope.contract.amtTobeCollDwPay2
                                    };

                                    $scope.contract.data.itemList = [];
                                    $scope.distance=false;
                                    $scope.vo.qtyError = [];
                                    angular
                                        .forEach(
                                            $scope.contract.itemList,
                                            function (value,
                                                key) {
                                                $scope.vo.qtyError
                                                    .push(false);

                                                var items = {
                                                    itemId: value.itemId,
                                                    orderedProdQty: value.orderedProdQty,
                                                    deliveredProdQty: ''
                                                };
                                                $scope.contract.data.itemList
                                                    .push(items);
                                            });
//                                    $scope.selectedLocation = {
//                                        locationId: $scope.contract.locationId
//                                    };

                                    $scope.selectedLocation={};


                                    $scope.selectedPayment = {
                                        paymentMethodId: 2
                                    };
                                    if (result.amtTobeCollDwPay2 === 0 ||
                                        result.amtTobeCollDwPay2 === "0" ||
                                        result.amtTobeCollDwPay2 === null ||
                                        result.amtTobeCollDwPay2 === undefined)
                                        $scope.showPaymentSection = false;
                                    dwnPayment2Amount = result.amtTobeCollDwPay2;
                                    $scope.deliveryQuantityCheck();



                                } else if (type === "submitPayment") {

                                 if (dwnPayment2Amount !== 0
                                         && dwnPayment2Amount !== "0") {

                                     if(result.nextPaymentDueDt!==null && result.nextPaymentDueDt!==''){
                                         modalEntity = $scope.paymentMade
                                         + " "
                                         + " collected from "
                                         + $scope.contract.custName;
                                         modalBody = "";
                                         modalMessge = " and items have been delivered with delivery order no "
                                             + result.deliveryCode
                                             + ". The next payment due date is "
                                             + result.nextPaymentDueDt;
                                     }else{
                                         modalEntity = $scope.paymentMade
                                         + " "
                                         + " collected from "
                                         + $scope.contract.custName;
                                         modalBody = "";
                                         modalMessge = " and items have been delivered with delivery order no "
                                             + result.deliveryCode
                                             + ".";
                                     }

                                 }

                                 if (dwnPayment2Amount === 0
                                         || dwnPayment2Amount === "0") {

                                     if(result.nextPaymentDueDt!==null && result.nextPaymentDueDt!==''){
                                     modalEntity = " Items have been delivered to "
                                         + $scope.contract.custName
                                         + " with delivery order no "
                                         + result.deliveryCode
                                         + " .";
                                     modalBody = "";
                                     modalMessge = " The next payment due date is "
                                         + result.nextPaymentDueDt;
                                     }else{
                                         modalEntity = " Items have been delivered to "
                                             + $scope.contract.custName
                                             + " with delivery order no "
                                             + result.deliveryCode
                                             + " .";
                                         modalBody = "";
                                     }
                                 }

                                    localStorageService.set(
                                        "modalStatus", true);
                                    modalOpen("success");
                                }

                            } else {
                               // console.log("Else");
                            }
                        } else {
                            $scope.loading = false;

                                                    console.log("Service not resolved");

                                                    if (!appInUse) {


                                                        //console.log(result);

                                                        errorCode = result.statusCode;
                                                        contractCode = $scope.contract.contractCode;
                                                        errorMessage = result.message;
                                                        localStorageService.set("modalStatus",
                                                            false);
                                                        modalOpen("error");

                                                    }

                                                    if (appInUse) {
                                                        offline = true;
                                                        if (type === 'collectPayment') {


                                                            var location = {countryId:loggedinUser.countryId};
                                                            app.fetchLocation(location, callbackLocation);



                                                        } else if (type === 'submitPayment') {

                                                            if($scope.selectedLocation!==undefined && $scope.selectedLocation.locationId!=undefined ){
                                                            $scope.contract.data.locationId = $scope.selectedLocation.locationId;

                                                                                            $scope.errorLocation=false;
                                                                                            $scope.contract.data.paymentTypeId = $scope.selectedPayment.paymentMethodId;
                                                                                            var data = $scope.contract.data;

                                                                                           // console.log("data>>>>>"+JSON.stringify(data));
                                                                                            data.isSync = false;
                                                                                            app.submitPayment(data, callbackSubmitPayment);
                                                            }else{
                                                            $scope.errorLocation=true;
                                                            }


                                                        }



                                                    }
                        }
                        $scope.loading = false;
                    };

                    function error(result) {
                        $scope.loading = false;

                        console.log("Service not resolved");

                        if (!appInUse) {


                            //console.log(result);

                            errorCode = result.statusCode;
                            contractCode = $scope.contract.contractCode;
                            errorMessage = result.message;
                            localStorageService.set("modalStatus",
                                false);
                            modalOpen("error");

                        }

                        if (appInUse) {
                            offline = true;
                            if (type === 'collectPayment') {


                                var location = {countryId:loggedinUser.countryId};
                                app.fetchLocation(location, callbackLocation);



                            } else if (type === 'submitPayment') {

                                if($scope.selectedLocation!==undefined && $scope.selectedLocation.locationId!=undefined ){
                                $scope.contract.data.locationId = $scope.selectedLocation.locationId;

                                                                $scope.errorLocation=false;
                                                                $scope.contract.data.paymentTypeId = $scope.selectedPayment.paymentMethodId;
                                                                var data = $scope.contract.data;

                                                               // console.log("data>>>>>"+JSON.stringify(data));
                                                                data.isSync = false;
                                                                app.submitPayment(data, callbackSubmitPayment);
                                }else{
                                $scope.errorLocation=true;
                                }


                            }



                        }

                    }
                }

                $scope.vo.voVerificationForm = {};
                $scope.vo.validateInput = function (index,
                    orderedProdQty) {

                    $scope.vo.voVerificationForm.disableSubmit = true;
                    $scope.vo.qtyError[index] = true;
                    $scope.showDeliveryError = false;
                    $scope.codDeliveryError = false;
                    var deliveredProdQty = $
                        .trim($scope.contract.data.itemList[index].deliveredProdQty);
                    //deliveredProdQty = (deliveredProdQty !== undefined && deliveredProdQty !== "")? parseInt(deliveredProdQty):deliveredProdQty;
                    var pattern = /^(\d)+$/;
                    if (pattern.test(deliveredProdQty) &&
                        (orderedProdQty - parseInt(deliveredProdQty) >= 0)) {

                        $scope.vo.qtyError[index] = false;
                        var submitDisable = false;
                        var count = 0;
                        var countForCOD = 0;
                        var len = $scope.contract.data.itemList.length;
                        angular.forEach($scope.vo.qtyError,
                            function (value, key) {
                            if (len > key) {
                                                         /*if ($scope.contract.orderType.toLowerCase()==='cod' &&
                                                                 parseInt($scope.contract.data.itemList[key].deliveredProdQty) !== $scope.contract.data.itemList[key].orderedProdQty) {
                                                                    countForCOD++;
                                                         }*/
                                                         if ($scope.contract.data.itemList[key].deliveredProdQty > 0) { count++; }
                                                     }
                                if (value) {
                                    submitDisable = true;
                                    return true;
                                }
                            });

if (count === 0) {$scope.showDeliveryError = true; submitDisable = true;}
                        $scope.vo.voVerificationForm.disableSubmit = submitDisable;
                        //$scope.deliveryQuantityCheck();

                    }
                }
                $scope.deliveryQuantityCheck = function () {
                    $scope.vo.voVerificationForm.disableSubmit = true;
                    $scope.showDeliveryError = false;
                    $scope.codDeliveryError = false;
                    var count = 0;
                    var countForCOD = 0;
                    for (var i = 0; i < $scope.contract.data.itemList.length; i++) {
                        if ($scope.contract.data.itemList[i].deliveredProdQty > 0) {
                            $scope.vo.voVerificationForm.disableSubmit = false;
                            count++;
                            break;
                        }
                    }
                  /*  if ($scope.contract.orderType.toLowerCase() === 'cod') {
                        for (var i = 0; i < $scope.contract.data.itemList.length; i++) {
                            if (parseInt($scope.contract.data.itemList[i].deliveredProdQty) !== $scope.contract.data.itemList[i].orderedProdQty) {
                                $scope.vo.voVerificationForm.disableSubmit = true;
                                countForCOD++;
                                break;
                            }
                        }
                    }*/
                    if (count === 0)
                        $scope.showDeliveryError = true;
                   /* if (countForCOD > 0)
                        $scope.codDeliveryError = true;*/


                }

                function callbackLocation(message) {
                                $scope.showPaymentSection = true;
                                $scope.contract = localStorageService.get("requestDetails");

                                if ($scope.contract.terms === "COD" || $scope.contract.terms === "CAD") {
                                    $scope.contract.orderType = $scope.contract.terms;
                                } else {
                                    $scope.contract.orderType = "Instalment";
                                }

                                $scope.contract.data = {
                                    userId: Session.userId,
                                    contractId: $scope.contract.contractId,
                                    amtToBeCollected: $scope.contract.customerDownPayment2
                                };

                                $scope.contract.itemList = [];

                                angular.forEach($scope.contract.productDetails, function (value, key) {
                                    var items = {

                                        itemId: value.itemId,
                                        itemProductCode: value.productId,
                                        itemDesc: value.productName,
                                        orderedProdQty: value.quantity

                                    }

                                    $scope.contract.itemList.push(items);

                                })


                                $scope.contract.data.itemList = [];
                                $scope.vo.qtyError = [];
                                angular
                                    .forEach(
                                        $scope.contract.itemList,
                                        function (value,
                                            key) {
                                            $scope.vo.qtyError
                                                .push(false);

                                            var items = {
                                                itemId: value.itemId,
                                                orderedProdQty: value.orderedProdQty,
                                                deliveredProdQty: ''
                                            };
                                            $scope.contract.data.itemList
                                                .push(items);
                                        });

                                    $scope.contract.locationList = message;
                                   // console.log("locationList"+JSON.stringify($scope.contract.locationList));

//                                    $scope.selectedLocation = {
//                                          locationId: $scope.contract.locationId
//                                    };
								$scope.selectedLocation={};
                                $scope.contract.paymentMethodList = [];

                                $scope.contract.paymentMethodList.push({
                                    paymentMethodId: 2,
                                    paymentMethodName: "Cash"
                                });
                                $scope.contract.paymentMethodList.push({
                                    paymentMethodId: 3,
                                    paymentMethodName: "Cheque"
                                });
                                $scope.contract.paymentMethodList.push({
                                    paymentMethodId: 1,
                                    paymentMethodName: "Credit Card"
                                });

                                $scope.selectedPayment = {
                                    paymentMethodId: 2
                                };


                                $scope.contract.amtTobeCollDwPay2 = $scope.contract.customerDownPayment2;



                                if ($scope.contract.amtTobeCollDwPay2 === 0 ||
                                    $scope.contract.amtTobeCollDwPay2 === "0" ||
                                    $scope.contract.amtTobeCollDwPay2 === null ||
                                    $scope.contract.amtTobeCollDwPay2 === undefined)
                                    $scope.showPaymentSection = false;
                                dwnPayment2Amount = $scope.contract.amtTobeCollDwPay2;

                                if($scope.contract.amtTobeCollDwPay2 === null ||
                                                                       $scope.contract.amtTobeCollDwPay2 === undefined){
                                          dwnPayment2Amount=0;
                                                   }
                                $scope.deliveryQuantityCheck();

                                $scope.distance=false;

                    $scope.$apply();

                     var bankData = {countryId: loggedinUser.countryId};
                     app.fetchBankListForCreditCard(bankData, callbackBankList);
                }

                function callbackSubmitPayment(message) {


                    if (dwnPayment2Amount !== 0 &&
                        dwnPayment2Amount !== "0") {
                        modalEntity = $scope.paymentMade +
                            " " +
                            " collected from " +
                            $scope.contract.custName;
                        modalBody = "";
                        modalMessge = " and items have been delivered.";
                    }

                    if (dwnPayment2Amount === 0 ||
                        dwnPayment2Amount === "0") {
                        modalEntity = " Items have been delivered to " +
                            $scope.contract.custName +
                            ".";
                        modalBody = "";

                    }

                    localStorageService.set(
                        "modalStatus", true);
                    modalOpen("success");
                }

                function callbackBankList(result){
                    $scope.bankList=result;
                     $scope.$apply();
                }

            }
        ]);