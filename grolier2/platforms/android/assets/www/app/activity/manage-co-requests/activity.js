app
.controller(
		"CoRequestCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'adapter',
		 'validate',
		 'adjustHeight',
		 '$compile',
		 '$rootScope',
		 'ModalService',
		 'Session',
		 function($scope, $state, $uibModal,
				 localStorageService, adapter, validate,
				 adjustHeight, $compile,$rootScope,ModalService, Session) {
			 $scope.sel = {};
			 $scope.loading = true;
			 $scope.recordType = "Unassigned";
			 var unassingedOp = true;
			 $scope.image = true;
			 var opRow;
			 $scope.opTypeCO = "assigned";
			 var singleAssignId;
			 $("#singleRowAssign").css('display', 'none');
			 $scope.listofCo = [];
			 $scope.selectedBulkAssignCO = "";
			 var singleRequest = {};
			 var listToAssign = [];
			 var type = "";
			 $scope.showAfterLoading=false;
			 var requestData = null;
			 // $scope.parts=true;
			 $scope.openGrid = function(type1) {
				 if (type1 === 'unassigned') {
					 $scope.opTypeCO = "assigned";
					 $('#singleRowAssign')
					 .css('display', 'none');
					 $scope.recordType = "Unassigned";
					 $scope.coloumnHeaders = {
							 "defaultColumns":{
								 "contractCode" : "Contract Code",
								 "custName" : "Customer Name",
								 "addressType":"Address Type",
								 "custHomeAddr" : "Address",
								 "nextPaymentDueDt" : "Due Date",
								 "fiscalYear" : "Fiscal Week/Year",
								 "status" : "Status",
								 "collectionOfficerName" : "Collection Officer",
								 "assign" : "Action"
							 },
							 "allColumns":{
								 "contractCode" : "Contract Code",
								 "custName" : "Customer Name",
								 "addressType":"Address Type",
								 "custHomeAddr" : "Address",
								 "nextPaymentDueDt" : "Due Date",
								 "fiscalYear" : "Fiscal Week/Year",
								 "status" : "Status",
								 "collectionOfficerName" : "Collection Officer",
								 "assign" : "Action"
							 }

					 };
					 localStorageService.set('manageHouseSales',$scope.coloumnHeaders);
					 $scope.$parent.gridOptions.data=[];
					 $scope.$parent.serviceData=[];
					 $scope.$parent.coList = [];
					 $scope.listofCo = [];
					 $scope.parts = true;
					 $('#Unassigned').addClass('tablink_active');
					 $('#Assigned')
					 .removeClass('tablink_active');
					 type = "unassigned";
					 unassingedOp = true;
					 getServiceCall();
				 }
				 if (type1 === "assigned") {
					 $scope.opTypeCO = "reassigned";
					 $('#singleRowAssign')
					 .css('display', 'none');
					 $scope.recordType = "Assigned";
					 $scope.coloumnHeaders = {
							 "defaultColumns":{
								 "contractCode" : "Contract Code",
								 "custName" : "Customer Name",
								 "addressType":"Address Type",
								 "custHomeAddr" : "Address",
								 "nextPaymentDueDt" : "Due Date",
								 "fiscalYear" : "Fiscal Week/Year",
								 "status" : "Status",
								 "collectionOfficerName" : "Collection Officer",
								 "assign" : "Action"
							 },
							 "allColumns":{
								 "contractCode" : "Contract Code",
								 "custName" : "Customer Name",
								 "addressType":"Address Type",
								 "custHomeAddr" : "Address",
								 "nextPaymentDueDt" : "Due Date",
								 "fiscalYear" : "Fiscal Week/Year",
								 "status" : "Status",
								 "collectionOfficerName" : "Collection Officer",
								 "assign" : "Action"
							 }

					 };
					 localStorageService.set('manageHouseSales',$scope.coloumnHeaders);
					 $scope.$parent.gridOptions.data=[];
					 $scope.$parent.serviceData=[];
					 $scope.$parent.coList = [];
					 $scope.listofCo = [];
					 $scope.parts = false;
					 $('#Assigned').addClass('tablink_active');
					 $('#Unassigned').removeClass(
					 'tablink_active');
					 type = "assigned";
					 unassingedOp = false;
					 getServiceCall();
				 }
			 }
			 var tab=localStorageService.get('coCollectionContractsTab');
			 if(tab!==null && tab!==undefined)
				 {
				 	if(tab===1)
				 		{
				 			$scope.openGrid('unassigned');
				 		}
				 	else if(tab===2)
				 		{
				 			$scope.openGrid('assigned');
				 		}

				 }
			 else
		 		{
		 			$scope.openGrid('unassigned');
		 		}

			 $scope.ShowContractPage = function(row, event) {
				localStorageService.set('contractRow', row);
				if (event.ctrlKey) window.open('#/AdminBoard/ViewContract', '_blank'); // in new tab
		        else $state.go('AdminBoard.ViewContract');
			 }
			 $scope.ShowCustomerPage = function(row, event) {
				localStorageService.set('customerCode', row.custCode);
				if (event.ctrlKey) window.open('#/AdminBoard/EditCustomer', '_blank'); // in new tab
		        else $state.go('AdminBoard.EditCustomer');
			 }

			 $scope.showVerificationReport = function(row) {
				 localStorageService.set('voVerificationRow',
						 row);
				 // console.log(localStorageService.get('voVerificationRow'));
				 $state
				 .go('AdminBoard.ViewVoVerificationReport');
			 }

			 $scope.verificationOffcierCode = localStorageService
			 .get('verificationOfficerList');

			 $scope.clearAll = false;
			 // $scope.unassigned=true;
			 // $scope.assigned=false;
			 //$('#Unassigned').addClass('tablink_active');
			 //$('#Assigned').removeClass('tablink_active');
			 var completePageData;


			 document.getElementById("divClear").style.display = "none";

			 /*
			  * $scope.selectAll = function(data) {
			  * console.log($scope.bulkSelect);
			  * console.log('---'); $scope.bulkSelect =
			  * $("input[name='selectAll']").prop("checked");
			  * angular.forEach(data, function(value, key) {
			  * $scope.chkbox[key] = $scope.bulkSelect; }); };
			  */
			 var selObjLength = Object.keys($scope.sel).length;
			 if ($scope.sel === {} || selObjLength === 0) {
				 // $scope.isCheckBtn=true;
				 document.getElementById("bulkAssign_btnId").style.display = "none";
				 document.getElementById("chooseVo_Id").style.display = "none";
			 } else {
				 console.log("length: "
						 + Object.keys($scope.sel).length);
				 console.log("sel obj: " + $scope.sel);
				 $scope.isCheckBtn = false;
			 }
			 $scope.dropChoose = true;

			 $scope.selectAll = function(data) {

				 if ($scope.bulkSelect === undefined
						 || $scope.bulkSelect === 'undefined'
							 || $scope.bulkSelect === false) {
					 // $scope.bulkSelect = true;
					 $scope.isCheckBtn = true;
					 $scope.clearAll = true;
					 document.getElementById("bulkAssign_btnId").style.display = "none";
					 document.getElementById("chooseVo_Id").style.display = "none";
					 document.getElementById("showPerPage_Id").style.display = "block";
					 document.getElementById("threeDot_Id").style.display = "block";
					 document.getElementById("top_boxId").style.display = "block";
					 document.getElementById("bulkAssign_row").style.display = "none";
					 $scope.image = true;
					 document.getElementById("divClear").style.display = "none";
					 // $scope.dropChoose=false;
				 } else {
					 $scope.isCheckBtn = false;
					 $scope.clearAll = false;
					 document.getElementById("bulkAssign_btnId").style.display = "block";
					 document.getElementById("chooseVo_Id").style.display = "block";
					 document.getElementById("showPerPage_Id").style.display = "none";
					 document.getElementById("threeDot_Id").style.display = "none";
					 $scope.image = false;
					 document.getElementById("divClear").style.display = "block";
					 // $scope.bulkSelect = false;

					 // $scope.dropChoose=true;
				 }
				 angular.forEach(data, function(row) {
					 row.Selected = $scope.bulkSelect;
					 // th-check.Selected = true;
					 // "sel['checkBox'+
					 // gridOptions.data.data.indexOf(row)]" =
					 // $scope.bulkSelect;
				 });

				 // completePageData=data;
				 // ModalService.regArr=data;
				 /* $scope.bulkAssign(); */

			 };

			 $scope.clear = function(data) {
				 angular.forEach(data, function(row) {
					 row.Selected = false;
					 // th-check.Selected = false;
					 // "sel['checkBox'+
					 // gridOptions.data.data.indexOf(row)]" =
					 // $scope.bulkSelect;
				 });
				 $scope.image = true;
				 // $scope.bulkSelect=false;
				 $('#th-check').prop('checked', false);
				 document.getElementById("bulkAssign_btnId").style.display = "none";
				 document.getElementById("chooseVo_Id").style.display = "none";
				 document.getElementById("showPerPage_Id").style.display = "block";
				 document.getElementById("threeDot_Id").style.display = "block";
				 document.getElementById("top_boxId").style.display = "block";
				 document.getElementById("bulkAssign_row").style.display = "none";
				 document.getElementById("divClear").style.display = "none";

			 }

			 $scope.selectCheckBox = function(index) {
				 completePageData = $scope.gridOptions.data;
				 ModalService.regArr = completePageData;
				 var count = 0;
				 for ( var i = 0; i < ModalService.regArr.length; i++) {
					 var id = '#checkBox' + i;
					 if ($(id).is(':checked')) {
						 count++;
					 }

				 }
				 if (count > 0) {
					 $('#divClear').css('display', 'block');
					 $('#threeDot_Id').css('display', 'none');
					 $('#bulkAssign_btnId').css('display',
					 'block');
					 $('#showPerPage_Id')
					 .css('display', 'block');
					 $('#chooseVo_Id').css('display', 'none');
					 $('#threeDot_Id').css('display', 'block');
					 if (count === 1) {
						 $('#bulkAssign_btnId').css('display',
						 'none');
						 $scope.image = true;
					 } else {
						 // $('#bulkAssign_btnId').css('display','block');
						 $scope.isCheckBtn = false;
						 $scope.clearAll = false;
						 $('#bulkAssign_btnId').css('display',
						 'block');
						 $('#chooseVo_Id').css('display',
						 'block');
						 $('#showPerPage_Id').css('display',
						 'none');
						 $('#threeDot_Id')
						 .css('display', 'none');
						 $('#divClear').css('display', 'block');
						 $scope.image = false;
					 }

				 } else {
					 // nothing seleceted
					 $('#divClear').css('display', 'none');
					 $('#threeDot_Id').css('display', 'block');
					 $('#bulkAssign_btnId').css('display',
					 'none');
					 $('#showPerPage_Id')
					 .css('display', 'block');
					 $('#chooseVo_Id').css('display', 'none');
				 }
				 $scope.count_check = count;
				 // $scope.bulkAssign();

			 };

			 $scope.assignTemplate = function(productIndex,
					 type1) {
				 var checkId = 'check' + productIndex;
				 var Contractid = 'contract' + productIndex;
				 var customerName = 'custName' + productIndex;
				 var customerAddress = 'custAddress'
					 + productIndex;
				 var fisalYearWeek = 'fiscalYear' + productIndex;
				 var verificationId = 'verification'
					 + productIndex;

				 var idAssignBtn = 'assign' + productIndex;
				 var idRequestBtn = 'request' + productIndex;

				 if (type1 === 1) {
					 document.getElementById(checkId).style.display = 'none';
					 document.getElementById(Contractid).style.display = 'none';
					 document.getElementById(customerName).style.display = 'none';
					 document.getElementById(customerAddress).style.display = 'none';
					 document.getElementById(fisalYearWeek).style.display = 'none';
					 document.getElementById(verificationId).style.display = 'none';

					 document.getElementById(idAssignBtn).style.display = 'none';
					 document.getElementById(idRequestBtn).style.display = 'table-cell';
				 }
				 if (type1 === 2) {
					 document.getElementById(checkId).style.display = 'table-cell';
					 document.getElementById(Contractid).style.display = 'table-cell';
					 document.getElementById(customerName).style.display = 'table-cell';
					 document.getElementById(customerAddress).style.display = 'table-cell';
					 document.getElementById(fisalYearWeek).style.display = 'table-cell';
					 document.getElementById(verificationId).style.display = 'table-cell';

					 document.getElementById(idAssignBtn).style.display = 'table-cell';
					 document.getElementById(idRequestBtn).style.display = 'none';
				 }

			 }
			 $scope.acceptData = function(productIndex) {
				 $scope.gridOptions.data.splice(productIndex, 1);
			 };
			 $scope.undo = function(prevTemp, i) {
				 angular.Element(i).innerHTML = prevTemp;
			 }

			 $scope.bulkAssign = function() {

				 document.getElementById("top_boxId").style.display = "none";
				 document.getElementById("bulkAssign_row").style.display = "block";
				 var count = 0;
				 var list = "";
				 listToAssign = [];
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {

					 if ($scope.$parent.gridOptions.data[i].Selected === true) {
						 count++;
						 listToAssign
						 .push($scope.$parent.gridOptions.data[i].contractId);
					 }
				 }
				 $scope.count_check = count;
				 $scope.voName = $scope.verificationOfficerCode.coName;

			 }

			 $scope.assignCO = function(id, row) {

				 var selectId = '#verification' + id
				 + ' option:selected';
				 if ($(selectId).val() !== "") {
					 type = "assignSingleRequest";
					 singleRequest = id;
					 opRow = row;
					 $scope.getSingleAssignCo();
					 $('#singleRowAssign').css('display',
					 'block');
				 } else {
					 var idV = '#verification' + id;
					 $(idV).css('border', '1px solid red');
				 }

				 // getServiceCall();

			 }
			 $scope.reassignCO = function(id, row) {

				 type = "assignSingleRequest";
				 singleRequest = id;
				 opRow = row;
				 $scope.getSingleAssignCo();
				 $('#singleRowAssign').css('display', 'block');

				 /*
				  * var table =
				  * document.getElementById("reAssignTable"); var
				  * row = table.insertRow(id); var cell1 =
				  * row.insertCell(0); singleAssignId=id;
				  * cell1.setAttribute("colspan",9);
				  * cell1.innerHTML = "<div class='template_bulk
				  * id='singleRowAssign' style='display:
				  * block;'>"+ "<div class='span_grid col-xs-8'><p class='ng-binding'>Contracts
				  * has been reassigned successfully to <span
				  * class='vo_color
				  * ng-binding'>"+$scope.singleAssignVo+"</span></p></div>"+ "<div
				  * id='singleAssignTemplateOk'
				  * class='template_btn'><button class='btn
				  * singleAssignButton'>OK</button>&nbsp; &nbsp;
				  * &nbsp; &nbsp;"+ "<div
				  * id='singleAssignTemplateUndo'
				  * class='undo_vo'><button
				  * class='undo_btn_manage_vo'><img
				  * src='distribution/images/Undo_icon.png'></button></div>"+ "</div></div>";
				  */
				 // getServiceCall();
			 }
			 $scope.submit = function() {
				 $scope.bulkSelect = false;
				 document.getElementById("top_boxId").style.display = "block";
				 $('#divClear').css('display', 'none');
				 $('#threeDot_Id').css('display', 'block');
				 $('#bulkAssign_btnId').css('display', 'none');
				 $('#showPerPage_Id').css('display', 'block');
				 $('#chooseVo_Id').css('display', 'none');
				 document.getElementById("bulkAssign_row").style.display = "none";
				 type = "bulkAssign";
				 getServiceCall();
			 }

			 $scope.undotop = function() {
				 document.getElementById("top_boxId").style.display = "block";
				 $('#divClear').css('display', 'none');
				 $('#threeDot_Id').css('display', 'block');
				 $('#bulkAssign_btnId').css('display', 'none');
				 $('#showPerPage_Id').css('display', 'block');
				 $('#chooseVo_Id').css('display', 'none');
				 document.getElementById("bulkAssign_row").style.display = "none";
				 $scope.bulkSelect = false;
				 $('#singleRowAssign').css('display', 'none');
				 /*
				  * if($scope.assigned){ type="manageAssigned";
				  * }else if($scope.unassigned){
				  * type="manageUnassigned"; }
				  */
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
					 if ($scope.$parent.gridOptions.data[i].Selected === true) {
						 $scope.$parent.gridOptions.data[i].Selected = false;
					 }
				 }
				 // getServiceCall();

			 }

			 $scope.userVoAssign = true;

			 function getServiceCall() {

				 var data = {};
				 var url = "";
				 if (type === 'unassigned') {
					 url = "CollectionController/manageUnassignedCollection";
					 data = {
							 userId : Session.userId,
							 pageSize:$scope.$parent.selectedCnt,
							 currentPage:$scope.$parent.currentPage,
							 filtered:localStorageService.get('isFiltered'),
							 filterObj:localStorageService.get('filterObj')
					 };
					 $scope.loading = true;
				 } else if (type === "assigned") {
					 url = "CollectionController/manageAssignedCollection";
					 data = {
							 userId : Session.userId,
							 pageSize:$scope.$parent.selectedCnt,
							 currentPage:$scope.$parent.currentPage,
							 filtered:localStorageService.get('isFiltered'),
							 filterObj:localStorageService.get('filterObj')
					 };
					 $scope.loading = true;
				 } else if (type === "assignSingleRequest") {
					 url = "CollectionController/assignContract";
					 data = {
							 contractId : $scope.$parent.gridOptions.data[singleRequest].contractId,
							 collectionOfficerCode : $scope.$parent.gridOptions.data[singleRequest].collectionOfficerCode
					 };
				 } else if (type === "bulkAssign") {
					 url = "CollectionController/assignContract";
					 data = {
							 bulkContractId : listToAssign.join(),
							 collectionOfficerCode : $scope.verificationOfficerCode
					 };
				 }
				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {
					 if (result.statusCode === 200) {
						 $scope.loading = false;
						 $scope.image = true;
						 if (type === "unassigned") {
							 $scope.$parent.gridOptions.data = result.collectionList;
							 $scope.$parent.colData = result.collectionList;
							 $scope.$parent.serviceData = result.collectionList;
							 $scope.$parent.coList = result.collectionOfficersMap;

							 $scope.$parent.totalItemCount=result.totalNumberOfRecords;
			                    localStorageService.set('isFiltered',result.filtered);
			                    if(result.currentPage===1){
			                    	$scope.$parent.currentPage=1;
			                    }
							 $scope.listofCo = result.collectionOfficersMap;
							 $scope.$parent.gpopulation();
							 $scope.showAfterLoading=true;
						 }
						 if (type === "assigned") {
							 $scope.$parent.gridOptions.data = result.collectionList;
							 $scope.$parent.colData = result.collectionList;
							 $scope.$parent.serviceData = result.collectionList;
							 $scope.$parent.coList = result.collectionOfficersMap;
							 $scope.listofCo = result.collectionOfficersMap;

							 $scope.$parent.totalItemCount=result.totalNumberOfRecords;
			                    localStorageService.set('isFiltered',result.filtered);
			                    if(result.currentPage===1){
			                    	$scope.$parent.currentPage=1;
			                    }

							 $scope.$parent.gpopulation();
							 $scope.showAfterLoading=true;
						 }
						 if (type === "assignSingleRequest") {
							 if (unassingedOp === true) {
								 type = "unassigned";
								 getServiceCall();
							 }

							 $scope.getSingleAssignCo();
							 if ($scope.recordType === 'Assigned') {
								 $scope.$parent.coList = [];
								 $scope.listofCo = [];
								 type = 'assigned';
								 getServiceCall();
							 } else {
								 $scope.$parent.coList = [];
								 $scope.listofCo = [];
								 type = 'unassigned';
								 getServiceCall();
							 }
						 }
						 if (type === "bulkAssign") {
							 if ($scope.recordType === 'Assigned') {
								 $scope.recordType = "Assigned";
								 type = 'assigned';
								 getServiceCall();
							 } else {
								 type = 'unassigned';
								 getServiceCall();
							 }
						 }
					 }
				 }
				 function error() {
					 $scope.loading = false;
					 console.log("Service not resolved");
				 }

			 }
			 $scope.getBulkAssignCo = function() {
				 $
				 .each(
						 $scope.listofCo,
						 function(index, value) {
							 if (index === $scope.verificationOfficerCode) {
								 $scope.selectedBulkAssignCO = value;
							 }
						 })
			 }
			 $scope.getSingleAssignCo = function() {
				 var id = "#verification" + singleRequest;
				 var coName = $(id + " option:selected").text();
				 $scope.singleAssignVo = coName;
			 }
			 $scope.concatanateString = function(str) {
				 return str.replace(/\s+/g, '-').toLowerCase();
			 }
			 $scope.hideSingleAssign = function() {
				 $("#singleRowAssign").css('display', 'none');
				 getServiceCall();
			 }
			 $scope.$on('ngRepeatFinished', function(
					 ngRepeatFinishedEvent) {
				 var selectedRecords=localStorageService.get('coCollectionContractsTab');
				 var cmDashboardType=localStorageService.get('cmCompletedRequests',type);
				 if($scope.recordType==='Unassigned' && selectedRecords!=null)
		     			$rootScope.$broadcast('cmContractType', { type:'cmUnassigned'});
				 else if($scope.recordType==='Assigned' && selectedRecords!=null && cmDashboardType==='assigned' )
					 	$rootScope.$broadcast('cmContractType', { type:'cmAssigned'});
				 else if($scope.recordType==='Assigned' && selectedRecords!=null && cmDashboardType==='uncollected' ){
					 $rootScope.$broadcast('cmContractType', { type:'uncollected'});
					 localStorageService.set('cmCompletedRequests',null);
				 }

				 localStorageService.set('coCollectionContractsTab',null);

			 });
			 $scope.$on('CollectionContracts', function (event, args) {
				 if ($scope.recordType === 'Assigned') {
					 $scope.recordType = "Assigned";
					 type = 'assigned';
					 getServiceCall();
				 } else {
					 type = 'unassigned';
					 getServiceCall();
				 }
				 });

		 } ]);
