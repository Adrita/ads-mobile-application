app
    .controller(
        "VoTurnInCtrl", [
            '$scope',
            '$state',
            '$rootScope',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'ModalService',
            'Session',
            function($scope, $state, $rootScope, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, ModalService, Session) {
                // $scope.loading=true;
                $scope.recordType = "Collection";
                $scope.tab1 = true;
                $scope.tab2 = false;
                $scope.tab3 = false;
                $scope.opName = "Receive";
                $scope.loader = false;
                $scope.typeCash = [];
                $scope.typeCredit = [];
                $scope.typeCheque = [];
                var type = "";
                $scope.cashSummary = {};
                $scope.chequeSummary = {};
                $scope.creditCardSummary = {};
                $scope.topcashSummary = {};
                $scope.topchequeSummary = {};
                $scope.topcreditCardSummary = {};
                $scope.showReceiveButton = false;
                $scope.turnInHistory = [];
                $scope.loader = false;
                $scope.count = 0;
                var tempArr = [];
                $scope.totalofCash = 0;
                $scope.totalofCheque = 0;
                $scope.totalofCreditCard = 0;
                $scope.singleButtonPressed = false;
                $scope.callAgain = true;
                
                //Table head total
                $scope.tableHeadCashBankIn=0;
                $scope.tableHeadCashInHand=0;
                $scope.tableHeadChequeBankIn=0;
                $scope.tableHeadChequeInHand=0;
                $scope.totalNoOfChequeBankIn=0;
                $scope.totalNoOfChequeInHand=0;
                //End
                
                
                $scope.rowWidth = ["6%", "11.75%", "11.75%",
                    "11.75%", "11.75%", "11.75%", "11.75%",
                    "11.75%", "11.75%"
                ];
                $scope.$on('ngRepeatFinished', function(
                    ngRepeatFinishedEvent) {
                    //$('[data-toggle="popover"]').popover();
                    if ($scope.tab1) {
                        $('table').stickyTableHeaders('destroy');
                        $('.subTableForStickyHeading').stickyTableHeaders();
                        $(window).trigger('resize.stickyTableHeaders');
                        $('.subTableForStickyHeading').stickyTableHeaders({
                            fixedOffset: 0
                        });
                    } else if ($scope.tab2 || $scope.tab3) {
                        $('table').stickyTableHeaders('destroy');
                        $('table').stickyTableHeaders();
                        $(window).trigger('resize.stickyTableHeaders');
                        $('.subTableForStickyHeading').stickyTableHeaders({
                            fixedOffset: 57
                        });
                    }

                });

                $('#Unassigned').addClass('tablink_active');
                $('#Assigned').removeClass('tablink_active');
                $scope.subTableHeadings = {
                    "date": {
                        "title": "Date",
                        "width": "10%"
                    },
                    "PaymentType": {
                        "title": "Payment Type",
                        "width": "10%"
                    },
                    "Amount": {
                        "title": "Amount",
                        "width": "10%"
                    },
                    "chqDate": {
                        "title": "Cheque Date",
                        "width": "10%"
                    },
                    "chqNumber": {
                        "title": "Cheque Number",
                        "width": "10%"
                    },
                    "bankIn": {
                        "title": "Bank In Slip/ Transaction No.",
                        "width": "10%"
                    },
                    "Remarks": {
                        "title": "Remarks",
                        "width": "15%"
                    },
                    "clearedBy": {
                        "title": "Cleared By",
                        "width": "13%"
                    },
                    "status": {
                        "title": "Status",
                        "width": "12%"
                    }

                };

                $scope.openGrid = function(openTab) {
                    $scope.bulkSelect = false;
                    $scope.cashSelect = false;
                    $scope.chequeSelect = false;
                    $scope.cardSelect = false;
                    if (openTab === 'history') {
                        $scope.tab1 = true;
                        $scope.tab2 = false;
                        $scope.tab3 = false;
                        $scope.$parent.colInfo = {
                            "date": {
                                "title": "Date",
                                "width": "10%"
                            },
                            "noOfTransactions": {
                                "title": "No Of Transactions",
                                "width": "20%"
                            },
                            "processedBy": {
                                "title": "Processed By",
                                "width": "20%"
                            },
                            "totalAmount": {
                                "title": "Total Amount",
                                "width": "20%"
                            },
                            "batchNumber": {
                                "title": "Batch Number",
                                "width": "20%"
                            }
                        };
                        
                        $scope.typeCash = [];
                        $scope.typeCredit = [];
                        $scope.typeCheque = [];
                        
                        $scope.$parent.gridOptions.data = [];
                        $scope.$parent.colData = [];
                        $scope.$parent.serviceData = [];
                        $scope.turnInHistory=[];
                        $scope.$parent.configGrid();
                        type = "history";
                        getServiceCall();
                    }
                    if (openTab === "receive") {
                        $scope.tab1 = false;
                        $scope.tab2 = true;
                        $scope.tab3 = false;
                        $scope.$parent.colInfo = {
                            "ReceivingFrom": {
                                "title": "Receiving From",
                                "width": "13.42%"
                            },
                            "date": {
                                "title": "Date",
                                "width": "13.42%"
                            },
                            "Amount": {
                                "title": "Amount",
                                "width": "13.42%"
                            },
                            "ChequeDate": {
                                "title": "Cheque Date",
                                "width": "13.42%"
                            },
                            "ChequeNumber": {
                                "title": "Cheque Number",
                                "width": "13.42%"
                            },
                            "TransactionNumber": {
                                "title": "Transaction Number",
                                "width": "13.42%"
                            }
                        };
                        $scope.rowWidth = ["6%", "13.42%",
                            "13.42%", "13.42%", "13.42%",
                            "13.42%", "13.42%", "13.42%"
                        ];
                        
                        $scope.typeCash = [];
                        $scope.typeCredit = [];
                        $scope.typeCheque = [];
                        $scope.$parent.gridOptions.data = [];
                        $scope.$parent.colData = [];
                        $scope.$parent.serviceData = [];
                        $scope.turnInHistory=[];
                        $scope.$parent.configGrid();
                        type = "receive";
                        getServiceCall();
                    }
                    if (openTab === "pending") {
                        $scope.tab1 = false;
                        $scope.tab2 = false;
                        $scope.tab3 = true;
                        $scope.$parent.colInfo = {
                            "ReceivingFrom": {
                                "title": "Received Date",
                                "width": "13.42%"
                            },
                            "Amount": {
                                "title": "Amount",
                                "width": "13.42%"
                            },
                            "ChequeDate": {
                                "title": "Cheque Date",
                                "width": "13.42%"
                            },
                            "ChequeNumber": {
                                "title": "Cheque Number",
                                "width": "13.42%"
                            },
                            "TransactionNumber": {
                                "title": "Transaction Number",
                                "width": "13.42%"
                            },
                            "Remarks": {
                                "title": "Remarks",
                                "width": "13.42%"
                            },
                            "Action": {
                                "title": "Action",
                                "width": "13.42%"
                            }
                        };
                        $scope.rowWidth = ["6%", "13.42%", "13.42%",
                            "13.42%", "13.42%", "13.42%", "13.42%",
                            "13.42%", "13.42%"
                        ];
                        
                        $scope.typeCash = [];
                        $scope.typeCredit = [];
                        $scope.typeCheque = [];
                        $scope.opName = "Bank-in";
                        $scope.$parent.gridOptions.data = [];
                        $scope.$parent.colData = [];
                        $scope.$parent.serviceData = [];
                        $scope.turnInHistory=[];
                        $scope.$parent.configGrid();
                        type = "pendingForBankIn";
                        getServiceCall();
                    }

                }
                var lastTab = localStorageService.get("turnInTab");
                if (lastTab === 1)
                    $scope.openGrid('history');
                else if (lastTab === 2)
                    $scope.openGrid('receive');
                else if (lastTab === 3)
                    $scope.openGrid('pending');
                else
                    $scope.openGrid('history');

                function getServiceCall() {

                    var data = {};
                    var url = "";
                    if (type === 'Summary') {
                        url = "VoBankInTurnInController/voBankInTurnInSummaryRequest";
                        data = {
                            userId: Session.userId
                        };
                        $scope.loader = true;
                    } else if (type === 'history') {
                        url = "VoBankInTurnInController/voBankInTurnInHistoryRequest";
                        data = {
                            userId: Session.userId
                        };
                        $scope.loader = true;
                    } else if (type === "receive") {
                        url = "VoBankInTurnInController/voBankInTurnInReceiveRequest";
                        data = {
                            userId: Session.userId
                        };
                        $scope.loader = true;
                    } else if (type === "pendingForBankIn") {
                        url = "VoBankInTurnInController/voBankInTurnInPendingForClearanceRequest";
                        data = {
                            userId: Session.userId
                        };
                        $scope.loader = true;
                    }
                    adapter.getServiceData(url, data).then(success,
                        error);

                    function success(result) {
                        $scope.loader = false;
                        if (result.statusCode === 200) {
                            if (type === 'Summary') {
                                $scope.cashSummary = result.cashSummary;
                                $scope.chequeSummary = result.chequeSummary;
                                $scope.creditCardSummary = result.creditCardSummary;


                            } else if (type === 'history') {
                                $scope.turnInHistory = result.voBankInTurnInHistoryRowList;

                                localStorageService
                                    .set(
                                        'voTurnInHistory',
                                        result.voBankInTurnInHistoryRowList);
                                localStorageService.set("turnInTab", 1);
                                type = "Summary";
                                getServiceCall();

                            } else if (type === 'receive') {
                                $scope.callAgain = false;
                                for (var i = 0; i < result.voBankInTurnInReceiveRows.length; i++)
                                    result.voBankInTurnInReceiveRows[i].id = i;
                                $scope.$parent.gridOptions.data = result.voBankInTurnInReceiveRows;
                                $scope.$parent.colData = result.voBankInTurnInReceiveRows;
                                $scope.$parent.serviceData = result.voBankInTurnInReceiveRows;
                                $scope.$parent.configGrid();
                                $scope.loader = false;
                                localStorageService.set("turnInTab", 2);
                                type = 'Summary';
                                getServiceCall();
                            } else if (type === 'pendingForBankIn') {
                                $scope.callAgain = false;
                                for (var i = 0; i < result.voBankInTurnInPendingClearanceRows.length; i++)
                                    result.voBankInTurnInPendingClearanceRows[i].id = i;
                                $scope.$parent.gridOptions.data = result.voBankInTurnInPendingClearanceRows;
                                $scope.$parent.colData = result.voBankInTurnInPendingClearanceRows;
                                $scope.$parent.serviceData = result.voBankInTurnInPendingClearanceRows;
                                $scope.$parent.configGrid();
                                $scope.topcashSummary = result.cashSummary;
                                $scope.topchequeSummary = result.chequeSummary;
                                $scope.topcreditCardSummary = result.creditCardSummary;
                                
                                getTotalPendingBankIn($scope.$parent.gridOptions.data);
                                
                                $scope.loader = false;
                                localStorageService.set("turnInTab", 3);
                                type = 'Summary';
                                getServiceCall();
                            }
                        } else if (result.statusCode === 401) {
                            /*if (type === 'Summary') {
                            	type = 'history';
                            	getServiceCall();
                            }*/

                        }
                    }

                    function error() {
                        $scope.loader = false;
                        console.log("Service not resolved");
                    }

                }

                $scope.concatanateString = function(str) {
                    //return str.replace(/\s+/g, '-').toLowerCase();
                }

                $scope.collapseExpand = function(pos) {
                    for (var i = 0; i < $scope.turnInHistory.length; i++) {
                        if (i === parseInt(pos)) {
                            if ($scope.turnInHistory[i].showSubTable === undefined ||
                                $scope.turnInHistory[i].showSubTable === null)
                                $scope.turnInHistory[i].showSubTable = false;
                            $scope.turnInHistory[i].showSubTable = !$scope.turnInHistory[i].showSubTable;
                        }
                    }

                }

                $scope.selectAll = function() {
                    if ($scope.tab2) {
                        $scope.bulkSelect = true;
                        for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
                            $scope.$parent.gridOptions.data[i].Selected = true;
                    } else if ($scope.tab3) {
                        $scope.bulkSelect = true;
                        for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
                            if ($scope.$parent.gridOptions.data[i].bankSlipNo === null)
                                $scope.$parent.gridOptions.data[i].Selected = true;
                    }
                    $scope.cashSelect = true;
                    $scope.chequeSelect = true;
                    $scope.cardSelect = true;

                }
                $scope.reset = function() {
                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
                        $scope.$parent.gridOptions.data[i].Selected = false;
                    $scope.bulkSelect = false;
                    $scope.cashSelect = false;
                    $scope.chequeSelect = false;
                    $scope.cardSelect = false;
                }
                $scope.checkUncheck = function() {
                    if ($scope.bulkSelect)
                        $scope.selectAll();
                    else {
                        for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
                            $scope.$parent.gridOptions.data[i].Selected = false;
                    }
                }
                $scope.bulkAssign = function() {
                    $state.go('VoBoard.VoBankIn');
                }
                $scope.filterPaymentType = function(type) {
                    var filterData = [];
                    if (type === 'cash') {
                        $scope.totalofCash = 0;
                        for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                            if ($scope.$parent.gridOptions.data[i].paymentMethod
                                .toLowerCase() === 'cash') {
                                filterData
                                    .push($scope.$parent.gridOptions.data[i]);
                                $scope.totalofCash += $scope.$parent.gridOptions.data[i].amount;
                            }

                        }
                    } else if (type === 'credit card') {
                        $scope.totalofCreditCard = 0;
                        for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                            if ($scope.$parent.gridOptions.data[i].paymentMethod
                                .toLowerCase() === 'credit card') {
                                filterData
                                    .push($scope.$parent.gridOptions.data[i]);
                                $scope.totalofCreditCard += $scope.$parent.gridOptions.data[i].amount;
                            }

                        }
                    } else if (type === 'cheque') {
                        $scope.totalofCheque = 0;
                        for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                            if ($scope.$parent.gridOptions.data[i].paymentMethod
                                .toLowerCase() === 'cheque') {
                                filterData
                                    .push($scope.$parent.gridOptions.data[i]);
                                $scope.totalofCheque += $scope.$parent.gridOptions.data[i].amount;
                            }

                        }
                    }
                    return filterData;
                }
                $scope
                    .$watch(
                        '$parent.gridOptions.data',
                        function() {
                            console
                                .log("Change in GridOptions");
                            var filteredOutResult = false;
                            var countfilteredOutResult = 0;
                            if ($scope.$parent.gridOptions.data.length > 0)
                                var receivingFromRow = $scope.$parent.gridOptions.data[0].srName;
                            if ($scope.tab2) {
                                for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                                    if ($scope.$parent.gridOptions.data[i].srName === receivingFromRow)
                                        countfilteredOutResult++;
                                }
                                if (countfilteredOutResult === $scope.$parent.gridOptions.data.length && $scope.tab2) {
                                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                                        $scope.$parent.gridOptions.data[i].Selected = true;
                                    }
                                } else {
                                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                                        $scope.$parent.gridOptions.data[i].Selected = false;
                                    }
                                }
                            }
                            if ($scope.tab3) {
                                for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                                    if ($scope.$parent.gridOptions.data[i].paymentMethod.toLowerCase() === 'cheque') {
                                        var currentDate = new Date();
                                        currentDate.setHours(0);
                                        currentDate.setMinutes(0);
                                        currentDate.setMilliseconds(0);
                                        var diff = currentDate - new Date($scope.$parent.gridOptions.data[i].chequeDate);
                                        if (currentDate < new Date($scope.$parent.gridOptions.data[i].chequeDate.replace('-', ' ')))
                                            $scope.$parent.gridOptions.data[i].Selected = false;
                                    }
                                }
                            }
                            for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
                                $scope.$parent.gridOptions.data[i].id = i;
                            if ($scope.tab2) {
                                $scope.typeCash = $scope
                                    .filterPaymentType('cash');
                                $scope.typeCredit = $scope
                                    .filterPaymentType('credit card');
                                $scope.typeCheque = $scope
                                    .filterPaymentType('cheque');
                            } else if ($scope.tab3) {
                                $scope.typeCash = $scope
                                    .filterPaymentType('cash');
                                $scope.typeCredit = $scope
                                    .filterPaymentType('credit card');
                                $scope.typeCheque = $scope
                                    .filterPaymentType('cheque');
                            }


                            if ($scope.tab2 || $scope.tab3) {
                                if ($scope.$parent.gridOptions.data.length >= 1 && $scope.tab2) {
                                    var firstItem = $scope.$parent.gridOptions.data[0].srName;
                                    var count = 0;
                                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                                        if ($scope.$parent.gridOptions.data[i].srName === firstItem)
                                            count++;
                                    }
                                    if (count === $scope.$parent.gridOptions.data.length)
                                        $scope.showReceiveButton = true;
                                    else
                                        $scope.showReceiveButton = false;

                                }
                                $scope.count = 0;
                                tempArr = [];
                                for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                                    if ($scope.$parent.gridOptions.data[i].Selected) {

                                        $scope.count++;
                                        tempArr.push($scope.$parent.gridOptions.data[i]);
                                    }

                                }
                                console.log(tempArr);
                                localStorageService.set('voTurnInData', tempArr);
                                /*if($scope.singleButtonPressed === true && $scope.count>0)
                                $scope.submitPayment();*/

                            }

                        }, true);
                $scope
                    .$on(
                        'eventName',
                        function(event, args) {
                            if ($scope.tab1 === true) {
                                $scope.paymentList = args.paymentOptions;
                                $scope.turnInStatus = args.turnInStatus;
                                var opentabs = $scope
                                    .getOpenTabs();
                                var tempArr = [];
                                $scope.turnInHistory = localStorageService
                                    .get("voTurnInHistory");
                                if ($scope.paymentList.length > 0) {
                                    for (var i = 0; i < $scope.turnInHistory.length; i++) {
                                        tempArr = [];
                                        $scope.turnInHistory[i].showSubTable = opentabs[i];
                                        for (var j = 0; j < $scope.turnInHistory[i].rowDetails.length; j++) {
                                            if ($scope
                                                .contains($scope.turnInHistory[i].rowDetails[j].paymentMethod)) {
                                                tempArr
                                                    .push($scope.turnInHistory[i].rowDetails[j]);
                                            }
                                        }
                                        $scope.turnInHistory[i].rowDetails = tempArr;
                                    }

                                } else if ($scope.turnInStatus.length > 0) {
                                    for (var i = 0; i < $scope.turnInHistory.length; i++) {
                                        tempArr = [];
                                        $scope.turnInHistory[i].showSubTable = opentabs[i];
                                        for (var j = 0; j < $scope.turnInHistory[i].rowDetails.length; j++) {
                                            if ($scope
                                                .containsStatus($scope.turnInHistory[i].rowDetails[j].status)) {
                                                tempArr
                                                    .push($scope.turnInHistory[i].rowDetails[j]);
                                            }
                                        }
                                        $scope.turnInHistory[i].rowDetails = tempArr;

                                    }
                                } else {
                                    $scope.turnInHistory = localStorageService
                                        .get("voTurnInHistory");
                                    for (var i = 0; i < $scope.turnInHistory.length; i++)
                                        $scope.turnInHistory[i].showSubTable = opentabs[i];
                                }

                            }
                        });
                $scope.contains = function(paymentType) {
                    for (var i = 0; i < $scope.paymentList.length; i++) {
                        if (paymentType.toUpperCase() === $scope.paymentList[i]
                            .toUpperCase()) {
                            return true;
                        }
                    }
                }
                $scope.containsStatus = function(turnInStatus) {
                    for (var i = 0; i < $scope.turnInStatus.length; i++) {
                        if (turnInStatus.toUpperCase() === $scope.turnInStatus[i]
                            .toUpperCase()) {
                            return true;
                        }
                    }
                }
                $scope.getOpenTabs = function() {
                    var opentabs = [];
                    for (var i = 0; i < $scope.turnInHistory.length; i++) {
                        opentabs
                            .push($scope.turnInHistory[i].showSubTable);
                    }
                    return opentabs;
                }
                $scope.checkUncheck = function() {
                    tempArr = [];
                    var obj = {};
                    if ($scope.bulkSelect) {
                        $scope.cashSelect = true;
                        $scope.chequeSelect = true;
                        $scope.cardSelect = true;
                        $scope.cashSelectAll();
                        $scope.chequeSelectAll();
                        $scope.creditCardSelectAll();
                    } else {
                        $scope.cashSelect = false;
                        $scope.chequeSelect = false;
                        $scope.cardSelect = false;
                        $scope.bankIn = false;
                        $scope.clearanceReset = false;
                        $scope.clearanceSelect = false;
                        $scope.threeDotted = true;
                        $scope.perPage = true;
                        $scope.image = true;
                        for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
                            $scope.$parent.gridOptions.data[i].Selected = false;
                    }

                }
                $scope.cashSelectAll = function() {
                    var tempArr = [];
                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                        if ($scope.$parent.gridOptions.data[i].paymentMethod
                            .toLowerCase() === 'cash') {
                            if ($scope.cashSelect)
                                $scope.$parent.gridOptions.data[i].Selected = true;
                            else
                                $scope.$parent.gridOptions.data[i].Selected = false;
                            if ($scope.tab3 && $scope.$parent.gridOptions.data[i].bankSlipNo !== null)
                                $scope.$parent.gridOptions.data[i].Selected = false;
                        }

                    }

                }
                $scope.chequeSelectAll = function() {
                    var tempArr = [];
                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                        if ($scope.$parent.gridOptions.data[i].paymentMethod
                            .toLowerCase() === 'cheque') {
                            if ($scope.chequeSelect)
                                $scope.$parent.gridOptions.data[i].Selected = true;
                            else
                                $scope.$parent.gridOptions.data[i].Selected = false;
                            if ($scope.tab3 && $scope.$parent.gridOptions.data[i].bankSlipNo !== null)
                                $scope.$parent.gridOptions.data[i].Selected = false;
                        }

                    }

                }
                $scope.creditCardSelectAll = function() {
                    for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                        if ($scope.$parent.gridOptions.data[i].paymentMethod
                            .toLowerCase() === 'credit card') {
                            if ($scope.cardSelect)
                                $scope.$parent.gridOptions.data[i].Selected = true;
                            else
                                $scope.$parent.gridOptions.data[i].Selected = false;
                            if ($scope.tab3 && $scope.$parent.gridOptions.data[i].bankSlipNo !== null)
                                $scope.$parent.gridOptions.data[i].Selected = false;
                        }

                    }
                }
                $scope.submitPayment = function() {
                    if ($scope.tab2) {
                        $state.go('VoBoard.VoReceive');
                    } else if ($scope.tab3) {
                        $state.go('VoBoard.VoBankIn');
                    }
                }
                $scope.singleSubmit = function(row) {
                    $scope.stateChange = false;
                    if ($scope.$parent.gridOptions.data.length === 1 && $scope.tab2)
                        $scope.stateChange = true;
                    if ($scope.tab3)
                        $scope.stateChange = true;
                    var diff = 0;
                    if ($scope.tab2 && $scope.$parent.gridOptions.data[row.id].paymentMethod.toLowerCase() === 'cheque' && $scope.$parent.gridOptions.data[row.id].chequeDate !== null) {
                        var currentDate = new Date();
                        currentDate.setMilliseconds(0);
                        currentDate.setMinutes(0);
                        currentDate.setHours(0);
                        diff = currentDate - new Date($scope.$parent.gridOptions.data[row.id].chequeDate);
                    }
                    if (diff >= 0) {
                        $scope.$parent.gridOptions.data[row.id].Selected = true;
                        $scope.count = 0;
                        var tempArr = [];
                        for (var i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
                            if ($scope.$parent.gridOptions.data[i].Selected) {

                                $scope.count++;
                                tempArr.push($scope.$parent.gridOptions.data[i]);
                            }

                        }
                        console.log(tempArr);
                        localStorageService.set('voTurnInData', tempArr);
                        //$scope.singleButtonPressed=true;
                        if ($scope.stateChange && $scope.count > 0)
                            $scope.submitPayment();
                    }




                }
                $scope.filterOut = function(srName) {
                    filterObj = {
                        'name': srName,
                        'usertype': [],
                        'salesdesig': [],
                        'divcode': [],
                        'country': [],
                        'status': [],
                        'currentState': [],
                        'paymentType': [],
                        'campaignCode': [],
                        'dateRanage': [],
                        'state': [],
                        'co': [],
                        'statusListCO': [],
                        'vo': [],
                        'SalesRep': [],
                        'shipmentStatus': [],
                        'fiscalWeek': [],
                        'fiscalYear': [],
                        'turnInStatus': [],
                        'requestType': [],
                        'deductionTypeList': [],
                        'delinquentMonthList': [],
                        'vmFilter': [],
                        'cmFilter': []
                    };
                    $rootScope.$broadcast('filterObj', filterObj);
                    $rootScope.$broadcast('receiveSRName', srName);

                }
                
                
                function getTotalPendingBankIn(data){
                	$scope.tableHeadCashBankIn=0;
                    $scope.tableHeadCashInHand=0;
                    $scope.tableHeadChequeBankIn=0;
                    $scope.tableHeadChequeInHand=0;
                    $scope.totalNoOfChequeBankIn=0;
                    $scope.totalNoOfChequeInHand=0;
                	for(var i=0;i<data.length;i++){
                		if(data[i].paymentMethod==='Cash'){
                			if(data[i].bankedInBy==='With VO'){
                				$scope.tableHeadCashInHand=$scope.tableHeadCashInHand+data[i].amount;
                			}else{
                				$scope.tableHeadCashBankIn=$scope.tableHeadCashBankIn+data[i].amount;
                			}
                		}else if(data[i].paymentMethod ==='Cheque'){
                			if(data[i].bankedInBy==='With VO'){
                				$scope.tableHeadChequeInHand=$scope.tableHeadChequeInHand+data[i].amount;
                				$scope.totalNoOfChequeInHand=$scope.totalNoOfChequeInHand+1;
                			}else{
                				$scope.tableHeadChequeBankIn=$scope.tableHeadChequeBankIn+data[i].amount;
                				$scope.totalNoOfChequeBankIn=$scope.totalNoOfChequeBankIn+1;
                			}
                		}
                	}
                }

            }
        ]);