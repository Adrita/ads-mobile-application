app.controller("ManageVoCompleteCtrl", [
		'$scope',
		'$state',
		'$uibModal',
		'localStorageService',
		'adapter',
		'validate',
		'adjustHeight',
		'$compile',
		'commonServices',
		function($scope, $state, $uibModal, localStorageService, adapter,
				validate, adjustHeight, $compile,commonServices) {
			adjustHeight.adjustTopBar();
			$scope.ShowContractPage = function(row, event) {
				localStorageService.set('contractRow', row);
				$state.go('AdminBoard.ViewContract');
			}
			$scope.ShowCustomerPage = function(row) {
				localStorageService.set('customerCode', row.custCode);
				$state.go('AdminBoard.EditCustomer');
			}
			
			$scope.showVerificationReport = function(row){
				localStorageService.set('contractRow', row);
				$state.go('VoBoard.ViewVerificationReport');
			}
			$scope.concatanateString = function (str) {
				if (str !== undefined && str !== null && str !== "")
				return str.replace(/\s+/g, '-').toLowerCase();
			}

			$scope.exportToCsv=function(e,data){
		    	e.preventDefault();

		    	var filterData=localStorageService.get('filteredContract');

		    	 var selectedColumns = localStorageService.get('selectedColumns');

		    	if(filterData!==null){

		    		var filterOutData=[];

		    		for(var i=0;i<filterData.length;i++){
		    		    angular.forEach(filterData[i], function(value, key) {
		    			  if(selectedColumns.hasOwnProperty(key)){
		    				  console.log(key);
		    			  }else{
		    				//  filterData.remove(key);
		    				delete filterData[i][key];
		    			  }
		    			});
		    		}

		    		commonServices.exportToCsvCommon(filterData, "Contract Report", true);

		    	}else{
		    		if(data == '')
		                return;
		    		for(var i=0;i<data.length;i++){
		    		    angular.forEach(data[i], function(value, key) {
		    			  if(selectedColumns.hasOwnProperty(key)){
		    				  console.log(key);
		    			  }else{
		    				//  filterData.remove(key);
		    				delete data[i][key];
		    			  }
		    			});
		    		}

		    		commonServices.exportToCsvCommon(data, "Contract Report", true);

		    		//JSONToCSVConvertor(data, "Contract Report", true);
		    	}




		    }


		} ]);