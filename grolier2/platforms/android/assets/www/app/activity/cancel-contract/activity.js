(function () {
	'use strict';
	app.controller('CancelContractCtrl', CancelContract);
	CancelContract.$inject = ['$scope','$rootScope','$state','$uibModal','localStorageService','adapter','adjustHeight','Session'];
	
	function CancelContract ($scope,$rootScope,$state,$uibModal,localStorageService,adapter,adjustHeight,Session) {
		
		//Adjust Gap between navigation and page title
		adjustHeight.adjustTopBar();
		
		var managePageLink, modalEntity, backText, createText, editText, confirmModal = false, grayBtnText = "";
		
		$scope.vo = {};
		$scope.vo.voCancellationForm = {};
		
		if (Session.userRoleName === "Sales Rep") $scope.isSalesRep = true; //table for Sales Rep||CO.
		else if (Session.userRoleName.toLowerCase() === "collection officer") $scope.isCO = true;
		else if(Session.userRoleName.toLowerCase()==="admin") $scope.isAdmin=true;

		//Back to previous page
		$scope.previousStateTitle = $rootScope.previousStateTitle;
		$scope.previousState = $rootScope.previousState;
		if ($rootScope.previousState === "") {
			if ($scope.isCO) {
				$scope.previousStateTitle = "Manage Collection Requests"
				$scope.previousState = "CoBoard.ManageCollectorCollectionRequest";
			} else {
				$scope.previousStateTitle = "Manage Contract"
				$scope.previousState = "AdminBoard.ManageContract";
			}
		}
		setCancellationReportTab();

	 	$scope.gotoContract = function (row) {
	 		localStorageService.set('contractRow', row);
	 		$state.go('AdminBoard.ViewContract');
	 	}

	 	$scope.gotoCustomer = function (customerCode) {
	 		localStorageService.set('customerCode',customerCode);
			$state.go('AdminBoard.EditCustomer');
	 	}

	 	$scope.concatanateString = function (str) {
			if (str !== undefined && str !== null && str !== "")
				return str.replace(/\s+/g, '-').toLowerCase();
			return str;
		}

	 	$scope.vo.cancelCancellationRequest = function () {
	 		$state.go('AdminBoard.ViewContract');
		}

	 	var storedContract = localStorageService.get('contractRow');
	 	$scope.contract = (angular.isDefined(storedContract) && storedContract !== null)?storedContract:{};

	 	getServiceCall("viewContract");

	 	//////////////////////


	 	function getServiceCall(type) {

			if(type === 'viewContract') {
				$scope.loading = true;
				var data = {
					userId:Session.userId,
					contractCode:$scope.contract.contractCode
				},
				url="ContractController/viewContract";

			} else if(type === 'cancelContract') {
				$scope.loading = true;

				$scope.vo.cancelData.userId = Session.userId;
				$scope.vo.cancelData.contractId = $scope.contract.contractId;
				var data = $scope.vo.cancelData,
				url="ContractCancellationController/initiateCancellation";
			}
			//calling a service
			adapter.getServiceData(url, data).then(success, error);

			function success(result) {
		        if(result.statusCode === 200){
		        	if(result.message.toLowerCase() === "success") {

		        		if(type === 'viewContract') {
		        			$scope.contract = result;

		        			////////////



		        			if (!$scope.isSalesRep) { //Not for Sales Rep

			        			$scope.vo.cancelQtyError = [];
			        			$scope.vo.cancelData.productDetails = [];
			        			angular.forEach($scope.contract.productDetails, function (value, key) {

			        				$scope.contract.productDetails[key].remainingProdQty = $scope.contract.productDetails[key].deliveredQty - $scope.contract.productDetails[key].returnedQty;
			        				$scope.contract.productDetails[key].remarks = "";

			        				var obj = {
			        						itemId: value.itemId,
			        						quantity: $scope.contract.productDetails[key].quantity,
			        						deliveredQty: $scope.contract.productDetails[key].deliveredQty,
			        						returnedQty: $scope.contract.productDetails[key].returnedQty,
			        						remainingProdQty: $scope.contract.productDetails[key].remainingProdQty,
			        						remarks: $scope.contract.productDetails[key].remarks
			        				};

			        				$scope.vo.cancelData.productDetails.push(obj);
			        				$scope.vo.cancelQtyError.push(false);
			        	    	});
		        			}
		        			$scope.vo.cancelData.cancelRequestDate = getFormattedDate(new Date());

		        		} else if(type === 'cancelContract') {


		        			cancelContractModelSetting ();
		        			localStorageService.set("modalStatus",true);
		        	  		modalOpen("success");
		        		}
		        	}
		        	$scope.loading = false;
		        }
		        else {
		        	console.log("Service Not Resolved");
			        $scope.loading = false;

			        errorModelSetting ();
			        localStorageService.set("modalStatus", false);
	                modalOpen("error");
		        }
		    };

		    function error() {
		    	$scope.loading = false;
		   	    console.log("Service not resolved");

		   	    errorModelSetting ();
			   	localStorageService.set("modalStatus",false);
				modalOpen("error");
		      }

	 	}


	 	//setting cancellation Report tab data
		function setCancellationReportTab() {
			$scope.vo.submitCancellationRequest = function () {

				confirmModelSetting ();
				localStorageService.set("modalStatus",true);
		  		modalOpen("success");
			}

			$scope.vo.cancelData = {};

			///Reset Start
			/*if (!$scope.isSalesRep) { //Not for Sales Rep
				var cancelReason = $scope.vo.cancelData.cancelReason = "Customer doesn't have enough money to pay for all the products.";

				$scope.vo.resetCancellationRequest = function () {

					$scope.vo.cancelData.cancelReason = cancelReason;
					angular.forEach($scope.contract.productDetails, function (value, key) {
						$scope.vo.cancelData.productDetails[key].returnedQty = value.returnedQty;
						//$scope.vo.cancelData.productDetails[key].partiallyReturned = value.partiallyReturned;
						$scope.vo.cancelData.productDetails[key].remarks = value.remarks;

						$scope.vo.cancelQtyError[key] = false;
						$scope.vo.voCancellationForm.disableSubmit = false;
			    	});
				}
			}*/
			///Reset End

			$scope.vo.validateCancelQtyInput = function (index, deliveredQty, returnedQty) {

				$scope.vo.voCancellationForm.disableSubmit = true;
				$scope.vo.cancelQtyError[index] = true;

				var pattern=/^(\d)+$/;
			    if (pattern.test(returnedQty)) {

			    	$scope.vo.cancelData.productDetails[index].remainingProdQty = deliveredQty - returnedQty;
			    	if (deliveredQty - returnedQty >= 0) {
				    	$scope.vo.cancelQtyError[index] = false;
				    	var submitDisable = false;
				    	angular.forEach($scope.vo.cancelQtyError, function (value, key) {
				    		if (value) { submitDisable = true; return true; }
				    	});
				    	$scope.vo.voCancellationForm.disableSubmit = submitDisable;
			    	}
			    }
			}

			$scope.vo.cancelProductColList = [{
				col: "S.No",
				width: "8%"
			}, {
				col: "Product",
				width: "36%"
			}, {
				col: "Ordered Qty",
				width: "8%"
			}, {
				col: "Delivered Qty",
				width: "8%"
			}, {
				col: "Returned Qty",
				width: "8%"
			}, {
				col: "Remaining Qty",
				width: "9%"
			}, {
				col: "Remarks (Optional)",
				width: "23%"
			}];
			$scope.vo.cancelProductSalesCol = [{
				col: "S.No",
				width: "10%"
			}, {
				col: "Product",
				width: "78%"
			}, {
				col: "Ordered Qty",
				width: "12%"
			}];
		}

		//setting cancellation Report tab data
	 	function initiateCancellationRequest () {
			getServiceCall("cancelContract");
		}

	 	function confirmModelSetting () {
	 		managePageLink = $scope.previousState;
			modalEntity = "Are you sure you want to save the cancellation request for contract ";
			backText = $scope.previousStateTitle;
			createText = "Yes";
			grayBtnText = "No";
			editText = "";
			confirmModal = true;
	 	}
	 	function cancelContractModelSetting () {
	 		managePageLink = $scope.previousState;
			modalEntity = "Contract cancellation request for contract ";
			backText = $scope.previousStateTitle;
			editText = " has been submitted successfully";
			createText = "";
			grayBtnText = "";
	 	}
	 	function errorModelSetting () {
	 		managePageLink = $scope.previousState;
			modalEntity = "";
			backText = $scope.previousStateTitle;
			editText = "";
			createText = "";
			grayBtnText = "";
	 	}


		/*MODAL IMPLEMENTATION*/
		$scope.animationsEnabled = true;

		 $scope.open = function () {
		   var modalInstance = $uibModal.open({
		     animation: $scope.animationsEnabled,
		     templateUrl: 'app/modals/role/modal.html',
		     controller: 'modalCtrl',
		     windowClass: 'center-modal',
		     backdrop:'static',
		     keyboard: false,
		     size: 'md'
		   }).result.then(function (data) {
	           if(data === "close") {
	        	   if (confirmModal) {	//confirm modal NO

		        	   confirmModal = false;
	        	   }
	        	   else {
	        		   $state.go(managePageLink);
	        	   }

	           } else if(data === "createAnother"){
	        	   if (confirmModal) {	//confirm modal YES

		        	   confirmModal = false;
	        		   initiateCancellationRequest();
	        	   }
	        	   else {
	        		   $state.reload();
	        	   }
	           }
	        });
		 };

		var errorName = "Contract";
		var errorCode = "Test1234";
		var contractCode = ($scope.contract!==undefined && $scope.contract.contractCode!==undefined)?$scope.contract.contractCode:"";
		var errorMessage="";
	 	function modalOpen(modalType){
	 		if(errorName!==null && errorName!=="" && errorName!==undefined) {
	 			if(modalType==="success") {
					var modalObj = {
						"modalEntity" : modalEntity,
						"body":"("+errorName+" - " + contractCode+") ",
						"backLink": backText,
						"modalBtnText": createText,
						"modalMessge": editText,
						"singleBtn" : true,
						"grayBtnText" : grayBtnText,
						"confirmModal": confirmModal
					};
				} else {
					var modalObj = {
						"modalEntity" : "Failed!! ",
						"body":errorMessage,
						"backLink": backText,
						"modalBtnText": createText,
						"singleBtn" : true,
						"grayBtnText" : grayBtnText,
						"confirmModal": confirmModal
					};
				}

				localStorageService.set("modalObj",modalObj);
				$scope.open();
	 		}
		}
	 	$scope.showProductDetails=function(){
	 		if($scope.isCO && ($scope.contract.status.toLowerCase()==='cancellation approved for product reprocessed' || $scope.contract.status.toLowerCase()==='cancelled'))
	 			return true;
	 		else if($scope.isAdmin && ($scope.contract.status.toLowerCase()==='product reprocessed' || $scope.contract.status.toLowerCase()==='cancelled'))
	 			return true;
	 	}
	 	/*END*/
	}
})();