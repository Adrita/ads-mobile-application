app
    .controller(
        "SRTurnInCtrl", [
            '$scope',
            '$state',
            '$uibModal',
            'localStorageService',
            'adapter',
            'validate',
            'adjustHeight',
            '$compile',
            'ModalService',
            'Session',
            '$cordovaNetwork',
            'commonServices',
            '$rootScope',
            'authentication',
            function($scope, $state, $uibModal,
                localStorageService, adapter, validate,
                adjustHeight, $compile, ModalService, Session, $cordovaNetwork,commonServices,$rootScope,authentication) {

                adjustHeight.adjustTopBar();
                $rootScope.$broadcast('showCollapseMenu', false);

                $scope.historyTab=true;
				$scope.Clearance=false;
				$scope.detailsView=false;
				var type="";

				$scope.isOnline=$cordovaNetwork.isOnline();

				if($scope.isOnline){
					type="historyTab";
				}else{

					$scope.historyTab=false;
					$scope.Clearance=true;
					type="pendingClearance";
				}


				$scope.$on('$cordovaNetwork:online', function(event, networkState){

					if(!$scope.isOnline){
//						$state.reload();
						$scope.isOnline=true;


						var tokenCreatedTime = commonServices.getTokenCreatedTime();
						var currentDate = new Date();

						if (currentDate - tokenCreatedTime > 1, 800, 000) {

							alert("Your Sesson has expired!");
							$scope.loading = true;
							app.readLogFile(successRead, errorRead);

						} else {

							//checkForLocation();
							// $scope.loading = true;
							localStorage.setItem('tryCount',1);

							$rootScope.$broadcast('connectionEstablished',true);
							$scope.openGrid("history");


						}
					}
				});


				function successRead(result) {

                            var userName = result[0].userName;
                            var passWord = result[0].password;
                            $scope.EncryptAuthInfo = $base64.encode(userName + ":" + passWord);
                            config = {
                                headers: {
                                    'Authorization': $scope.EncryptAuthInfo
                                }
                            };

                            var req = {
                                userName: $scope.UserName,
                                password: $scope.Password
                            };

                            authentication.loginService(req, config).then(success, error);

                            function success(result) {
                                $scope.loading = false;


                                if ($state.current.name === 'SalesRep.ManageTurnIn') {
                                    //$state.reload();
                                    $state.reload();
                                }

                                $scope.$apply();
                            }

                            function error(result) {
                                $scope.loading = false;
                                //alert(result);
                            }


                        }

                        function errorRead(result) {
                            $scope.loading = false;
                            //alert(result);
                        }

				$scope.$on('$cordovaNetwork:offline', function(event, networkState){
					$scope.isOnline=false;
					$state.reload();
                });


				getServiceCall(type);




                function getServiceCall(type) {

                    $scope.loader = true;
                    var url = "";
                    var data = {};
                    if (type === 'pendingClearance') {
                        url = "SalesRepTurnInController/salesRepTurnInPendingForClearanceRequest";
                        data = {
                            userId: Session.userId
                        };

                    }else if(type==='historyTab'){
						url = "SalesRepTurnInController/salesRepTurnInHistoryRequest";
						 data = {
								 userId : Session.userId
						 };
					}else if(type==='summary'){
						url="SalesRepTurnInController/salesRepTurnInSummaryRequest";
						data={
							userId:Session.userId
						}
					}

                    adapter.getServiceData(url, data).then(success, error);


                    function success(response) {
                        $scope.loader = false;
                        if (response.statusCode === 200) {
//                            $scope.cashSummary = response.cashSummary;
//                            $scope.chequeSummary = response.chequeSummary;
//                            $scope.creditCardSummary = response.creditCardSummary;

                            if(type==='pendingClearance'){
								$scope.cashDetailsList=response.cashDetailsList;
								$scope.chequeDetailsList=response.chequeDetailsList;
								$scope.creditCardDetailsList=response.creditCardDetailsList;
								app.saveDataIntoFile('saveSalesRepTurnIn',response);

								type="summary";
								getServiceCall(type);
							}else if(type==='historyTab'){
								$scope.listTurnIn=response.srTurnInHistoryRowList;
								type="summary";
								getServiceCall(type);
							}else if(type === 'summary'){
								$scope.cashSummary = response.cashSummary;
								$scope.chequeSummary = response.chequeSummary;
								$scope.creditCardSummary = response.creditCardSummary;
								app.saveDataIntoFile('saveSRSummary',response);
							}


                           // app.saveSalesRepTurnIn(response, successCallback, errorCallback);


                        } else {
                        $scope.loader = false;
                           // app.fetchSalesRepTurnIn(successCallbackFetch, errorCallbackFetch);
                           app.fetchDataFromFile('saveSalesRepTurnIn',successCallbackFetch,errorCallbackFetch);
                        }
                    }

                    function error() {
                    $scope.loader = false;
                        app.fetchDataFromFile('saveSalesRepTurnIn',successCallbackFetch, errorCallbackFetch);

                    }
                }



                $scope.openGrid=function(tabName){
					if(tabName==='history'){
						$scope.historyTab=true;
						$scope.Clearance=false;
						type="historyTab";
						getServiceCall(type);
					}else{
						$scope.historyTab=false;
						$scope.Clearance=true;
						type="pendingClearance";
						getServiceCall(type);
					}
				}

				$scope.viewDetails=function(row){
					$scope.detailsView=true;
					$scope.detailList=row;
				}
				$scope.backToTurnIn=function(){
					$scope.detailsView=false;
				}

                $scope.concatanateString = function(str) {
                    if (str) {
                        return str.replace(/\s+/g, '-').toLowerCase();
                    }

                }

                function successCallbackFetch(response) {
                    if (response.statusCode === 200) {
//                        $scope.cashSummary = response.cashSummary;
//                        $scope.chequeSummary = response.chequeSummary;
//                        $scope.creditCardSummary = response.creditCardSummary;
                        $scope.cashDetailsList = response.cashDetailsList;
                        $scope.chequeDetailsList = response.chequeDetailsList;
                        $scope.$apply();

                        app.fetchDataFromFile('saveSRSummary',successFetchSummary,errorCallbackFetch);


                    }
                }

                function errorCallbackFetch(message) {
                    alert(message);
                }


                function successFetchSummary(response){
					$scope.cashSummary = response.cashSummary;
					$scope.chequeSummary = response.chequeSummary;
					$scope.creditCardSummary = response.creditCardSummary;
					$scope.$apply();
                }


            }
        ]);