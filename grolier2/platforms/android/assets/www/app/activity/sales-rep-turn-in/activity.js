app
.controller(
		"SalesRepTurnInCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'adapter',
		 'validate',
		 'adjustHeight',
		 '$compile',
		 'ModalService',
		 'Session',
		 function($scope, $state, $uibModal,
				 localStorageService, adapter, validate,
				 adjustHeight, $compile, ModalService, Session) {
			 $scope.historyTab = true;
			 $scope.Clearance = false;
			 $scope.bankIn = false;
			 $scope.clearanceReset = false;
			 $scope.clearanceSelect = false;
			 $scope.threeDotted = true;
			 $scope.perPage = true;
			 $scope.image = true;
			 $scope.rowWidth = [ "5%", "15%", "15%", "10%",
			                     "15%", "15%", "15%", "10%" ];
			 // $scope.loading=true;
			 var type = "";
			 $scope.totalOutStandingCreditCard = 0;
			 $scope.totalOutStandingCash = 0;
			 $scope.totalOutStandingCheque = 0;
			 $scope.cashTotal = 0;
			 $scope.chequeTotal = 0;
			 $scope.creditCardTotal = 0;
			 $scope.loader = false;
			 $scope.common = {};
			 $scope.common.currency = currency; // from global
			 // js
			 var backUpData = [];
			 $scope.turnInHistoryData = [];

			 $scope.$on('ngRepeatFinished', function(
					 ngRepeatFinishedEvent) {
				 $('[data-toggle="popover"]').popover();
				 if($scope.historyTab)
					 {
					 	$('table').stickyTableHeaders('destroy');
						$('.subTableForStickyHeading').stickyTableHeaders();
						$(window).trigger('resize.stickyTableHeaders');
						$('.subTableForStickyHeading').stickyTableHeaders({fixedOffset:0});
					 }
				 else if($scope.Clearance)
					 {
					 	$('table').stickyTableHeaders('destroy');
						$('table').stickyTableHeaders();
						$(window).trigger('resize.stickyTableHeaders');
						$('.subTableForStickyHeading').stickyTableHeaders({fixedOffset: 57});
					 }
			 });

			 $scope.typeCash = [];
			 $scope.typeCredit = [];
			 $scope.typeCheque = [];
			 $scope.rowWidth = [];
			 $scope.rowWidth = [ "5%", "15%", "15%", "10%",
			                     "15%", "15%", "15%", "10%" ];
			 $scope.cashSummary = {};
			 $scope.chequeSummary = {};
			 $scope.creditCardSummary = {};
			 $scope.recordType = "Unassigned";
			 $('#History').addClass('tablink_active');
			 $('#PendingClearance')
			 .removeClass('tablink_active');
			 $scope.subTableHeadings = {
					 "contractId" : {
						 "title" : "Date",
						 "width" : "10%"
					 },
					 "orNo" : {
						 "title" : "Payment Type",
						 "width" : "10%"
					 },
					 "Amount" : {
						 "title" : "Amount",
						 "width" : "10%"
					 },
					 "contractCode" : {
						 "title" : "Contract Code",
						 "width" : "10%"
					 },
					 "chqDate" : {
						 "title" : "Cheque Date",
						 "width" : "10%"
					 },
					 "chqNumber" : {
						 "title" : "Cheque Number",
						 "width" : "10%"
					 },
					 "paymentType" : {
						 "title" : "Transaction No",
						 "width" : "10%"
					 },
					 "turnedInBy" : {
						 "title" : "Remarks",
						 "width" : "10%"
					 },
					 "status" : {
						 "title" : "Cleared By",
						 "width" : "10%"
					 },
					 "bankIn" : {
						 "title" : "Status",
						 "width" : "10%"
					 }

			 };

			 $scope.openGrid = function(openTab) {
				 if (openTab === 'history') {
					 $scope.historyTab = true;
					 $scope.Clearance = false;
					 $scope.threeDotted = true;
					 $scope.perPage = true;
					 $scope.loader = true;
					 $scope.$parent.colInfo = {
							 "date" : {
								 "title" : "Date",
								 "width" : "10%"
							 },
							 "noOfTransactions" : {
								 "title" : "Number Of Transactions",
								 "width" : "20%"
							 },
							 "processedBy" : {
								 "title" : "Processed By",
								 "width" : "20%"
							 },
							 "totalAmount" : {
								 "title" : "Total Amount",
								 "width" : "20%"
							 },
							 "batchNumber" : {
								 "title" : "Batch Number",
								 "width" : "20%"
							 }
					 };
					 $scope.$parent.gridOptions.data = [];
					 $scope.$parent.colData = [];
					 $scope.$parent.serviceData = [];
					 $scope.$parent.configGrid();
					 $scope.loader = true;
					 type = "turnInHistory";
					 getServiceCall();
				 }
				 if (openTab === "pending") {
					 $scope.bankIn = false;
					 $scope.historyTab = false;
					 $scope.Clearance = true;
					 $scope.threeDotted = true;
					 $scope.perPage = true;
					 $scope.image = true;
					 $scope.loader = true;
					 $scope.$parent.colInfo = {
							 "datePayment" : {
								 "title" : "Date",
								 "width" : "15%"
							 },
							 "sumOfPaymentAmt" : {
								 "title" : "Amount",
								 "width" : "15%"
							 },
							 "ChequeDate" : {
								 "title" : "Cheque Date",
								 "width" : "15%"
							 },
							 "ChequeNumber" : {
								 "title" : "Cheque Number",
								 "width" : "15%"
							 },
							 "TransactionNumber" : {
								 "title" : "Transaction No.",
								 "width" : "25%"
							 },
							 "turnInStatus":{
								 "title": "Status",
								 "width" : "15%"
							 }
					 };
					 $scope.rowWidth = [ "10%", "15%", "15%",
					                     "15%", "15%", "15%","15%" ];
					 type = "getPendingClearance";
					 $scope.$parent.gridOptions.data = [];
					 $scope.$parent.colData = [];
					 $scope.$parent.serviceData = [];
					 $scope.$parent.configGrid();
					 $scope.loader = true;
					 type = 'pendingClearance';
					 getServiceCall();

				 }
			 }	 

			 function getServiceCall() {

				 var data = {};
				 var url = "";
				 if (type === 'turnInHistory') {
					 url = "SalesRepTurnInController/salesRepTurnInHistoryRequest";
					 data = {
							 userId : Session.userId
					 };
				 } else if (type === 'pendingClearance') {
					 url = "SalesRepTurnInController/salesRepTurnInPendingForClearanceRequest";
					 data = {
							 userId : Session.userId
					 };
				 }

				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {
					 $scope.loader = false;
					 if (result.statusCode === 200) {
						 if (type === 'turnInHistory') {
							 for(var i=0;i<result.srTurnInHistoryRowList;i++)
								 result.srTurnInHistoryRowList[i].showSubTable=false;
							 $scope.$parent.gridOptions.data = result.srTurnInHistoryRowList;
							 $scope.$parent.colData = result.srTurnInHistoryRowList;
							 $scope.$parent.serviceData = result.srTurnInHistoryRowList;
							 $scope.$parent.configGrid();
							 $scope.turnInHistoryData = result.srTurnInHistoryRowList;
							 localStorageService
							 .set(
									 "turnInHistory",
									 result.srTurnInHistoryRowList);
							 $scope.cashSummary = result.cashSummary;
							 $scope.chequeSummary = result.chequeSummary;
							 $scope.creditCardSummary = result.creditCardSummary;
						 } else if (type === 'pendingClearance') {
							 $scope.$parent.gridOptions.data = $scope
							 .getSingleDataSource(result);
							 $scope.$parent.colData = $scope
							 .getSingleDataSource(result);
							 $scope.$parent.serviceData = $scope
							 .getSingleDataSource(result);
							 $scope.$parent.configGrid();
							 $scope.cashSummary = result.cashSummary;
							 $scope.chequeSummary = result.chequeSummary;
							 $scope.creditCardSummary = result.creditCardSummary;
						 }
					 }
				 }
				 function error() {
					 $scope.loader = false;
					 console.log("Service not resolved");
					 // $scope.$parent.loading=false;
				 }

			 }

			 function updateTotal(gridData) {
				 $scope.totalOutStandingCreditCard = 0;
				 $scope.totalOutStandingCash = 0;
				 $scope.totalOutStandingCheque = 0;
				 angular
				 .forEach(
						 gridData,
						 function(value, key) {
							 if (value.typePayment === 'Credit Card') {
								 $scope.totalOutStandingCreditCard += parseFloat(value.sumOfPaymentAmt);
							 } else if (value.typePayment === 'Cash') {
								 $scope.totalOutStandingCash += parseFloat(value.sumOfPaymentAmt);
							 } else {
								 $scope.totalOutStandingCheque += parseFloat(value.sumOfPaymentAmt);
							 }
						 });
			 }

			 function updateHistoryTotal(gridData) {
				 $scope.totalOutStandingCreditCard = 0;
				 $scope.totalOutStandingCash = 0;
				 $scope.totalOutStandingCheque = 0;
				 angular
				 .forEach(
						 gridData,
						 function(value, key) {
							 angular
							 .forEach(
									 gridData[key].listTurnIn,
									 function(
											 value,
											 key) {
										 if (value.typePayment === 'Credit Card') {
											 $scope.totalOutStandingCreditCard += parseFloat(value.sumOfPaymentAmt);
										 } else if (value.typePayment === 'Cash') {
											 $scope.totalOutStandingCash += parseFloat(value.sumOfPaymentAmt);
										 } else {
											 $scope.totalOutStandingCheque += parseFloat(value.sumOfPaymentAmt);
										 }
									 });

						 });
			 }

			 

			 $scope.concatanateString = function(str) {
				 return str.replace(/\s+/g, '-').toLowerCase();
			 }

			 $scope.collapseExpand = function(pos) {
				 for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
					 if (i === parseInt(pos)) {
						 if ($scope.turnInHistoryData[i].showSubTable === undefined
								 || $scope.turnInHistoryData[i].showSubTable === null)
							 $scope.turnInHistoryData[i].showSubTable = false;
						 $scope.turnInHistoryData[i].showSubTable = !$scope.turnInHistoryData[i].showSubTable;
					 }

				 }

			 }

			 $scope.selectAll = function() {
				 if ($scope.Clearance) {
					 $scope.image = false;
					 $scope.threeDotted = false;
					 $scope.perPage = false;
					 $scope.clearanceReset = true;
					 $scope.clearanceSelect = true;
					 $scope.bankIn = true;
					 $scope.bulkSelect = true;
					 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
						 $scope.$parent.gridOptions.data[i].Selected = true;
				 }
			 }

			 $scope.reset = function() {
				 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
					 $scope.$parent.gridOptions.data[i].Selected = false;
				 $scope.bulkSelect = false;
				 $scope.bankIn = false;
				 $scope.clearanceReset = false;
				 $scope.clearanceSelect = false;
				 $scope.threeDotted = true;
				 $scope.perPage = true;
				 $scope.image = true;
			 }

			 $scope.checkUncheck = function() {
				 if ($scope.bulkSelect)
					 $scope.selectAll();
				 else {
					 $scope.bankIn = false;
					 $scope.clearanceReset = false;
					 $scope.clearanceSelect = false;
					 $scope.threeDotted = true;
					 $scope.perPage = true;
					 $scope.image = true;
					 for ( var i = 0; i < $scope.$parent.gridOptions.data.length; i++)
						 $scope.$parent.gridOptions.data[i].Selected = false;
				 }
			 }

			 $scope.bankInPage = function(datePayment) {
				 localStorageService.set('datePayment',
						 datePayment);
				 $state.go('CoBoard.CollectorBankIn');
			 }

			 $scope.filterPaymentType = function(typeofPayment) {
				 var filterData = [];

				 if (type === 'pendingClearance') {
					 if (typeofPayment === 'cash') {
						 $scope.cashTotal = 0;
						 for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
							 if ($scope.$parent.gridOptions.data[i].paymentType
									 .toLowerCase() === 'cash') {
								 filterData
								 .push($scope.$parent.gridOptions.data[i]);
								 $scope.cashTotal += parseFloat($scope.$parent.gridOptions.data[i].amount);
							 }

						 }
					 } else if (typeofPayment === 'credit card') {
						 $scope.creditCardTotal = 0;
						 for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
							 if ($scope.$parent.gridOptions.data[i].paymentType
									 .toLowerCase() === 'credit card') {
								 filterData
								 .push($scope.$parent.gridOptions.data[i]);
								 $scope.creditCardTotal += parseFloat($scope.$parent.gridOptions.data[i].amount);
							 }

						 }
					 } else if (typeofPayment === 'cheque') {
						 $scope.chequeTotal = 0;
						 for (i = 0; i < $scope.$parent.gridOptions.data.length; i++) {
							 if ($scope.$parent.gridOptions.data[i].paymentType
									 .toLowerCase() === 'cheque') {
								 filterData
								 .push($scope.$parent.gridOptions.data[i]);
								 $scope.chequeTotal += parseFloat($scope.$parent.gridOptions.data[i].amount);
							 }

						 }
					 }
				 }

				 return filterData;
			 }
			 $scope
			 .$watch(
					 '$parent.gridOptions.data',
					 function() {
						 console
						 .log("Change in GridOptions");
						 if (type === 'pendingClearance') {
							 $scope.typeCash = $scope
							 .filterPaymentType('cash');
							 $scope.typeCredit = $scope
							 .filterPaymentType('credit card');
							 $scope.typeCheque = $scope
							 .filterPaymentType('cheque');
						 }

					 }, true);

			 $scope.getSingleDataSource = function(result) {
				 var dataList = [];
				 if (result.cashDetailsList !== null)
					 for ( var i = 0; i < result.cashDetailsList.length; i++)
						 dataList
						 .push(result.cashDetailsList[i]);

				 if (result.chequeDetailsList !== null)
					 for ( var i = 0; i < result.chequeDetailsList.length; i++)
						 dataList
						 .push(result.chequeDetailsList[i]);

				 if (result.creditCardDetailsList !== null)
					 for ( var i = 0; i < result.creditCardDetailsList.length; i++)
						 dataList
						 .push(result.creditCardDetailsList[i]);
				 return dataList;
			 }
			 $scope.getOpenTabs = function() {
					var opentabs = [];
					for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
						opentabs
								.push($scope.turnInHistoryData[i].showSubTable);
					}
					return opentabs;
				}

			 $scope
			 .$on(
					 'eventName',
					 function(event, args) {
						 if (type === "turnInHistory") {
							 $scope.paymentList = args.paymentOptions;
							 var opentabs = $scope
								.getOpenTabs();
							 var tempArr = [];
							 
							 if ($scope.paymentList.length > 0) {
								 $scope.turnInHistoryData = localStorageService
								 .get("turnInHistory");
								 for ( var i = 0; i < $scope.turnInHistoryData.length; i++) {
									 tempArr = [];
									 $scope.turnInHistoryData[i].showSubTable = opentabs[i];
									 for ( var j = 0; j < $scope.turnInHistoryData[i].rowDetails.length; j++) {
										 if ($scope
												 .contains($scope.turnInHistoryData[i].rowDetails[j].paymentMethod)) {
											 tempArr
											 .push($scope.turnInHistoryData[i].rowDetails[j]);
										 }
									 }
									 $scope.turnInHistoryData[i].rowDetails = tempArr;
								 }

							 } else
								 {
								 
								 var opentabs = $scope
									.getOpenTabs();
								 $scope.turnInHistoryData = localStorageService.get("turnInHistory");
								 for ( var i = 0; i < $scope.turnInHistoryData.length; i++)
								 $scope.turnInHistoryData[i].showSubTable = opentabs[i];
								
						 }
						 }
					 });
			 $scope.contains = function(paymentType) {
				 for ( var i = 0; i < $scope.paymentList.length; i++) {
					 if (paymentType.toUpperCase() === $scope.paymentList[i]
					 .toUpperCase()) {
						 return true;
					 }
				 }
			 }
			 $scope.getProcessedByList=function(arr)
			 {
				 if(arr.length<=2)
					 return arr.join()
				  else
					  {
					  	var x=arr[0]+','+arr[1];
					  }
			 }

		 } ]);
