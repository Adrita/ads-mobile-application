app
.controller(
		"coBankInCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'adapter',
		 'validate',
		 'adjustHeight',
		 '$compile',
		 'Session',
		 function($scope, $state, $uibModal,
				 localStorageService, adapter, validate,
				 adjustHeight, $compile, Session) {
			 adjustHeight.adjustTopBar();
			 $scope.$parent.loader = false;
			 $scope.chequeDateError=false;
			 $scope.chequePastDateError=false;
//			 var paymentIdListCalling = localStorageService
//			 .get('paymentIdListCalling');
			 var type = "";
			 
			 var paymentType="Cash";

//			 if (paymentIdListCalling !== null) {
//				 type = "getCollection";
//				/* $scope.loader = true;*/
//				 getServiceCall();
//
//			 }
			 $scope.CashBankIn = [];
			 $scope.ChequeBankIn = [];
			 $scope.CreditCardBankIn = [];
			 $scope.cashTotal = 0;
			 $scope.chequeTotal = 0;
			 $scope.creditCardTotal = 0;
			 var paymentIdList = [];
			 $scope.submitPressed=false;

			 $scope.submitData=[];
			  var obj={};
			  $scope.selectedBank=null;
			 $scope.localData=localStorageService.get('voTurnInData');
			 var loggedinUser = Session.userGlobalRole;


			 if($scope.localData==null)
             				  $state.go('CoBoard.CollectionTurnIn');
             			  else
             				  {
             				  	for(var i=0;i<$scope.localData.length;i++)
             				  		{
             				  			if($scope.localData[i].paymentMethod.toLowerCase()==='cash')
             				  				{
             				  				 $scope.CashBankIn.push($scope.localData[i]);
             				  				 $scope.cashTotal+=$scope.localData[i].amount;
             				  				}
             				  			else if($scope.localData[i].paymentMethod.toLowerCase()==='cheque')
             				  				{

             				  					paymentType="Cheque";
             				  					$scope.ChequeBankIn.push($scope.localData[i]);
             				  					$scope.chequeTotal+=$scope.localData[i].amount;
             				  				}

             				  			else if($scope.localData[i].paymentMethod.toLowerCase()==='credit card')
             				  				{
             				  				 	$scope.CreditCardBankIn.push($scope.localData[i]);
             				  				 	$scope.creditCardTotal+=$scope.localData[i].amount;
             				  				}
             				  			$scope.totalItemCount=$scope.localData.length;
             				  			//$scope.cashAmount=$scope.cashTotal+$scope.chequeTotal+$scope.creditCardTotal;

             				  		}
             				  	for(var i=0;i<$scope.localData.length;i++)
             				  		{
             				  			obj={paymentMethod:$scope.localData[i].paymentMethod,amount:$scope.localData[i].amount,paymentIdList:$scope.localData[i].paymentIdList};
             				  			$scope.submitData.push(obj);
             				  		}
             				  	$scope.cashAmount=$scope.cashTotal;

             				  }
             			  type="getCollection";
             			  getServiceCall();


			 function getServiceCall() {
				 $scope.$parent.loader=true;
				 var data = {

				 };
				 var url = "";
				 if (type === 'getCollection') {
					 url = "FetchListController/getBankList";
					 data = {
							 userId : Session.userId
					 };
				 } else if (type === 'bankIn') {
					 url = "CollectorManageTurnInController/bankInCollector";
//					 paymentIdList=[];
//					 for(var i=0;i<$scope.getCollectionServiceData.listTurnIn.length;i++){
//						 if($scope.getCollectionServiceData.listTurnIn[i].paymentIdList!==null){
//							 for(var j=0;j<$scope.getCollectionServiceData.listTurnIn[i].paymentIdList.length;j++){
//								 paymentIdList.push($scope.getCollectionServiceData.listTurnIn[i].paymentIdList[j]);
//							 }
//						 }
//					 }
					 data = {
							 userId : Session.userId,
							 slipNumber:$scope.slipNumber,
							 selectedBank : $scope.bankName,
							 remarks:$scope.remarks,
							 actualCashAmount:$scope.cashAmount,
							receivedCashAmount:$scope.cashTotal,
							receiveInBankInDetails:$scope.submitData,
							bankedInDate:$scope.bankedInDate
					 }
				 }
				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {
					 if (result.statusCode === 200) {
						 $scope.$parent.loader=false;
						 if (type === 'getCollection') {
							 $scope.bankNameList = result.bankList;
//							 $scope.getCollectionServiceData=result;
//							 filterBankIn(result.listTurnIn);
							 
						 } else if (type === 'bankIn') {
							 // Show Success Modal
							 
							 var total=0;
								if($scope.cashTotal>0)
								total+=parseFloat($scope.cashAmount);
								if($scope.chequeTotal>0)
								total+=parseFloat($scope.chequeTotal);
								if($scope.creditCardTotal>0)
								total+=parseFloat($scope.creditCardTotal);
								
							 modalEntity = "RM "+total
							 + " "
							 + " has been received successfully.";
							 modalBody = "";
							 localStorageService.set(
									 "modalStatus", true);
							 modalOpen("success");
						 }
					 }else{
						$scope.$parent.loader=false;
						 localStorageService.set("modalStatus",false);
							modalOpen("error");
					 }

					 // console.log(result.listTurnIn);
				 }
				 function error() {
					 $scope. $parent.loader=false;
					 console.log("Service not resolved");
//					 localStorageService.set("modalStatus",false);
//						modalOpen("error");
					if(type==='getCollection'){
						data.countryId=loggedinUser.countryId;
						app.fetchBankList(data,successBankList);
					}else{
						app.saveBankInDetailsCO(data,successCallback,errorCallback);
					}
				
				 }
			 }

			 function successCallback(result){
             			var total=0;
						if($scope.cashTotal>0)
						total+=parseFloat($scope.cashAmount);
						if($scope.chequeTotal>0)
						total+=parseFloat($scope.chequeTotal);
						if($scope.creditCardTotal>0)
						total+=parseFloat($scope.creditCardTotal);

					 modalEntity = "RM "+total
					 + " "
					 + " has been received successfully.";
					 modalBody = "";
					 localStorageService.set(
							 "modalStatus", true);
					 modalOpen("success");
             		 }

             		 function errorCallback(message){

             		 }


			 function successBankList(result){
             		 	$scope.bankNameList=result;
                         $scope.loader = false;
                         $scope.$apply();


                        // turnInSync.fetchExtraAmt(Session.userId,successCallFetch,errorCallback);
             		 }

			 function filterBankIn(turnInList) {
             				 /*var cashRecords=localStorageService.get('cashRecordsCoTurnIn');
             				 for(var i=0;i<cashRecords.length;i++){
             					 $scope.CashBankIn.push(cashRecords[i]);
             					 $scope.cashTotal += cashRecords[i].sumOfPaymentAmt;
             				 }*/
             				 angular
             				 .forEach(
             						 turnInList,
             						 function(value, key) {
             							 if (value.paymentMethod.toLowerCase() === "cash") {
             								 $scope.CashBankIn
             								 .push(value);
             								 $scope.cashTotal += value.amount;
             							 }

             							 else if (value.paymentMethod.toLowerCase() === "cheque") {
             								 $scope.chequeTotal += value.amount;

             								 paymentType="Cheque";
             								 $scope.ChequeBankIn
             								 .push(value);
             							 } else if (value.paymentMethod.toLowerCase() === "credit card") {
             								 $scope.CreditCardBankIn
             								 .push(value);
             								 $scope.creditCardTotal += value.amount;
             							 }

             						 });
             			 }
			 $scope.collectorBankIn={};
			 $scope.serviceHit = function() {
				 $scope.submitPressed=true;
				 if($scope.collectorBankIn.$valid && chequeDateValidation()){
					 type = "bankIn";
					 getServiceCall();
				 }
				 
			 };
			 
			 function chequeDateValidation(){
				 
				 var currentDate=new Date();
					currentDate.setHours(0);
					currentDate.setMinutes(0);
					currentDate.setSeconds(0);
					currentDate.setMilliseconds(0);
					
					
					var selectedDate=new Date($scope.bankedInDate.replace('-',' '));
					
					var past3Date=new Date();
					past3Date.setHours(-72);
					past3Date.setMinutes(0);
					past3Date.setSeconds(0);
					past3Date.setMilliseconds(0);
					
					
					var futureDiff=selectedDate-currentDate;
					
					
					if(futureDiff>0){
						$scope.chequeDateError=true;
						$scope.chequePastDateError=false;
						return false;
						
					}else if(selectedDate<past3Date && paymentType.toLowerCase()!=='cheque'){
						$scope.chequePastDateError=true;
						$scope.chequeDateError=false;
						return false;
						
						
					}else{
						$scope.chequeDateError=false;
						$scope.chequePastDateError=false;
						return true;
					}
				 
				 
			 }

			 /* MODAL IMPLEMENTATION */
			 $scope.animationsEnabled = true;

			 $scope.open = function() {
				 var modalInstance = $uibModal.open({
					 animation : $scope.animationsEnabled,
					 templateUrl : 'app/modals/role/modal.html',
					 controller : 'modalCtrl',
					 windowClass : 'center-modal',
					 backdrop : 'static',
					 keyboard : false,
					 size : 'md'
				 }).result.then(function(data) {
					 if (data === "close") {
						 $state.go('CoBoard.CollectionTurnIn');
					 }
					 if (data === "createAnother") {
						 $state.reload();
					 }
				 });
			 };

			 var errorName = "Turn In";
			 
			 var errorCode = "Test1234";
			 var contractCode = "";
			 var errorMessage=""; 

			 function modalOpen(modalType) {
				 if (errorName !== null && errorName !== "") {
					 if (modalType === "success") {
						 var modalObj = {
								 "modalEntity" : modalEntity,
								 "body" : modalBody,
								 "modalBtnText" : "OK",
								 "backLink" : "OK",
								 "singleBtn" : true
						 };
					 } else {
						 var modalObj = {
								 "modalEntity" : "Failed!! ",
								 "body" : errorMessage,
								 "modalBtnText" : "",
								 "backLink" : "Ok",
								 "singleBtn" : true
						 };
					 }

					 localStorageService.set("modalObj",
							 modalObj);
					 $scope.open();
				 }
			 }
			 
			 $scope.cancel=function()
			 {
				 $state.go('CoBoard.CollectionTurnIn');
			 }

			 $scope.validateLength=function(inputModel,limit){
			 	if(inputModel.toString().length>limit){
			 		event.preventDefault();
			 		//$scope.$apply();
			 	}

			 }
		 } ]);