app
.controller(
		"EditCustomerCtrl",
		[
		 '$scope',
		 '$state',
		 '$uibModal',
		 '$timeout',
		 'localStorageService',
		 'validate',
		 'adapter',
		 'adjustHeight',
		 'grid',
		 'stickyHeader',
		 'Session',
		 function($scope, $state, $uibModal, $timeout,
				 localStorageService, validate, adapter,
				 adjustHeight, grid, stickyHeader, Session) {
			 var contractList = [];
			 var colHeadings = {};
			 var old_dob;
			 var contractCodeForNewRemark = "";
			 $scope.loader = false;
			 $scope.submitPressed = false;
			 $('#editPage').css('display', 'none');
			 $('[data-toggle="tooltip"]').tooltip();
			 $scope.tab1=true;
			 $scope.tab2=false;
			 $scope.tab3=false;
			 $scope.tab4=false;
			 $scope.addRemarkPressed = false;
			 $scope.addRemarkForm = {};
			 $scope.loader = false;
			 $scope.viewPage=true;
			 $scope.editPage=false;
			 var i = 0;

			  $scope.dialCode=Session.userGlobalRole.dialCode;

			 if (Session.userRoleName.toLowerCase() === "admin") $scope.isAdmin = true;
			 else if (Session.userRoleName.toLowerCase() === "collection officer") $scope.isCO = true;
			 else if (Session.userRoleName.toLowerCase().indexOf("sales") > -1) $scope.isSalesRep = true;
			 else if (Session.userRoleName.toLowerCase() === "verification officer") $scope.isVO = true;

			 $scope.open = function() {
				 var modalInstance = $uibModal.open({
					 animation : $scope.animationsEnabled,
					 templateUrl : 'app/modals/role/modal.html',
					 controller : 'modalCtrl',
					 windowClass : 'center-modal',
					 backdrop : 'static',
					 keyboard : false,
					 size : 'md'
				 }).result
				 .then(function(data) {
					 if (data === "close") {
						 $state
						 .go('AdminBoard.ManageCustomer');
					 }
					 if (data === "createAnother") {
						 $state.reload();
					 }
				 });
			 };

			 var custName = "customer";
			 function modalOpen(modalType) {
				 if (custName !== null && custName !== "") {
					 if (modalType === "success") {
						 var modalObj = {
								 "modalEntity" : modalEntity,
								 "modalMessge" : modalMessge,
								 "body" : modalBody,
								 "backLink" : "Back to Manage Customer",
								 "modalBtnText" : "",
								 "singleBtn" : true
						 };
					 } else {
						 var modalObj = {
								 "modalEntity" : "Failed!! ",
								 "body" : errorMessage,
								 "backLink" : "Back to Manage Customer",
								 "modalBtnText" : "",
								 "singleBtn" : true
						 };
					 }

					 localStorageService.set("modalObj",
							 modalObj);
					 $scope.open();
				 }
			 }

			 enableStickyHeader($(".sticky-block-view"));

			 $scope.remarkList = [];
			 $scope.nav1Img = false;
			 $scope.nav2Img = true;
			 $scope.nav3Img = true;
			 $scope.nav4Img = true;
			 $scope.nav1Current = true;
			 $scope.nav2Current = false;
			 $scope.nav3Current = false;
			 $scope.nav4Current = false;
			 $scope.hoverIn = function(navItem) {
				 if (navItem === 'nav1' && !$scope.nav1Current)
					 $scope.nav1Img = false;
				 else if (navItem === 'nav2'
					 && !$scope.nav2Current)
					 $scope.nav2Img = false;
				 else if (navItem === 'nav3'
					 && !$scope.nav3Current)
					 $scope.nav3Img = false;
				 else if (navItem === 'nav4'
					 && !$scope.nav4Current)
					 $scope.nav4Img = false;
			 };
			 $scope.hoverOut = function(navItem) {
				 if (navItem === 'nav1' && !$scope.nav1Current)
					 $scope.nav1Img = true;
				 else if (navItem === 'nav2'
					 && !$scope.nav2Current)
					 $scope.nav2Img = true;
				 else if (navItem === 'nav3'
					 && !$scope.nav3Current)
					 $scope.nav3Img = true;
				 else if (navItem === 'nav4'
					 && !$scope.nav4Current)
					 $scope.nav4Img = true;
			 }
			 $scope.setCurrent = function(navItem) {
				 if (navItem === 'nav1') {
					 $scope.nav1Current = true;
					 $scope.nav2Current = false;
					 $scope.nav3Current = false;
					 $scope.nav4Current = false;
					 $scope.nav1Img = false;
					 $scope.nav2Img = true;
					 $scope.nav3Img = true;
					 $scope.nav4Img = true;
					 $scope.tab1=true;
					 $scope.tab2=false;
					 $scope.tab3=false;
					 $scope.tab4=false;
				 } else if (navItem === 'nav2') {
					 $scope.nav1Current = false;
					 $scope.nav2Current = true;
					 $scope.nav3Current = false;
					 $scope.nav4Current = false;
					 $scope.nav1Img = true;
					 $scope.nav2Img = false;
					 $scope.nav3Img = true;
					 $scope.nav4Img = true;
					 $scope.tab1=false;
					 $scope.tab2=true;
					 $scope.tab3=false;
					 $scope.tab4=false;
					 $scope.$parent.colInfo = {
							 "contractCode" : "Contract Code",
							 "commenceMentDate" : "Order Date",
							 "salesRepName" : "Sales Rep",
							 "verificationOfficerName" : "Verification Officer",
							 "collectorName" : "Collection Officer",
							 "installmentPaymentMethod" : "Payment Method",
							 "orderDueAmt" : "Outstanding Amount",
							 "status" : "Status"
					 };
					 $scope.$parent.gridOptions.data = contractList;
					 $scope.$parent.colData = contractList;
					 $scope.$parent.serviceData = contractList;
					 $scope.$parent.configGrid();
					 enableStickyHeader($(".sticky-block-view"));

				 } else if (navItem === 'nav3') {
					 $scope.nav1Current = false;
					 $scope.nav2Current = false;
					 $scope.nav3Current = true;
					 $scope.nav4Current = false;
					 $scope.nav1Img = true;
					 $scope.nav2Img = true;
					 $scope.nav3Img = false;
					 $scope.nav4Img = true;
					 $scope.tab1=false;
					 $scope.tab2=false;
					 $scope.tab3=true;
					 $scope.tab4=false;
					 $scope.$parent.colInfo = {
							 "postingDetails" : "Posting Details",
							 "contractNo" : "Contract Code",
							 "orNo" : "OR No.",
							 "orDate" : "OR Date",
							 "type" : "Type",
							 "accNo" : "Acc No.",
							 "refNum" : "Refrenece No.",
							 "amontPaid" : "Amount Paid",
							 "outBalance" : "Outstanding Balance",
							 "lastUpdt" : "Last Updated"
					 };
					 $scope.$parent.gridOptions.data = getPaymentHistory(contractList);
					 $scope.$parent.colData = getPaymentHistory(contractList);
					 $scope.$parent.serviceData = getPaymentHistory(contractList);
					 $scope.$parent.configGrid();
				 } else if (navItem === 'nav4') {
					 $scope.nav1Current = false;
					 $scope.nav2Current = false;
					 $scope.nav3Current = false;
					 $scope.nav4Current = true;
					 $scope.nav1Img = true;
					 $scope.nav2Img = true;
					 $scope.nav3Img = true;
					 $scope.nav4Img = false;
					 $scope.tab1=false;
					 $scope.tab2=false;
					 $scope.tab3=false;
					 $scope.tab4=true;
					 $scope.contractRemark = "All Contracts";
					 getRemarkList(contractList, 'All Contracts');
				 }
			 }
			 $scope.raceList = [ "Chinese", "Indian", "Malay","Others" ];
			 $scope.accTypeList = [ 'Savings', 'Current' ];
			 $scope.relationList = [ 'Mother', 'Father',
			                         'Sister', 'Brother', 'Spouse', 'Other' ];
			 // $scope.customer.relationships=[{relation:'',name:'',phoneNo:'',email:'',addressLine1:'',addressLine2:'',city:{cityName:''},state:{},country:{countryName:''},pinCode:''}];
			 $scope.addReference = function() {
				 $scope.customer.relationships.push({
					 relation : '',
					 name : '',
					 phoneNo : '',
					 email : '',
					 addressLine1 : '',
					 addressLine2 : '',
					 cityName : '',
					 stateName : "",
					 country : {
						 countryName : ''
					 },
					 pinCode : '',
					 newlyAdded : true
				 });
				 enableStickyHeader($(".sticky-block-view"));
				 $scope.submitPressed = false;
			 }
			 $scope.delRelationship = function(index) {
				 $scope.customer.relationships.splice(index, 1);
				 enableStickyHeader($(".sticky-block-view"));
			 };
			 $scope.showEditPage=function(){
				 $scope.viewPage=false;
				 $scope.editPage=true;
				 enableStickyHeader($(".sticky-block-view"));
			 }
			 $('.showEditPage').click(function() {
				 $('#viewPage').css('display', 'none');
				 $('#editPage').css('display', 'block');
				 enableStickyHeader($(".sticky-block-view"));

			 });
			 $scope.userForm = {};
			 $scope.editCustomer = function() {
				 $scope.submitPressed = true;
				 if ($scope.userForm.$valid) {
					 type = "editCustomer";
					 $scope.loader = true;
					 getServiceCall();

				 }
			 }
			 var customerCode = localStorageService
			 .get('customerCode');
			 type = "viewCustomer";
			 getServiceCall();
			 $scope.loader = true;
			 function getServiceCall() {
				 enableStickyHeader($(".sticky-block-view"));
				 if (type === 'viewCustomer') {
					 $scope.loader = true;
					 var data = {
							 userId : Session.userId,
							 customerCode : customerCode
					 }
					 url = "CustomerController/viewCustomer";
				 } else if (type === 'editCustomer') {
					 $scope.submitPressed = false;
					 $scope.loader = true;
					 var dob;

					 if ($scope.customer.dob !== null) {
						 old_dob = $scope.customer.dob;
						 var dtArr = $scope.customer.dob
						 .split("-");
						 var month = ('0' + (parseInt($.inArray(
								 dtArr[1], shortMonthArr)) + 1))
								 .slice(-2);
						 var custDob = dtArr[2] + "-" + month
						 + "-" + dtArr[0];

						 dob = new Date(custDob);
						 $scope.customer.dob = dob;
					 }
					 var data = $scope.customer;
					 url = "CustomerController/editCustomer";
				 } else if (type === "addRemark") {
					 $scope.addRemarkPressed = false;
					 contractCodeForNewRemark = $scope.selectedContractCode;
					 var data = {
							 userId : Session.userId,
							 custId : $scope.customer.custId,
							 contractId : $scope.selectedContractCode===null || $scope.selectedContractCode===undefined ?null:getContractId($scope.selectedContractCode),
							 remarks : $scope.newComment
					 };
					 url = "CustomerController/addRemark";
					 $scope.loader = true;
				 }

				 adapter.getServiceData(url, data).then(success,
						 error);

				 function success(result) {
					 $scope.loader = false;
					 if (result.statusCode === 200) {
						 console.log(result);
						 if (type === 'editCustomer') {
							 $scope.customer.dob = old_dob;
							 modalEntity = "";
							 modalBody = "";
							 modalMessge = "Edited Successfully";
							 localStorageService.set(
									 "modalStatus", true);
							 modalOpen("success");
						 }
						 if (type === 'viewCustomer') {
							 $scope.loader = true;
							 $scope.customer = result;
							 $scope.remarkList = $scope.customer.remarkList;
							 contractList = $scope.customer.contractList;
							 $scope.loader = false;
						 } else if (type === "addRemark") {
							 $scope.newComment = "";
							 var obj = {
									 showShortRemark : true,
									 recordedBy : result.recordedBy,
									 recorderDate : result.recordedDt,
									 remarks : result.remarks
							 };
							 $scope.remarkList.push(obj);
							 insertRemark(result,
									 contractCodeForNewRemark);
							 getRemarkList(contractList,
									 'All Contracts');
							 $scope.loader = false;
						 }

					 }
				 }
			 }
			 ;
			 function error() {
				 $scope.loader = false;
				 console.log("Service not resolved");
				 if (type === 'editCustomer') {
					 $scope.customer.dob = old_dob;
					 localStorageService.set("modalStatus",
							 false);
					 errorMessage = result;
					 modalOpen("error");

				 }
			 }

			 function getPaymentHistory(contractList) {
				 var arr = [];
				 var obj = {};
				 var contractNo, postingDetails, type, bank, accNo, chqDate, prevBalance, amontPaid, outBalance, lastUpdt;
				 for ( var i = 0; i < contractList.length; i++) {
					 contractNo = contractList[i].contractCode;

					 for ( var j = 0; j < contractList[i].paymentDetails.length; j++) {

						 postingDetails = contractList[i].paymentDetails[j].lastPaymentDt;
						 type = contractList[i].paymentDetails[j].paymentMethod;
						 referenceNum = contractList[i].paymentDetails[j].paymentRefNum;
						 // bank=contractList[i].paymentDetails[j].bankName;
						 console
						 .log(contractList[i].paymentDetails[j].bankName);
						 accNo = contractList[i].paymentDetails[j].bankAccountNo;
						 // chqDate=contractList[i].paymentDetails[j].lastPaymentDt;
						 // prevBalance="NA";
						 amontPaid = contractList[i].paymentDetails[j].paymentAmt;
						 outBalance = contractList[i].paymentDetails[j].orderDueAmt;
						 lastUpdt = contractList[i].paymentDetails[j].lastPaymentDt;
						 obj = {
								 "contractNo" : contractNo,
								 "postingDetails" : postingDetails,
								 "type" : type,
								 "bank" : bank,
								 "accNo" : accNo,
								 "chqDate" : chqDate,
								 "prevBalance" : prevBalance,
								 "amontPaid" : amontPaid,
								 "outBalance" : outBalance,
								 "lastUpdt" : lastUpdt,
								 "orNo" : contractList[i].doNum,
								 "orDate" : contractList[i].orderDate,
								 "refNum" : referenceNum
						 };
						 arr.push(obj);
					 }

				 }
				 return arr;
			 }
			 function getRemarkList(contractList, contractCode) {
				 $scope.remarkList = [];
				 if (contractCode === 'All Contracts')
					 $scope.remarkList = $scope.customer.remarkList;
				 else {
					 for ( var i = 0; i < $scope.customer.contractList.length; i++) {
						 if (contractCode === $scope.customer.contractList[i].contractCode) {
							 for ( var j = 0; j < $scope.customer.contractList[i].remarkDetails.length; j++) {
								 $scope.remarkList
								 .push($scope.customer.contractList[i].remarkDetails[j]);
							 }
							 break;
						 }
					 }
				 }
				 for ( var i = 0; i < $scope.remarkList.length; i++)
					 $scope.remarkList[i].showShortRemark = true;
			 }
			 function enableStickyHeader(obj) {
				 var didScroll = true;
				 $(window).scroll(function(event) {
					 if (didScroll) {
						 didScroll = false;
						 stickyHeader.enableStickyHeader(obj);
					 }
				 });
			 }
			 $scope.showViewPage = function() {
				 $('#viewPage').css('display', 'block');
				 $('#editPage').css('display', 'none');
				 enableStickyHeader($(".sticky-block-view"));
			 }
			 $scope.PageCntChange = function() {
				 $scope.$parent.selectedCnt = $scope.selectedCnt;
				 $scope.$parent.PageCntChange();
			 }
			 $scope.pageChanged = function(e) {
				 $scope.$parent.currentPage = $scope.currentPage;
				 $scope.$parent.pageChanged(e);
			 }
			 
			 $scope.addNewRelationship = function() {
				 $scope.customer.relationships.push({
					 relation : '',
					 name : '',
					 phoneNo : '',
					 email : '',
					 addressLine1 : '',
					 addressLine2 : '',
					 cityName : '',
					 stateName : "",
					 country : {
						 countryName : ''
					 },
					 pinCode : '',
					 newlyAdded : true
				 });
			 }
			 $scope.concatanateString = function(str) {
				 return str.replace(/\s+/g, '-').toLowerCase();
			 }
			 $scope.addRemark = function() {
				 $scope.addRemarkPressed = true;
				 if ($scope.addRemarkForm.$valid) {
					 type = "addRemark";
					 getServiceCall();
				 }

			 }

			 $scope.shortRemark = function(text) {
				 var shortText = '';
				 if (text.length > 200) {
					 shortText = text.substring(0, 200);
					 return shortText;
				 } else
					 return text;
			 }
			 $scope.readMore = function(pos) {
				 for ( var i = 0; i < $scope.remarkList.length; i++) {
					 if (i === parseInt(pos)) {
						 $scope.remarkList[i].showShortRemark = false;
						 break;
					 }

				 }
			 }

			 $scope.readLess = function(pos) {
				 for ( var i = 0; i < $scope.remarkList.length; i++) {
					 if (i === parseInt(pos)) {
						 $scope.remarkList[i].showShortRemark = true;
						 break;
					 }

				 }
			 }
			 $scope.changeRemarkBlock = function(contractCode) {
				 getRemarkList(contractList, contractCode);
			 }
			 function getContractId(contractCode) {
				 for ( var i = 0; i < $scope.customer.contractList.length; i++) {
					 if (contractCode === $scope.customer.contractList[i].contractCode) {
						 return $scope.customer.contractList[i].contractId;
					 }
				 }
			 }
			 function insertRemark(result,
					 contractCodeForNewRemark) {
				 for ( var i = 0; i < $scope.customer.contractList.length; i++) {
					 if (contractCodeForNewRemark === $scope.customer.contractList[i].contractCode) {
						 var obj = {
								 remarkId : '',
								 contractId : '',
								 remarkType : '',
								 contractCode : '',
								 remarks : result.remarks,
								 recordedBy : result.recordedBy,
								 recorderDate : result.recordedDt
						 };
						 $scope.customer.contractList[i].remarkDetails
						 .push(obj);
						 return $scope.customer.contractList[i].contractId;
					 }
				 }
			 }

		 } ]);