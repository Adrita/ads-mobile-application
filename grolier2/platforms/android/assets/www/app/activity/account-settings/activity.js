var MyAccount = angular.module('MyAccount', []);

MyAccount
.controller(
		"MyAccountCtrl",
		[
		 '$scope',
		 '$rootScope',
		 '$state',
		 '$uibModal',
		 'localStorageService',
		 'adapter',
		 'validate',
		 'adjustHeight',
		 '$compile',
		 'Session',
		 '$uibModal',
		 'authentication',
		 function($scope, $rootScope, $state, $uibModal,
				 localStorageService, adapter, validate,
				 adjustHeight, $compile, Session,$uibModal,authentication) {
			 $scope.userName = localStorageService
			 .get('globalUserRoles').userInfo.userDetails.userName;
			 $scope.toggleImageUpload = true;
			 $scope.switchOption = true;
			 $rootScope.$broadcast('TrackNavBar', false);
			 $rootScope.$broadcast('HideMobileNav', true);
			 $scope.passwordError = true;
			 $scope.users = {};
			 adjustHeight.adjustTopBar();

			 $scope.$on('SubStateType', function(event, arg) {
				 if (arg === 'myaccount') {
					 $scope.switchOption = true;
				 }
				 if (arg === 'changepwd') {
					 $scope.switchOption = false;
				 }
			 });

			 $scope.ShowProfile = function() {
				 $scope.switchOption = true;
			 }
			 $scope.ShowChangePwd = function() {
				 $scope.switchOption = false;
			 }
			 $scope.toggleUpload = function($event) {
				 $event.preventDefault();
				 $scope.toggleImageUpload = !$scope.toggleImageUpload;
			 }

			 $scope.checkPassword = function() {

				 if ($scope.newPassword !== "") {
					 if ($scope.newPassword.length >= 6) {
						 $('.caution:eq(0)').css("color",
						 "#85CD00");
						 console
						 .log("Password length color green");
					 } else {
						 $('.caution:eq(0)').css("color", "red");
						 console
						 .log("Password length color red");
					 }

					 if (/\d/.test($scope.newPassword)) {
						 $('.caution:eq(3)').css("color",
						 "#85CD00");
						 console.log("Password has one number");
					 } else {
						 $('.caution:eq(3)').css("color", "red");
						 console
						 .log("Password dont have any number");
					 }

					 if (/[a-z]/g.test($scope.newPassword)) {
						 $('.caution:eq(1)').css("color",
						 "#85CD00");
						 console
						 .log("Password has one lower case");
					 } else {
						 $('.caution:eq(1)').css("color", "red");
						 console
						 .log("Password dont have lower case");
					 }

					 if (/[A-Z]/g.test($scope.newPassword)) {
						 $('.caution:eq(2)').css("color",
						 "#85CD00");
						 console
						 .log("Password has one upper case");
					 } else {
						 $('.caution:eq(2)').css("color", "red");
						 console
						 .log("Password dont have upper case");
					 }

					 if (/(?=.*[!@#\$%\^&\*])/g
							 .test($scope.newPassword)) {
						 $('.caution:eq(4)').css("color",
						 "#85CD00");
						 console
						 .log("Password has one special character");
					 } else {
						 $('.caution:eq(4)').css("color", "red");
						 console
						 .log("Password dont have special character");
					 }

				 } else {
					 $('.caution').css("color", "red");
				 }
			 }

			 $scope.confirmPasswordCheck = function() {
				 if ($scope.newPassword !== $scope.confirmPassword) {
                 					 $scope.confirmPasswordError=true;
                 					 $scope.passwordError = true;
                 				 } else {
                 					 $scope.confirmPasswordError=false;
                 					 $scope.passwordError = false;
                 				 }
                 				 return $scope.passwordError;
			 }

			 $scope.logout = function() {
				 authentication.logout().then(function(result) {
					 if (result.errorCode === "200") {
						 Session.destroy();
						 $state.go('Login');
					 } 
				 });
			 }

			 var type = "getAccount";
			 getServiceCall();

			 function getServiceCall() {

				 var data = {};
				 var url = "";
				 if (type === 'getAccount') {
					 url = "UserRegisterController/getAccountDetails";
					 data = {
							 userId : Session.userId
					 };
				 } else if (type === 'changePassword') {
					 url = "UserRegisterController/changePassword";
					 data = {
							 userId : Session.userId,
							 currentPswd : $scope.user.currentPswd,
							 newPswd : $scope.newPassword,
							 repeatPswd : $scope.confirmPassword
					 }
				 } else if (type === 'changeProfilePic') {
					 url = "UserRegisterController/changeProfilePicture";
					 data = {
							 userId : Session.userId,
							 profilePhoto : $scope.UserPhoto
					 }
					 $rootScope.$broadcast('ChangeProfile', $scope.UserPhoto);
				 }

				 adapter.getServiceData(url, data).then(success,
						 error);
				 function success(result) {
					 if (result.status === 200||result.statusCode === 200) {
						 if (type === 'getAccount') {
							 $scope.user = result;
							 $scope.user.profileImg = "data:image/jpeg;base64,"
								 + result.profilePhotoPath;
							 $scope.user.firstName = result.name
							 .substring(0, result.name
									 .indexOf(' '));
							 $scope.user.lastName = result.name
							 .substring(result.name
									 .indexOf(' ') + 1);
							 
							 var address="";
							 if(result.addressLine1!==null && result.addressLine1!==""){
								 address=result.addressLine1;
							 }
							 if(result.addressLine2!==null && result.addressLine2!==""){
								 address=address+" ,"+result.addressLine2;
							 }
							 if(result.city!==null && result.city!==""){
								 address=address+" ,"+result.city;
							 }
							 if(result.state!==null){
								 address=address+" ,"+result.state.stateName;
							 }
							 
							 $scope.address=address;
						 }else if(type==='changePassword'){
						 $scope.passwordChangeSuccess=true;
							 modalEntity = "Password has been changed successfully."
							 modalBody = "";
							 localStorageService.set(
									 "modalStatus", true);
							 modalOpen("success");
							 $scope.user.currentPswd='';
                             $scope.newPassword='';
                             $scope.confirmPassword='';
							 
						 } else {
							 type = "getAccount";
							 getServiceCall();
							 console.log(result);
						 }

					 }
					 else if(result.statusCode===401){
                     						 if(type==='changePassword'){
                     							 errorMessage = result.message;
                     								 modalBody = "";
                     								 localStorageService.set(
                     										 "modalStatus", false);
                     								 modalOpen("error");
                     								 $scope.user.currentPswd='';
                     								 $scope.newPassword='';
                     								 $scope.confirmPassword='';
                     						 }
                     					 }
				 }
				 function error() {
					 console.log("Service not resolved");
				 }

			 }

			 $scope.submit = function() {
				 $scope.submitPressed=true;
                 				 if ($scope.passwordChangeForm.$valid && !checkValidation() && !$scope.confirmPasswordCheck()) {
                 					 type = "changePassword";
                 					 getServiceCall();
                 				 }

			 }

			 function checkValidation() {
             				 var strongRegex = new RegExp(
             				 "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");

             				 if (strongRegex.test($scope.newPassword)
             						 && $scope.newPassword !== $scope.confirmPassword) {
             					 $scope.passwordError = true;
             					 $scope.confirmPasswordError=true;
             				 }
             				 else if($scope.newPassword!=null && $scope.newPassword!='' && $scope.newPassword!=undefined &&  $scope.user.currentPswd===$scope.newPassword){
             					 $scope.passwordError = true;
             				 }
             				 else {
             					 $scope.passwordError = false;
             				 }
             				 if($scope.newPassword!=null && $scope.newPassword!='' && $scope.newPassword!=undefined && $scope.user.currentPswd===$scope.newPassword){
             					 $scope.samePasswordError=true;
             				 }
             				 else{
             					 $scope.samePasswordError=false;
             				 }

             				 return $scope.passwordError;
             			 }

			 function encodeImageFileAsURL() {

				 var filesSelected = document
				 .getElementById("photo-upload_Id").files;
				 if (filesSelected.length > 0) {
					 var fileToLoad = filesSelected[0];

					 var fileReader = new FileReader();

					 fileReader.onload = function(
							 fileLoadedEvent) {
						 var srcData = fileLoadedEvent.target.result; // <---
						 // data:
						 // base64

						 var newImage = document
						 .createElement('img');
						 newImage.src = srcData;

						 document.getElementById("imgTest").innerHTML = newImage.outerHTML;
						 alert("Converted Base64 version is "+ document.getElementById("imgTest").innerHTML);
						 console.log("Converted Base64 version is "
								 + document
								 .getElementById("imgTest").innerHTML);
					 }
					 fileReader.readAsDataURL(fileToLoad);
				 }
			 }

			 $scope.uploadFile = function(input) {

				 if (input.files && input.files[0]) {
					 var reader = new FileReader();
					 reader.onload = function(e) {

						 //Sets the Old Image to new New Image

						 //Create a canvas and draw image on Client Side to get the byte[] equivalent
						 var canvas = document
						 .createElement("canvas");
						 var imageElement = document
						 .createElement("img");

						 imageElement.setAttribute('src',
								 e.target.result);
						 canvas.width = imageElement.width;
						 canvas.height = imageElement.height;
						 var context = canvas.getContext("2d");
						 context.drawImage(imageElement, 0, 0);
						 var base64Image = canvas
						 .toDataURL("image/jpeg");

						 //Removes the Data Type Prefix 
						 //And set the view model to the new value
						 $scope.UserPhoto = base64Image
						 .replace(
								 /data:image\/jpeg;base64,/g,
						 '');
						 $scope.user.profileImg = "data:image/jpeg;base64,"
							 + $scope.UserPhoto;
						 // $scope.sales.profilePic="data:image/jpeg;base64,"+ $scope.UserPhoto;
					 }

					 //Renders Image on Page
					 reader.readAsDataURL(input.files[0]);
				 }
			 };

			 $scope.uploadImg = function() {
				 type = "changeProfilePic";
				 getServiceCall();
			 }
			 
			 
			 /* MODAL IMPLEMENTATION */
			 $scope.animationsEnabled = true;

			 $scope.open = function() {
				 var modalInstance = $uibModal.open({
					 animation : $scope.animationsEnabled,
					 templateUrl : 'app/modals/role/modal.html',
					 controller : 'modalCtrl',
					 windowClass : 'center-modal',
					 backdrop : 'static',
					 keyboard : false,
					 size : 'md'
				 }).result.then(function(data) {
					 if(type==='changePassword' && $scope.passwordChangeSuccess){
					     $state.reload();
					 }
                     else if (data === "close") {
                         $state.go('AdminBoard.MyAccount');

                     }
                     if (data === "createAnother") {
                         $state.reload();
                     }
				 });
			 };

			 var errorName = "Turn In";
			 
			 var errorCode = "Test1234";
			 var contractCode = "";
			 var errorMessage=""; 

			 function modalOpen(modalType) {
				 if (errorName !== null && errorName !== "") {
					 if (modalType === "success") {
						 var modalObj = {
								 "modalEntity" : modalEntity,
								 "body" : modalBody,
								 "modalBtnText" : "OK",
								 "backLink" : "OK",
								 "singleBtn" : true
						 };
					 } else {
						 var modalObj = {
								 "modalEntity" : "Failed!! ",
								 "body" : errorMessage,
								 "modalBtnText" : "",
								 "backLink" : "OK",
								 "singleBtn" : true
						 };
					 }

					 localStorageService.set("modalObj",
							 modalObj);
					 $scope.open();
				 }
			 }

		 } ]);