app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('onFinishRender', function($timeout) {
	return {
		restrict : 'A',
		link : function(scope, element, attr) {
			if (scope.$last === true) {
				$timeout(function() {
					scope.$emit('ngRepeatFinished');
				});
			}
		}
	}
});

app.directive('onFinishRender', function ($timeout) {
	return {
	    restrict: 'A',
	    link: function (scope, element, attr) {
	        if (scope.$last === true) {
	            $timeout(function () {
	                scope.$emit(attr.onFinishRender);
	            });
	        }
	    }
	}
});

//jQuery UI datepicker
app.directive("datepicker",['$window', function($window) {
	return {
		restrict : "A",
		require : "ngModel",
		link : function(scope, elem, attrs, ngModelCtrl) {
			var updateModel = function(dateText) {
				scope.$apply(function() {
					ngModelCtrl.$setViewValue(dateText);
				});
			};
			var options = {
				dateFormat : "dd-M-yy",
				changeMonth : true,
				changeYear : true,
				yearRange : "-70:+5",
				showOtherMonths : true,
				selectOtherMonths : true,
				//showOn : "button",
				showOn : "both",
				buttonImage : "distribution/images/calender_blue.png",
				/*beforeShowDay: function(dateText) {
					if( window.innerWidth < 960 ) $('input').blur();
                },*/
				 onClose: function(dateText, inst) 
				    { 
				        $(this).attr("readonly", false);
				        if( window.innerWidth < 960 ) $('input').blur();
				    },
				    beforeShow: function(input, inst) 
				    {
				        $(this).attr("readonly", true);
				       if( window.innerWidth < 960 ) $('input').blur();
				    },
				onSelect : function(dateText) {
					updateModel(dateText);
				}
			};
			elem.datepicker(options);
		}
	}
}]);

app.directive('allowPattern', function () {
	return {
		restrict : "A",
		compile : function(tElement, tAttrs) {
			return function(scope, element, attrs) {
				// I handle key events
				element.bind("keypress", function(event) {
					var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
					var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.

					// If the keyCode char does not match the allowed
					// Regex Pattern, then don't allow the input into
					// the field.
					if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
						event.preventDefault();
						return false;
					}
				});
			};
		}
	}
});

app.directive('customPopover', function ($compile,adapter) {
    return {
        //template: '<span>{{label}}222</span>',
        restrict: "EA",
        replace: true,
        scope:{
        	data:"="
        },
        //scope: { title: "@", content: "@", placement: "@", animation: "&", isOpen: "&" },
        link: function (scope, el, attrs,ctrl) {
            //scope.label = attrs.popoverLabel;
        	scope.data = attrs.popoverLabel;
        	var template;
        	template=$compile('<div class="popover"><div class="arrow"></div>'+
			  '<h3 class="popover-title">{{label}}</h3><div class="Content"><textarea class="popover-content popover-textarea" ng-model="data"></textarea>'+
			  '</div><div class="popover-footer pull-right"><img src="distribution/images/Add_icon.png" class="pop-ok"></img>&nbsp;'+
			  '<img src="distribution/images/decline_inactive.png" class="pop-cancel"></img></div></div>')(scope);
        	//console.log(scope.data);
            $(el).popover({
                trigger : 'click',  
				placement : 'bottom', 
				html: 'true',
				content : attrs.popoverLabel,
				template: template
            }).on('shown.bs.popover', function () {
            	 var $popup = $(this);
            	 //console.log($(this).next('.popover').attr('id'));
            	 var id='#'+$(this).next('.popover').attr('id');
            	 var length=scope.data.length+50;
            	 $(this).next('.popover').find('.Content').css('height',length+'px');
            	 $(this).next('.popover').css('left',parseInt($(this).next('.popover').css('left'))-74+'px');
            	 $(this).next('.popover').css('top',parseInt($(this).next('.popover').css('top'))+10+'px');
            	 $(this).next('.popover').find('.arrow').css('left','90%');
            	 $(this).next(id).find('.pop-ok').on('click',function(e){
            		 //alert(attrs.popoverContract);
            		 //console.log(scope.data);
            		 /*$scope.$broadcast('addComment', {
            			  someProp: {"contractId":popoverContract,"collectorNote":scope.data} // send whatever you want
            			});*/
            		 var url="CollectionController/saveNote";
            		 var data={contractId:attrs.popoverContract,collectorNote:scope.data};
            		 adapter.getServiceData(url,data).then(success,error);
            		 function success(result){
      			        if(result.statusCode === 200){
      			        		console.log("Comment Added!!");
      			        	}
      			     }
      			   	 function error(){
      			   	    console.log("Service not resolved");
      			     }
            		 $popup.popover('hide');
            	 });
            	 $(this).next(id).find('.pop-cancel').on('click',function(e){
            		 $popup.popover('hide');
            		 
            	 });
           
            });
        }
    }
});

//Remove an element from DOM
//parameter true/false
app.directive('removeElement', function() {
	return {
		restrict : 'A',
		link : function(scope, element, attributes) {
			var delElement = attributes.removeElement;
			if (delElement.toLowerCase() === "true")
				element && element.remove && element.remove();
		}
	}
	
});

//define has access
//usage: has-access="edit", has-access="!edit"
app.directive('hasAccess', ['authentication', 'rmvElement', 'Session', function(authentication, rmvElement, Session) {
	return {
		restrict : 'A',
		link : function(scope, element, attributes) {
			var hasAccess = false;
			var role = Session.userRole;
			var contractViewAccess = Session.contractViewAccess;
			var userViewAccess = Session.userViewAccess;
			var accessType = attributes.hasAccess;
			
			if (angular.isDefined(role)) {
				
				if ((accessType === "summary" && authentication.hasSummaryAccess(role)) || 
						(accessType === "!summary" && !authentication.hasSummaryAccess(role)) ||
							(accessType === "view" && authentication.hasViewDetailsAccess(role)) ||
								(accessType === "!view" && !authentication.hasViewDetailsAccess(role)) ||
									(accessType === "edit" && authentication.hasEditDetailsAccess(role)) ||
										(accessType === "!edit" && !authentication.hasEditDetailsAccess(role)) ||
											(accessType === "draft" && authentication.hasDraftAccess(role)) ||
												(accessType === "!draft" && !authentication.hasDraftAccess(role))) {
					hasAccess = true;
				}
			}
			
			if (!hasAccess && angular.isDefined(contractViewAccess)) {
				if ((accessType === "viewContract" && authentication.hasViewDetailsAccess(contractViewAccess)) || 
						(accessType === "!viewContract" && !authentication.hasViewDetailsAccess(contractViewAccess)) ||
							(accessType === "editContract" && authentication.hasEditDetailsAccess(contractViewAccess)) || 
								(accessType === "!editContract" && !authentication.hasEditDetailsAccess(contractViewAccess))) {
					hasAccess = true;
				}
			}
			
			if (!hasAccess && angular.isDefined(userViewAccess)) {
				if ((accessType === "viewUser" && authentication.hasViewDetailsAccess(userViewAccess)) || 
						(accessType === "!viewUser" && !authentication.hasViewDetailsAccess(userViewAccess)) ||
							(accessType === "editUser" && authentication.hasEditDetailsAccess(userViewAccess)) || 
								(accessType === "!editUser" && !authentication.hasEditDetailsAccess(userViewAccess))) {
					hasAccess = true;
				}
			}

			if (!hasAccess) {
				angular.forEach(element.children(), function(
						child) {
					rmvElement(child);
				});
				rmvElement(element);
			}
		}
	}
} ]).constant('rmvElement', function(element) {
	element && element.remove && element.remove();
});

//directive for login background image
app.directive("loginBackground", function() {
	
	return {
		restrict : 'A',
		link : function(scope, element, attr) {
			//var theWindow = $(window),
			var theWindow = $("#bgWrapper"),
		    $bg = $("#loginBanner"),
		    //$bg = angular.element(element),
		    aspectRatio = $bg.width() / $bg.height();
		    			    		
			function resizeBg() {
				
				if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
				    $bg.removeClass().addClass('bgheight');
				    
				} else {
				    $bg.removeClass().addClass('bgwidth');
				}
							
			}
			                   			
			theWindow.resize(function() {
				resizeBg();
			}).trigger("resize");
			//resizeBg();
		}
	}
	
});
app.directive("weekpicker",['$window', function($window) {
    return {
        restrict : "A",
        require : "ngModel",
        link : function(scope, elem, attrs, ngModelCtrl) {
            var updateModel = function(dateText) {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                changeMonth : true,
                changeYear : true,
                yearRange : "-70:+5",
                showOtherMonths : true,
                selectOtherMonths : true,
                //showOn : "button",
                showOn : "both",
                buttonImage : "distribution/images/calender_blue.png",
                /*beforeShowDay: function(dateText) {
                    if( window.innerWidth < 960 ) $('input').blur();
                },*/
                 onClose: function(dateText, inst) 
                    { 
                        $(this).attr("readonly", false);
                        if( window.innerWidth < 960 ) $('input').blur();
                    },
                    beforeShow: function(elem, ui) 
                    {
                        $(this).attr("readonly", true);
                       if( window.innerWidth < 960 ) $('elem').blur();
                       
                       $(ui.dpDiv).on('click', 'tbody .ui-datepicker-week-col', function() {
                              console.log(ui);
                                $(elem).val($(this).text()+'/'+ui.selectedYear).datepicker( "hide" );
                                updateModel($(this).text()+'/'+ui.selectedYear);
                       });
                    },
                onSelect : function(dateText) {
                    
                	Date.prototype.getWeek = function() {
                	    var onejan = new Date(this.getFullYear(), 0, 1);
                	    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                	}
                    updateModel(dateText.getWeek()+"/"+dateText.getFullYear());
                }
            };
            elem.datepicker(options);
        }
    }
}]);


app.directive('onLongPress', function($timeout) {
        return {
          restrict: 'A',
          link: function($scope, $elm, $attrs) {
            $elm.bind('touchstart', function(evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.longPress = true;

                // We'll set a timeout for 600 ms for a long press
                $timeout(function() {
                    if ($scope.longPress) {
                        // If the touchend event hasn't fired,
                        // apply the function given in on the element's on-long-press attribute
                        $scope.$apply(function() {
                            $scope.$eval($attrs.onLongPress)
                        });
                    }
                }, 600);
            });

            $elm.bind('touchend', function(evt) {
                // Prevent the onLongPress event from firing
                $scope.longPress = false;
              // If there is an on-touch-end function attached to this element, apply it
              if ($attrs.onTouchEnd) {
                $scope.$apply(function() {
                  $scope.$eval($attrs.onTouchEnd)
                    });
                }
          });
        }
      };
      });

      app.directive('limitChar',function(){
      	'use strict';
      	return{
      		require:'ngModel',
      		link:function(scope,element,attrs,ngModelCtrl){
      			var maxlength= Number(attrs.limitChar);
      			function fromUser(text){
      			if(text.length>maxlength){
      				var transformedInput = text.substring(0,maxlength);
      				ngModelCtrl.$setViewValue(transformedInput);
      				ngModelCtrl.$render();
      				return transformedInput;
      			}
      			return text;

      		}
      		ngModelCtrl.$parsers.push(fromUser);
      		}

      	};
      });

      app.directive('numbersOnly',function(){
      	return{
      		require:'ngModel',
      		link:function(scope,element,attr,ngModelCtrl){
      			function fromUser(text){
      				if(text){
      					var transformedInput=text.replace(/[^0-9]/g,'');
      					if(transformedInput !== text){
      						ngModelCtrl.$setViewValue(transformedInput);
      						ngModelCtrl.$render();
      					}
      					return transformedInput;
      				}
      				return undefined;
      			}
      			ngModelCtrl.$parsers.push(fromUser);
      		}
      	};
      });